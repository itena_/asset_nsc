jun.AssetTransGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Asset Transfer",
        id:'docs-jun.AssetTransGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
        {
            header:'Activa',
            sortable:true,
            resizable:true,
            dataIndex:'ati',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Name',
            sortable:true,
            resizable:true,
            dataIndex:'asset_name',
            width:100,
            filter: {xtype: "textfield"}
        },
                                {
			header:'BU Pengirim',
			sortable:true,
			resizable:true,                        
            dataIndex:'bu_pengirim',
			width:100,
                                    filter: {xtype: "textfield"}
		},
                                {
			header:'BU Penerima',
			sortable:true,
			resizable:true,                        
            dataIndex:'bu_penerima',
			width:100,
                                    filter: {xtype: "textfield"}
		},

                {
			header:'Branch Pengirim',
			sortable:true,
			resizable:true,                        
            dataIndex:'store_pengirim',
			width:70,
                    filter: {xtype: "textfield"}
		},
                                {
			header:'Branch Penerima',
			sortable:true,
			resizable:true,                        
            dataIndex:'store_penerima',
			width:70,
                                    filter: {xtype: "textfield"}
		},
                                {
			header:'Amount',
			sortable:true,
			resizable:true,                        
            dataIndex:'amount',
			width:70,
                                    filter: {xtype: "textfield"}
		},
                                {
			header:'Note',
			sortable:true,
			resizable:true,                        
            dataIndex:'note',
			width:100,
                                    filter: {xtype: "textfield"}
		},
                                {
			header:'Date',
			sortable:true,
			resizable:true,                        
            dataIndex:'tdate',
			width:80,
                                    filter: {
                                        xtype: "xdatefield",
                                        format: 'd/m/Y'
                                    }
		},
        /*{
            header:'Approval',
            sortable:true,
            resizable:true,
            dataIndex:'approval',
            width:100
        },*/
        {
           header:'Status',
           sortable:true,
           resizable:true,
           dataIndex:'status',
           width:100,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 2 :
                        metaData.style += "background-color: #9CFD7B;";
                        return 'LEND';
                    case 3 :
                        metaData.style += "background-color: #9CFD7B;";
                        return 'SELL';
                    case 4 :
                        metaData.style += "background-color: #9CFD7B;";
                        return 'RENT';
                    case 8 :
                        metaData.style += "background-color: #9CFD7B;";
                        return 'RELOCATION';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [2, 'LEND'], [3, 'SELL'],[4, 'RENT'],[8, 'RELOCATION']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
       },
        {
            header: 'Approval',
            sortable: true,
            resizable: true,
            dataIndex: 'approval',
            width: 100,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 1 :
                        metaData.style += "background-color: #9CFD7B;";
                        return 'APPROVED';
                    case 0 :
                        metaData.style += "background-color: #FCEB19;";
                        return 'DISAPPROVED';
                    case 2 :
                        metaData.style += "background-color: #f02213;";
                        return 'PENDING';
                    case 3 :
                        metaData.style += "background-color: #9CFD7B;";
                        return 'GENERATED';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [1, 'APPROVED'], [0, 'DISAPPROVED'],[2, 'PENDING'],[3, 'GENERATED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        /*
                                {
			header:'visible',
			sortable:true,
			resizable:true,                        
            dataIndex:'visible',
			width:100
		},
                		*/
		
	],
	initComponent: function(){
	this.store = jun.rztAssetTrans;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Edit',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Delete',
                        ref: '../btnDelete'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'APPROVE',
                        ref: '../btnAppv'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'DISAPPROVE',
                        ref: '../btnDisappv'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'GENERATE',
                        ref: '../btnGenerate'
                    }


                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.AssetTransGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.btnAppv.on('Click', this.Approve, this);
                this.btnDisappv.on('Click', this.Disappv, this);
                this.btnGenerate.on('Click', this.Generate, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
    Generate: function(){


        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
            return;
        }

        var app = record.json.approval;
        if(app == '0' || app == '2')
        {
            Ext.MessageBox.alert("Warning","Data Belum di Approve");
            return;
        }
        if(app == '3')
        {
            Ext.MessageBox.alert("Warning","Data telah di Generate");
            return;
        }


        Ext.Ajax.request({
            url: 'AssetTrans/TranferAsset/id/' + record.json.trans_id,
            method: 'POST',
            success:function (f, a) {
                jun.rztAssetTrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },

    Disappv: function(){

        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'AssetTrans/DisApprove/id/' + record.json.trans_id,
            method: 'POST',
            success:function (f, a) {
                jun.rztAssetTrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },

    Approve: function(){

        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'AssetTrans/approve/id/' + record.json.trans_id,
            method: 'POST',
            success:function (f, a) {
                jun.rztAssetTrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },

        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },
        
        loadForm: function(){
            var form = new jun.AssetTransWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.trans_id;
            var form = new jun.AssetTransWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'AssetTrans/delete/id/' + record.json.trans_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztAssetTrans.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                        title:'Info',
                        msg:response.msg,
                        buttons:Ext.MessageBox.OK,
                        icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        
        }
})
