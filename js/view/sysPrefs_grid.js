jun.SysPrefsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SysPrefs",
    id: 'docs-jun.SysPrefsGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Bank Name',
            sortable: true,
            resizable: true,
            dataIndex: 'bank_id',
            renderer: jun.renderBank,
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztBankFilterPayment;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Kas/Bank :'
                },
                {
                    xtype: 'combo',
                    //typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    ref: '../bank',
                    store: jun.rztBankCmpPusat,
                    valueField: 'bank_id',
                    displayField: 'nama_bank'
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.SysPrefsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var bank_id = this.bank.getValue();
        var a = jun.rztBankFilterPayment.findExact("bank_id", bank_id);
        if (a > -1) {
            Ext.MessageBox.alert("Error", "Bank sudah diinput.");
            return;
        }
        var c = jun.rztBankFilterPayment.recordType,
            d = new c({
                bank_id: bank_id
            });
        jun.rztBankFilterPayment.add(d);
        this.bank.reset();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
})
