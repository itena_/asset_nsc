jun.PrintLabelWin = Ext.extend(Ext.Window, {
    title: 'Print Label',
    modez: 1,
    width: 438,
    height: 250,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: true,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-PrintLabelWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Doc. Ref',
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Aktiva',
                        name: 'ati',
                        ref: '../ati',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'BU',
                        name: 'bu',
                        ref: '../bu',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Store Code',
                        name: 'store_kode',
                        ref: '../store_kode',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Description',
                        name: 'description',
                        ref: '../description',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        name: 'asset_id',
                        hidden: true,
                        ref: '../asset_id',
                    },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Print Label',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.PrintLabelWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // this.on("activate", this.onActivate, this);
    },
    onActivate: function ()
    {
        var doc_ref = this.doc_ref.getValue();
        var description = this.description.getValue();
        document.getElementById("doc_ref").textContent=doc_ref;
        document.getElementById("description").textContent=description;
    },
    btnDisabled:function(status){
        this.btnSaveClose.setDisabled(status);
    },
    printRules: function(response, printer) {
        var printWindow = window.open('', '', 'height=0px,width=0px');

        printWindow.document.write('<html>');
        printWindow.document.write('<head>');
        printWindow.document.write('<script src="js/qr-code-styling.js"></script>'); // Include the QR code library
        printWindow.document.write('<script src="js/barcode.js"></script>');  // Include your barcode script

        switch (printer) {
            case 'Brother': printer = STYLECARD; break;
            case 'Citizen': printer = STYLECARDCitizen; break;
        }

        printWindow.document.write('<link rel="stylesheet" type="text/css" href="'+printer+'">');
        printWindow.document.write('</head>');
        printWindow.document.write('<body>');
        printWindow.document.write('<div class="mycard">');

        // Print the document reference
        printWindow.document.write('<div class="doc_ref" style="">');
        printWindow.document.write(response.ati.getValue());
        printWindow.document.write('</div>');

        // Print the description
        printWindow.document.write('<div class="description">');
        printWindow.document.write("BU (store) : ");
        printWindow.document.write(response.bu.getValue());
        printWindow.document.write(" ( ");
        printWindow.document.write(response.store_kode.getValue());
        printWindow.document.write(" ) <br>");
        printWindow.document.write(response.description.getValue());
        printWindow.document.write('</div>');

        // Placeholder div for QR code
        printWindow.document.write('<div id="qrcode" class="qr-code"></div>');  // Create a div for the QR code

        printWindow.document.write('</div>');
        printWindow.document.write('</body>');

        printWindow.document.write('</html>');

        // Close document writing to allow scripts to load
        printWindow.document.close();

        // Wait for the window and content to be ready before generating QR code
        printWindow.onload = function() {
            var qrCode = new QRCodeStyling({
                width: 150,
                height: 150,
                data: response.ati.getValue(),  // You can change this to your dynamic data
                dotsOptions: {
                    color: "#000000",
                    type: "rounded"
                },
                backgroundOptions: {
                    color: "#ffffff"
                }
            });

            // Append QR code to the print window
            qrCode.append(printWindow.document.getElementById("qrcode"));

            // Delay print to allow QR code rendering
            setTimeout(function () {
                printWindow.print();
                printWindow.close();
            }, 500);  // Delay for QR code to render
        };
    },
    saveForm : function() {
        var printer = this.printer;
        this.printRules(this, printer);
    },
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnCancelclick: function(){
        this.close();
    }
});