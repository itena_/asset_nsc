jun.AssetComparationGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Assets Comparation",
    id: 'docs-jun.AssetComparationGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_store',
            width: 60,
            // filter: {xtype: "textfield"}
        },
        {
            header: 'Version',
            sortable: true,
            resizable: true,
            dataIndex: 'version',
            width: 10,
            // filter: {xtype: "textfield"}
        },
        {
            header: 'Comparation Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 60,
            // filter: {xtype: "textfield"}
        },
        {
            header: 'User',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_user',
            width: 60,
            // filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 40,
            // filter: {xtype: "textfield"}
        },
    ],
    initComponent: function () {
        this.store = jun.rztAsseComparation;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Create Comparation Asset',
                    ref: '../btnAdd',
                    iconCls: "silk13-add",
                },'-',
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit',
                    iconCls: "silk13-application_edit",
                },'-'
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();

        jun.AssetComparationGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.btnAddOnClick, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    btnAddOnClick: function () {
        var form = new jun.AssetComparationWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        editForm(selectedz);

        var idz = selectedz.json.asset_comparation_id;
        var form = new jun.AssetComparationWin({
            modez: 1, id: idz,
            title: 'Asset Comparation v.' + selectedz.json.version,
        });

        var storesLoaded = 0;
        var totalStores = 2;

        function checkAndCompare() {
            if (storesLoaded === totalStores) {
                form.gridDetails.store.compareStores(form.gridDetails.store, form.gridDetailsExisting.store);
            }
        }

        form.gridDetails.store.baseParams = {
            asset_comparation_id: idz,
            tipe: 'new'
        };

        form.gridDetails.store.load({
            callback: function () {
                storesLoaded++; // Increment when store loads
                checkAndCompare();
            }
        });

        form.gridDetails.store.baseParams = {};

        form.gridDetailsExisting.store = jun.rztAssetComparationDetailExisting;
        form.gridDetailsExisting.store.baseParams = {
            asset_comparation_id: idz,
            tipe: 'existing'
        };

        form.gridDetailsExisting.store.load({
            callback: function () {
                storesLoaded++; // Increment when store loads
                checkAndCompare();
            }
        });

        form.gridDetailsExisting.store.baseParams = {};

        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.gridDetails.ati.store.reload({params: {store_id: selectedz.json.store_id}});
        form.branch.setReadOnly(true);
    }
});