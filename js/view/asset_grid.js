jun.AssetGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Assets Acquisition",
    id: 'docs-jun.AssetGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Activa',
            sortable: true,
            resizable: true,
            dataIndex: 'ati',
            width: 90,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Description',
            sortable: true,
            resizable: true,
            dataIndex: 'description',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Store',
            sortable: true,
            resizable: true,
            dataIndex: 'store_kode',
            width: 40,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price_acquisition',
            width: 80,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'date_acquisition',
            width: 50,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },
        {
            header: 'Condition',
            sortable: true,
            resizable: true,
            dataIndex: 'conditions',
            width: 50,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case 0 :
                        metaData.style += "background-color: #9CFD7B;";
                        return 'NEW';
                    case 1 :
                        metaData.style += "background-color: #FCEB19;";
                        return 'SECOND';
                    case 2 :
                        metaData.style += "background-color: #f02213;";
                        return 'BROKEN';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                editable: false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NEW'], [1, 'SECOND'], [2, 'BROKEN']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Status ',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 60,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case 0 :
                        metaData.style += "background-color: #ff7373;";
                        return 'NON ACTIVE';
                    case 1 :
                        metaData.style += "background-color: #9ffb8a;";
                        return 'ACTIVE';
                    case 2 :
                        metaData.style += "background-color: #f1f26f;";
                        return 'LEND';
                    case 3 :
                        metaData.style += "background-color: #ff7373;";
                        return 'SELL';
                    case 4 :
                        metaData.style += "background-color: #f1f26f;";
                        return 'RENT';
                    case 5 :
                        metaData.style += "background-color: #ff7373;";
                        return 'BROKEN';
                    case 6 :
                        metaData.style += "background-color: #ff7373;";
                        return 'LOST';
                    case 7 :
                        metaData.style += "background-color: #f57b1c;";
                        return 'MAINTENANCE';
                    case 8 :
                        metaData.style += "background-color: #f57b1c;";
                        return 'RELOCATION';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                editable: false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NON ACTIVE'], [1, 'ACTIVE'], [2, 'LEND'], [3, 'SELL'], [4, 'RENT'], [5, 'BROKEN'], [6, 'LOST'], [7, 'MAINTENANCE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
    ],
    initComponent: function () {
        if (jun.rztAssetGroupLib.getTotalCount() === 0) {
            jun.rztAssetGroupLib.load();
        }
        if (jun.rztAssetBarangAtiCmp.getTotalCount() === 0) {
            jun.rztAssetBarangAtiCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztAssetGroup.getTotalCount() === 0) {
            jun.rztAssetGroup.load();
        }
        if (jun.rztAssetCategorySubLib.getTotalCount() === 0) {
            jun.rztAssetCategorySubLib.load();
        }
        this.store = jun.rztAsset;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Asset',
                    ref: '../btnAdd',
                    iconCls: "silk13-add",
                },'-',
                {
                    xtype: 'button',
                    text: 'Edit Asset',
                    ref: '../btnEdit',
                    iconCls: "silk13-application_edit",
                },'-',
                {
                    xtype: 'button',
                    text: 'Show Detail Asset',
                    ref: '../btnShowDetail',
                    iconCls: "silk13-layout"
                },'-',
                {
                    xtype: 'button',
                    text: 'Show Depreciation',
                    ref: '../btnDepreciation',
                    iconCls: "silk13-layout"
                },'-',
                {
                    xtype: 'button',
                    text: 'Show Report Depreciation',
                    ref: '../btnShowReportDepreciation',
                    iconCls: "silk13-report"
                },'-',
                /*{
                    xtype: 'button',
                    text: 'HISTORY',
                    ref: '../btnHistory',
                    iconCls: "silk13-time",
                },'-',
                {
                    xtype: 'button',
                    text: 'HIDE',
                    ref: '../btnHide',
                    iconCls: "silk13-eye",
                },'-',
                {
                    xtype: 'button',
                    text: 'STATUS',
                    ref: '../btnSold',
                    iconCls: "silk13-money",
                },'-',*/
                {
                    xtype: 'label',
                    text: 'Pilih Printer '
                },
                {
                    xtype: 'combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: [
                        ["Brother", "Brother"],
                        ["Citizen", "Citizen"],
                    ],
                    ref: '../printer',
                    value: "Brother",
                    hiddenName: 'printer',
                    valueField: 'printer',
                    width: 110
                },'-',
                {
                    xtype: 'button',
                    text: 'Print Label',
                    ref: '../btnPrint',
                    iconCls: "silk13-printer",
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();

        jun.AssetGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.btnAddOnClick, this);
        this.btnEdit.on('Click', this.btnEditOnClick, this);
        this.btnShowDetail.on('Click', this.btnShowDetailOnClick, this);
        this.btnShowReportDepreciation.on('Click', this.btnShowReportDepreciationOnClick, this);
        this.btnDepreciation.on('Click', this.depreciationForm, this);
        this.btnPrint.on('Click', this.btnPrintonClick, this);

        /*this.btnHide.on('Click', this.ShowHide, this);
        this.btnHistory.on('Click', this.ShowHistory, this);
        this.btnSold.on('Click', this.ActionSold, this);*/

        this.on('rowdblclick', this.btnEditOnClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    btnPrintonClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a member");
            return;
        }
        console.log(selectedz.json);
        var idz = selectedz.json.asset_id;
        var doc_ref = selectedz.json.doc_ref;
        var ati = selectedz.json.ati;
        var bu = selectedz.json.businessunit_code;
        var store_kode = selectedz.json.store_kode;
        var description = selectedz.json.description;

        var record = this.sm.getSelected();
        record.set('asset_id', idz);
        record.set('doc_ref', doc_ref);
        record.set('ati', ati);
        record.set('bu', bu);
        record.set('store_kode', store_kode);
        record.set('description', description);
        record.commit();

        var form = new jun.PrintLabelWin({modez: 0, asset_id: idz, printer: this.printer.getValue()});
        form.formz.getForm().loadRecord(this.record);
        form.show(this);
    },
    btnAddOnClick: function () {
        var form = new jun.AssetWin({modez: 0});
        form.show();
    },
    btnEditOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data");
            return;
        }
        var idz = selectedz.json.asset_id;
        var form = new jun.AssetWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);

        var price = this.record.data.price_acquisition;
        var ppn = price * PPNASSET/100;
        form.ppnasset.setValue(ppn);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    loadForm: function () {
        var form = new jun.AssetDepriciationWin({modez: 0});
        form.show();
    },
    btnShowDetailOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data");
            return;
        }
        var idz = selectedz.json.asset_detail_id;
        var form = new jun.AssetDetailWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.category.setValue(selectedz.json.category_name);
        form.category_sub.setValue(selectedz.json.sub_name);
    },
    btnShowReportDepreciationOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data");
            return;
        }
        var idz = selectedz.json.asset_detail_id;
        var form = new jun.ReportAssetDepriciation({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.asset_trans_date.setValue(selectedz.json.date_acquisition);
        form.tglfrom.setValue(selectedz.json.date_acquisition);
        form.tglto.setValue(DATE_NOW);
    },
    depreciationForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data");
            return;
        }
        var price = 0;
        var accumulation = 0;
        var response_balance;
        Ext.Ajax.request({
            url: 'AssetDetail/GetBalance/id/' + selectedz.json.asset_detail_id,
            method: 'POST',
            scope: this,
            success: function (f, a) {
                response_balance = Ext.decode(f.responseText);
                price = response_balance.success[0];
                accumulation = response_balance.success[1];

                var idz = selectedz.json.asset_detail_id;
                var form = new jun.AssetDepriciationWin({modez: 1, id: idz, balance: response_balance, accumulation: accumulation});
                form.show(this);
                form.formz.getForm().loadRecord(this.record);

                form.nilaibukudep.setValue(price);
                form.accumulationdep.setValue(accumulation);

                jun.rztAssetPeriode.baseParams = {
                    asset_detail_id: idz
                };
                jun.rztAssetPeriode.load();
                jun.rztAssetPeriode.baseParams = {};
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
});