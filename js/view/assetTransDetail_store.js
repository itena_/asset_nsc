jun.AssetTransDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetTransDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetTransDetailStoreId',
            url: 'AssetTransDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'trans_detail_id'},
                {name:'trans_id'},
                {name:'asset_id'},
                {name:'ati'},
                {name:'asset_name'},
                {name:'amount'},
                {name:'bookvalue'},
                {name:'note'},
                {name:'tdate'},
                {name:'visible'},
            ]
        }, cfg));
    }
});
jun.rztAssetTransDetail = new jun.AssetTransDetailstore();
//jun.rztAssetTransDetail.load();
