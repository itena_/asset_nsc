jun.AssetComparationDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AssetComparationDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetComparationDetailstoreId',
            url: 'AssetComparationDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'asset_comparation_detail_id'},
                {name: 'asset_comparation_id'},
                {name: 'ati'},
                {name: 'description'},
                {name: 'status'},
                {name: 'qty', type: 'int'},
            ]
        }, cfg));
        this.on('add', this.compareStores, this);
        this.on('update', this.compareStores, this);
        this.on('remove', this.compareStores, this);
    },
    compareStores: function () {
        var storeA = jun.rztAssetComparationDetail;
        var storeB = jun.rztAssetComparationDetailExisting;
        var countDifferent = 0;
        storeA.each(function (recordA) {
            var atiValue = recordA.get('ati');
            var indexB = storeB.findExact('ati', atiValue);

            if (indexB !== -1) {
                var recordB = storeB.getAt(indexB);

                var isDifferent = (
                    recordA.get('qty') !== recordB.get('qty') ||
                    recordA.get('description') !== recordB.get('description')
                );

                recordA.set('status', isDifferent ? 'different' : 'match');

                if(isDifferent)
                    countDifferent++;

            } else {
                recordA.set('status', 'different');
                countDifferent++;
            }
        });
        var totaledItemsDifferent = storeB.data.length - storeA.data.length;

        Ext.getCmp('differenceItem').update("Difference by detailed item(s) : " + countDifferent + " item(s)");
        Ext.getCmp('differenceTotalItem').update("Difference by totaled item(s) : " + totaledItemsDifferent + " item(s)");

        var differenceStatus = countDifferent === 0 && totaledItemsDifferent === 0 ? 'Match' : 'Not Match';
        Ext.getCmp('differenceStatus').setValue(differenceStatus);
    }
});
jun.rztAssetComparationDetail = new jun.AssetComparationDetailstore();
jun.rztAssetComparationDetailCmp = new jun.AssetComparationDetailstore();
jun.rztAssetComparationDetailExisting = new jun.AssetComparationDetailstore();
