jun.AssetRequestWin = Ext.extend(Ext.Window, {
    title: 'Asset Request',
    modez:1,
    width: 400,
    height: 422,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
/*        if (jun.rztAssetGroup.getTotalCount() === 0) {
            jun.rztAssetGroup.load();
        }*/
        if (jun.rztAssetBarangAtiCmp.getTotalCount() === 0) {
            jun.rztAssetBarangAtiCmp.load();
        }
        /*if (jun.rztBarangAsst.getTotalCount() === 0) {
            jun.rztBarangAsst.load();
        }*/
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
/*        if (jun.rztAssetCategorySub.getTotalCount() === 0) {
            jun.rztAssetCategorySub.load();
        }*/
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetRequest',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
/*                                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Item',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'barang_id',
                                    id:'barang_idid',
                                    ref:'../barang_id',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                },
                                */
                   /* {
                        xtype: 'combo',
                        hidden:false,
                        typeAhead: true,
                        fieldLabel: 'Item',
                        ref: '../barang_id',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetBarang,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kode_barang} </br> {nama_barang} </br> {ket} </span></h3>',
                            "</div></tpl>"),
                        listWidth: 450,
                        lastQuery: "",
                        pageSize: 20,
                        forceSelection: true,
                        autoSelect: false,
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        hiddenName: 'barang_id',
                        name: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'nama_barang',
                        emptyText: "Type Items...",

                        anchor: '100%'
                    },*/
                   /* {
                       xtype: 'combo',
                       hidden:false,
                       typeAhead: true,
                       fieldLabel: 'Item',
                       ref: '../barang_id',
                       triggerAction: 'query',
                       lazyRender: true,
                       mode: 'local',
                       store: jun.rztAssetBarang,
                       itemSelector: "div.search-item",
                       tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                           '<h3><span">{kode_barang} </br> {nama_barang} </br> {ket} </span></h3>',
                           "</div></tpl>"),
                       listWidth: 450,
                       lastQuery: "",
                       pageSize: 20,
                       forceSelection: true,
                       autoSelect: false,
                       hideTrigger: true,
                       minChars: 3,
                       matchFieldWidth: !1,
                       hiddenName: 'barang_id',
                       name: 'barang_id',
                       valueField: 'barang_id',
                       displayField: 'nama_barang',
                       emptyText: "Type Items...",

                       anchor: '100%'
                   },*/
                    /*{
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Item',
                        ref: '../barang_id',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetBarangAtiCmp,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kode_barang} - {nama_barang}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'barang_id',
                        name: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'nama_barang',
                        emptyText: "Choose Items...",
                        id:'barang_id',
                        allowBlank: false,
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'combo',
                        hidden:false,
                        typeAhead: true,
                        fieldLabel: 'Asset Item',
                        ref: '../barang_id',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetBarangAtiCmp,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kode_barang} </br> {nama_barang} </br> {ket} </span></h3>',
                            "</div></tpl>"),
                        listWidth: 450,
                        lastQuery: "",
                        pageSize: 20,
                        forceSelection: true,
                        autoSelect: false,
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        hiddenName: 'barang_id',
                        name: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'nama_barang',
                        emptyText: "Choose Items...",
                        //value: STORE,
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'Merk',
                        name: 'merk',
                        id: 'merkid',
                        ref: '../merk',
                        emptyText: "(Optional)",
                        width: 175,
                        readOnly: false,
                        maxLength: 50,
                        anchor: '100%'
                    },
                                                                     {
                                    xtype: 'numericfield',
                                    fieldLabel: 'Qty',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'qty',
                                    id:'qtyid',
                                    ref:'../qty',
                                    maxLength: 11,
                                                                         allowBlank: false,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                /*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'doc_ref',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'doc_ref',
                                    id:'doc_refid',
                                    ref:'../doc_ref',
                                    maxLength: 255,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, */
                         /*                                            {
                            xtype: 'xdatefield',
                            ref:'../tdate',
                            fieldLabel: 'tdate',
                            name:'tdate',
                            id:'tdateid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, */
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../tgl',
                            fieldLabel: 'Purchase Date',
                            name:'tgl',
                            id:'tglid',
                            format: 'd M Y',
                            allowBlank: false,
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, 

                                 /*                                    {
                                    xtype: 'textfield',
                                    fieldLabel: 'store_kode',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'store_kode',
                                    id:'store_kodeid',
                                    ref:'../store_kode',
                                    maxLength: 30,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, */
                                /*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'businessunit_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'businessunit_id',
                                    id:'businessunit_idid',
                                    ref:'../businessunit_id',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, */
                                                                     {
                                    xtype: 'numericfield',
                                    fieldLabel: 'Price',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'price',
                                    id:'priceid',
                                    ref:'../price',
                                    maxLength: 30,
                                                                         allowBlank: false,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                 /*                                    {
                                    xtype: 'textfield',
                                    fieldLabel: 'Category',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'category_id',
                                    id:'category_idid',
                                    ref:'../category_id',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                },*/
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Category',
                        ref: '../category',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategory,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{category_code} - {category_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_id',
                        name: 'category_id',
                        valueField: 'category_id',
                        displayField: 'category_code',
                        emptyText: "Category...",
                        id:'categoryreq',
                        allowBlank: false,
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Sub Category',
                        ref: '../subcategory',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategorySub,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{sub_code} - {sub_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_sub_id',
                        name: 'category_sub_id',
                        valueField: 'category_sub_id',
                        displayField: 'sub_code',
                        emptyText: "Sub Category...",
                        allowBlank: false,
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'Serial Number',
                        name: 'serialnumber',
                        id: 'serialnumberid',
                        ref: '../serialnumber',
                        emptyText: "(Optional)",
                        width: 175,
                        readOnly: false,
                        maxLength: 50,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'Location',
                        name: 'location',
                        id: 'locationid',
                        ref: '../location',
                        emptyText: "(Optional)",
                        width: 175,
                        readOnly: false,
                        maxLength: 50,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Condition',
                        name:'conditions',
                        id:'conditionid',
                        ref:'../conditions',
                        hiddenName: 'conditions',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['0', 'NEW'],
                                ['1', 'SECOND'],
                                ['2', 'BROKEN'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "Choose Condition",
                        mode : 'local',
                        anchor: "100%"
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        hideLabel:false,
                        //hidden:true,
                        name:'note',
                        id:'noteid',
                        ref:'../note',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    /*{
                        xtype: 'checkbox',
                        fieldLabel: 'Non Ati',
                        typeAhead: true,
                        hideLabel: false,
                        value: 1,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: 'nonati',
                        id: "nonati",
                        //ref: '../hide'
                    },*/
                    /*                                    {
                       xtype: 'textfield',
                       fieldLabel: 'approved',
                       hideLabel:false,
                       //hidden:true,
                       name:'approved',
                       id:'approvedid',
                       ref:'../approved',
                       maxLength: 4,
                       //allowBlank: 1,
                       anchor: '100%'
                   },
                                                        {
                       xtype: 'textfield',
                       fieldLabel: 'user_id',
                       hideLabel:false,
                       //hidden:true,
                       name:'user_id',
                       id:'user_idid',
                       ref:'../user_id',
                       maxLength: 50,
                       //allowBlank: 1,
                       anchor: '100%'
                   },
                                                        {
                       xtype: 'textfield',
                       fieldLabel: 'visible',
                       hideLabel:false,
                       //hidden:true,
                       name:'visible',
                       id:'visibleid',
                       ref:'../visible',
                       maxLength: 4,
                       //allowBlank: 1,
                       anchor: '100%'
                   }, */
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetRequestWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.category.on('Select', this.categoryonselect, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    categoryonselect: function(c,r,i)
    {
            var catid =r.get('category_id');
            this.subcategory.store.reload({params:{category_id:catid}});
            this.subcategory.setVisible(true);
            this.subcategory.allowBlank = false;
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);

            //if(this.modez == 1 || this.modez== 2)
            //{

                /*urlz= 'asset/AssetRequest/update/id/' + this.id;

                }*/

        var urlz= 'asset/AssetRequest/create/';
                //}
             
            Ext.getCmp('form-AssetRequest').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                method: 'POST',
                params: {
                    id: this.id,
                    mode:this.modez
                },
                success: function(f,a){
                    jun.rztAssetRequest.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-AssetRequest').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});