jun.AssetPeriodeGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Asset Periode",
    id: 'docs-jun.AssetPeriodeGrid',
    stripeRows: true,
    viewConfig: {
        forceFit: !0
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: !0
    }),
    columns: [
        {
            header: 'Class',
            sortable: true,
            resizable: true,
            dataIndex: 'class',
            width: 50
        },
        {
            header: 'Period',
            sortable: true,
            resizable: true,
            dataIndex: 'period',
            width: 50
        },
        {
            header: 'Tariff(%)',
            sortable: true,
            resizable: true,
            dataIndex: 'tariff',
            width: 50
        },
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price_acquisition',
            width: 100
        },
        {
            header: 'Depreciation PerMonth',
            sortable: true,
            resizable: true,
            dataIndex: 'penyusutanperbulan',
            width: 100
        },
        {
            header: 'Accumulation',
            sortable: true,
            resizable: true,
            dataIndex: 'akumulasipenyusutan',
            width: 100
        },
        {
            header: 'Balance',
            sortable: true,
            resizable: true,
            dataIndex: 'balance',
            width: 100
        },
        {
            header: 'Depreciation Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tglpenyusutan',
            width: 100
        },
    ],
    initComponent: function () {
        this.store = jun.rztAssetPeriode;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    text: 'Depreciation',
                },
            ]
        };
        jun.AssetPeriodeGrid.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },

    getrow: function (sm, idx, r) {
        this.record = r;
    },
});
