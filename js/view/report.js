jun.ReportAssetDepriciation = Ext.extend(Ext.Window, {
    title: "Show Report Depriciation",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAssetDepriciation",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_detail_idid',
                        name: "asset_detail_id",
                        ref: '../asset_detail_id',
                        hidden:true,
                        //valueField: 'asset_detail_id',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'No.Activa',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'atiid',
                        name: "ati",
                        ref: '../ati',
                        readOnly: true,
                        //valueField: 'asset_detail_id',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'Acquisition Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "asset_trans_date",
                        ref: '../asset_trans_date',
                        readOnly: true,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAssetDepriciation.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportAssetDepriciation").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAssetDepriciation").getForm().url = "Report/AssetDepriciation";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAssetDepriciation').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/AssetDepriciation";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportAssetDepriciation").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportAssetDepriciation").getForm().url = url;
            var form = Ext.getCmp('form-ReportAssetDepriciation').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportAssetDepriciation").getForm().getValues());
        }
    }
});

jun.ReportAssetTotal = Ext.extend(Ext.Window, {
    title: "Report Asset",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 279,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAssetTotal",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_detail_idid',
                        name: "asset_detail_id",
                        ref: '../asset_detail_id',
                        hidden:true,
                        //valueField: 'asset_detail_id',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Sub Category',
                        ref: '../subcategory',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategorySub,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{sub_code} - {sub_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_sub_id',
                        name: 'category_sub_id',
                        valueField: 'category_sub_id',
                        displayField: 'sub_code',
                        emptyText: "Sub Category...",
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Status',
                        name:'status',
                        id:'statusasset',
                        ref:'../statusasset',
                        hiddenName: 'statusasset',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['0', 'NON ACTIVE'],
                                ['1', 'ACTIVE'],
                                ['2', 'LEND'],
                                ['3', 'SELL'],
                                ['4', 'RENT'],
                                ['5', 'BROKEN'],
                                ['6', 'LOST'],
                                ['7', 'MAINTENANCE']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "All Status",
                        mode : 'local',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showreportasset",
                        ref: '../showreportasset',
                        defaults: {xtype: "radio",name: "showreportasset"},
                        items: [
                            {
                                boxLabel: "Total",
                                inputValue: "T",
                                checked: true
                            },
                            {
                                boxLabel: "Detail",
                                inputValue: "D"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAssetTotal.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.category.on('Select', this.categoryonselect, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    categoryonselect: function(c,r,i)
    {
        var catid =r.get('category_id');
        this.subcategory.store.reload({params:{category_id:catid}});
        this.subcategory.setVisible(true);
        this.subcategory.allowBlank = false;

    },
    onbtnPdfclick: function () {
        if((this.tglfrom.getValue() == '' && this.tglto.getValue() != '') || this.tglfrom.getValue() != '' && this.tglto.getValue() == '') {
            Ext.MessageBox.alert("Error", "Date harus terisi from dan to nya atau tidak diisi sama sekali.");
            return;
        }
        /*if(this.branch.getValue() == '') {
            Ext.MessageBox.alert("Error", "Branch tidak boleh kosong.");
            return;
        }*/

        var url = "Report/AssetTotal";
        Ext.getCmp("form-ReportAssetTotal").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAssetTotal").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAssetTotal').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "";
        var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportAssetTotal").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportAssetTotal").getForm().url = url;
            var form = Ext.getCmp('form-ReportAssetTotal').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportAssetTotal").getForm().getValues());
        }
    }
});

jun.ReportAssetList = Ext.extend(Ext.Window, {
    title: "Report Asset List",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 227,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAssetList",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Sub Category',
                        ref: '../subcategory',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategorySub,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{sub_code} - {sub_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_sub_id',
                        name: 'category_sub_id',
                        valueField: 'category_sub_id',
                        displayField: 'sub_code',
                        emptyText: "Sub Category...",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAssetList.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
        this.category.on('Select', this.categoryonselect, this);
    },
    categoryonselect: function(c,r,i)
    {
        var catid =r.get('category_id');
        this.subcategory.store.reload({params:{category_id:catid}});
        this.subcategory.setVisible(true);
        this.subcategory.allowBlank = false;

    },
    onbtnPdfclick: function () {
        var url = "Report/AssetList";
        Ext.getCmp("form-ReportAssetList").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAssetList").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAssetList').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/AssetList";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportAssetList").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportAssetList").getForm().url = url;
            var form = Ext.getCmp('form-ReportAssetList').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportAssetList").getForm().getValues());
        }
    }
});


jun.ReportAssetTotalNonActive = Ext.extend(Ext.Window, {
    title: "Report Asset (NonActive)",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 244,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAssetTotalNonActive",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_detail_idid',
                        name: "asset_detail_id",
                        ref: '../asset_detail_id',
                        hidden:true,
                        //valueField: 'asset_detail_id',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Sub Category',
                        ref: '../subcategory',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategorySub,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{sub_code} - {sub_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_sub_id',
                        name: 'category_sub_id',
                        valueField: 'category_sub_id',
                        displayField: 'sub_code',
                        emptyText: "Sub Category...",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showreportasset",
                        ref: '../showreportasset',
                        defaults: {xtype: "radio",name: "showreportasset"},
                        items: [
                            {
                                boxLabel: "Total",
                                inputValue: "T",
                                checked: true
                            },
                            {
                                boxLabel: "Detail",
                                inputValue: "D"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAssetTotalNonActive.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        var url = "";
        var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotalNonActive";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetailNonActive";
        }

        Ext.getCmp("form-ReportAssetTotalNonActive").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAssetTotalNonActive").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAssetTotalNonActive').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {

        var url = "";
        var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotalNonActive";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetailNonActive";
        }

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportAssetTotalNonActive").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportAssetTotalNonActive").getForm().url = url;
            var form = Ext.getCmp('form-ReportAssetTotalNonActive').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportAssetTotalNonActive").getForm().getValues());
        }
    }
});

jun.ReportRentAsset = Ext.extend(Ext.Window, {
    title: "Report Rent Asset",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 227,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRentAsset",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_detail_idid',
                        name: "asset_detail_id",
                        ref: '../asset_detail_id',
                        hidden:true,
                        //valueField: 'asset_detail_id',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Sub Category',
                        ref: '../subcategory',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategorySub,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{sub_code} - {sub_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_sub_id',
                        name: 'category_sub_id',
                        valueField: 'category_sub_id',
                        displayField: 'sub_code',
                        emptyText: "Sub Category...",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    /*{
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showreportasset",
                        ref: '../showreportasset',
                        defaults: {xtype: "radio",name: "showreportasset"},
                        items: [
                            {
                                boxLabel: "Total",
                                inputValue: "T"
                            },
                            {
                                boxLabel: "Detail",
                                inputValue: "D"
                            }
                        ]
                    }*/
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRentAsset.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        var url = "Report/RentAsset";
        /*var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }*/

        Ext.getCmp("form-ReportRentAsset").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRentAsset").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRentAsset').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {

        var url = "Report/RentAsset";
        /*var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/RentAsset";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }*/

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportRentAsset").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportRentAsset").getForm().url = url;
            var form = Ext.getCmp('form-ReportRentAsset').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportRentAsset").getForm().getValues());
        }
    }
});

jun.ReportJurnalAsset = Ext.extend(Ext.Window, {
    title: "Report Jurnal Asset",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 171,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportJurnalAsset",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    /*{
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_detail_idid',
                        name: "asset_detail_id",
                        ref: '../asset_detail_id',
                        hidden:true,
                        //valueField: 'asset_detail_id',
                        anchor: "100%"
                    },*/
                    /*{
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },*/
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    /*{
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showreportasset",
                        ref: '../showreportasset',
                        defaults: {xtype: "radio",name: "showreportasset"},
                        items: [
                            {
                                boxLabel: "Total",
                                inputValue: "T"
                            },
                            {
                                boxLabel: "Detail",
                                inputValue: "D"
                            }
                        ]
                    }*/
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportJurnalAsset.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        var url = "Report/JurnalAsset";
        /*var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }*/

        Ext.getCmp("form-ReportJurnalAsset").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportJurnalAsset").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportJurnalAsset').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {

        var url = "Report/JurnalAsset";
/*        var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }*/

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportJurnalAsset").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportJurnalAsset").getForm().url = url;
            var form = Ext.getCmp('form-ReportJurnalAsset').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportJurnalAsset").getForm().getValues());
        }
    }
});


jun.ReportAssetNonAktiva = Ext.extend(Ext.Window, {
    title: "Report Asset Non Activa",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 247,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAssetNonAktiva",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_detail_idid',
                        name: "asset_detail_id",
                        ref: '../asset_detail_id',
                        hidden:true,
                        //valueField: 'asset_detail_id',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Sub Category',
                        ref: '../subcategory',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategorySub,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{sub_code} - {sub_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_sub_id',
                        name: 'category_sub_id',
                        valueField: 'category_sub_id',
                        displayField: 'sub_code',
                        emptyText: "Sub Category...",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showreportasset",
                        ref: '../showreportasset',
                        defaults: {xtype: "radio",name: "showreportasset"},
                        items: [
                            {
                                boxLabel: "Total",
                                inputValue: "T",
                                checked: true
                            },
                            {
                                boxLabel: "Detail",
                                inputValue: "D"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAssetNonAktiva.superclass.initComponent.call(this);
        this.category.on('Select', this.categoryonselect, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    categoryonselect: function(c,r,i)
    {
        //var categoryreq = Ext.getCmp('categoryreq').getRawValue();

        /*if(tobu == 'OTHER')
        {
            this.branch.setVisible(false);
            this.branch.allowBlank = true;
            this.branch.allowBlank = true;
        }*/

        var catid =r.get('category_id');
        this.subcategory.store.reload({params:{category_id:catid}});
        this.subcategory.setVisible(true);
        this.subcategory.allowBlank = false;

    },
    onbtnPdfclick: function () {
        var url = "";
        var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotalNonAktiva";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetailNonAktiva";
        }

        Ext.getCmp("form-ReportAssetNonAktiva").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAssetNonAktiva").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAssetNonAktiva').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {

        var url = "";
        var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotalNonAktiva";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetailNonAktiva";
        }

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportAssetNonAktiva").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportAssetNonAktiva").getForm().url = url;
            var form = Ext.getCmp('form-ReportAssetNonAktiva').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportAssetNonAktiva").getForm().getValues());
        }
    }
});


//CABANG
jun.ReportShowAsset = Ext.extend(Ext.Window, {
    title: "Export Assets",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 224,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }

        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportShowAsset",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    /*{
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_detail_idid',
                        name: "asset_detail_id",
                        ref: '../asset_detail_id',
                        hidden:true,
                        //valueField: 'asset_detail_id',
                        anchor: "100%"
                    },*/
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Sub Category',
                        ref: '../subcategory',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategorySub,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{sub_code} - {sub_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_sub_id',
                        name: 'category_sub_id',
                        valueField: 'category_sub_id',
                        displayField: 'sub_code',
                        emptyText: "Sub Category...",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportShowAsset.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
        this.category.on('Select', this.categoryonselect, this);
    },
    categoryonselect: function(c,r,i)
    {
        //var categoryreq = Ext.getCmp('categoryreq').getRawValue();

        /*if(tobu == 'OTHER')
        {
            this.branch.setVisible(false);
            this.branch.allowBlank = true;
            this.branch.allowBlank = true;
        }*/

        var catid =r.get('category_id');
        this.subcategory.store.reload({params:{category_id:catid}});
        this.subcategory.setVisible(true);
        this.subcategory.allowBlank = false;

    },
    onbtnPdfclick: function () {
        var url = "Report/ShowAsset";
        /*var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }*/

        Ext.getCmp("form-ReportShowAsset").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportShowAsset").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportShowAsset').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {

        var url = "Report/ShowAsset";
        /*        var selectedz = this.showreportasset.getValue();
                if(selectedz && selectedz.inputValue == 'T')
                {
                    url = "Report/AssetTotal";
                    //url = "Report/bep";
                }
                else
                {
                    url = "Report/AssetTotalDetail";
                }*/

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportShowAsset").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportShowAsset").getForm().url = url;
            var form = Ext.getCmp('form-ReportShowAsset').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportShowAsset").getForm().getValues());
        }
    }
});

jun.BeritaAcaraWin = Ext.extend(Ext.Window, {
    title: 'Berita Acara',
    iconCls: "silk13-report",
    modez: 1,
    width: 438,
    height: 400,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,

    initComponent: function() {
        if(jun.rztBusinessunitCmp.getTotalCount() === 0) {
           jun.rztBusinessunitCmp.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-BeritaAcara',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'asset_history_id',
                        hideLabel:false,
                        hidden:true,
                        name:'asset_history_id',
                        id:'asset_history_idid',
                        ref:'../ati',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Activa',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'ati',
                        id:'atiid',
                        ref:'../ati',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref:'../tdate',
                        fieldLabel: 'Tanggal',
                        name:'tdate',
                        id:'tdateid',
                        format: 'd M Y',
                        value: DATE_NOW,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        ref:'../lokasi',
                        fieldLabel: 'Lokasi',
                        name:'lokasi',
                        id:'lokasiid',
                        maxLength: 20,
                        emptyText: 'Yogyakarta',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama PIC 1',
                        hideLabel:false,
                        //hidden:true,
                        name:'pic1',
                        id:'pic1id',
                        ref:'../pic1',
                        maxLength: 20,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Jabatan PIC 1',
                        hideLabel:false,
                        //hidden:true,
                        name:'jabatan1',
                        id:'jabatan1id',
                        ref:'../jabatan1',
                        maxLength: 20,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama PIC 2',
                        hideLabel:false,
                        //hidden:true,
                        name:'pic2',
                        id:'pic2id',
                        ref:'../pic2',
                        maxLength: 20,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Jabatan PIC 2',
                        hideLabel:false,
                        //hidden:true,
                        name:'jabatan2',
                        id:'jabatan2id',
                        ref:'../jabatan2',
                        maxLength: 20,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Mengetahui',
                        hideLabel:false,
                        //hidden:true,
                        name:'mengetahui',
                        id:'mengetahuiid',
                        ref:'../mengetahui',
                        maxLength: 20,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Jabatan Mengetahui',
                        hideLabel:false,
                        //hidden:true,
                        name:'jmengetahui',
                        id:'jmengetahuiid',
                        ref:'../jmengetahui',
                        maxLength: 20,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    /*{
                        xtype: 'textfield',
                        fieldLabel: 'BU Mengetahui',
                        hideLabel:false,
                        //hidden:true,
                        name:'bumengetahui',
                        id:'bumengetahuiid',
                        ref:'../bumengetahui',
                        maxLength: 20,
                        //allowBlank: ,
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'BU Mengetahui',
                        ref: '../bumengetahui',
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztBusinessunitCmp,
                        hiddenName: 'bumengetahui',
                        name: 'bumengetahui',
                        valueField: 'businessunit_code',
                        displayField: 'businessunit_name',
                        emptyText: "Choose...",
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                    /* {
                         xtype: 'combo',
                         fieldLabel: 'Type',
                         name:'type',
                         id:'typeid',
                         ref:'../type',
                         hiddenName: 'type',
                         store: new Ext.data.SimpleStore({
                             data: [
                                 ['PT', 'PT'],
                                 ['CV', 'CV'],
                                 ['Pte.Ltd', 'Pte.Ltd'],
                                 ['Yayasan', 'Yayasan'],
                                 ['Sdn.Bhd', 'Sdn.Bhd'],
                                 ['Firma', 'Firma'],
                                 ['Co.Ltd', 'Co.Ltd'],
                                 ['KSO', 'KSO'],
                                 ['OTHER', 'OTHER'],
                             ],
                             id: 0,
                             fields: ['value', 'text']
                         }),
                         valueField: 'value',
                         displayField: 'text',
                         triggerAction: 'all',
                         editable: false,
                         emptyText: "type",
                         mode : 'local',
                         anchor: "100%",
                     },*/
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.BeritaAcaraWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        //this.btnDisabled(true);
        var url = "Report/BeritaAcara";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-BeritaAcara").getForm().standardSubmit = !0;
            Ext.getCmp("form-BeritaAcara").getForm().url = url;
            var form = Ext.getCmp('form-BeritaAcara').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-BeritaAcara").getForm().getValues());
        }
    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }

});
