jun.SecurityRolesWin = Ext.extend(Ext.Window, {
    title: "Security Role",
    modez: 1,
    width: 800,
    height: 600,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: 1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-SecurityRoles",
                labelWidth: 100,
                labelAlign: "left",
                layout: "accordion",
                ref: "formz",
                border: !1,
                anchor: "100% 100%",
                items: [
                    {
                        xtype: "panel",
                        title: "Description",
                        layout: "form",
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        items: [
                            {
                                xtype: "textfield",
                                fieldLabel: "Role Name",
                                hideLabel: !1,
                                name: "role",
                                id: "roleid",
                                ref: "../role",
                                maxLength: 30,
                                anchor: "100%"
                            },
                            {
                                xtype: "textarea",
                                fieldLabel: "Description",
                                enableKeyEvents: true,
                                style: {textTransform: "uppercase"},
                                listeners: {
                                    change: function (field, newValue, oldValue) {
                                        field.setValue(newValue.toUpperCase());
                                    }
                                },
                                hideLabel: !1,
                                name: "ket",
                                id: "ketid",
                                ref: "../ket",
                                maxLength: 255,
                                anchor: "100%"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Master Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Class",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "504"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Categories",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "509"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sub Categories",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "510"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Items",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "605"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Region / Area",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "116"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Branch / Store",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "115"
                            },





                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Transaction Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            /*{
                                columnWidth: .33,
                                boxLabel: "Asset",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "505"
                            },*/
                            {
                                columnWidth: .33,
                                boxLabel: "Assets",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "506"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Asset Non Activa",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "511"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Request Approval",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "512"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Asset Requests",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "513"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Show Asset",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "704"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Asset Transfer",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "514"
                            },

                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Report Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [

                            {
                                columnWidth: .33,
                                boxLabel: "Report Asset",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "700"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Report Asset (NonActive)",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "701"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Report Asset Depreciation",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "706"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Report Rent Asset",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "702"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Report Jurnal Asset",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "703"
                            },

                            {
                                columnWidth: .33,
                                boxLabel: "Report Non Activa",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "705"
                            },

                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Administration Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Business Units",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "800"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "User Management",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "400"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Security Roles",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "401"
                            },
                            /*{
                                columnWidth: .33,
                                boxLabel: "Backup / Restore",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "402"
                            },*/
                            /*{
                                columnWidth: .33,
                                boxLabel: "Import",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "403"
                            },*/
                            /*{
                                columnWidth: .33,
                                boxLabel: "Setting Client",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "404"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Preferences",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "405"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Employee",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "124"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "User Employee",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "406"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Restrict Date",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "502"
                            },*/
                            {
                                columnWidth: .33,
                                boxLabel: "Import Data",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "817"
                            },
                            /*{
                                columnWidth: .33,
                                boxLabel: "Upload History",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "503"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sync",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "508"
                            }*/

                        ]
                    },
                    {
                        xtype: "panel",
                        title: "General Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Change Password",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "000"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Logout",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "001"
                            }
                            ,
                            {
                                columnWidth: .33,
                                boxLabel: "Super User (SU)",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "9999"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SecurityRolesWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        var a;
        this.modez == 1 || this.modez == 2 ? a = "SecurityRoles/update/id/" + this.id : a = "SecurityRoles/create/",
            Ext.getCmp("form-SecurityRoles").getForm().submit({
                url: a,
                scope: this,
                success: function (a, b) {
//                    var a = BASE_URL;
                    window.location = BASE_URL;
//                    jun.rztSecurityRoles.reload();
//                    jun.sidebar.getRootNode().reload();
//                    var c = Ext.decode(b.response.responseText);
//                    this.close();
//                    this.closeForm ? this.close() : (c.data != undefined && Ext.MessageBox.alert("Pelayanan", c.data.msg),
//                        this.modez == 0 && Ext.getCmp("form-SecurityRoles").getForm().reset());
                },
                failure: function (a, b) {
                    Ext.MessageBox.alert("Error", "Can't Communicate With The Server");
                    this.btnDisabled(!1);
                }
            });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.SettingClientsWin = Ext.extend(Ext.Window, {
    title: "Setting Client",
    modez: 1,
    width: 350,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: !1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-SettingClients",
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Setting Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'name_',
                        id: 'name_id',
                        ref: '../name_',
                        maxLength: 50,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Value',
                        hideLabel: false,
                        //hidden:true,
                        name: 'value_',
                        ref: '../value_',
                        maxLength: 50,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SettingClientsWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        if (this.modez == 0) {
            var c = jun.rztSettingClients.recordType,
                d = new c({
                    name_: this.name_.getValue(),
                    value_: this.value_.getValue()
                });
            jun.rztSettingClients.add(d);
        } else {
            this.r.set('name_', this.name_.getValue());
            this.r.set('value_', this.value_.getValue());
            this.r.commit();
        }
        var data = {};
        var arr = [];
        jun.rztSettingClients.each(function (rec) {
                arr.push(rec.data);
            }
        );
        data.data = arr;
        // Lockr.set('settingClient', data);
        localStorage.setItem("settingClient", JSON.stringify(data));
        this.close();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});