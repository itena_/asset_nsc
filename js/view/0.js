function getMsg(msg, important = null) {
    if(important) {
        for(let i=0;i<important.length;i++) {
            msg = msg.replace(important[i], '<b style="color: red">'+important[i]+'</b>');
        }
    }
    return msg;
}

function getMsgAlert(msg, important = null) {
    Ext.MessageBox.alert("Warning", getMsg(msg, important));
}

function editForm(t) {
    if (t == undefined) {
        Ext.MessageBox.alert("Warning", getMsg("You are not choosing any data.",['data']));
        return;
    }
}

var saveCloseCancel = [
    {
        xtype: 'button',
        text: 'Save',
        hidden: false,
        ref: '../btnSave'
    },
    {
        xtype: 'button',
        text: 'Save & Close',
        ref: '../btnSaveClose'
    },
    {
        xtype: 'button',
        text: 'Cancel',
        ref: '../btnCancel'
    }
];

function successNotification(f, a, t, response) {
    Ext.MessageBox.show({
        title: 'Info',
        msg: response.msg,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.INFO
    });
    if (t.modez == 0) {
        t.formz.getForm().reset();
        t.btnDisabled(false);
    }
    if (t.closeForm) {
        t.close();
    }
}

function failureNotification(f, a, t) {
    switch (a.failureType) {
        case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
        case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
        case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
    }
    t.btnDisabled(false);
}

jun.getAssetDetail = function (a) {
    var b = jun.rztAssetDetail, c = b.findExact("asset_detail_id", a);
    return b.getAt(c);
};
jun.getBarangAsset = function (a) {
    var b = jun.rztBarangAssetLib, c = b.findExact("barang_asset_id", a);
    return b.getAt(c);
};
jun.renderBarangAsset = function (a, b, c) {
    var jb = jun.getBarangAsset(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama_barang_asset;
};
jun.renderKodeBarangAsset = function (a, b, c) {
    var jb = jun.getBarangAsset(a);
    if (jb == null) {
        return '';
    }
    return jb.data.kode_barang_asset;
};

jun.getStore = function (a) {
    var b = jun.rztStoreCmp, c = b.findExact("store_kode", a);
    return b.getAt(c);
};
jun.renderStore = function (a, b, c) {
    var jb = jun.getStore(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama_store;
};
jun.getStoreId = function (a) {
    var b = jun.rztStoreCmp, c = b.findExact("store_id", a);
    return b.getAt(c);
};
jun.renderStoreCode = function (a, b, c) {
    var jb = jun.getStoreId(a);
    if (jb == null) {
        return '';
    }
    return jb.data.store_kode;
};

jun.login = new Ext.extend(Ext.Window, {
    width: 390,
    height: 150,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Login",
    padding: 5,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-Login",
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                plain: !0,
                items: [
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "usernameid",
                        ref: "../username",
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Username",
                        name: "loginUsername",
                        allowBlank: !1
                    },
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "passwordid",
                        ref: "../password",
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Password",
                        name: "loginPassword",
                        inputType: "password",
                        allowBlank: !1
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Login",
                    hidden: !1,
                    ref: "../btnLogin"
                }
            ]
        };
        jun.login.superclass.initComponent.call(this);
        this.btnLogin.on("click", this.onbtnLoginClick, this);
    },
    onbtnLoginClick: function () {
        var username = this.username.getValue();
        var password = this.password.getValue();
        if (username.trim() == "" || password.trim() == "") {
            Ext.Msg.alert("Warning!", "Login Failed");
            return;
        }
        var a = Ext.getCmp("passwordid").getValue();
        a = jun.EncryptPass(a);
        Ext.getCmp("passwordid").setValue(a);
        Ext.getCmp("form-Login").getForm().submit({
            scope: this,
            url: "site/loginOverride",
            waitTitle: "Connecting",
            waitMsg: "Sending data...",
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                this.close();
                //Ext.getCmp('discid').setReadOnly(false);
                Ext.getCmp('discrpid').setDisabled(false);
                Ext.getCmp('priceid').setDisabled(false);
                Ext.getCmp('discdetilid').setDisabled(false);
                Ext.getCmp('discadetilid').setDisabled(false);
                Ext.getCmp('overrideid').setValue(response.msg);
            },
            failure: function (f, a) {
                Ext.getCmp("form-Login").getForm().reset();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.BackupRestoreWin = new Ext.extend(Ext.Window, {
    width: 550,
    height: 125,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Backup / Restore",
    padding: 5,
    iswin: true,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 100,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4;padding: 10px",
            id: "form-BackupRestoreWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            border: !1,
            plain: !0,
            defaults: {
                allowBlank: false,
                msgTarget: 'side'
            },
            items: [
                {
                    xtype: "fileuploadfield",
                    hideLabel: !1,
                    fieldLabel: "File Name",
                    emptyText: 'Select an file restore (*.pos.gz)',
                    id: "filename",
                    ref: "../filename",
                    name: "filename",
                    anchor: "95%"
                }
            ]
        });
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Delete All Transaction",
                    hidden: !1,
                    ref: "../btnDelete"
                },
                {
                    xtype: "button",
                    text: "Download Backup",
                    hidden: !1,
                    ref: "../btnBackup"
                },
                {
                    xtype: "button",
                    text: "Upload Restore",
                    hidden: !1,
                    ref: "../btnRestore"
                }
            ]
        };
        jun.BackupRestoreWin.superclass.initComponent.call(this);
        this.btnBackup.on("click", this.onbtnBackupClick, this);
        this.btnRestore.on("click", this.btnRestoreClick, this);
        this.btnDelete.on("click", this.deleteRec, this);
    },
    onbtnBackupClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().reset();
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = !0;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "Site/BackupAll";
        var form = Ext.getCmp('form-BackupRestoreWin').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnRestoreClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = false;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "site/RestoreAll";
        if (Ext.getCmp("form-BackupRestoreWin").getForm().isValid()) {
            Ext.getCmp("form-BackupRestoreWin").getForm().submit({
                url: 'site/RestoreAll',
                waitMsg: 'Uploading your restore...',
                success: function (f, a) {
                    var response = Ext.decode(a.response.responseText);
                    Ext.Msg.alert('Successfully', response.msg);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Confirm', 'Are you sure delete all transaction?', this.btnDeleteClick, this);
    },
    btnDeleteClick: function (btn) {
        if (btn == 'no') {
            return;
        }
        Ext.Ajax.request({
            method: 'POST',
            scope: this,
            url: 'site/DeleteTransAll',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                Ext.Msg.alert('Successfully', response.msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
var itemFile = null;
jun.ImportXlsx = new Ext.extend(Ext.Window, {
    width: 500,
    height: 90,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Import Data",
    iswin: true,
    padding: 5,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 75,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4;padding: 10px",
            id: "form-BackupRestoreWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            html: "File Type : <select id='importType'><option  value='item'>Item</option><option value='cust'>Customers</option></select><input id='inputFile' type='file' name='uploaded'/>",
            border: !1,
            plain: !0,
            listeners: {
                afterrender: function () {
                    itemFile = document.getElementById("inputFile");
                    itemFile.addEventListener('change', EventChange, false);
                }
            }
        });
        jun.ImportXlsx.superclass.initComponent.call(this);
    }
});
function to_json(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function (sheetName) {
        var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            result[sheetName] = roa;
        }
    });
    return result;
}
function EventChange(e) {
    var files = itemFile.files;
    var f = files[0];
    if (f.name.split('.').pop() != "xlsx") {
        Ext.Msg.alert('Failure', 'Need file *.xlsx');
        return;
    }
    var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" &&
        typeof FileReader.prototype.readAsBinaryString !== "undefined";
    var reader = new FileReader();
    reader.onload = function (e) {
        if (typeof console !== 'undefined') console.log("onload", new Date());
        var data = e.target.result;
        var wb = XLSX.read(data, {type: 'binary'});
        var s = wb.Strings;
        var e = document.getElementById("importType");
        var tipe = e.value;
        var iscust = tipe == "cust";
        if (iscust) {
            if (s[1].h.indexOf("NOBASE") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column NOBASE');
                return;
            }
            if (s[2].h.indexOf("NAMACUS") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column NAMACUS');
                return;
            }
            if (s[4].h.indexOf("ALAMAT") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column ALAMAT');
                return;
            }
            if (s[6].h.indexOf("TELP") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column TELP');
                return;
            }
            if (s[8].h.indexOf("TGLLH") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column TGLLH');
                return;
            }
            if (s[9].h.indexOf("SEX") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column SEX');
                return;
            }
            if (s[11].h.indexOf("KERJA") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column KERJA');
                return;
            }
            if (s[12].h.indexOf("AWAL") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column AWAL');
                return;
            }
            if (s[13].h.indexOf("AKHIR") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column AKHIR');
                return;
            }
        } else {
            if (s[0].h.indexOf("KODEBRG") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column KODEBRG');
                return;
            }
            if (s[1].h.indexOf("JNSBRG") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column JNSBRG');
                return;
            }
            if (s[2].h.indexOf("NAMABRG") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column NAMABRG');
                return;
            }
            if (s[3].h.indexOf("SATUAN1") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column SATUAN1');
                return;
            }
            if (s[8].h.indexOf("HJUAL") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column HJUAL');
                return;
            }
        }
        var output = to_json(wb);
        var url = iscust ? "customers/import" : "barang/import";
        Ext.Ajax.request({
            method: 'POST',
            scope: this,
            url: url,
            params: {
                detil: Ext.encode(output)
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                var win = new Ext.Window({
                    width: 1000,
                    height: 600,
                    modal: !0,
                    border: !1,
                    layout: 'fit',
                    autoScroll: true,
                    title: "Duplicate Data",
                    iswin: true,
                    padding: 5,
                    html: response.msg
                });
                win.show();
//                Ext.Msg.alert('Successfully', response.msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
    reader.readAsBinaryString(f);
}
jun.renderPackageIco = function (a, b, c) {
    return a == "" || a == null || a == undefined ? '<img alt="" src="css/silk_v013/icons/pill.png" class="x-action-col-icon x-action-col-0  " ext:qtip="PCS">' :
        '<img alt="" src="css/silk_v013/icons/package_green.png" class="x-action-col-icon x-action-col-0  " ext:qtip="Package">';
};
//======================================= Untuk Baca File Excel===============================
jun.dataStockOpname = '';
jun.dataSales = '';
jun.namasheet = '';
jun.dataImport = '';

function readFileExcel(e) {
    dataImport = "";
    if (itemFile.files.length == 0) return;
    var files = itemFile.files;
    var f = files[0];
    {
        var reader = new FileReader();
        var name = f.name;
        reader.onload = function (e) {
            var data = e.target.result;
            var wb;
            var arr = fixdata(data);
            wb = XLS.read(btoa(arr), {type: 'base64'});
            jun.dataImport = to_json(wb);
            jun.namasheet = wb.SheetNames[0];
        };
        reader.readAsArrayBuffer(f);
    }
}
function to_json(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function (sheetName) {
        var roa = XLS.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            result[sheetName] = roa;
        }
    });
    return result;
}
function fixdata(data) {
    var o = "", l = 0, w = 10240;
    for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
    o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
    return o;
}
//======================================= END OF : Untuk Baca File Excel=============================


jun.round = function (number, decimal){
    var d = Math.pow(10,decimal);
    return Math.round(number*d)/d;
};
jun.roundDown = function (number, decimal){
    var d = Math.pow(10,decimal);
    return Math.floor(number*d)/d;
};