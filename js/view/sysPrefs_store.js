jun.SysPrefsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SysPrefsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SysPrefsStoreId',
            url: 'SysPrefs',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sys_prefs_id'},
                {name: 'name_'},
                {name: 'value_'}
            ]
        }, cfg));
    }
});
jun.SysPrefsstoreReferral = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SysPrefsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SysPrefsStoreId',
            url: 'SysPrefs/referral',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sys_prefs_id'},
                {name: 'name_'},
                {name: 'value_'}
            ]
        }, cfg));
    }
});
jun.rztSysPrefs = new jun.SysPrefsstore();
jun.rztSysPrefsReferral = new jun.SysPrefsstoreReferral();
//jun.rztSysPrefs.load();
