jun.BusinessunitGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Business Units",
    id: 'docs-jun.BusinessunitGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Business Code',
            sortable: true,
            resizable: true,
            dataIndex: 'businessunit_code',
            width: 100
        },
        {
            header: 'Business Name',
            sortable: true,
            resizable: true,
            dataIndex: 'businessunit_name',
            width: 100
        },
        {
            header: 'Description',
            sortable: true,
            resizable: true,
            dataIndex: 'description',
            width: 100
        },
    ],
    initComponent: function () {
        this.store = jun.rztBusinessunit;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.BusinessunitGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    loadForm: function () {
        var form = new jun.BusinessunitWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.businessunit_id;
        var form = new jun.BusinessunitWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
});
