jun.AssetComparationWin = Ext.extend(Ext.Window, {
    title: 'Asset Comparation v.1',
    modez: 1,
    width: 1200,
    height: 110 + (30 * 16),
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    statusActive: -1,
    onEsc: function () {
        Ext.MessageBox.confirm('Questions', 'Are you sure want exit?', function (btn) {
                if (btn == 'yes') {
                    this.close();
                }
            },
            this);
    },
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background: none; padding: 5px',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                ref: 'formz',
                border: false,
                fileUpload: true,
                items: [
                    {
                        layout: {
                            type: 'hbox'
                        },
                        bodyStyle: 'background: none;',
                        border: false,
                        height: 55,
                        items: [
                            {
                                layout: 'form',
                                bodyStyle: 'background: none; padding-left: 10px; padding-right: 10px;',
                                border: false,
                                labelSeparator: ':',
                                labelWidth: 100,
                                labelAlign: 'left',
                                flex: 1,
                                items: [
                                    {
                                        xtype: 'xdatefield',
                                        ref: '../tgl',
                                        fieldLabel: 'Date',
                                        name: 'tgl',
                                        format: 'd M Y',
                                        readOnly: !EDIT_TGL,
                                        value: DATE_NOW,
                                        allowBlank: false,
                                        anchor: '100%',
                                    },
                                    {
                                        xtype: 'combo',
                                        typeAhead: true,
                                        allowBlank: false,
                                        fieldLabel: 'Branch/Store',
                                        ref: '../../../branch',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: jun.rztStoreCmp,
                                        emptyText: 'All Store',
                                        hiddenName: 'store_id',
                                        valueField: 'store_id',
                                        displayField: 'nama_store',
                                        anchor: '100%'
                                    },
                                ]
                            },
                            {
                                layout: 'form',
                                bodyStyle: 'background: none; padding-left: 10px; padding-right: 10px;',
                                border: false,
                                labelSeparator: ':',
                                labelWidth: 100,
                                labelAlign: 'left',
                                flex: 1,
                                items: [
                                    {
                                        xtype: 'box',
                                        id: 'differenceItem',
                                    },
                                    {
                                        xtype: 'box',
                                        id: 'differenceTotalItem',
                                    },
                                    {
                                        xtype: 'hidden',
                                        id: 'differenceStatus',
                                        ref: '../../../status'
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        layout: 'form',
                        bodyStyle: 'background: none; padding-left: 10px; padding-right: 10px;',
                        border: false,
                        labelSeparator: ':',
                        labelWidth: 100,
                        labelAlign: 'left',
                        height: 330 + 95,
                        flex: 1,
                        items: [
                            {
                                layout: {
                                    type: 'hbox'
                                },
                                bodyStyle: 'background: none;',
                                border: false,
                                items: [
                                    {
                                        layout: 'form',
                                        bodyStyle: 'background: none; padding-left: 10px; padding-right: 10px;',
                                        border: false,
                                        labelSeparator: ':',
                                        labelWidth: 100,
                                        labelAlign: 'left',
                                        flex: 1,
                                        items: [
                                            new jun.AssetComparationDetailGrid({
                                                ref: '../../../../gridDetails',
                                                height: 320 + 95 + 10
                                            })
                                        ]
                                    },
                                    {
                                        xtype: 'panel',
                                        title: 'Existing Asset Details',
                                        layout: 'form',
                                        bodyStyle: 'background: none; padding-left: 10px; padding-right: 10px;',
                                        border: false, // No border
                                        labelSeparator: ':',
                                        labelWidth: 100,
                                        labelAlign: 'left',
                                        flex: 1,
                                        items: [
                                            new jun.AssetExistingDetailGrid({
                                                ref: '../../../../gridDetailsExisting',
                                                height: 320 + 95
                                            })
                                        ]
                                    }
                                ]
                            },
                        ]
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            buttonAlign: 'left',
            items: [
                '->',
                saveCloseCancel
            ]
        };
        jun.AssetComparationWin.superclass.initComponent.call(this);
        this.branch.on('select', this.branchOnSelect, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    branchOnSelect: function (c,r,i) {
        // console.log(this.gridDetails);
        this.gridDetails.ati.store.reload({params:{store_id: c.getValue()}});
        this.gridDetailsExisting.store.reload({params:{store_id : c.getValue()}});
        this.gridDetails.barcode.focus(50, true);
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        if(this.branch.getValue() == '') {
            getMsgAlert("Branch / Store can't be blank.",['Branch / Store']);
            return;
        }
        if (this.gridDetails.store.data.length == 0) {
            Ext.Msg.alert('Error', "Details must set");
            return;
        }

        this.btnDisabled(true);

        var form = this.formz.getForm();
        var customParams = {
            mode: this.modez,
            id: this.id,
            detil: Ext.encode(Ext.pluck(this.gridDetails.store.data.items, "data")),
            detilExisting: Ext.encode(Ext.pluck(this.gridDetailsExisting.store.data.items, "data")),
            tgl: DATE_NOW,
            status: this.status.getValue()
        };
        var params = Ext.apply({}, form.getValues(), customParams);

        Ext.Ajax.request({
            url     : 'AssetComparation/Create',
            timeout : 10000,
            scope   : this,
            params  : params,
            success: function (f, a) {
                jun.rztAsseComparation.reload();

                var response = Ext.decode(f.responseText);

                if(response.success == false){
                    this.closeForm = false;
                    this.btnDisabled(false);
                }

                successNotification(f, a, this, response);
            },
            failure: function (f, a) {
                failureNotification(f, a, this);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        Ext.MessageBox.confirm('Questions', 'Are you sure want exit?', function (btn) {
                if (btn == 'yes') {
                    this.close();
                }
            },
            this);
    },
    onWinClose: function () {
        this.gridDetails.store.removeAll();
        this.gridDetailsExisting.store.removeAll();
    }
});