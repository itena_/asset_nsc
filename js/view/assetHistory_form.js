jun.AssetHistoryWin = Ext.extend(Ext.Window, {
    title: 'Asset History',
    modez:1,
    width: 1302,
    height: 469,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetHistory',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    new jun.AssetHistoryGrid({
                        height: 405 - 30,
                        frameHeader: !1,
                        header: !1,
                        ref: "../gridDetail",
                        x: 5,
                        y: 65 + 30
                    }),
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                /*{
                    xtype: 'button',
                    text: 'Isi Berita Acara',
                    hidden: false,
                    ref:'../btnBeritaAcara'
                },*/
                /*{
                    xtype: 'button',
                    text: 'Export',
                    hidden: false,
                    ref:'../btnExport'
                },*/
                {
                    xtype: 'button',
                    text: 'Close',
                    ref:'../btnClose'
                }
            ]
        };

        jun.AssetHistoryWin.superclass.initComponent.call(this);
        this.gridDetail.store.baseParams=this.historyid;
        this.gridDetail.paging.doRefresh();
        this.btnClose.on('click', this.onbtnCloseclick, this);
        //this.btnBeritaAcara.on('click', this.onbtnBeritaAcaraclick, this);
        //this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function(sm, idx, r){
        this.record = r;

        var selectedz = this.sm.getSelections();
    },

    onbtnBeritaAcaraclick: function(){
        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
        if(selectedz == undefined){
            Ext.MessageBox.alert("Warning","Anda belum memilih data");
            return;
        }
        var idz = selectedz.json.businessunit_id;
        var form = new jun.BeritaAcaraWin({modez:1, id:idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        /*var form = new jun.BeritaAcaraWin({modez:0});
        form.show();*/
    },
    onbtnCloseclick: function(){
        this.close();
    }

});

