jun.AssetDetailWin = Ext.extend(Ext.Window, {
    title: 'Asset Detail',
    modez:1,
    width: 400,
    height: 403-60,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetShowDetail',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Activa',
                        name: 'ati',
                        ref: '../ati',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Doc. Ref',
                        name: 'doc_ref',
                        ref: '../docref_other',
                        readOnly: true,
                        anchor: '100%'
                    },
                    /*{
                        xtype: 'textfield',
                        fieldLabel: 'Asset Name',
                        name: 'asset_name',
                        ref: '../asset_trans_name',
                        readOnly: true,
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Merk',
                        name: 'merk',
                        ref: '../merk',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Location',
                        name: 'location',
                        ref: '../location',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'S/N',
                        name: 'serialnumber',
                        ref: '../serialnumber',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        readOnly: true,
                        allowBlank: false,
                        fieldLabel: 'Branch/Store',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        emptyText: 'All',
                        hiddenName: 'store_id',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Description',
                        name: 'description',
                        ref: '../description',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Category',
                        name: 'category_name',
                        ref: '../category',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Sub Category',
                        name: 'sub_name',
                        ref: '../category_sub',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Asset Price',
                        name: 'price_acquisition',
                        ref: '../asset_trans_price',
                        readOnly: true,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Close',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetDetailWin.superclass.initComponent.call(this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    onbtnCancelclick: function(){
        this.close();
    }
});