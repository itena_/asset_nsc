jun.UsersGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "User Management",
    id: "docs-jun.UsersGrid",
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    columns: [
        {
            header: "Username",
            sortable: true,
            resizable: true,
            dataIndex: "user_id",
            width: 100
        },
        {
            header: "Name",
            sortable: true,
            resizable: true,
            dataIndex: "name",
            width: 100
        },
        {
            header: 'Role',
            sortable: true,
            resizable: true,
            dataIndex: 'role',
            width: 100
        },
        {
            header: "Last Login",
            sortable: true,
            resizable: true,
            dataIndex: "last_visit_date",
            width: 100
        },
        {
            header: 'Business Unit',
            sortable: true,
            resizable: true,
            dataIndex: 'businessunit_code',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        },
    ],
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztSecurityRolesCmp.getTotalCount() === 0) {
            jun.rztSecurityRolesCmp.load();
        }
        if (jun.rztBusinessunitCmp.getTotalCount() === 0) {
            jun.rztBusinessunitCmp.load();
        }
        this.store = jun.rztUsers,
            this.bbar = {
                items: [
                    {
                        xtype: "paging",
                        store: this.store,
                        displayInfo: true,
                        pageSize: 20
                    }
                ]
            };
            this.tbar = {
                xtype: "toolbar",
                items: [
                    {
                        iconCls: "asp-user2_add",
                        xtype: "button",
                        text: "Add User",
                        ref: "../btnAdd"
                    },
                    {
                        xtype: "tbseparator"
                    },
                    {
                        xtype: "button",
                        iconCls: "asp-access",
                        text: "Reset Password",
                        ref: "../btnReset"
                    },
                    {
                        xtype: "tbseparator"
                    },
                    {
                        xtype: "button",
                        iconCls: "asp-user2_delete",
                        text: "Edit User",
                        ref: "../btnEdit"
                    }
                ]
            };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.UsersGrid.superclass.initComponent.call(this);
        this.btnAdd.on("Click", this.loadForm, this);
        this.btnEdit.on("Click", this.loadEditForm, this);
        this.btnReset.on("Click", this.loadResetForm, this);
        this.getSelectionModel().on("rowselect", this.getrow, this);
    },
    getrow: function(sm, idx, r){
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.UsersWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.id;
        var form = new jun.UsersWin({modez: 1, id: idz, height: 223, arrayStore: this.store});
        form.show(this);
        form.formz.getForm().loadRecord(selectedz);

        form.password.setVisible(false);
        form.passwordConfirmation.setVisible(false);
        form.cmbActive.setVisible(false);
    },
    loadResetForm: function () {
        Ext.MessageBox.confirm("Pertanyaan", "Are you sure want reset this password?", this.resetRecYes,
            this);
    },
    resetRecYes: function (a) {
        if (a == "no") return;
        var b = this.sm.getSelected();
        if (b == "") {
            Ext.MessageBox.alert("Warning", "You have not selected a user");
            return;
        }
        var c = jun.StringGenerator(8, "#aA"), d = jun.EncryptPass(c);
        Ext.Ajax.request({
            url: "Users/update/id/" + b.json.id,
            method: "POST",
            params: {
                password: d
            },
            scope: this,
            success: function (a, b) {
                jun.rztUsers.reload();
                var d = Ext.decode(a.responseText);
                Ext.MessageBox.show({
                    title: "Info",
                    msg: d.msg + "<br>Password : " + c,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (a, b) {
                var c = Ext.decode(a.responseText);
                Ext.MessageBox.show({
                    title: "Warning",
                    msg: c.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
    }
});