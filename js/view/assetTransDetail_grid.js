jun.AssetTransDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Asset Trans Detail",
    id: 'docs-jun.AssetTransDetailGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Activa',
            sortable: true,
            resizable: true,
            dataIndex: 'ati',
            width: 150,
            //renderer: jun.renderKodeBarangAll
        },
        {
            header: 'Asset',
            sortable: true,
            resizable: true,
            dataIndex: 'asset_name',
            width: 250,
            //renderer: jun.renderNamaBarang
        },
        {
            header: 'Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            //renderer: jun.renderNamaBarang
        },
        {
            header: 'PPN',
            sortable: true,
            resizable: true,
            dataIndex: 'ppn',
            width: 70,
            align: "right",
            //renderer: jun.renderNamaBarang
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            align: 'left',
            width: 100,
            //renderer: Ext.util.Format.numberRenderer('0,0')
        },
        /*{
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            align: 'right',
            width: 100
        }*/
    ],
    initComponent: function () {
        this.store = jun.rztAssetTransDetail;
        if (jun.rztAssetDetail.getTotalCount() === 0) {
            jun.rztAssetDetail.load();
        }
        if(!this.readOnly){
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 8,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                           /* {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Asset :'
                            },
                            {
                                xtype: 'combo',
                                hidden:false,
                                typeAhead: true,
                                fieldLabel: 'Asset',
                                ref: '../asset_detail_id',
                                triggerAction: 'query',
                                lazyRender: true,
                                mode: 'local',
                                store: jun.rztAssetDetail,
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<h3><span">{ati} </br> {asset_trans_name} </br> {description} </span></h3>',
                                    "</div></tpl>"),
                                listWidth: 450,
                                lastQuery: "",
                                pageSize: 20,
                                forceSelection: true,
                                autoSelect: false,
                                hideTrigger: true,
                                minChars: 3,
                                matchFieldWidth: !1,
                                hiddenName: 'asset_detail_id',
                                name: 'asset_detail_id',
                                valueField: 'asset_detail_id',
                                displayField: 'ati',
                                emptyText: "Choose Asset",

                                //value: STORE,
                                //readOnly: !HEADOFFICE,
                                anchor: '100%'
                            },*/
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Asset :'
                            },
                            {
                                xtype: 'mfcombobox',
                                searchFields: [
                                    'ati',
                                    'asset_trans_name',
                                    'description',
                                    'serialnumber',
                                    'docnumber',
                                ],
                                typeAhead: false,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<span style="font-weight:bold;">{ati}</span> | <span>{asset_trans_name}</span><br><span>{serialnumber}</span><br><span>{description}</span>',
                                    "</div></tpl>"),
                                forceSelection: true,
                                store: jun.rztAssetDetail,
                                hiddenName: 'asset_detail_id',
                                valueField: 'asset_detail_id',
                                ref: '../../asset',
                                displayField: 'ati',
                                listWidth: 300,
                                width: 200,
                                /*listeners:{
                                    select: function (m,e,x){
                                        this.refOwner.satuan.setText(e.data.sat);
                                    }
                                }*/
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Book Value :'
                            },
                            {
                                xtype: 'numericfield',
                                id: 'bookvalueid',
                                ref: '../../bookvalue',
                                width: 80,
                                value: 0,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Amount :'
                            },
                            {
                                xtype: 'numericfield',
                                id: 'amountid',
                                ref: '../../amount',
                                width: 80,
                                value: 0,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'PPN :'
                            },
                            {
                                xtype: 'numericfield',
                                id: 'ppnid',
                                ref: '../../ppn',
                                width: 80,
                                value: 0,
                                minValue: 0,
                                listeners: {
                                    ppn : 'ppnstatuschange'
                                }
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Note :'
                            },
                            {
                                xtype: 'textfield',
                                id: 'noteid',
                                ref: '../../note',
                                width: 400,
                                colspan: 4,
                                maxLength: 255
                            },
                            /*{
                                xtype: 'mfcombobox',
                                searchFields: [
                                    'kode_barang',
                                    'nama_barang'
                                ],
                                typeAhead: false,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<span style="font-weight:bold;">{kode_barang}</span><br><span>{nama_barang}</span>',
                                    "</div></tpl>"),
                                forceSelection: true,
                                store: jun.rztBarangWHAll,
                                hiddenName: 'barang_id',
                                valueField: 'barang_id',
                                ref: '../../barang',
                                displayField: 'kode_barang',
                                listWidth: 300,
                                listeners:{
                                    select: function (m,e,x){
                                        this.refOwner.satuan.setText(e.data.sat);
                                    }
                                }
                            },*/
                            /*{
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                id: 'qtyid',
                                ref: '../../qty',
                                width: 80,
                                value: 1,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                ref: '../../satuan',
                                text: 'Satuan',
                                width: 100
                            }*/
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        defaults: {
                            scale: 'small',
                            width: 40
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                ref: '../../btnAdd'
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                ref: '../../btnEdit'
                            },
                            {
                                xtype: 'button',
                                text: 'Del',
                                ref: '../../btnDelete'
                            }
                        ]
                    }
                ]
            };
        }
        jun.AssetTransDetailGrid.superclass.initComponent.call(this);
        if(!this.readOnly){
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.loadEditForm, this);
            this.btnDelete.on('Click', this.deleteRec, this);
            this.asset.on('select', this.assetOnSelect, this);
            this.getSelectionModel().on('rowselect', this.getrow, this);
        }
    },
    ppnstatuschange: function(c,r,i)
    {
        var amountstatus = this.amount.getValue();
        var ppn = amountstatus * PPNASSET/100;
        this.ppn.setValue(ppn);
    },

    assetOnSelect: function (combo, record, index)
    {
        var price = 0;
        var response_balance = 0; // define here or you won't be able to access it outside of success: f()
        Ext.Ajax.request({
            url: 'AssetDetail/GetBalance/id/' + record.data.asset_detail_id,
            method: 'POST',
            scope: this,
            success:function (f, a) {
                //jun.rztAssetDetail.reload();
                response_balance = Ext.decode(f.responseText);
                price = response_balance.success;
                Ext.getCmp('bookvalueid').setValue(response_balance.success);
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

        //this.bookvalue.setValue(price);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadForm: function () {
        var asset_id = this.asset.getValue();
        if (asset_id == "") {
            Ext.MessageBox.alert("Error", "You have not selected a Asset");
            return
        }
        var a;
//      kunci barang sama tidak boleh masuk dua kali
        if (this.btnEdit.text != 'Save') {
            a = jun.rztAssetTransDetail.findExact("asset_id", asset_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Asset already inputted");
                return
            }
        }

        var assetdtl = jun.getAssetDetail(asset_id);

        var amount = parseFloat(this.amount.getValue());
        var ppn = parseFloat(this.ppn.getValue());
        var ati = assetdtl.data.ati;//this.asset.get();
        var assetname = assetdtl.data.asset_trans_name;//this.asset.get();
        var note = this.note.getValue();
        var bookvalue = this.bookvalue.getValue();

        var businessunit = this.refOwner.businessunit.getValue();
        var branch = this.refOwner.branch.getValue();

        if ( branch == '')
        {
            Ext.MessageBox.alert("Error", "Mohon tentukan branch / store di isi terlebih dahulu");
            return;
        }

        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('asset_id', asset_id);
            record.set('amount', amount);
            record.set('ppn', ppn);
            record.set('bookvalue', bookvalue);
            record.set('note', note);
            record.commit();
        } else {
            var c = jun.rztAssetTransDetail.recordType,
                d = new c({
                    asset_id: asset_id,
                    ati: ati,
                    asset_name: assetname,
                    amount: amount,
                    bookvalue: bookvalue,
                    ppn: ppn,
                    note: note
                });
            jun.rztAssetTransDetail.add(d);
        }

        this.asset.reset();
        this.amount.reset();
        this.bookvalue.reset();
        this.ppn.reset();
        this.note.reset();
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a Asset");
            return;
        }
        if (btn.text == 'Edit') {
            this.asset.setValue(record.data.asset_id);
            this.amount.setValue(record.data.amount);
            this.bookvalue.setValue(record.data.bookvalue);
            this.ppn.setValue(record.data.ppn);
            this.note.setValue(record.data.note);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});


/*
jun.AssetTransDetailGrid=Ext.extend(Ext.grid.GridPanel ,{
	title:"AssetTransDetail",
        id:'docs-jun.AssetTransDetailGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
                        {
			header:'trans_detail_id',
			sortable:true,
			resizable:true,
            dataIndex:'trans_detail_id',
			width:100
		},
                                {
			header:'trans_id',
			sortable:true,
			resizable:true,
            dataIndex:'trans_id',
			width:100
		},
                                {
			header:'asset_id',
			sortable:true,
			resizable:true,
            dataIndex:'asset_id',
			width:100
		},
                                {
			header:'amount',
			sortable:true,
			resizable:true,
            dataIndex:'amount',
			width:100
		},
                                {
			header:'note',
			sortable:true,
			resizable:true,
            dataIndex:'note',
			width:100
		},
                                {
			header:'tdate',
			sortable:true,
			resizable:true,
            dataIndex:'tdate',
			width:100
		},
                		/!*
                {
			header:'visible',
			sortable:true,
			resizable:true,
            dataIndex:'visible',
			width:100
		},
                		*!/

	],
	initComponent: function(){
	this.store = jun.rztAssetTransDetail;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };

           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Tambah',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Ubah',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Hapus',
                        ref: '../btnDelete'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.AssetTransDetailGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},

        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },

        loadForm: function(){
            var form = new jun.AssetTransDetailWin({modez:0});
            form.show();
        },

        loadEditForm: function(){

            var selectedz = this.sm.getSelected();

            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.trans_detail_id;
            var form = new jun.AssetTransDetailWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },

        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },

        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'AssetTransDetail/delete/id/' + record.json.trans_detail_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztAssetTransDetail.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });

        }
})
*/
