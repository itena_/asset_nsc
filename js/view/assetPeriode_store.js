jun.AssetPeriodestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetPeriodestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetPeriodeStoreId',
            url: 'AssetPeriode',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'asset_periode_id'},
                {name:'businessunit_id'},
                {name:'asset_detail_id'},
                {name:'asset_id'},
                {name:'asset_group_id'},
                {name:'barang_id'},
                {name:'docref'},
                {name:'docref_other'},
                {name:'asset_trans_name'},
                {name:'ati'},
                {name:'asset_trans_branch'},
                {name:'price_acquisition'},
                {name:'new_price_acquisition'},
                {name:'asset_trans_date'},
                {name:'description'},
                {name:'class'},
                {name:'period'},
                {name:'tariff'},
                {name:'tglpenyusutan'},
                {name:'penyusutanperbulan'},
                {name:'penyusutanpertahun'},
                {name:'balance'},
                {name:'akumulasipenyusutan'},
                {name:'status'},
                {name:'startdate'},
                {name:'enddate'},
                {name:'created_at'},
                {name:'updated_at'}
            ]
        }, cfg));
    }
});
jun.rztAssetPeriode = new jun.AssetPeriodestore();
//jun.rztAssetPeriode.load();
