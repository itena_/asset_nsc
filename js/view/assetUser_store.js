jun.AssetUserstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AssetUserstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetUserstoreId',
            url: 'AssetUser',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'name'},
            ]
        }, cfg));
    },
});
jun.rztAssetUser = new jun.AssetUserstore();
jun.rztAssetUserLib = new jun.AssetUserstore();
