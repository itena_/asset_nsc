jun.UsersWin = Ext.extend(Ext.Window, {
    title: "Form User",
    iconCls: "asp-user2",
    modez: 1,
    arrayStore: '',
    width: 425,
    height: 301,
    layout: "form",
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-Users",
                labelWidth: 125,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "uctextfield",
                        fieldLabel: "Complete Name",
                        hideLabel: !1,
                        name: "name",
                        ref: "../name",
                        maxLength: 100,
                        anchor: "100%"
                    },
                    {
                        xtype: "textfield",
                        fieldLabel: "Username",
                        hideLabel: !1,
                        name: "user_id",
                        ref: "../user_id",
                        maxLength: 60,
                        anchor: "100%"
                    },
                    {
                        xtype: "textfield",
                        fieldLabel: "Password",
                        hideLabel: !1,
                        name: "password",
                        id: "passwordid",
                        ref: "../password",
                        maxLength: 100,
                        anchor: "100%",
                        inputType: "password"
                    },
                    {
                        xtype: "textfield",
                        fieldLabel: "Confirm Password",
                        hideLabel: !1,
                        id: "password-cfrmid",
                        ref: "../passwordConfirmation",
                        maxLength: 100,
                        anchor: "100%",
                        inputType: "password",
                        initialPassField: "passwordid"
                    },
                    new jun.comboActive({
                        fieldLabel: "Status",
                        hideLabel: !1,
                        width: 100,
                        height: 20,
                        ref: "../cmbActive",
                        id: "statusid"
                    }),
                    {
                        xtype: "combo",
                        typeAhead: !0,
                        triggerAction: "all",
                        lazyRender: !0,
                        mode: "local",
                        fieldLabel: "Role",
                        store: jun.rztSecurityRoles,
                        hiddenName: "security_roles_id",
                        valueField: "security_roles_id",
                        forceSelection: !0,
                        displayField: "role",
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: "Business Unit",
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztBusinessunitCmp,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{businessunit_code} - {businessunit_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'businessunit_id',
                        //name: 'store',
                        valueField: 'businessunit_id',
                        displayField: 'businessunit_code',
                        allowBlank: false,
                        //readOnly: !HEADOFFICE,
                        ref: '../businessunituser',
                        id: 'businessunituser',
                        emptyText: "Business Unit",
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        valueField: 'store_id',
                        displayField: 'bu_mix',
                        emptyText: "All Branch",
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Simpan",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    text: "Simpan & Tutup",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Batal",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.UsersWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
        this.businessunituser.on('Select', this.businessunitonselect, this);
        this.on('close', this.onWinClose, this);
    },
    onWinClose: function () {
        this.formz.getForm().reset();
    },
    businessunitonselect: function (c, r, i) {
        var tobu = Ext.getCmp('businessunituser').getRawValue();

        if (tobu === 'OTHER') {
            this.store.setVisible(false);
            this.store.allowBlank = true;
            this.store.allowBlank = true;
        } else {
            var buid = r.get('businessunit_id');
            this.store.store.reload({params: {businessunit_id: buid}});
            this.store.setVisible(true);
            this.store.allowBlank = true;
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    onActivate: function () {
        this.btnSave.hidden = false;
    },
    saveForm: function () {
        this.btnDisabled(true);
        var encryptedPassword = jun.EncryptPass(this.password.getValue());
        this.password.setValue(encryptedPassword);
        this.passwordConfirmation.setValue(encryptedPassword);
        var urlz = this.modez === 0 ? "Users/create" : "Users/UpdateRole";
        this.formz.getForm().submit({
            url: urlz,
            scope: this,
            params: {
                id: this.id,
                mode: this.modez,
                store: this.store.getValue()
            },
            success: function (f, a) {
                this.arrayStore.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez === 0) {
                    this.formz.getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    /*saveForm: function () {
        this.btnDisabled(!0);
        var a, b = Ext.getCmp("passwordid").getValue(), c = b;
        b = jun.EncryptPass(b);
        Ext.getCmp("passwordid").setValue(b);
        this.modez == 0 ? a = "Users/create" : a = "Users/UpdateRole";
        Ext.getCmp("form-Users").getForm().submit({
            url: a,
            scope: this,
            success: function (a, b) {
                var c = Ext.decode(b.response.responseText);
                Ext.MessageBox.show({
                    title: "Info",
                    msg: c.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Users').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (a, b) {
                if (b.failureType == "client") return;
                Ext.getCmp("passwordid").setValue(c);
                var d = Ext.decode(b.response.responseText);
                Ext.MessageBox.show({
                    title: "Warning",
                    msg: d.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                }), this.btnDisabled(!1);
            }
        });

        jun.rztUsers.reload();
    },*/
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.PasswordWin = Ext.extend(Ext.Window, {
    title: "Change Password",
    iconCls: "asp-access",
    modez: 1,
    width: 425,
    height: 170,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-Password",
                labelWidth: 150,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "textfield",
                        fieldLabel: "Old Password",
                        hideLabel: !1,
                        name: "passwordold",
                        id: "passwordoldid",
                        ref: "../passwordold",
                        maxLength: 100,
                        allowBlank: !1,
                        anchor: "100%",
                        inputType: "password"
                    },
                    {
                        xtype: "textfield",
                        fieldLabel: "New Password",
                        hideLabel: !1,
                        name: "password",
                        id: "passwordid",
                        ref: "../password",
                        maxLength: 100,
                        allowBlank: !1,
                        anchor: "100%",
                        inputType: "password"
                    },
                    {
                        xtype: "textfield",
                        fieldLabel: "Retype New Password",
                        hideLabel: !1,
                        name: "password-cfrm",
                        id: "password-cfrmid",
                        ref: "../password-cfrm",
                        maxLength: 100,
                        anchor: "100%",
                        inputType: "password",
                        initialPassField: "passwordid"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save",
                    hidden: !1,
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.rztSecurityRoles.reload();
        jun.PasswordWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSave.setDisabled(a);
    },
    onActivate: function () {
        this.btnSave.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        var a = Ext.getCmp("passwordoldid").getValue(), b = a;
        a = jun.EncryptPass(a), Ext.getCmp("passwordoldid").setValue(a);
        var c = Ext.getCmp("passwordid").getValue(), d = c;
        c = jun.EncryptPass(c), Ext.getCmp("passwordid").setValue(c);
        var e = Ext.getCmp("password-cfrmid").getValue(), f = e;
        e =
            jun.EncryptPass(e), Ext.getCmp("password-cfrmid").setValue(e), Ext.getCmp("form-Password").getForm().submit({
            url: "users/UpdatePass",
            scope: this,
            success: function (a, b) {
                var c = Ext.decode(b.response.responseText);
                Ext.MessageBox.show({
                    title: "Info",
                    msg: c.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                }), this.close();
            },
            failure: function (a, c) {
                if (c.failureType == "client") return;
                var e = Ext.decode(c.response.responseText);
                Ext.getCmp("passwordoldid").setValue(b), Ext.getCmp("passwordid").setValue(d), Ext.getCmp("passwordid").setValue(f),
                    Ext.MessageBox.show({
                        title: "Warning",
                        msg: e.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING
                    }), this.btnDisabled(!1);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0, this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1, this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
