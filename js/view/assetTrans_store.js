jun.AssetTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetTransStoreId',
            url: 'AssetTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'trans_id'},
                {name:'asset_id'},
                {name:'ati'},
                {name:'asset_name'},
                {name:'barang_id'},
                {name:'businessunit_id'},
                {name:'bu_pengirim'},
                {name:'bu_penerima'},
                {name:'store_pengirim'},
                {name:'store_penerima'},
                {name:'amount'},
                {name:'approval'},
                {name:'note'},
                {name:'tdate'},
                {name:'status'},
                {name:'visible'},
                
            ]
        }, cfg));
    }
});
jun.rztAssetTrans = new jun.AssetTransstore();
//jun.rztAssetTrans.load();
