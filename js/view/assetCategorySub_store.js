jun.AssetCategorySubstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AssetCategorySubstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetCategorySubStoreId',
            url: 'AssetCategorySub',
            root: 'results',
            autoLoad: true,
            totalProperty: 'total',
            fields: [
                {name: 'category_sub_id'},
                {name: 'category_id'},
                {name: 'businessunit_id'},
                {name: 'sub_code'},
                {name: 'sub_name'},
                {name: 'sub_desc'},
                {name: 'timestamp'},
                {name: 'category_name'},
                {name: 'category_desc'}
            ]
        }, cfg));
    }
});
jun.rztAssetCategorySub = new jun.AssetCategorySubstore();
jun.rztAssetCategorySubLib = new jun.AssetCategorySubstore();
