jun.Assetstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Assetstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetStoreId',
            url: 'Asset',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'asset_id'},
                {name:'barang_id'},
                {name:'category_sub_id'},
                {name:'docnumber'},
                {name:'source'},
                {name:'status'},
                {name:'asset_name'},
                {name:'ati'},
                {name:'doc_ref'},
                {name:'store_id'},
                {name:'store_kode'},
                {name:'qty'},
                {name:'description'},
                {name:'date_acquisition'},
                {name:'price_acquisition'},
                {name:'new_price_acquisition'},
                {name:'asset_group_id'},
                {name:'created_at'},
                {name:'updated_at'},
                {name:'category'},
                {name:'category_id'},
                {name:'merk'},
                {name:'location'},
                {name:'serialnumber'},
                {name:'conditions'},
            ]
        }, cfg));
    }
});
jun.rztAsset = new jun.Assetstore();
jun.rztAssetCmp = new jun.Assetstore();
//jun.rztAsset.load();
