jun.AssetNonAtiGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Asset Non Activa",
        id:'docs-jun.AssetNonAtiGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
        /*                {
			header:'nonati_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'nonati_id',
			width:100
		},
                                {
			header:'barang_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'barang_id',
			width:100
		},*/                        {
            header:'Doc.Ref',
            sortable:true,
            resizable:true,
            dataIndex:'doc_ref',
            width:100
        },
        {
            header:'Item Code',
            sortable:true,
            resizable:true,
            dataIndex:'kode_barang',
            width:70
        },
        {
            header:'Item Name',
            sortable:true,
            resizable:true,
            dataIndex:'nama_barang',
            width:100
        },
        {
            header:'Merk',
            sortable:true,
            resizable:true,
            dataIndex:'merk',
            width:100
        },
        {
			header:'Qty',
			sortable:true,
			resizable:true,                        
            dataIndex:'qty',
			width:30
		},

        /*                        {
			header:'tdate',
			sortable:true,
			resizable:true,                        
            dataIndex:'tdate',
			width:100
		},*/
        {
            header:'Price',
            sortable:true,
            resizable:true,
            dataIndex:'price',
            width:80
        },
        {
            header:'Category',
            sortable:true,
            resizable:true,
            dataIndex:'category_name',
            width:100
        },
        {
            header:'Sub Category',
            sortable:true,
            resizable:true,
            dataIndex:'sub_name',
            width:100
        },
        {
            header:'Location',
            sortable:true,
            resizable:true,
            dataIndex:'location',
            width:100
        },
        {
            header:'Description',
            sortable:true,
            resizable:true,
            dataIndex:'note',
            width:100
        },
                                {
			header:'Purchase Date',
			sortable:true,
			resizable:true,                        
            dataIndex:'tgl',
			width:80
		},
        {
            header:'Store',
            sortable:true,
            resizable:true,
            dataIndex:'store_kode',
            width:80
        },
        {
            header: 'Status ',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 60,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 0 :
                        metaData.style += "background-color: #ff7373;";
                        return 'NON ACTIVE';
                    case 1 :
                        metaData.style += "background-color: #9ffb8a;";
                        return 'ACTIVE';
                    case 5 :
                        metaData.style += "background-color: #ff7373;";
                        return 'BROKEN';
                    case 6 :
                        metaData.style += "background-color: #ff7373;";
                        return 'LOST';
                    case 7 :
                        metaData.style += "background-color: #f57b1c;";
                        return 'MAINTENANCE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NON ACTIVE'], [1, 'ACTIVE'], [5, 'BROKEN'], [6, 'LOST'], [7, 'MAINTENANCE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header:'Status Note',
            sortable:true,
            resizable:true,
            dataIndex:'status_note',
            width:80
        },
                		/*
                {
			header:'note',
			sortable:true,
			resizable:true,                        
            dataIndex:'note',
			width:100
		},
                                {
			header:'store_kode',
			sortable:true,
			resizable:true,                        
            dataIndex:'store_kode',
			width:100
		},
                                {
			header:'businessunit_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'businessunit_id',
			width:100
		},
                                {
			header:'price',
			sortable:true,
			resizable:true,                        
            dataIndex:'price',
			width:100
		},
                                {
			header:'category_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'category_id',
			width:100
		},
                                {
			header:'category_sub_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'category_sub_id',
			width:100
		},
                                {
			header:'group_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'group_id',
			width:100
		},
                                {
			header:'user_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'user_id',
			width:100
		},
                                {
			header:'visible',
			sortable:true,
			resizable:true,                        
            dataIndex:'visible',
			width:100
		},
                		*/
		
	],
	initComponent: function(){
	this.store = jun.rztAssetNonAti;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Edit',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Delete',
                        ref: '../btnDelete'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Status',
                        ref: '../btnStatus'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.AssetNonAtiGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.btnStatus.on('Click', this.statusForm, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },
        statusForm: function(){

            var selectedz = this.sm.getSelected();
            if(selectedz == undefined){
                Ext.MessageBox.alert("Warning","Anda belum memilih Data");
                return;
            }


            var idz = selectedz.json.nonati_id;
            var form = new jun.AssetNonAtiStatusWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
            //form.assetvalue.setValue(price);


            //var form = new jun.AssetNonAtiStatusWin({modez:0});
            //form.show();
        },
        loadForm: function(){
            var form = new jun.AssetNonAtiWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.nonati_id;
            var form = new jun.AssetNonAtiWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'AssetNonAti/delete/id/' + record.json.nonati_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztAssetNonAti.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
})
