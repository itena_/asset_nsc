jun.AssetCategorySubWin = Ext.extend(Ext.Window, {
    title: 'Asset Sub Category',
    modez:1,
    width: 400,
    height: 262,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztAssetGroup.getTotalCount() === 0) {
            jun.rztAssetGroup.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetCategorySub',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Category',
                        ref: '../category',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategory,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{category_code} - {category_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_id',
                        name: 'category_id',
                        valueField: 'category_id',
                        displayField: 'category_code',
                        emptyText: "Category...",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Class',
                        store: jun.rztAssetGroup,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{golongan} - {desc} - period ({period}) - year ({year})</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'asset_group_id',
                        valueField: 'asset_group_id',
                        emptyText: "Class...",
                        displayField: 'golongan',
                        anchor: '100%'
                    },
                             /*                                                        {
                                    xtype: 'textfield',
                                    fieldLabel: 'category_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'category_id',
                                    id:'category_idid',
                                    ref:'../category_id',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, */
                                /*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'businessunit_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'businessunit_id',
                                    id:'businessunit_idid',
                                    ref:'../businessunit_id',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, */
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Sub Code',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'sub_code',
                                    id:'sub_codeid',
                                    ref:'../sub_code',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Sub Name',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'sub_name',
                                    id:'sub_nameid',
                                    ref:'../sub_name',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textarea',
                                    fieldLabel: 'Sub Desc',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'sub_desc',
                                    id:'sub_descid',
                                    ref:'../sub_desc',
                                    maxLength: 500,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                /*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'timestamp',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'timestamp',
                                    id:'timestampid',
                                    ref:'../timestamp',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, */
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetCategorySubWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'assetCategorySub/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'assetCategorySub/create/';
                }
             
            Ext.getCmp('form-AssetCategorySub').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztAssetCategorySub.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-AssetCategorySub').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});