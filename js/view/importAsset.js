/*
var itemFile = null;
jun.ImportDataProjection = new Ext.extend(Ext.Window, {
    width: 500,
    height: 90,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Import Data",
    iswin: true,
    padding: 5,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 75,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4;padding: 10px",
            id: "form-BackupRestoreWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            html: "File Type : " +
            "<select id='importType'>" +
            "<option  value='businessunit'>Business Units</option>" +
            "<option value='groupproduct'>Product Groups</option>" +
            "<option value='product'>Products</option>" +
            "<option value='outlet'>Outlets</option>" +
            "<option value='unit'>Units</option>" +
            "<option value='transaction'>Transactions</option>" +
            "<option value=''>------------------</option>" +
            "<option value='account'>Accounts</option>" +
            "<option value='accountgroup'>Account Groups</option>" +
            "<option value='accountcategory'>Account Categorys</option>" +
            "<option value='budget'>Budgets</option>" +
            "<option value='realization'>Realizations</option>" +
            "</select>" +
            "<input id='inputFile' type='file' name='uploaded'/>",
            border: !1,
            plain: !0,
            listeners: {
                afterrender: function () {
                    itemFile = document.getElementById("inputFile");
                    itemFile.addEventListener('change', EventChange, false);
                }
            }
        });
        jun.ImportDataProjection.superclass.initComponent.call(this);
    }
});
function to_json_new(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function (sheetName) {
        var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            result[sheetName] = roa;
        }
    });
    return result;
}
function EventChange(e) {
    var files = itemFile.files;
    var f = files[0];
    if (f.name.split('.').pop() != "xlsx") {
        Ext.Msg.alert('Failure', 'Need file *.xlsx');
        return;
    }
    var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" &&
        typeof FileReader.prototype.readAsBinaryString !== "undefined";
    var reader = new FileReader();
    reader.onload = function (e) {
        if (typeof console !== 'undefined') console.log("onload", new Date());
        var data = e.target.result;
        var wb = XLSX.read(data, {type: 'binary'});
        var s = wb.Strings;
        var e = document.getElementById("importType");
        var tipe = e.value;
        if (tipe == "unit") {
            if (s[0].h.indexOf("NAME") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column NAME');
                return;
            }
            if (s[1].h.indexOf("CODE") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column CODE');
                return;
            }
            if (s[2].h.indexOf("DESCRIPTION") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column DESCRIPTION');
                return;
            }
        }
        var output = to_json_new(wb);
        var url = "projection/import/unit";
        Ext.Ajax.request({
            method: 'POST',
            scope: this,
            url: url,
            params: {
                detil: Ext.encode(output),
                filename: files.name
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                var win = new Ext.Window({
                    width: 1000,
                    height: 600,
                    modal: !0,
                    border: !1,
                    layout: 'fit',
                    autoScroll: true,
                    title: "Duplicate Data",
                    iswin: true,
                    padding: 5,
                    html: response.msg
                });
                win.show();
//                Ext.Msg.alert('Successfully', response.msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
    reader.readAsBinaryString(f);
}
*/

/**
 * Created by wisnu on 21/02/2018.
 */


jun.ImportDataAsset = Ext.extend(Ext.Window, {
    title: "Import Data",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 380 + 20,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
/*        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }*/
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ImportDataAsset",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl",
                        ref: '../tgl',
                        //readOnly: !EDIT_TGL,
                        value: new Date(),
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Import',
                        hiddenName: 'importtype',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['I', 'Item'],
                                ['CL', 'Class'],
                                ['C', 'Categories'],
                                ['B', 'Branch'],
                                ['A', 'Assets'],
                                ['AM', 'Assets Migration'],
                                ['BU','Business Units']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'120',
                        //height: '150',
                        ref: '../cmbImport'
                    },
                    {
                        html: '<input type="file" name="xlfile" id="inputFile" />',
                        name: 'file',
                        xtype: "panel",
                        listeners: {
                            render: function (c) {
                                new Ext.ToolTip({
                                    target: c.getEl(),
                                    html: 'Format file : Excel (.xls)<br>Hanya Sheet pertama yang akan terbaca.<br>Pastikan sudah sesuai format pada <b>TEMPLATE</b>'
                                });
                            },
                            afterrender: function () {
                                itemFile = document.getElementById("inputFile");
                                itemFile.addEventListener('change', readFileExcelNew, false);
                            }
                        }
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Check File",
                    ref: "../btnCheck"
                },
                /*{
                    xtype: "button",
                    text: "Generate Query",
                    ref: "../btnQuery"
                },*/
                {
                    xtype: "button",
                    text: "Save",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ImportDataAsset.superclass.initComponent.call(this);
        this.btnCheck.on("click", this.onbtnCheckclick, this);
        //this.btnQuery.on("click", this.onbtnQueryclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnCheckclick: function () {
        if (jun.dataImport == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file.");
            return;
        }

        var selectedz = this.cmbImport.getValue();

        /*if(selectedz == 'I')
        {
            jun.importDataItemStore.loadData(jun.dataImport[jun.namasheetNew]);
            var win = new jun.CheckInputItemDataWin({generateQuery: 0});
            win.show(this);
        }*/
        if(selectedz == 'I')
        {
            jun.importDataItemStore.loadData(jun.dataImport[jun.namasheetNew]);
            var win = new jun.CheckInputItemDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'B')
        {
            jun.importDataBranchStore.loadData(jun.dataImport[jun.namasheetNew]);
            var win = new jun.CheckInputBranchDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'C')
        {
            jun.importDataBranchStore.loadData(jun.dataImport[jun.namasheetNew]);
            var win = new jun.CheckInputBranchDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'CL')
        {
            jun.importDataClassStore.loadData(jun.dataImport[jun.namasheetNew]);
            var win = new jun.CheckInputClassDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'A')
        {
            jun.importDataAssetStore.loadData(jun.dataImport[jun.namasheetNew]);
            var win = new jun.CheckInputAssetDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'AM')
        {
            jun.importDataAssetMigrationStore.loadData(jun.dataImport[jun.namasheetNew]);
            var win = new jun.CheckInputAssetMigrationDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'BU')
        {
            jun.importDataBUStore.loadData(jun.dataImport[jun.namasheetNew]);
            var win = new jun.CheckInputBUDataWin({generateQuery: 0});
            win.show(this);
        }

    },
    /*onbtnQueryclick: function () {
        console.log(jun.namasheetNew);
        if (jun.dataImport == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file.");
            return;
        }
        jun.importDataUnitStore.loadData(jun.dataImport[jun.namasheetNew]);
        var win = new jun.QueryStockOpnameWin({
            tgl: this.tgl.getValue(),
            store: this.store.getValue()
        });
        win.show(this);
    },*/
    onbtnSaveclick: function () { //console.log(JSON.stringify(jun.dataImportNew));
        Ext.getCmp('form-ImportDataAsset').getForm().submit({
            url: 'Asset/Import',
            timeOut: 1000,
            scope: this,
            params: {
                data: Ext.encode(jun.dataImport)
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});

jun.importDataItemStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataItemStore',
    idProperty: 'subkode',
    fields: ['subkode', 'nama', 'deskripsi', 'tipe']
});
jun.CheckInputItemDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Product / Item",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataItemStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'subkode', dataIndex: 'subkode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'nama', dataIndex: 'nama', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },

                    {
                        header: 'deskripsi', dataIndex: 'deskripsi', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'tipe', dataIndex: 'tipe', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }
                ]
            }
        ];
        jun.CheckInputItemDataWin.superclass.initComponent.call(this);
    }
});

//BRANCH
jun.importDataBranchStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataBranchStore',
    idProperty: 'kode',
    fields: ['kode', 'nama', 'alamat']
});
jun.CheckInputBranchDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Branch",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataBranchStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'kode', dataIndex: 'kode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            if (jun.rztStoreCmp.findExact('store_kode', value) != null)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    },
                    {
                        header: 'nama', dataIndex: 'nama', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },

                    {
                        header: 'alamat', dataIndex: 'alamat', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }
                ]
            }
        ];
        jun.CheckInputBranchDataWin.superclass.initComponent.call(this);
    }
});

//CATEGORY
jun.importDataCategoryStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataCategoryStore',
    idProperty: 'kode',
    fields: ['kode', 'nama', 'alamat']
});
jun.CheckInputCategoryDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Category",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataCategoryStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'kode', dataIndex: 'kode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            if (jun.rztAssetCategoryLib.findExact('category_code', value) != null)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    },
                    {
                        header: 'nama', dataIndex: 'nama', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },

                    {
                        header: 'alamat', dataIndex: 'keterangan', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }
                ]
            }
        ];
        jun.CheckInputCategoryDataWin.superclass.initComponent.call(this);
    }
});

//CLASS
jun.importDataClassStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataClassStore',
    idProperty: 'kode',
    fields: ['golongan', 'tariff', 'period', 'desc' ]
});
jun.CheckInputClassDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Class",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataClassStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'golongan', dataIndex: 'golongan', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'tariff', dataIndex: 'tariff', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'period', dataIndex: 'period', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'desc', dataIndex: 'desc', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }
                ]
            }
        ];
        jun.CheckInputClassDataWin.superclass.initComponent.call(this);
    }
});

//ASSETS
jun.importDataAssetStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataAssetStore',
    idProperty: '',
    fields: ['itemcode', 'qty', 'branch', 'date', 'price', 'class', 'category', 'desc', 'hide']
});
jun.CheckInputAssetDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Asset",
    width: 743,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataAssetStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'itemcode', dataIndex: 'itemcode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'qty', dataIndex: 'qty', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'branch', dataIndex: 'branch', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'date', dataIndex: 'date', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'price', dataIndex: 'price', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'class', dataIndex: 'class', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'category', dataIndex: 'category', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'desc', dataIndex: 'desc', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'hide', dataIndex: 'hide', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }
       /*             {
                        header: 'activaold', dataIndex: 'activaold', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }*/
                ]
            }
        ];
        jun.CheckInputAssetDataWin.superclass.initComponent.call(this);
    }
});

//ASSETS MIGRATION
jun.importDataAssetMigrationStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataAssetStore',
    idProperty: 'activaold',
    fields: ['itemcode', 'qty', 'branch', 'date', 'price', 'class', 'category', 'desc', 'hide', 'activaold']
});
jun.CheckInputAssetMigrationDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Asset Migration",
    width: 743,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataAssetMigrationStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'itemcode', dataIndex: 'itemcode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'qty', dataIndex: 'qty', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'branch', dataIndex: 'branch', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'date', dataIndex: 'date', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'price', dataIndex: 'price', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'class', dataIndex: 'class', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'category', dataIndex: 'category', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'desc', dataIndex: 'desc', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'hide', dataIndex: 'hide', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'activaold', dataIndex: 'activaold', width: 60, align: "right",
                        /*renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }*/
                        renderer: function (value, metaData, record, rowIndex) {
                            //if (jun.importDataAssetMigrationStore.findExact(ati_old, value) != null)
                            if (jun.rztAssetDetail.find('ati_old', value) != true)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    }
                ]
            }
        ];
        jun.CheckInputAssetMigrationDataWin.superclass.initComponent.call(this);
    }
});

//BUSINESS UNIT
jun.importDataBUStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataBUStore',
    idProperty: 'kode',
    fields: ['kode', 'nama', 'tipe', 'deskripsi']
});
jun.CheckInputBUDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Business Units",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataBUStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'kode', dataIndex: 'kode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            if (jun.rztBusinessunitCmp.find('businessunit_code', value) == true)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    },
                    {
                        header: 'nama', dataIndex: 'nama', width: 100, align: "left",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'tipe', dataIndex: 'tipe', width: 60, align: "left",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'deskripsi', dataIndex: 'deskripsi', width: 60, align: "left",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }

                ]
            }
        ];
        jun.CheckInputBUDataWin.superclass.initComponent.call(this);
    }
});

jun.QueryStockOpnameWin = Ext.extend(Ext.Window, {
    title: "Query Input Stock Opname",
    width: 600,
    height: 500,
    layout: "absolute",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'textarea',
                ref: "txtQuery",
                anchor: '100% 100%'
            }
        ];
        jun.QueryStockOpnameWin.superclass.initComponent.call(this);
        var query = "INSERT INTO posng.nscc_stock_moves(stock_moves_id,type_no,trans_no,tran_date,price,reference,qty,barang_id,store,tdate,id_user,up) VALUES ";
        for (i = 0; i < jun.importDataUnitStore.getCount(); i++) {
            var r = jun.importDataUnitStore.getAt(i);
            var idx = jun.rztUnit.find('code', r.get('code'));
            var rb = jun.rztUnit.getAt(idx);
            query += "\n(UUID(),-1,trans_no,'" + this.tgl.format('Y-m-d') + "',0,'SALDO AWAL'," + r.get('qty') + ",'" + rb.get('barang_id') + "','" + this.store + "',NOW(),'fff40883-0858-11e6-8e69-00ff55a5a602',0),";
        }
        this.txtQuery.setValue(query);
    }
});
