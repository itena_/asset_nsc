jun.AssetDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetDetailStoreId',
            url: 'AssetDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'asset_detail_id'},
                {name:'asset_id'},
                {name:'asset_group_id'},
                {name:'barang_id'},
                {name:'ati'},
                {name:'ati_old'},
                {name:'qty', type: 'int'},
                {name:'price_acquisition'},
                {name:'docref'},
                {name:'docref_other'},
                {name:'asset_trans_name'},
                {name:'asset_trans_branch'},
                {name:'asset_trans_price'},
                {name:'asset_trans_new_price'},
                {name:'asset_trans_date'},
                {name:'description'},
                {name:'penyusutanperbulan'},
                {name:'penyusutanpertahun'},
                {name:'class'},
                {name:'period'},
                {name:'tariff'},
                {name:'status'},
                {name:'statusdesc'},
                {name:'hide'},
                {name:'created_at'},
                {name:'updated_at'},
                {name:'category_id'},
                {name:'category_sub_id'},
                {name:'category'},
                {name:'category_sub'},
                {name:'kode_barang'},
                {name:'serialnumber'},
                {name:'location'},
                {name:'conditions'},
                {name:'merk'}
            ]
        }, cfg));
    }
});
jun.rztAssetDetail = new jun.AssetDetailstore();
jun.rztAssetDetailCmp = new jun.AssetDetailstore();

jun.rztAssetDetailHide = new jun.AssetDetailstore(
    {
        baseParams: {mode: "hide_activa", f: "cmp", hide:'1'},
        method: 'POST'
    });
