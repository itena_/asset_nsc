jun.Wilayahstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Wilayahstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'WilayahStoreId',
            url: 'Wilayah',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'wilayah_id'},
                {name: 'kode_wilayah'},
                {name: 'nama_wilayah'}
            ]
        }, cfg));
    }
});
jun.rztWilayah = new jun.Wilayahstore();
jun.rztWilayahCmp = new jun.Wilayahstore();