jun.Businessunitstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Businessunitstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BusinessunitStoreId',
            url: 'Businessunit',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'businessunit_id'},
                {name:'businessunit_code'},
                {name:'businessunit_name'},
                {name:'type'},
                {name:'description'},
                {name:'created_at'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                
            ]
        }, cfg));
    }
});
jun.rztBusinessunit = new jun.Businessunitstore();
jun.rztBusinessunitCmp = new jun.Businessunitstore();
jun.rztBusinessunit.load();
