jun.QuickSalesMenu = Ext.extend(Ext.Window, {
    id: 'form-QuickSalesMenu',
    width: 600,
    height: 450,
    layout: 'absolute',
    closable: false,
    resizable: false,
    draggable: false,
    initComponent: function () {
        if (jun.rztBusinessunit.getTotalCount() === 0) {
            jun.rztBusinessunit.load();
        }
        this.items = [
            {
                xtype: 'panel',
                html: '<img src="css/silk_v013/icons/user_green.png" />',
                bodyStyle: 'background-color: #E8E8E8; border: none;',
                height: 16,
                width: 16,
                x: 10,
                y: 8,
                frame: false
            },
            {
                xtype: 'label',
                text: this.username,
                x: 30,
                y: 10
            },
            {
                xtype: 'panel',
                id: 'logomenu',
//                html: '<img src='+'"images/naavagreen-logo.png"'+' />',
                html: SYSTEM_LOGO,
                bodyStyle: 'background-color: #E8E8E8; border: none;',
                height: 80,
                width: 340,
                x: (this.width - 340) / 2,
                y: 100,
                frame: false
            },
            {
                xtype: 'combo',
                width: 300,
                height: 200,
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                forceSelection: true,
                store: jun.rztBusinessunit,
                itemSelector: "div.search-item",
                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                    '<h3><span">{businessunit_code} - {businessunit_name}</span></h3>',
                    "</div></tpl>"),
                hiddenName:'businessunit_id',
                valueField: 'businessunit_id',
                ref:'businessunit',
                id: 'buadmin',
                displayField: 'businessunit_code',
                emptyText: "Choose Business Unit",
                x: (this.width - 300) / 2,
                y: 220 - 35,
            },
            {
                xtype: 'button',
                text: 'Go to App',
                cls: 'big-text',
                ref:'gotoapp',
                width: 100,
                // height: 30,
                hidden: this.state == 1 ? true : false,
                x: (this.width - 100) / 2,
                y: 220,

            },
            {
                xtype: 'button',
                text: 'Logout',
                width: 100,
                cls: 'big-text',
                x: (this.width - 100) / 2,
                y: 220 + 35 + 35 + 35 - (this.state == 1 ? 35 : 0),
                listeners: {
                    click: function () {
                        Ext.MessageBox.confirm('Logout', 'Are you sure want to Logout?', function (btn) {
                                if (btn == 'yes') {
                                    location.href = "Site/Logout";
                                }
                            },
                            this);
                    }
                }
            }
        ];
        jun.QuickSalesMenu.superclass.initComponent.call(this);

        this.gotoapp.on('click', this.onGotoappclick, this);
    },

    setCookie : function(cname,cvalue,exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },

    onGotoappclick:function()
    {
        var selectedzid = this.businessunit.getValue();
        var selectedzccode = Ext.getCmp('buadmin').getRawValue();

        if(selectedzid == "")
        {
            Ext.MessageBox.alert("Warning","Eiiiiits............... Pilih Bisnis Unit Dulu...");
            return;
        }


        this.setCookie('businessunitid',selectedzid);
        this.setCookie('businessunitcode',selectedzccode);

        var form = document.createElement("form");
        form.setAttribute("method", "POST");
        form.setAttribute("action", "");
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", 'app');
        hiddenField.setAttribute("value", true);

/*        var hiddenField1 = document.createElement("input1");
        hiddenField1.setAttribute("type", "hidden");
        hiddenField1.setAttribute("name", 'bu');
        hiddenField1.setAttribute("value", selectedzid);*/

        form.appendChild(hiddenField);
        //form.appendChild(hiddenField1);
        document.body.appendChild(form);
        form.submit();

        /*Ext.Ajax.request({
            url: '',
            params: {
                businessunitid: selectedzid,
                businessunitcode: selectedzccode,
            },
            method: 'POST',
            success:function (f, a) {
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });*/

    }
});
