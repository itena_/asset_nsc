jun.AssetHistoryGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"AssetHistory",
        id:'docs-jun.AssetHistoryGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
       /*                 {
			header:'asset_history_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_history_id',
			width:100
		},
                                {
			header:'asset_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_id',
			width:100
		},
                                {
			header:'businessunit_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'businessunit_id',
			width:100
		},*/
        /*                        {
			header:'Docref',
			sortable:true,
			resizable:true,                        
            dataIndex:'docref',
			width:100
		},*/
                                {
			header:'Activa',
			sortable:true,
			resizable:true,                        
            dataIndex:'ati',
			width:110
		},
                                {
			header:'Name',
			sortable:true,
			resizable:true,                        
            dataIndex:'name',
			width:100
		},
        {
            header:'Branch',
            sortable:true,
            resizable:true,
            dataIndex:'branch',
            width:40
        },
        {
            header:'Price',
            sortable:true,
            resizable:true,
            dataIndex:'price',
            width:80
        },
        /*{
            header:'Newprice',
            sortable:true,
            resizable:true,
            dataIndex:'newprice',
            width:80
        },*/
        /*{
            header:'Class',
            sortable:true,
            resizable:true,
            dataIndex:'class',
            width:30
        },
        {
            header:'Tariff',
            sortable:true,
            resizable:true,
            dataIndex:'tariff',
            width:30
        },
        {
            header:'Period',
            sortable:true,
            resizable:true,
            dataIndex:'period',
            width:30
        },*/


        {
            header:'To Bu',
            sortable:true,
            resizable:true,
            dataIndex:'tobu',
            width:50
        },
        {
            header:'To Branch',
            sortable:true,
            resizable:true,
            dataIndex:'tobranch',
            width:50
        },
        {
            header:'Amount',
            sortable:true,
            resizable:true,
            dataIndex:'amount',
            width:70
        },
        {
            header:'Description',
            sortable:true,
            resizable:true,
            dataIndex:'desc',
            width:100
        },
        {
            header:'Status Date',
            sortable:true,
            resizable:true,
            dataIndex:'sdate',
            width:100
        },
        {
            header: 'Status ',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 0 :
                        metaData.style += "background-color: #ff7373;";
                        return 'NON ACTIVE';
                    case 1 :
                        metaData.style += "background-color: #9ffb8a;";
                        return 'ACTIVE';
                    case 2 :
                        metaData.style += "background-color: #f1f26f;";
                        return 'LEND';
                    case 3 :
                        metaData.style += "background-color: #ff7373;";
                        return 'SELL';
                    case 4 :
                        metaData.style += "background-color: #f1f26f;";
                        return 'RENT';
                    case 5 :
                        metaData.style += "background-color: #ff7373;";
                        return 'BROKEN';
                    case 6 :
                        metaData.style += "background-color: #ff7373;";
                        return 'LOST';
                    case 7 :
                        metaData.style += "background-color: #f57b1c;";
                        return 'MAINTENANCE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NON ACTIVE'], [1, 'ACTIVE'], [2, 'LEND'], [3, 'SELL'], [4, 'RENT'], [5, 'BROKEN'], [6, 'LOST'], [7, 'MAINTENANCE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
                          /*{
            header:'Created Date',
            sortable:true,
            resizabletat:true,
            dataIndex:'created_at',
            width:100
        },*/
                		/*
                {
			header:'businessunit_name',
			sortable:true,
			resizable:true,                        
            dataIndex:'businessunit_name',
			width:100
		},



                                {
			header:'updated_at',
			sortable:true,
			resizable:true,                        
            dataIndex:'updated_at',
			width:100
		},
                		*/
		
	],
	initComponent: function(){
	this.store = jun.rztAssetHistory;
        this.bbar = {
            items: [
           {
               xtype: 'paging',
               store: this.store,
               displayInfo: true,
               pageSize: 20,
               ref:"../paging"
           }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Berita Acara',
                    ref: '../btnAdd'
                },
                /*{
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    ref: '../btnDelete'
                },
                {
                    xtype:'tbseparator'
                },*/
                /*{
                    xtype: 'button',
                    text: 'Import',
                    iconCls: "silk13-database_go",
                    ref: '../btnImport'
                }*/
            ]
        };

		jun.AssetHistoryGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadEditForm, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},

        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },

    loadEditForm: function(){

        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
        if(selectedz == undefined){
            Ext.MessageBox.alert("Warning","Anda belum memilih data");
            return;
        }
        var idz = selectedz.json.asset_history_id;
        var form = new jun.BeritaAcaraWin({modez:1, id:idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
})
