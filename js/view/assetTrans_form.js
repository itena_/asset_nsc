/*
jun.AssetTransWin = Ext.extend(Ext.Window, {
    title: 'Asset Transfer',
    modez:1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        if (jun.rztBusinessunitCmp.getTotalCount() === 0) {
            jun.rztBusinessunitCmp.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetTrans',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                               /!*                                                      {
                                    xtype: 'textfield',
                                    fieldLabel: 'asset_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'asset_id',
                                    id:'asset_idid',
                                    ref:'../asset_id',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, *!/
                               /!*                                      {
                                    xtype: 'textfield',
                                    fieldLabel: 'barang_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'barang_id',
                                    id:'barang_idid',
                                    ref:'../barang_id',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, *!/
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: "To Business Unit",
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztBusinessunitCmp,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{businessunit_code} - {businessunit_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'businessunit_id',
                        //name: 'store',
                        valueField: 'businessunit_id',
                        displayField: 'businessunit_name',
                        allowBlank: false,
                        hidden:true,
                        //readOnly: !HEADOFFICE,
                        ref: '../businessunit',
                        id:'businessunitstatus',
                        emptyText: "Business Unit",
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'To Branch / Store',
                        hidden:true,
                        ref: '../branch',
                        id:'branchstatus',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "Branch",
                        allowBlank: false,
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                                /!*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'bu_pengirim',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'bu_pengirim',
                                    id:'bu_pengirimid',
                                    ref:'../bu_pengirim',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, *!/
                    /!*                                    {
                       xtype: 'textfield',
                       fieldLabel: 'bu_penerima',
                       hideLabel:false,
                       //hidden:true,
                       name:'bu_penerima',
                       id:'bu_penerimaid',
                       ref:'../bu_penerima',
                       maxLength: 50,
                       //allowBlank: 1,
                       anchor: '100%'
                   }, *!/
                               /!*                                      {
                                    xtype: 'textfield',
                                    fieldLabel: 'store_pengirim',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'store_pengirim',
                                    id:'store_pengirimid',
                                    ref:'../store_pengirim',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, *!/
                                /!*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'store_penerima',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'store_penerima',
                                    id:'store_penerimaid',
                                    ref:'../store_penerima',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, *!/
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'amount',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'amount',
                                    id:'amountid',
                                    ref:'../amount',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                            /!*                                         {
                                    xtype: 'textfield',
                                    fieldLabel: 'approval',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'approval',
                                    id:'approvalid',
                                    ref:'../approval',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, *!/
                                                                     {
                            xtype: 'textarea',
                            fieldLabel: 'note',
                            hideLabel:false,
                            //hidden:true,
                            name:'note',
                            id:'noteid',
                            ref:'../note',
                            anchor: '100%'
                            //allowBlank: 1
                        }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../tdate',
                            fieldLabel: 'tdate',
                            name:'tdate',
                            id:'tdateid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Status',
                        name:'status',
                        id:'statusasset',
                        ref:'../statusasset',
                        hiddenName: 'statusasset',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['0', 'NON ACTIVE'],
                                ['1', 'ACTIVE'],
                                ['2', 'LEND'],
                                ['3', 'SELL'],
                                ['4', 'RENT'],
                                ['5', 'BROKEN'],
                                ['6', 'LOST'],
                                ['7', 'MAINTENANCE'],
                                ['8', 'RELOCATION']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "Status",
                        mode : 'local',
                        anchor: "100%"
                    },
                         /!*                                            {
                                    xtype: 'textfield',
                                    fieldLabel: 'visible',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'visible',
                                    id:'visibleid',
                                    ref:'../visible',
                                    maxLength: 4,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                },*!/

                    new jun.AssetTransDetailGrid({
                       /!* x: 5,
                        y: 95,*!/
                        anchor: '100% 100%',
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        readOnly: ((this.modez < 2)?false:true)
                    })
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetTransWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'AssetTrans/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'AssetTrans/create/';
                }
             
            Ext.getCmp('form-AssetTrans').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztAssetTrans.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-AssetTrans').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});
*/

jun.AssetTransWin = Ext.extend(Ext.Window, {
    title: 'Asset Transfer',
    modez:1,
    width: 850,
    height: 524,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        if (jun.rztBusinessunitCmp.getTotalCount() === 0) {
            jun.rztBusinessunitCmp.load();
        }
        if (jun.rztAssetDetail.getTotalCount() === 0) {
            jun.rztAssetDetail.load();
        }
        /*if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }*/
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetTrans',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    /*{
                        xtype: 'textfield',
                        hideLabel: false,
                        hidden: true,
                        fieldLabel: 'Id',
                        name: 'asset_detail_id',
                        id: 'asset_detail_id',
                        ref: '../asset_detail_id',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'No. Activa',
                        name: 'ati',
                        id: 'atiid',
                        ref: '../ati',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'DocRef',
                        name: 'docref_other',
                        id: 'docref_otherid',
                        ref: '../docref_other',
                        width: 175,
                        hidden:true,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'Asset Name',
                        name: 'asset_trans_name',
                        id: 'asset_trans_nameid',
                        ref: '../asset_trans_name',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Asset Price',
                        name: 'asset_trans_price',
                        id: 'asset_trans_priceid',
                        ref: '../asset_trans_price',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        hidden:false,
                        fieldLabel: 'Book Value',
                        name: 'assetvalue',
                        id: 'assetvalue',
                        ref: '../assetvalue',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'

                    },*/
                    /*{
                        xtype: 'textfield',
                        fieldLabel: 'Asset',
                        hideLabel:false,
                        //hidden:true,
                        name:'nama_barang',
                        id:'nama_barangid',
                        ref:'../nama_barang',
                        maxLength: 500,
                        //allowBlank: 1,
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'xdatefield',
                        ref:'../tdate',
                        fieldLabel: 'Date',
                        name:'tdate',
                        id:'tdateid',
                        format: 'd M Y',
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Status',
                        name:'status',
                        id:'statusasset',
                        ref:'../statusasset',
                        hiddenName: 'statusasset',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['2', 'LEND'],
                                ['3', 'SELL'],
                                ['4', 'RENT'],
                                ['8', 'RELOCATION']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "Status",
                        mode : 'local',
                        anchor: "100%"
                    },
                    /*{
                        xtype: 'numericfield',
                        hideLabel: false,
                        hidden:false,
                        fieldLabel: 'Amount',
                        name: 'amountstatus',
                        id: 'amountstatus',
                        ref: '../amountstatus',
                        width: 175,
                        readOnly: false,
                        anchor: '100%',
                        listeners: {
                            ppn : 'ppnstatuschange'
                        },

                    },*/
                   /* {
                        xtype: 'numericfield',
                        hideLabel: false,
                        hidden:true,
                        fieldLabel: 'PPN',
                        name: 'ppnstatus',
                        id: 'ppnstatus',
                        ref: '../ppnstatus',
                        width: 175,
                        readOnly: false,
                        anchor: '100%',
                        /!*listeners: {
                            ppn : 'ppnstatuschange'
                            /!*ppnstatus: function () {
                                //field.setValue(newValue.toUpperCase());
                                this.ppnstatus.setValue(newValue);
                            }*!/
                        },*!/
                    },*/
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: "To",
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztBusinessunitCmp,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{businessunit_code} - {businessunit_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'businessunit_id',
                        //name: 'store',
                        valueField: 'businessunit_id',
                        displayField: 'businessunit_name',
                        allowBlank: false,
                        hidden:false,
                        //readOnly: !HEADOFFICE,
                        ref: '../businessunit',
                        id:'businessunitstatus',
                        emptyText: "Business Unit",
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        hidden:false,
                        ref: '../branch',
                        id:'branchstatus',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "Branch",
                        allowBlank: false,
                        //value: STORE,
                        //emptyText: "Asset Name",
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Note',
                        hideLabel:false,
                        //hidden:true,
                        name:'ket',
                        id:'ketid',
                        ref:'../ket',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    new jun.AssetTransDetailGrid({
                        /* x: 5,
                         y: 95,*/
                        //anchor: '100% 100%',
                        height: 260,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        readOnly: ((this.modez < 2)?false:true)
                    }),

                   /* new jun.AssetHistoryGrid({
                        height: 405 - 30,
                        frameHeader: !1,
                        header: !1,
                        ref: "../gridDetail",
                        x: 5,
                        y: 65 + 30
                    }),*/

                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetTransWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.businessunit.on('Select', this.businessunitonselect, this);
        this.statusasset.on('Select', this.statusassetonselect, this);
        //this.amountstatus.on('change', this.ppnstatuschange, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    ppnstatuschange: function(c,r,i)
    {
        var amountstatus = this.amountstatus.getValue();
        var ppn = amountstatus * PPNASSET/100;
        this.ppnstatus.setValue(ppn);
    },

    statusassetonselect: function(c,r,i)
    {
        var status = Ext.getCmp('statusasset').getValue();

        if(status == '8')
        {
            this.businessunit.setVisible(true);
            this.branch.setVisible(true);
            this.amountstatus.setVisible(false);
            this.ppnstatus.setVisible(false);
            this.businessunit.allowBlank = false;
            this.branch.allowBlank = false;
            this.ppnstatus.allowBlank = true;
            this.amountstatus.allowBlank = true;
        }
        else {
            this.businessunit.setVisible(true);
            this.ppnstatus.setVisible(true);
            this.amountstatus.setVisible(true);
            this.branch.setVisible(true);
            this.businessunit.allowBlank = true;
            this.branch.allowBlank = true;
            this.amountstatus.allowBlank = true;
            this.ppnstatus.allowBlank = true;
        }


    },

    businessunitonselect: function(c,r,i)
    {
        var tobu = Ext.getCmp('businessunitstatus').getRawValue();

        if(tobu == 'OTHER')
        {
            this.branch.setVisible(false);
            this.branch.allowBlank = true;
            this.branch.allowBlank = true;
        }
        else {
            var buid =r.get('businessunit_id');
            this.branch.store.reload({params:{businessunit_id:buid}});
            this.branch.setVisible(true);
            this.branch.allowBlank = false;
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        this.btnDisabled(true);
        //var urlz= 'asset/AssetDetail/status/id/';
        //var urlz= 'asset/AssetTrans/create/id/';
        var urlz= 'asset/AssetTrans/create/';


        Ext.getCmp('form-AssetTrans').getForm().submit({
            url:urlz,
            timeOut: 1000,
            method: 'POST',
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztAssetTransDetail.data.items, "data")),
                id: this.id,
                mode: this.modez
            },

            success: function(f,a){
                jun.rztAssetTransDetail.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                if(this.modez == 0){
                    Ext.getCmp('form-AssetTrans').getForm().reset();
                    this.btnDisabled(false);
                }
                if(this.closeForm){
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }

});
