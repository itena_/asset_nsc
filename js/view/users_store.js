jun.Usersstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (a) {
        a = a || {};
        jun.Usersstore.superclass.constructor.call(this, Ext.apply({
            storeId: "UsersStoreId",
            url: "Users",
            root: "results",
            totalProperty: "total",
            fields: [
                {name: "id"},
                {name: "user_id"},
                {name: "name"},
                {name: "password"},
                {name: "last_visit_date"},
                {name: "active"},
                {name: "security_roles_id"},
                {name: 'store'},
                {name: 'businessunit_code'},
                {name: 'businessunit_id'},
                {name: 'role'}
            ]
        }, a))
    }
});
jun.rztUsers = new jun.Usersstore;