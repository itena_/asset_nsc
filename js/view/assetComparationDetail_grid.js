jun.AssetComparationDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    stripeRows      : true,
    enableKeyEvents : true,
    viewConfig: {
        forceFit: true
    },
    id: 'docs-jun.AssetComparationDetailGrid',
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Aktiva',
            sortable: true,
            dataIndex: 'ati',
            width: 25,
            renderer: function (value,r,m) {
                return m.data.status === 'different' ? '<span style="color:red;">'+value+'</span>' : value;
            }
        },
        {
            header: 'Description',
            sortable: true,
            dataIndex: 'description',
            width: 65,
            renderer: function (value,r,m) {
                return m.data.status === 'different' ? '<span style="color:red;">'+value+'</span>' : value;
            }
        },
        {
            header: 'Qty',
            sortable: true,
            dataIndex: 'qty',
            width: 10,
            renderer: function (value,r,m) {
                return m.data.status === 'different' ? '<span style="color:red;">'+value+'</span>' : value;
            }
        }
    ],
    initComponent: function () {
        this.store = jun.rztAssetComparationDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 5,
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'No. Aktiva'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../barcode',
                            enableKeyEvents: true,
                            width: 5
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            allowBlank: false,
                            ref: '../../ati',
                            triggerAction: 'all',
                            lazyRender: true,
                            forceSelection: true,
                            mode: 'local',
                            enableKeyEvents: true,
                            store: jun.rztAssetDetailCmp,
                            hiddenName: 'ati',
                            valueField: 'ati',
                            displayField: 'ati',
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../qty',
                            width: 30
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: this.id + 'Button',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Edit',
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            width: 40,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.AssetComparationDetailGrid.superclass.initComponent.call(this);
        this.ati.on('select', this.loadForm, this);
        this.barcode.on('keyup', this.barcodeOnEnter, this);
        this.btnEdit.on('Click', this.onAddSaveClick, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    btnDisable: function (s) {
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    barcodeOnEnter: function (t, e) {
        if (e.getKey() == 13) {
            this.ati.setValue(t.getValue());
            this.atiOnScanned();
            t.reset();
        }
    },
    onAddSaveClick: function (b, e) {
        if (b.getText() === 'Edit') {
            this.onClickbtnEdit();
        } else {
            if (b.getText() === 'Save') {
                this.onClickbtnEdit();
            } else {
                this.loadForm();
            }
        }
    },
    onClickbtnEdit: function () {
        var record = this.sm.getSelected();

        if (record === undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }

        if (this.btnEdit.text === 'Edit') {
            this.barcode.setDisabled(true);
            this.ati.setDisabled(true);

            this.barcode.setValue('');
            this.ati.setValue(record.data.ati);
            this.qty.setValue(record.data.qty);
            this.btnEdit.setText("Save");
            this.btnDisable(true);
        } else {
            this.barcode.setDisabled(false);
            this.ati.setDisabled(false);

            this.loadForm();
        }
    },
    atiOnScanned: function () {
        var atiValue = this.ati.getRawValue().trim();
        var index = this.ati.store.findExact('ati', atiValue);

        if (index !== -1) {
            var rec = this.ati.store.getAt(index);
            this.loadForm(this.ati, rec);
        } else {
            Ext.MessageBox.alert("Warning", "Scanned item not found in store!");
        }
        this.barcode.focus();
        this.ati.reset();
    },
    loadForm: function (col, rec) {
        var c = this.store.recordType;
        var atiValue = this.ati.getValue();
        var index = this.store.find('ati', atiValue); // Find index of existing record

        var description, qty = '';
        if(rec) {
            description = rec.get('description');
            qty = rec.get('qty');
        } else {
            description = this.store.getAt(index).data.description;
            qty = this.qty.getValue();
        }

        if (index !== -1) {
            var existingRecord = this.store.getAt(index);
            existingRecord.set('description', description);
            existingRecord.set('qty', qty);
        } else {
            var d = new c({
                ati: atiValue,
                qty: qty,
                description: description,
                status: 'match'
            });
            this.store.add(d);
        }
        this.barcode.focus();
        this.ati.reset();
        this.qty.reset();
        this.btnEdit.setText('Edit');
        this.btnDisable(false);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        this.store.remove(record);
    }
});

jun.AssetExistingDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    stripeRows      : true,
    enableKeyEvents : true,
    viewConfig: {
        forceFit: true
    },
    id: 'docs-jun.AssetExistingDetailGrid',
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Aktiva',
            sortable: true,
            dataIndex: 'ati',
            width: 25
        },
        {
            header: 'Description',
            sortable: true,
            dataIndex: 'description',
            width: 65
        },
        {
            header: 'Qty',
            sortable: true,
            dataIndex: 'qty',
            width: 10
        },
    ],
    initComponent: function () {
        this.store = jun.rztAssetDetailCmp;
        jun.rztAssetComparationDetailExisting = this.store;
        jun.AssetComparationDetailGrid.superclass.initComponent.call(this);
    },
});
