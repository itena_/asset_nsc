function to_json_new(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function (sheetName) {
        var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            result[sheetName] = roa;
        }
    });
    return result;
}

jun.namasheetNew = '';
function readFileExcelNew(e) {
    dataImportNew = "";
    if (itemFile.files.length == 0) return;
    var files = itemFile.files;
    var f = files[0];
    {
        var reader = new FileReader();
        var name = f.name;
        reader.onload = function (e) {
            var data = e.target.result;
            var wb;
            var arr = fixdataNew(data);
            wb = XLS.read(btoa(arr), {type: 'base64'});
            jun.dataStockOpname = to_json_new(wb);
            jun.namasheetNew = wb.SheetNames[0];
        };
        reader.readAsArrayBuffer(f);
    }
}
function fixdataNew(data) {
    var o = "", l = 0, w = 10240;
    for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
    o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
    return o;
}
jun.ImportData = Ext.extend(Ext.Window, {
    title: "Import Additional",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 100,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                id: "form-ImportData",
                border: !1
            },
            {
                html: '<input type="file" name="xlfile" id="inputFile" />',
                name: 'file',
                xtype: "panel",
                ref: "doc",
                listeners: {
                    render: function (c) {
                        new Ext.ToolTip({
                            target: c.getEl(),
                            html: 'Format file : Excel (.xls)'
                        });
                    },
                    afterrender: function () {
                        itemFile = document.getElementById("inputFile");
                        itemFile.addEventListener('change', readFileExcelNew, false);
                    }
                }
            },
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-DownloadFormatTimeSheet",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'absolute',
                ref: "formz",
                border: !1,
                hidden: true,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Hapus semua data dan Import",
                    ref: "../btnDelImport",
                    style: {
                        marginRight: '130px'
                    }
                },
                {
                    xtype: "button",
                    text: "Import saja",
                    ref: "../btnImport"
                },
                {
                    xtype: "hidden",
                    ref: "../del"
                }
            ]
        };
        jun.ImportData.superclass.initComponent.call(this);
        this.btnImport.on("click", this.onbtnImportclick, this);
        this.btnDelImport.on("click", this.onbtnDelImportclick, this);
    },
    onbtnDelImportclick: function () {
        this.del.setValue(1);
        Ext.MessageBox.confirm('Questions', 'Data yang sudah tersimpan pada periode ini akan terhapus, apa anda yakin?', this.importForm, this);
    },
    onbtnImportclick: function () {
        this.del.setValue(0);
        this.importForm(1);
    },
    importForm: function (btn) {
        if (btn == 'no') {
            return;
        }
        var encode = Ext.encode(jun.dataStockOpname[jun.namasheetNew]);
        if (jun.namasheetNew == '') {
            Ext.MessageBox.alert("Warning", "Pilih file terlebih dahulu.");
            return;
        }
        var urlz = 'Asset/Import/';
        this.btnImport.setDisabled(true);
        Ext.getCmp('form-ImportData').getForm().submit({
            url: urlz,
            timeout: 30000,
            scope: this,
            params: {
                del: this.del.getValue(),
                detil: encode
            },
            success: function (f, a) {
                Ext.Ajax.timeout = 1800000;
                var response = Ext.decode(a.response.responseText);
                Ext.Ajax.request({
                    url: 'Asset/ImportData/',
                    method: 'POST',
                    // timeout: 30000,
                    scope: this,
                    success: function (f, a) {
                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
                var msg = response.msg;
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.btnImport.setDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnImport.setDisabled(false);
            }
        });
    }
});