jun.AssetRequestCodeWin = Ext.extend(Ext.Window, {
    title: 'Asset Request Code',
    modez:1,
    width: 400,
    height: 236,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetRequestCode',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Item Code',
                        hideLabel:false,
                        //hidden:true,
                        name:'code',
                        id:'codeid',
                        ref:'../code',
                        maxLength: 50,
                        readOnly:true,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                                    xtype: 'textfield',
                                    fieldLabel: 'Item Name',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'name',
                                    id:'nameid',
                                    ref:'../name',
                                    maxLength: 255,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                /*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'suggest',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'suggest',
                                    id:'suggestid',
                                    ref:'../suggest',
                                    maxLength: 255,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, */
                                                                     {
                                    xtype: 'textarea',
                                    fieldLabel: 'Description',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'desc',
                                    id:'descid',
                                    ref:'../desc',
                                    maxLength: 255,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Type',
                        name:'tipe',
                        id:'tipeid',
                        ref:'../tipe',
                        hiddenName: 'tipe',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['0', 'NON ATI'],
                                ['1', 'ATI']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "Choose Type",
                        mode : 'local',
                        anchor: "100%"
                    },

                                /*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'timestamps',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'timestamps',
                                    id:'timestampsid',
                                    ref:'../timestamps',
                                    maxLength: ,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, */
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'SET CODE',
                    hidden: false,
                    ref:'../btnSet'
                },
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetRequestCodeWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnSet.on('click', this.setCode, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSet.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSet.setVisible(false);
        }
        if (this.modez == 3 ) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.btnSet.setVisible(true);
            //this.code.readOnly
            Ext.getCmp('codeid').setReadOnly(false);

        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    setCode : function()
    {
        this.btnDisabled(true);

            urlz= 'asset/AssetRequestCode/SetCode/id/' + this.id;


        Ext.getCmp('form-AssetRequestCode').getForm().submit({
            url:urlz,
            timeOut: 1000,
            scope: this,
            success: function(f,a){
                jun.rztAssetRequestCode.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                if(this.modez == 0){
                    Ext.getCmp('form-AssetRequestCode').getForm().reset();
                    this.btnDisabled(false);
                }
                if(this.closeForm){
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'asset/AssetRequestCode/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'asset/AssetRequestCode/create/';
                }
             
            Ext.getCmp('form-AssetRequestCode').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztAssetRequestCode.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-AssetRequestCode').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});