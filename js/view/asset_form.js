jun.AssetWin = Ext.extend(Ext.Window, {
    title: 'Asset Acquisition',
    modez: 1,
    width: 823,
    height: 350,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                width: "100%",
                id: 'form-Asset',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'column',
                ref: 'formz',
                defaults: {
                    frame: false,
                    border: false
                },
                items: [
                    {
                        xtype: 'container',
                        layout: 'form',
                        columnWidth: .50,
                        items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Doc Ref',
                                name: 'doc_ref',
                                ref: '../doc_ref',
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'xdatefield',
                                ref: '../date_acquisition',
                                fieldLabel: 'Date',
                                name: 'date_acquisition',
                                format: 'd M Y',
                                value: DATE_NOW,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                hidden: true,
                                allowBlank: false,
                                fieldLabel: 'Category',
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                store: jun.rztAssetCategoryLib,
                                hiddenName: 'category_id',
                                valueField: 'category_id',
                                displayField: 'category_name',
                                ref: '../../category_id',
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                allowBlank: false,
                                fieldLabel: 'Sub Category',
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                store: jun.rztAssetCategorySubLib,
                                hiddenName: 'category_sub_id',
                                valueField: 'category_sub_id',
                                displayField: 'sub_name',
                                ref: '../../category_sub_id',
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                readOnly: true,
                                allowBlank: false,
                                fieldLabel: 'Golongan',
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                store: jun.rztAssetGroupLib,
                                hiddenName: 'asset_group_id',
                                valueField: 'asset_group_id',
                                displayField: 'desc',
                                ref: '../../asset_group_id',
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Document Num.',
                                name: 'docnumber',
                                ref: '../docnumber',
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                allowBlank: false,
                                fieldLabel: 'Branch/Store',
                                ref: '../branch',
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                store: jun.rztStoreCmp,
                                emptyText: 'All',
                                hiddenName: 'store_id',
                                valueField: 'store_id',
                                displayField: 'nama_store',
                                anchor: '100%'
                            },
                            {
                                xtype: 'textarea',
                                fieldLabel: 'Description',
                                height: 50,
                                hideLabel: false,
                                enableKeyEvents: true,
                                style: {textTransform: "uppercase"},
                                listeners: {
                                    change: function (field, newValue, oldValue) {
                                        field.setValue(newValue.toUpperCase());
                                    }
                                },
                                name: 'description',
                                ref: '../description',
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                fieldLabel: 'Condition',
                                name: 'conditions',
                                ref: '../conditions',
                                hiddenName: 'conditions',
                                store: new Ext.data.SimpleStore({
                                    data: [
                                        ['0', 'NEW'],
                                        ['1', 'SECOND'],
                                        ['2', 'BROKEN'],
                                    ],
                                    id: 0,
                                    fields: ['value', 'text']
                                }),
                                value: '0',
                                valueField: 'value',
                                displayField: 'text',
                                triggerAction: 'all',
                                editable: false,
                                allowBlank: false,
                                emptyText: "Choose Condition",
                                mode: 'local',
                                anchor: "100%"
                            },
                            {
                                xtype: 'combo',
                                fieldLabel: 'Active',
                                name:'active',
                                ref:'../activeasset',
                                hiddenName: 'activeasset',
                                store: new Ext.data.SimpleStore({
                                    data: [
                                        ['1', 'ACTIVE'],
                                        ['0', 'NON ACTIVE']
                                    ],
                                    id: 0,
                                    fields: ['value', 'text']
                                }),
                                value: 1,
                                valueField: 'value',
                                displayField: 'text',
                                triggerAction: 'all',
                                editable: false,
                                allowBlank: false,
                                mode : 'local',
                                anchor: "100%"
                            },
                        ]
                    },
                    {
                        xtype: 'panel',
                        layout: 'form',
                        columnWidth: .50,
                        padding: '0 0 0 10px',
                        bodyStyle: 'background-color: #E4E4E4',
                        items: [
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Qty',
                                name: 'qty',
                                ref: '../../qty',
                                value: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Price',
                                name: 'price_acquisition',
                                ref: '../../price_acquisition',
                                value: 0,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'New Price',
                                name: 'new_price_acquisition',
                                ref: '../new_price_acquisition',
                                value: 0,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                fieldLabel: 'PPN (%)',
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                store: [
                                    [10, '10%'],
                                    [11, '11%'],
                                    [12, '12%'],
                                ],
                                hiddenName: 'ppnpersen',
                                valueField: 'ppnpersen',
                                ref: '../../ppnpersen',
                                value: PPNASSET,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'PPN',
                                name: 'ppnasset',
                                ref: '../../ppnasset',
                                allowBlank: false,
                                readOnly: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Total Price',
                                name: 'totalprice',
                                ref: '../../totalprice',
                                allowBlank: false,
                                readOnly: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                fieldLabel: 'Asset User',
                                triggerAction: 'all',
                                lazyRender: true,
                                hidden: true,
                                mode: 'local',
                                store: jun.rztAssetUserLib,
                                hiddenName: 'asset_user',
                                valueField: 'name',
                                displayField: 'name',
                                anchor: '100%'
                            },
                            /*{
                                xtype: 'combo',
                                allowBlank: false,
                                fieldLabel: 'Location',
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                store: jun.rztLocationLib,
                                hiddenName: 'location',
                                valueField: 'name',
                                displayField: 'name',
                                anchor: '100%'
                            },*/
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Location',
                                name: 'location',
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'User',
                                name: 'asset_user',
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Note',
                                name: 'note',
                                anchor: '100%'
                            },
                            {
                                xtype: 'xdatefield',
                                ref: '../expdate',
                                fieldLabel: 'Expired Date',
                                name: 'expdate',
                                format: 'd M Y',
                                anchor: '100%'
                            },
                            {
                                xtype: 'checkbox',
                                fieldLabel: 'Hide on Branch',
                                value: 1,
                                inputValue: 1,
                                uncheckedValue: 0,
                                hidden: true,
                                name: 'hide'
                            }
                        ]
                    }
                ]
            },
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: saveCloseCancel
        };
        jun.AssetWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.category_sub_id.on('select', this.onSubCatSelect, this);
        this.price_acquisition.on('change', this.ppnassetchange, this);
        this.ppnpersen.on('select', this.ppnassetchange, this);
        this.qty.on('change', this.ppnassetchange, this);

        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    ppnassetchange: function () {
        var price = this.price_acquisition.getValue() * this.qty.getValue();
        var ppn = price * this.ppnpersen.getValue() / 100;
        this.ppnasset.setValue(ppn);

        this.totalprice.setValue(this.qty.getValue() * this.price_acquisition.getValue() + ppn);
    },
    onSubCatSelect: function (c, r, i) {
        this.category_id.setValue(r.json['category_id']);
        this.asset_group_id.setValue(r.json['asset_group_id']);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Asset/Create/';
        Ext.getCmp('form-Asset').getForm().submit({
            url: urlz,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztAssetDetail.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztAsset.reload();
                jun.rztAssetCmp.reload();
                jun.rztAssetDetail.reload();
                jun.rztAssetDetailCmp.reload();

                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Asset').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});