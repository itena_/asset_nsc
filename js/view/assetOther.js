jun.AssetDepriciationWin = Ext.extend(Ext.Window, {
    title: 'Asset Depriciation',
    modez: 1,
    width: 945,
    height: 595,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    iswin: true,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-AssetDetail',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. Activa:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'ati',
                        id: 'atiid',
                        ref: '../ati',
                        readOnly: true,
                        width: 175 + 60,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Docref Asset:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'doc_ref',
                        id: 'doc_ref',
                        ref: '../doc_ref',
                        readOnly: true,
                        width: 175 + 60,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Price:",
                        x: 290 + 60,
                        y: 35
                    },

                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        name: 'price_acquisition',
                        id: 'price_acquisition',
                        ref: '../price_acquisition',
                        readOnly: true,
                        width: 175 - 30,
                        x: 370 + 30,
                        y: 32
                    },

                    {
                        xtype: "label",
                        text: "Date:",
                        x: 290 + 60,
                        y: 5
                    },
                    {
                        xtype: "xdatefield",
                        ref: "../date_acquisition",
                        name: "date_acquisition",
                        id: "date_acquisition",
                        readOnly: true,
                        allowBlank: true,
                        format: "d M Y",
                        width: 175 - 30,
                        x: 370 + 30,
                        y: 2,
                    },
                    {
                        xtype: "label",
                        text: "Branch:",
                        x: 615,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'branch',
                        id: 'branch',
                        ref: '../branch',
                        readOnly: true,
                        maxLength: 20,
                        width: 175,
                        x: 715,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Description:",
                        x: 615,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        name: 'description',
                        id: 'descriptionid',
                        ref: '../description',
                        width: 175,
                        height: 50,
                        x: 715,
                        y: 32
                    },
                    new jun.AssetPeriodeGrid({
                        height: 405 - 30,
                        frameHeader: !1,
                        header: !1,
                        focusRow: 3,
                        x: 5,
                        y: 65 + 30
                    }),
                    {
                        xtype: "label",
                        text: "Accumulation (Now):",
                        x: 590,
                        y: 482
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        ref: "../accumulationdep",
                        name: "accumulationdep",
                        id: "accumulationdep",
                        value: 0,
                        readOnly: true,
                        width: 175,
                        x: 715,
                        y: 480
                    },
                    {
                        xtype: "label",
                        text: "Nilai Buku (Now):",
                        x: 290,
                        y: 482
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        ref: "../nilaibukudep",
                        id: "nilaibukudep",
                        name: "nilaibukudep",
                        value: 0,
                        readOnly: true,
                        width: 175,
                        x: 390,
                        y: 480
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                /*{
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },*/
                {
                    xtype: 'button',
                    text: 'Close',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetDepriciationWin.superclass.initComponent.call(this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        //this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        this.btnDisabled(true);
        var urlz;
        if(this.modez == 1 || this.modez== 2) {

            urlz= 'asset/AssetDetail/update/id/' + this.id;

        } else {

            urlz= 'asset/AssetDetail/create/';
        }

        Ext.getCmp('form-AssetDetail').getForm().submit({
            url:urlz,
            timeOut: 1000,
            scope: this,
            success: function(f,a){
                jun.rztAssetDetail.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                if(this.modez == 0){
                    Ext.getCmp('form-AssetDetail').getForm().reset();
                    this.btnDisabled(false);
                }
                if(this.closeForm){
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    /*onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },*/

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }

});
jun.AssetStatusConditionWin = Ext.extend(Ext.Window, {
    title: 'Asset Status',
    modez:1,
    width: 400,
    height: 263,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        /*if (jun.rztBusinessunitCmp.getTotalCount() === 0) {
            jun.rztBusinessunitCmp.load();
        }*/
        /*if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }*/
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetStatusCondition',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        hidden: true,
                        fieldLabel: 'Id',
                        name: 'asset_detail_id',
                        id: 'asset_detail_id',
                        ref: '../asset_detail_id',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'No. Activa',
                        name: 'ati',
                        id: 'atiid',
                        ref: '../ati',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'DocRef',
                        name: 'docref_other',
                        id: 'docref_otherid',
                        ref: '../docref_other',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'Asset Name',
                        name: 'asset_trans_name',
                        id: 'asset_trans_nameid',
                        ref: '../asset_trans_name',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    /*{
                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Asset Price',
                        name: 'asset_trans_price',
                        id: 'asset_trans_priceid',
                        ref: '../asset_trans_price',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    */
                    /*{
                        xtype: 'textfield',
                        fieldLabel: 'Asset',
                        hideLabel:false,
                        //hidden:true,
                        name:'nama_barang',
                        id:'nama_barangid',
                        ref:'../nama_barang',
                        maxLength: 500,
                        //allowBlank: 1,
                        anchor: '100%'
                    },*/
                    /*{
                        xtype: 'combo',
                        fieldLabel: 'Condition',
                        name:'conditions',
                        id:'conditionid',
                        ref:'../conditions',
                        hiddenName: 'conditions',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['0', 'NEW'],
                                ['1', 'SECOND'],
                                ['2', 'BROKEN'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "Choose Condition",
                        mode : 'local',
                        anchor: "100%"
                    },*/
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        hidden:true,
                        fieldLabel: 'Book Value',
                        name: 'assetvalue',
                        id: 'assetvalue',
                        ref: '../assetvalue',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'

                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Status',
                        name:'status',
                        id:'statusasset',
                        ref:'../statusasset',
                        hiddenName: 'statusasset',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['1', 'ACTIVE'],
                                ['5', 'BROKEN'],
                                ['6', 'LOST'],
                                ['7', 'MAINTENANCE']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "Status",
                        mode : 'local',
                        anchor: "100%"
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        hidden:true,
                        fieldLabel: 'Amount',
                        name: 'amountstatus',
                        id: 'amountstatus',
                        ref: '../amountstatus',
                        width: 175,
                        readOnly: false,
                        anchor: '100%',
                        listeners: {
                            ppn : 'ppnstatuschange'
                        },

                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        hidden:true,
                        fieldLabel: 'PPN',
                        name: 'ppnstatus',
                        id: 'ppnstatus',
                        ref: '../ppnstatus',
                        width: 175,
                        readOnly: false,
                        anchor: '100%',
                        /*listeners: {
                            ppn : 'ppnstatuschange'
                            /!*ppnstatus: function () {
                                //field.setValue(newValue.toUpperCase());
                                this.ppnstatus.setValue(newValue);
                            }*!/
                        },*/
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: "To",
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztBusinessunitCmp,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{businessunit_code} - {businessunit_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'businessunit_id',
                        //name: 'store',
                        valueField: 'businessunit_id',
                        displayField: 'businessunit_name',
                        allowBlank: false,
                        hidden:true,
                        //readOnly: !HEADOFFICE,
                        ref: '../businessunit',
                        id:'businessunitstatus',
                        emptyText: "Business Unit",
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        hidden:true,
                        ref: '../branch',
                        id:'branchstatus',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "Branch",
                        allowBlank: false,
                        //value: STORE,
                        //emptyText: "Asset Name",
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Note',
                        hideLabel:false,
                        //hidden:true,
                        name:'ket',
                        id:'ketid',
                        ref:'../ket',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetStatusConditionWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.businessunit.on('Select', this.businessunitonselect, this);
        this.statusasset.on('Select', this.statusassetonselect, this);
        this.amountstatus.on('change', this.ppnstatuschange, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    ppnstatuschange: function(c,r,i)
    {
        var amountstatus = this.amountstatus.getValue();
        var ppn = amountstatus * PPNASSET/100;
        this.ppnstatus.setValue(ppn);
    },

    statusassetonselect: function(c,r,i)
    {
        var status = Ext.getCmp('statusasset').getValue();

        if(status == '2' || status == '4')
        {
            this.businessunit.setVisible(true);
            this.branch.setVisible(true);
            this.amountstatus.setVisible(true);

            this.businessunit.allowBlank = false;
            this.branch.allowBlank = false;
            this.amountstatus.allowBlank = false;
        }
        else if(status == '3')
        {
            this.businessunit.setVisible(true);
            this.branch.setVisible(true);
            this.amountstatus.setVisible(true);
            this.ppnstatus.setVisible(true);

            this.businessunit.allowBlank = false;
            this.branch.allowBlank = false;
            this.amountstatus.allowBlank = false;
            this.ppnstatus.allowBlank = false;
        }
        else {
            this.businessunit.setVisible(false);
            this.ppnstatus.setVisible(false);
            this.amountstatus.setVisible(false);
            this.branch.setVisible(false);
            this.businessunit.allowBlank = true;
            this.branch.allowBlank = true;
            this.amountstatus.allowBlank = true;
            this.ppnstatus.allowBlank = true;
        }


    },

    businessunitonselect: function(c,r,i)
    {
        var tobu = Ext.getCmp('businessunitstatus').getRawValue();

        if(tobu == 'OTHER')
        {
            this.branch.setVisible(false);
            this.branch.allowBlank = true;
            this.branch.allowBlank = true;
        }
        else {
            var buid =r.get('businessunit_id');
            this.branch.store.reload({params:{businessunit_id:buid}});
            this.branch.setVisible(true);
            this.branch.allowBlank = false;
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        this.btnDisabled(true);
        var urlz= 'asset/AssetDetail/status/';


        Ext.getCmp('form-AssetStatusCondition').getForm().submit({
            url:urlz,
            timeOut: 1000,
            method: 'POST',
            scope: this,

            success: function(f,a){
                jun.rztAssetDetailHide.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                if(this.modez == 0){
                    Ext.getCmp('form-AssetStatusCondition').getForm().reset();
                    this.btnDisabled(false);
                }
                if(this.closeForm){
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }

});
jun.ShowAssetDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Show Assets",
    id: 'docs-jun.AssetGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Activa',
            sortable: true,
            resizable: true,
            dataIndex: 'ati',
            width: 100,
            filter: {xtype: "textfield"}
        },
        /*{
            header: 'Asset Name',
            sortable: true,
            resizable: true,
            dataIndex: 'asset_name',
            width: 100,
            filter: {xtype: "textfield"}
        },*/
        {
            header: 'Asset Code',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang',
            width: 50,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Merk',
            sortable: true,
            resizable: true,
            dataIndex: 'merk',
            width: 50,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Category',
            sortable: true,
            resizable: true,
            dataIndex: 'category',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'asset_trans_branch',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Description',
            sortable: true,
            resizable: true,
            dataIndex: 'description',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'asset_trans_date',
            width: 100,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },
        {
            header: 'Status ',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 60,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case 0 :
                        metaData.style += "background-color: #ff7373;";
                        return 'NON ACTIVE';
                    case 1 :
                        metaData.style += "background-color: #9ffb8a;";
                        return 'ACTIVE';
                    case 2 :
                        metaData.style += "background-color: #f1f26f;";
                        return 'LEND';
                    case 3 :
                        metaData.style += "background-color: #ff7373;";
                        return 'SELL';
                    case 4 :
                        metaData.style += "background-color: #f1f26f;";
                        return 'RENT';
                    case 5 :
                        metaData.style += "background-color: #ff7373;";
                        return 'BROKEN';
                    case 6 :
                        metaData.style += "background-color: #ff7373;";
                        return 'LOST';
                    case 7 :
                        metaData.style += "background-color: #f57b1c;";
                        return 'MAINTENANCE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                editable: false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NON ACTIVE'], [1, 'ACTIVE'], [2, 'LEND'], [3, 'SELL'], [4, 'RENT'], [5, 'BROKEN'], [6, 'LOST'], [7, 'MAINTENANCE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Status Note',
            sortable: true,
            resizable: true,
            dataIndex: 'statusdesc',
            width: 80,
            filter: {xtype: "textfield"}
        }
    ],
    initComponent: function () {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztAssetCategorySub.getTotalCount() === 0) {
            jun.rztAssetCategorySub.load();
        }
        this.store = jun.rztAssetDetailHide;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Export',
                    ref: '../btnExportNew',
                    iconCls: "silk13-report"
                },
                {
                    xtype: "form",
                    frame: !1,
                    border: !1,
                    ref: '../formz',
                    items: [
                        {
                            xtype: "hidden",
                            name: "asset_id",
                            ref: "../../asset_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                },
                ' ',
                {
                    xtype: 'button',
                    text: 'STATUS',
                    ref: '../btnCondition',
                    iconCls: 'silk13-contrast'
                },

            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.baseParams = {hide_value: 0};
        this.store.reload();
        jun.AssetGrid.superclass.initComponent.call(this);
        this.btnExportNew.on('Click', this.ExportNew, this);
        this.btnCondition.on('Click', this.Condition, this);
        this.getSelectionModel().on('doubleclick', this.rowdblclick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    rowdblclick: function () {
        isDblClick = true;
        log.innerHTML += '<div>row double click</div>';
        Ext.MessageBox.alert("Warning", "dbl");
        window.setTimeout(function () {
            isDblClick = false;
        }, 0);
    },
    btnDisabled: function (status) {
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    Condition: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data");
            return;
        }
        var idz = selectedz.json.asset_detail_id;
        var form = new jun.AssetStatusConditionWin({modez: 1, id: idz});
        form.show();
        form.formz.getForm().loadRecord(this.record);

    },
    loadForm: function () {
        var form = new jun.AssetDepriciationWin({modez: 0});
        form.show();
    },
    ExportNew: function () {
        var form = new jun.ReportShowAsset({modez: 1});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    Export: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data " + this.title + ".");
            return;
        }
        var idz = selectedz.json.asset_id;
        this.asset_id.setValue(idz);
        var form = this.formz.getForm();
        form.standardSubmit = !0;
        form.url = "Report/PrintAsset";
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    Print: function () {
        Ext.Ajax.request({
            url: 'Asset/PrintAsset/',
            method: 'POST',
            //timeOut: 1000,
            scope: this,
            params: {
                id: this.id,
                date: this.date.getValue(),
            },
            success: function (f, a) {

                var response = Ext.decode(f.responseText);
                var printWindow = window.open('', '', 'height=0px ,width=0px');

                printWindow.document.write('<html>');
                printWindow.document.write('<head>');
                printWindow.document.write('<style>table {font-family: arial, sans-serif; border: 3px;border-collapse: collapse;width: 100%;} td, th {border: 1px solid #dddddd;text-align: center;padding: 8px;}tr:nth-child(even) {background-color: #dddddd;} </style>');
                printWindow.document.write('</head>');

                printWindow.document.write('<body>');
                printWindow.document.write('<div>');
                printWindow.document.write('<table>');
                printWindow.document.write('<tr>');
                printWindow.document.write('<th>Name</th>');
                printWindow.document.write('<th>No.Activa</th>');
                printWindow.document.write('</tr>');

                for (var i = 0, len = response.data.length; i < len; i++) {
                    printWindow.document.write('<tr>');
                    printWindow.document.write('<td>' + response.data[i].asset_trans_name + '</td>');
                    printWindow.document.write('<td>' + response.data[i].ati + '</td>');
                    printWindow.document.write('</tr>');
                }

                printWindow.document.write('</table>');
                printWindow.document.write('</div>');

                printWindow.document.write('</body>');
                printWindow.document.write('</html>');

                setTimeout(function () {
                    printWindow.print();
                    printWindow.close();
                }, 500);

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'AssetDetail/delete/id/' + record.json.asset_detail_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztAssetDetail.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
});
