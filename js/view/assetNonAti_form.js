jun.AssetNonAtiWin = Ext.extend(Ext.Window, {
    title: 'Asset Non Activa',
    modez:1,
    width: 400,
    height: 365,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        if (jun.rztAssetBarangNonCmp.getTotalCount() === 0) {
            jun.rztAssetBarangNonCmp.load();
        }
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetNonAti',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'combo',
                        hidden:false,
                        typeAhead: true,
                        fieldLabel: 'Item',
                        ref: '../asset_barang_id',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetBarangNonCmp,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kode_barang} </br> {nama_barang} </br> {ket} </span></h3>',
                            "</div></tpl>"),
                        listWidth: 450,
                        lastQuery: "",
                        pageSize: 20,
                        forceSelection: true,
                        autoSelect: false,
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        hiddenName: 'asset_barang_id',
                        name: 'asset_barang_id',
                        valueField: 'barang_id',
                        displayField: 'nama_barang',
                        emptyText: "Type Items...",

                        //value: STORE,
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'Merk',
                        name: 'merk',
                        id: 'merkid',
                        ref: '../merk',
                        emptyText: "(Optional)",
                        width: 175,
                        readOnly: false,
                        maxLength: 500,
                        anchor: '100%'
                    },
                                                                     {
                                    xtype: 'numericfield',
                                    fieldLabel: 'Qty',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'qty',
                                    id:'qtyid',
                                    ref:'../qty',
                                    maxLength: 30,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                /*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'doc_ref',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'doc_ref',
                                    id:'doc_refid',
                                    ref:'../doc_ref',
                                    maxLength: 255,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, */
                        /*                                             {
                            xtype: 'xdatefield',
                            ref:'../tdate',
                            fieldLabel: 'tdate',
                            name:'tdate',
                            id:'tdateid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, */
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../tgl',
                            fieldLabel: 'Purchase Date',
                            name:'tgl',
                            id:'tglid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, 

                                /*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'store_kode',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'store_kode',
                                    id:'store_kodeid',
                                    ref:'../store_kode',
                                    maxLength: 30,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, */
                                /*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'businessunit_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'businessunit_id',
                                    id:'businessunit_idid',
                                    ref:'../businessunit_id',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                },*/
                                                                     {
                                    xtype: 'numericfield',
                                    fieldLabel: 'Price',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'price',
                                    id:'priceid',
                                    ref:'../price',
                                    maxLength: 30,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Category',
                        ref: '../category',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategory,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{category_code} - {category_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_id',
                        name: 'category_id',
                        valueField: 'category_id',
                        displayField: 'category_code',
                        emptyText: "Category...",
                        id:'categoryreq',
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Sub Category',
                        ref: '../subcategory',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategorySub,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{sub_code} - {sub_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_sub_id',
                        name: 'category_sub_id',
                        valueField: 'category_sub_id',
                        displayField: 'sub_code',
                        emptyText: "Sub Category...",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'Location',
                        name: 'location',
                        id: 'locationid',
                        ref: '../location',
                        emptyText: "(Optional)",
                        width: 175,
                        readOnly: false,
                        maxLength: 50,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        hideLabel:false,
                        //hidden:true,
                        name:'note',
                        id:'noteid',
                        ref:'../note',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetNonAtiWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.category.on('Select', this.categoryonselect, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    categoryonselect: function(c,r,i)
    {
        //var categoryreq = Ext.getCmp('categoryreq').getRawValue();

        /*if(tobu == 'OTHER')
        {
            this.branch.setVisible(false);
            this.branch.allowBlank = true;
            this.branch.allowBlank = true;
        }*/

        var catid =r.get('category_id');
        this.subcategory.store.reload({params:{category_id:catid}});
        this.subcategory.setVisible(true);
        this.subcategory.allowBlank = false;

    },
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'asset/AssetNonAti/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'asset/AssetNonAti/create/';
                }
             
            Ext.getCmp('form-AssetNonAti').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztAssetNonAti.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-AssetNonAti').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});


jun.AssetNonAtiStatusWin = Ext.extend(Ext.Window, {
    title: 'Asset Non Activa Status',
    modez:1,
    width: 400,
    height: 414,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetNonAtiStatus',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        hidden: true,
                        fieldLabel: 'Id',
                        name: 'nonati_id',
                        id: 'nonati_id',
                        ref: '../nonati_id',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'DocRef',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'Item Name',
                        name: 'kode_barang',
                        id: 'kode_barangid',
                        ref: '../kode_barang',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Price',
                        name: 'price',
                        id: 'priceid',
                        ref: '../price',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },

                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        hidden:false,
                        fieldLabel: 'Description',
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'

                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Status',
                        name:'status',
                        id:'statusasset',
                        ref:'../statusasset',
                        hiddenName: 'statusasset',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['0', 'NON ACTIVE'],
                                ['1', 'ACTIVE'],
                                ['5', 'BROKEN'],
                                ['6', 'LOST'],
                                ['7', 'MAINTENANCE']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "Status",
                        mode : 'local',
                        anchor: "100%"
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Note',
                        hideLabel:false,
                        //hidden:true,
                        name:'ket',
                        id:'ketid',
                        ref:'../ket',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetNonAtiStatusWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        //this.businessunit.on('Select', this.businessunitonselect, this);
        //this.statusasset.on('Select', this.statusassetonselect, this);
        //this.amountstatus.on('change', this.ppnstatuschange, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    ppnstatuschange: function(c,r,i)
    {
        var amountstatus = this.amountstatus.getValue();
        var ppn = amountstatus * PPNASSET/100;
        this.ppnstatus.setValue(ppn);
    },

    statusassetonselect: function(c,r,i)
    {
        var status = Ext.getCmp('statusasset').getValue();

        if(status == '2' || status == '4')
        {
            this.businessunit.setVisible(true);
            this.branch.setVisible(true);
            this.amountstatus.setVisible(true);

            this.businessunit.allowBlank = false;
            this.branch.allowBlank = false;
            this.amountstatus.allowBlank = false;
        }
        else if(status == '3')
        {
            this.businessunit.setVisible(true);
            this.branch.setVisible(true);
            this.amountstatus.setVisible(true);
            this.ppnstatus.setVisible(true);

            this.businessunit.allowBlank = false;
            this.branch.allowBlank = false;
            this.amountstatus.allowBlank = false;
            this.ppnstatus.allowBlank = false;
        }
        else {
            this.businessunit.setVisible(false);
            this.ppnstatus.setVisible(false);
            this.amountstatus.setVisible(false);
            this.branch.setVisible(false);
            this.businessunit.allowBlank = true;
            this.branch.allowBlank = true;
            this.amountstatus.allowBlank = true;
            this.ppnstatus.allowBlank = true;
        }


    },

    businessunitonselect: function(c,r,i)
    {
        var tobu = Ext.getCmp('businessunitstatus').getRawValue();

        if(tobu == 'OTHER')
        {
            this.branch.setVisible(false);
            this.branch.allowBlank = true;
            this.branch.allowBlank = true;
        }
        else {
            var buid =r.get('businessunit_id');
            this.branch.store.reload({params:{businessunit_id:buid}});
            this.branch.setVisible(true);
            this.branch.allowBlank = false;
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        this.btnDisabled(true);
        var urlz= 'asset/AssetNonAti/status';


        Ext.getCmp('form-AssetNonAtiStatus').getForm().submit({
            url:urlz,
            timeOut: 1000,
            method: 'POST',
            scope: this,
            success: function(f,a){
                jun.rztAssetNonAti.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                if(this.modez == 0){
                    Ext.getCmp('form-AssetNonAtiStatus').getForm().reset();
                    this.btnDisabled(false);
                }
                if(this.closeForm){
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }

});