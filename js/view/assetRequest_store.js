jun.AssetRequeststore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetRequeststore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetRequestStoreId',
            url: 'AssetRequest',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'request_id'},
                {name:'barang_id'},
                {name:'qty'},
                {name:'doc_ref'},
                {name:'tdate'},
                {name:'tgl'},
                {name:'note'},
                {name:'store_kode'},
                {name:'businessunit_id'},
                {name:'price'},
                {name:'category_id'},
                {name:'category_sub_id'},
                {name:'approved'},
                {name:'user_id'},
                {name:'visible'},
                {name:'non_ati'},
                {name:'kode_barang'},
                {name:'nama_barang'},
                {name:'serialnumber'},
                {name:'location'},
                {name:'conditions'},
                {name:'merk'}


            ]
        }, cfg));
    }
});
jun.rztAssetRequest = new jun.AssetRequeststore();
//jun.rztAssetRequest.load();
