jun.AssetBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AssetBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetBarangStoreId',
            url: 'AssetBarang',
            root: 'results',
            autoLoad: true,
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'grup_id'},
                {name: 'active'},
                {name: 'sat'},
                {name: 'up'},
                {name: 'tipe_barang_id'},
                {name: 'category_sub_id'},
                {name: 'category_id'},
                {name: 'owner'},
                {name: 'store_kode'}
            ]
        }, cfg));
    }
});
jun.rztAssetBarang = new jun.AssetBarangstore();
jun.rztAssetBarangCmp = new jun.AssetBarangstore();
jun.rztAssetBarangAtiCmp = new jun.AssetBarangstore({baseParams: 'loadati'});
jun.rztAssetBarangNonCmp = new jun.AssetBarangstore({baseParams: 'loadnonati'});