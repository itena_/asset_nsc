jun.Locationstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Locationstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LocationstoreId',
            url: 'Location',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'name'},
            ]
        }, cfg));
    },
});
jun.rztLocation = new jun.Locationstore();
jun.rztLocationLib = new jun.Locationstore();
