jun.AssetNonAtistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetNonAtistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetNonAtiStoreId',
            url: 'AssetNonAti',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'nonati_id'},
{name:'barang_id'},
{name:'qty'},
{name:'doc_ref'},
{name:'tdate'},
{name:'tgl'},
{name:'note'},
{name:'store_kode'},
{name:'businessunit_id'},
{name:'price'},
{name:'category_id'},
{name:'category_sub_id'},
{name:'group_id'},
{name:'user_id'},
{name:'visible'},
{name:'category_name'},
{name:'sub_name'},
{name:'kode_barang'},
{name:'nama_barang'},
{name:'status'},
{name:'location'},
{name:'status_note'},
{name:'merk'},

            ]
        }, cfg));
    }
});
jun.rztAssetNonAti = new jun.AssetNonAtistore();
//jun.rztAssetNonAti.load();
