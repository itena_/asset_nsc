jun.AsseComparationstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AsseComparationstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AsseComparationstoreId',
            url: 'AssetComparation',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'asset_comparation_id'},
                {name: 'store_id'},
                {name: 'user_id'},
                {name: 'tgl'},
                {name: 'nama_store'},
                {name: 'version'},
                {name: 'nama_user'},
                {name: 'tdate'},
                {name: 'status'},
            ]
        }, cfg));
    }
});
jun.rztAsseComparation = new jun.AsseComparationstore();
