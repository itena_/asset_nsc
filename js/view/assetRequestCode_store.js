jun.AssetRequestCodestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetRequestCodestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetRequestCodeStoreId',
            url: 'AssetRequestCode',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'request_code_id'},
{name:'name'},
{name:'suggest'},
{name:'desc'},
{name:'code'},
{name:'timestamps'},
{name:'tipe'},

            ]
        }, cfg));
    }
});
jun.rztAssetRequestCode = new jun.AssetRequestCodestore();
//jun.rztAssetRequestCode.load();
