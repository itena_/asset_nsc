jun.AssetRequestGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Asset Request",
        id:'docs-jun.AssetRequestGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
        /*                {
			header:'request_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'request_id',
			width:100
		},
                                {
			header:'barang_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'barang_id',
			width:100
		},*/
        /*                        {
			header:'qty',
			sortable:true,
			resizable:true,                        
            dataIndex:'qty',
			width:100
		},*/
                                {
			header:'Doc.Ref',
			sortable:true,
			resizable:true,                        
            dataIndex:'doc_ref',
			width:100,
                                    filter: {xtype: "textfield"}
		},
        /*                        {
			header:'tdate',
			sortable:true,
			resizable:true,                        
            dataIndex:'tdate',
			width:100
		},*/
        {
            header:'Item Code',
            sortable:true,
            resizable:true,
            dataIndex:'kode_barang',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Item Name',
            sortable:true,
            resizable:true,
            dataIndex:'nama_barang',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Merk',
            sortable:true,
            resizable:true,
            dataIndex:'merk',
            width:100,
            filter: {xtype: "textfield"}
        },
                                {
			header:'Purchase Date',
			sortable:true,
			resizable:true,                        
            dataIndex:'tgl',
			width:100,
                                    renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                                    filter: {
                                        xtype: "xdatefield",
                                        format: 'd/m/Y'
                                    }
		},
        {
            header:'Price',
            sortable:true,
            resizable:true,
            dataIndex:'price',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Serial Number',
            sortable:true,
            resizable:true,
            dataIndex:'serialnumber',
            width:100,
            filter: {xtype: "textfield"}
        },

        {
            header:'Location',
            sortable:true,
            resizable:true,
            dataIndex:'location',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Description',
            sortable:true,
            resizable:true,
            dataIndex:'note',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Condition',
            sortable: true,
            resizable: true,
            dataIndex: 'conditions',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 0 :
                        metaData.style += "background-color: #9CFD7B;";
                        return 'NEW';
                    case 1 :
                        metaData.style += "background-color: #FCEB19;";
                        return 'SECOND';
                    case 2 :
                        metaData.style += "background-color: #f02213;";
                        return 'BROKEN';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NEW'], [1, 'SECOND'],[2, 'BROKEN']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Approved',
            sortable: true,
            resizable: true,
            dataIndex: 'approved',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 0 :
                        metaData.style += "background-color: #FEE98B;";
                        return 'NEEDS APPROVAL';
                    case 1 :
                        metaData.style += "background-color: #9CFD7B;";
                        return 'APPROVED';
                    case 2 :
                        metaData.style += "background-color: #f02213;";
                        return 'DECLINED';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NEEDS APPROVAL'], [1, 'APPROVED'],[2, 'DECLINED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },

	],
	initComponent: function(){
	    if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztAssetCategorySub.getTotalCount() === 0) {
            jun.rztAssetCategorySub.load();
        }

        this.store = jun.rztAssetRequest;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Edit',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Delete',
                        ref: '../btnDelete'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.AssetRequestGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();

            var filterSync = r.get('approved') == '1' || r.get('approved') == '2';
            //this.btnCancel.setDisabled(filterSync ?"true":"false");
        //    this.btnApprove.setDisabled(filterSync);
            this.btnAdd.setDisabled(filterSync);
            this.btnEdit.setDisabled(filterSync);
            this.btnDelete.setDisabled(filterSync);


        },
        
        loadForm: function(){
            var form = new jun.AssetRequestWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.request_id;
            var form = new jun.AssetRequestWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'AssetRequest/delete/id/' + record.json.request_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztAssetRequest.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
})

jun.AssetRequestApprovalGrid=Ext.extend(Ext.grid.GridPanel ,{
    title:"Asset Request Approval",
    id:'docs-jun.AssetRequestApprovalGrid',
    iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
    plugins:[new Ext.ux.grid.GridHeaderFilters],
    columns:[
        /*                {
			header:'request_id',
			sortable:true,
			resizable:true,
            dataIndex:'request_id',
			width:100
		},
                                {
			header:'barang_id',
			sortable:true,
			resizable:true,
            dataIndex:'barang_id',
			width:100
		},*/
        /*                        {
			header:'qty',
			sortable:true,
			resizable:true,
            dataIndex:'qty',
			width:100
		},*/
        {
            header:'Doc.Ref',
            sortable:true,
            resizable:true,
            dataIndex:'doc_ref',
            width:100,
            filter: {xtype: "textfield"}
        },
        /*                        {
			header:'tdate',
			sortable:true,
			resizable:true,
            dataIndex:'tdate',
			width:100
		},*/
        {
            header:'Item Code',
            sortable:true,
            resizable:true,
            dataIndex:'kode_barang',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Item Name',
            sortable:true,
            resizable:true,
            dataIndex:'nama_barang',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Merk',
            sortable:true,
            resizable:true,
            dataIndex:'merk',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Purchase Date',
            sortable:true,
            resizable:true,
            dataIndex:'tgl',
            width:100,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },
        {
            header:'Price',
            sortable:true,
            resizable:true,
            dataIndex:'price',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Serial Number',
            sortable:true,
            resizable:true,
            dataIndex:'serialnumber',
            width:100,
            filter: {xtype: "textfield"}
        },

        {
            header:'Location',
            sortable:true,
            resizable:true,
            dataIndex:'location',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Description',
            sortable:true,
            resizable:true,
            dataIndex:'note',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Condition',
            sortable: true,
            resizable: true,
            dataIndex: 'conditions',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 0 :
                        metaData.style += "background-color: #9CFD7B;";
                        return 'NEW';
                    case 1 :
                        metaData.style += "background-color: #FCEB19;";
                        return 'SECOND';
                    case 2 :
                        metaData.style += "background-color: #f02213;";
                        return 'BROKEN';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NEW'], [1, 'SECOND'],[2, 'BROKEN']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Approved',
            sortable: true,
            resizable: true,
            dataIndex: 'approved',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 0 :
                        metaData.style += "background-color: #FEE98B;";
                        return 'NEEDS APPROVAL';
                    case 1 :
                        metaData.style += "background-color: #9CFD7B;";
                        return 'APPROVED';
                    case 2 :
                        metaData.style += "background-color: #f02213;";
                        return 'DECLINED';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NEEDS APPROVAL'], [1, 'APPROVED'],[2, 'DECLINED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },


    ],
    initComponent: function(){
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztAssetCategorySub.getTotalCount() === 0) {
            jun.rztAssetCategorySub.load();
        }
        this.store = jun.rztAssetRequest;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                /*{
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },*/
                /*{
                    xtype:'tbseparator'
                },*/
                {
                    xtype: 'button',
                    text: 'Approve',
                    ref: '../btnApprove'
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Decline',
                    ref: '../btnDecline'
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.AssetRequestApprovalGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.btnApprove.on('Click', this.Approve, this);
        this.btnDecline.on('Click', this.Decline, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },

    getrow: function(sm, idx, r){
        this.record = r;

        var selectedz = this.sm.getSelections();

        var filterSync = r.get('approved') == '1' || r.get('approved') == '2';
        //this.btnCancel.setDisabled(filterSync ?"true":"false");
        this.btnApprove.setDisabled(filterSync);
        this.btnDecline.setDisabled(filterSync);
        this.btnEdit.setDisabled(filterSync);
        this.btnDelete.setDisabled(filterSync);

    },
    Approve : function(){
        Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menerima data ini menjadi asset?', this.approveRecYes, this);
    },
    approveRecYes: function(btn){
        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
            return;
        }

        var idz = record.json.request_id;
        Ext.Ajax.request({
            url: 'AssetRequest/Approve',
            method: 'POST',
            params: {
                id: idz
            },
            success:function (f, a) {
                jun.rztAssetRequest.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },

    Decline : function(){
        Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menolak data ini?', this.declineRecYes, this);
    },
    declineRecYes: function(btn){

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
            return;
        }

        var idz = record.json.request_id;
        Ext.Ajax.request({
            url: 'AssetRequest/Declined',
            method: 'POST',
            params: {
                id: idz
            },
            success:function (f, a) {
                jun.rztAssetRequest.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },

    loadForm: function(){
        var form = new jun.AssetRequestWin({modez:0});
        form.show();
    },

    loadEditForm: function(){

        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
        if(selectedz == undefined){
            Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.request_id;
        var form = new jun.AssetRequestWin({modez:1, id:idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },

    deleteRec : function(){
        Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },

    deleteRecYes : function(btn){

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'AssetRequest/delete/id/' + record.json.request_id,
            method: 'POST',
            success:function (f, a) {
                jun.rztAssetRequest.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
})
