jun.AssetBarangWin = Ext.extend(Ext.Window, {
    title: 'Asset Items',
    modez: 1,
    width: 400,
    height: 290,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-AssetBarang',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Item Code',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_barang',
                        id: 'kode_barangid',
                        ref: '../kode_barang',
                        maxLength: 500,
                        allowBlank: true,
                        hidden: 1,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Category',
                        ref: '../category',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategoryLib,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{category_code} - {category_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_id',
                        valueField: 'category_id',
                        displayField: 'category_code',
                        emptyText: "Category...",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Sub Category',
                        ref: '../subcategory',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategorySubLib,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{sub_code} - {sub_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_sub_id',
                        valueField: 'category_sub_id',
                        displayField: 'sub_code',
                        emptyText: "Sub Category...",
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Item Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_barang',
                        id: 'nama_barangid',
                        ref: '../nama_barang',
                        maxLength: 500,
                        allowBlank: false,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ket',
                        id: 'ketid',
                        ref: '../ket',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Type',
                        name: 'tipe_barang_id',
                        id: 'tipe_barang_idid',
                        ref: '../tipe_barang_id',
                        hiddenName: 'tipe_barang_id',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['0', 'NON ATI'],
                                ['1', 'ATI']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "Choose Type",
                        mode: 'local',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../owner',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'owner',
                        valueField: 'store_id',
                        displayField: 'bu_mix',
                        emptyText: "All Branch",
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.AssetBarangWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.category.on('Select', this.categoryonselect, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);

        this.owner.on('select', this.ownerOnSelect, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    ownerOnSelect: function(c,r,i){
        console.log(r);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    categoryonselect: function (c, r, i) {
        var catid = r.get('category_id');
        this.subcategory.store.reload({params: {category_id: catid}});
        this.subcategory.setVisible(true);
        this.subcategory.allowBlank = false;
    },
    saveForm: function () {
        console.log(this.owner);
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'assetBarang/update/id/' + this.id;
        } else {
            urlz = 'assetBarang/create/';
        }
        Ext.getCmp('form-AssetBarang').getForm().submit({
            url: urlz,
            scope: this,
            success: function (f, a) {
                jun.rztAssetBarang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-AssetBarang').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});