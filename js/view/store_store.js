jun.Storestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Storestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StoreStoreId',
            url: 'Store',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'store_id'},
                {name: 'store_kode'},
                {name: 'nama_store'},
                {name: 'alamat'},
                {name: 'telp'},
                {name: 'email'},
                {name: 'tipe'},
                {name: 'tanggal_backup'},
                {name: 'transaksi_flag'},
                {name: 'up'},
                {name: 'businessunit_id'},
                {name: 'wilayah_id'},
                {name: 'nscc_store_group_id'},
                {name: 'beban_acc'},
                {name: 'conn'},
                {name: 'persen_beban_acc'},
                {name: 'owner'},
                {name: 'bu_mix'}

            ]
        }, cfg));
    },
});
jun.rztStore = new jun.Storestore();
jun.rztStoreCmp = new jun.Storestore();
jun.rztStoreAll = new jun.Storestore({baseParams:{param: 'all'}});

jun.StoreReport = new jun.Storestore();
