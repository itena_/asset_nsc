jun.ajaxCounter = 0;
jun.camera;
jun.usercabang = '';
jun.runner = new Ext.util.TaskRunner();
//jun.globalStore = new Ext.ux.state.LocalStorageProvider({prefix: 'nscc-'});
jun.TreeUi = Ext.extend(Ext.tree.TreePanel, {
    title: "Menu",
    useArrows: true,
    region: "west",
    split: true,
    autoScroll: true,
    rootVisible: false,
    collapsible: true,
    containerScroll: true,
    minSize: 175,
    maxSize: 400,
    width: 240,
    initComponent: function () {
        this.root = {
            text: "Menu"
        };
        jun.TreeUi.superclass.initComponent.call(this);
        Ext.Ajax.request({
            url: 'Store/GetUserCabang',
            method: 'POST',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if(!IS_ADMINISTRATOR) jun.rztStoreCmp.baseParams = {store_id: response.msg};
                jun.rztStoreCmp.reload();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.sidebar = new jun.TreeUi({
    dataUrl: "site/tree"
});
jun.sidebar.on("click", function (a, b) {
    b.stopEvent();
    if (a.isLeaf()) {
        if (a.id == "logout") {
            Ext.MessageBox.confirm("Logout", "Are sure want logout?", function (a) {
                if (a === "no") return;
                LOGOUT = true;
                window.location.href = "site/logout";
            }, this);
        } else {
            // jun.mainPanel.loadClass(a.id);
            var tabpanel = Ext.getCmp('mainpanel');
            var id = "docs-" + a.id;
            var tab = tabpanel.getComponent(id);
            if (tab) {
                tabpanel.setActiveTab(tab);
            }
            else {
                var obj = eval(a.id);
                var object = new obj({id: id, closable: !0});
                if (object.iswin) {
                    object.show();
                } else {
                    var p = tabpanel.add(object);
                    tabpanel.setActiveTab(p)
                }
            }
        }
    }
});
var clock = new Ext.Toolbar.TextItem("Jam");
jun.Send = Ext.extend(Ext.Window, {
    width: 1,
    height: 1,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-Send",
                labelWidth: 1,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: []
            }
        ];
        jun.Send.superclass.initComponent.call(this);
    }
});
//var send = new jun.Send({});
//send.show(), send.hide(),
// jun.mainPanel = new jun.TabsUi();
jun.ViewportUi = Ext.extend(Ext.Viewport, {
    layout: "border",
    initComponent: function () {
//        clearText();
        this.items = [
            {
                xtype: "box",
                region: "north",
                id: "app-header",
                style: 'margin:10px',
                html: SYSTEM_LOGO,
                height: 100
            },
            jun.sidebar,
            {
                xtype: 'tabpanel',
                activeTab: 0,
                region: "center",
                frame: !0,
                id: "mainpanel",
                enableTabScroll: !0
            }
            // jun.mainPanel
        ];
        jun.ViewportUi.superclass.initComponent.call(this);
//        displayText("Welcome To Natasha",0);
    }
});
jun.myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Processing... please wait"});
Ext.onReady(function () {
    Ext.USE_NATIVE_JSON = true;
    Ext.EventManager.addListener(document, "keypress", function (e) {
        if (jun.ajaxCounter > 0) {
            return false;
        }
    });
    Ext.Ajax.timeout = 1800000;
    var a = function () {
        Ext.get("loading").remove(), Ext.fly("loading-mask").fadeOut({
            remove: !0
        });
    };
    Ext.QuickTips.init();
    //    loadText = "Sedang proses... silahkan tunggu";
    Ext.Ajax.on("beforerequest", function (conn, opts) {
        if (opts.url == "site/Sync") return;
        jun.myMask.show();
        jun.ajaxCounter++;
    });
    Ext.Ajax.on("requestcomplete", function (conn, response, opts) {
        if (opts.url == "site/Sync") return;
        if (jun.ajaxCounter > 1) {
            jun.ajaxCounter--;
        } else {
            jun.ajaxCounter = 0;
            jun.myMask.hide();
        }
    });
    Ext.Ajax.on("requestexception", function (conn, response, opts) {
        if (opts.url == "site/Sync") return;
        if (jun.ajaxCounter > 1) {
            jun.ajaxCounter--;
        } else {
            jun.ajaxCounter = 0;
            jun.myMask.hide();
        }
        switch (response.status) {
            case 403:
                window.location.href = 'site/logout';
                break;
            case 500:
                Ext.Msg.alert('Internal Server Error', response.responseText);
                break;
            default :
                Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                break;
        }
    });

    new jun.ViewportUi({});

    /*var b = new jun.ViewportUi({});
    qz.websocket.setClosedCallbacks(function (evt) {
        updateState('Inactive', 'default');
        console.log(evt);
        if (evt.reason) {
            displayMessage("<strong>Connection closed:</strong> " + evt.reason, 'alert-warning');
        }
    });
    qz.websocket.setErrorCallbacks(handleConnectionError);
    qz.serial.setSerialCallbacks(function (port, output) {
        console.log('Serial', port, 'received output', output);
        displayMessage("Received output from serial port [" + port + "]: <em>" + output + "</em>");
    });
    openSerialPort("0x0002", "0x000D", "", COM_POSIFLEX);
    setTimeout(function () {
        twoRows("Selamat Datang di", PT_NEGARA);
    }, 2500);
    var scope = this;
    var updateSocket = function () {
        console.log(isLoaded());
    };
    var task = {
        run: updateSocket,
        interval: 30000 //1 second
    };
    jun.runner.start(task);*/
});