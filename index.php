<?php
// Early fatal errors handler, it will be replaced by a full featured one in Controller class
// (given it does not have any parse or fatal errors of its own)
function earlyFatalErrorHandler($unregister = false)
{
    // Functionality for "unregistering" shutdown function
    static $unregistered;
    if ($unregister) $unregistered = true;
    if ($unregistered) return;
    // 1. error_get_last() returns NULL if error handled via set_error_handler
    // 2. error_get_last() returns error even if error_reporting level less then error
    $error = error_get_last();
    // Fatal errors
    $errorsToHandle = E_ERROR | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING;
    if ((!defined('APP_PRODUCTION_MODE') || APP_PRODUCTION_MODE) && !is_null($error) && ($error['type'] & $errorsToHandle)) {
        $message = 'FATAL ERROR: ' . $error['message'];
        if (!empty($error['file'])) {
            $message .= ' (' . $error['file'] . ' :' . $error['line'] . ')';
            throw new Exception($message);
        }
    }
}

register_shutdown_function('earlyFatalErrorHandler');
// change the following paths if necessary
$yii = dirname(__FILE__) . '/../yii/YiiBase.php';
//$yii=dirname(__FILE__).'/../../yii/framework/Yiilite.php';
$config = dirname(__FILE__) . '/protected/config/main.php';
$yiiG = dirname(__FILE__) . '/protected/globals.php';
$GL = dirname(__FILE__) . '/protected/components/GL.php';
// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
require_once($yiiG);
require_once($yii);
require_once($GL);
//require_once($define);
class Yii extends YiiBase
{
    /**
     * @static
     * @return CWebApplication
     */
    public static function app()
    {
        return parent::app();
    }
}
//<script src="js/ext340/ext-all-debug-w-comments.js">
Yii::createWebApplication($config)->run();
/**
 *  TODO Buat dropping (dibuat sekali langsung diterima)
 *  TODO Buat order untuk dropping
 *  TODO sync untuk retur ke warehouse
 *
 */
?>
