<?php
class SystemNotification {
    public static function generateMessage($status, $msg) {
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
}