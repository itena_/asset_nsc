<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 27/12/2014
 * Time: 10:30
 */
class History
{
    private $_total_amount = 0;
    public $_id = array();
    public function add_history_status($businessunit_id, $masterassetid, $asset_id, $docref, $ati,
                                       $name, $branch, $price, $newprice, $class, $tariff, $period,
                                       $penyusutanperbulan, $penyusutanpertahun, $desc, $status)
    {
        $history = new AssetHistory;
        $history->businessunit_id = $businessunit_id;
        $history->masterassetid = $masterassetid;
        $history->asset_id = $asset_id;
        $history->docref = $docref;
        $history->ati = $ati;
        $history->name = $name;
        $history->branch = $branch;
        $history->price = $price;
        $history->newprice = $newprice;
        $history->class = $class;
        $history->tariff = $tariff;
        $history->period = $period;
        $history->penyusutanperbulan = $penyusutanperbulan;
        $history->penyusutanpertahun = $penyusutanpertahun;
        $history->desc =  $desc;
        $history->status =  $status;
        $history->sdate = new CDbExpression('NOW()');
        $history->created_at = new CDbExpression('NOW()');
        $history->updated_at = new CDbExpression('NOW()');

        if (!$history->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetHistory')) . CHtml::errorSummary($history));
        }
    }

    public static function saveHistory($model, $modeldetail, $import = 0, $params = []) {
        if($import == 0) {
            $history = new History();
            $history->add_history_status(
                $model->businessunit_id,
                $model->asset_id,
                $modeldetail->asset_detail_id,
                $model->doc_ref,
                $modeldetail->ati,
                $model->description,
                $model->store_kode,
                $modeldetail->price_acquisition,
                $modeldetail->new_price_acquisition,
                $modeldetail->class,
                $modeldetail->tariff,
                $modeldetail->period,
                $modeldetail->desc,
                $modeldetail->penyusutanperbulan,
                $modeldetail->penyusutanpertahun,
                $modeldetail->status);
        } else {
            return U::getQuery([
                DbCmd::uuid(),
                $params['asset_id'],
                $params['assetDetail_id'],
                $model->businessunit_id,
                $model->doc_ref,
                $modeldetail->ati,
                $model->description,
                $_POST['bu_name'],
                $model->store_kode,
                $modeldetail->price_acquisition,
                $modeldetail->new_price_acquisition,
                $modeldetail->class,
                $modeldetail->tariff,
                $modeldetail->period,
                $modeldetail->penyusutanperbulan,
                $modeldetail->penyusutanpertahun,
                $modeldetail->desc,
                $modeldetail->status,
                NULL,
                NULL,
                0,
                new CDbExpression('NOW()'),
                new CDbExpression('NOW()'),
                new CDbExpression('NOW()')
            ]);
        }
    }
}