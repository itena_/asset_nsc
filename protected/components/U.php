<?php

class U
{

    static function get_selisih_stok()
    {
        $select [] = "DISTINCT(trans_no) trans_no";
        $select [] = "tran_date";
        $select [] = "reference";
        $select [] = "memo_ note";
        $query = Yii::app()->db->createCommand();
        $query->select($select);
        $query->from = "{{stock_moves}} psm";
        $query->leftJoin("{{comments}} c", '(psm.type = c.type) AND (psm.trans_no = c.type_no) ');
        $query->andWhere('psm.type = :type', array(':type' => SELISIHSTOK));
        return $query->queryAll(true);
    }

    // ------------------------------------------------ Void ----------------------------------------------------------------

    static function get_max_type_no_stock($type)
    {
        $type_no = app()->db->createCommand()->select("MAX(trans_no)")
            ->from("{{stock_moves}}")->where('type=:type', array(':type' => $type))->queryScalar();
        return $type_no == false ? 0 : $type_no;
    }

    // --------------------------------------------- Bank Trans -------------------------------------------------------------

    // --------------------------------------------- Gl Trans ---------------------------------------------------------------
    static function is_bank_account($account_code)
    {
//        $criteria = new CDbCriteria();
//        $criteria->addCondition("account_code = :account_code");
//        $criteria->addCondition("kategori = :kas");
//        $criteria->addCondition("kategori = :bank", 'OR');
//        $criteria->params = array(
//            ":account_code" => $account_code,
//            ':kas' => SysPrefs::get_val('coa_grup_kas'),
//            ':bank' => SysPrefs::get_val('coa_grup_bank')
//        );
        $comm = Yii::app()->db->createCommand("SELECT account_code FROM nscc_chart_master WHERE
        account_code = :account_code AND (kategori = :kas OR kategori = :bank)");
        $bank_act = $comm->queryAll(true, array(
            ":account_code" => $account_code,
            ':kas' => COA_GRUP_KAS,
            ':bank' => COA_GRUP_BANK
        ));
        return count($bank_act) > 0;
    }

    static function add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount, $person_id, $cf, $store = null)
    {
        $gl_trans = new GlTrans();
        $gl_trans->type = $type;
        $gl_trans->type_no = $trans_id;
        $gl_trans->tran_date = $date_;
        $gl_trans->account_code = $account;
        $gl_trans->memo_ = $memo_;
        $gl_trans->id_user = $person_id;
        $gl_trans->amount = $amount;
        $gl_trans->cf = $cf;
        $gl_trans->store = $store;
        if (!$gl_trans->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'General Ledger')) . CHtml::errorSummary($gl_trans));
        }/* else{
          if(PUSH_PUSAT){
          U::runCommand('gltrans', '--id=' . $gl_trans->counter, 'gl_'.$gl_trans->counter.'.log');
          } else {
          if (PUSH_PUSAT) {
          U::runCommand('gltrans', '--id=' . $gl_trans->counter, 'gl_' . $gl_trans->counter . '.log');
          }
          } */
    }

    static function add_bank_trans(
        $type, $trans_no, $bank_act, $ref, $date_, $amount, $person_id, $store = null
    )
    {
        $bank_trans = new BankTrans;
        $bank_trans->type_ = $type;
        $bank_trans->trans_no = $trans_no;
        $bank_trans->bank_id = $bank_act;
        $bank_trans->ref = $ref;
        $bank_trans->tgl = $date_;
        $bank_trans->amount = $amount;
        $bank_trans->id_user = $person_id;
        $bank_trans->store = $store;
        if (!$bank_trans->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bank transaction')) . CHtml::errorSummary($bank_trans));
        }/* else{
          if(PUSH_PUSAT){
          U::runCommand('banktrans', '--id=' . $bank_trans->bank_trans_id,  'banktrans_'.$bank_trans->bank_trans_id.'.log');
          }
          } */
    }

    static function add_comments($type, $type_no, $date_, $memo_)
    {
        if ($memo_ != null && $memo_ != "") {
            $comment = new Comments();
            $comment->type = $type;
            $comment->type_no = $type_no;
            $comment->date_ = $date_;
            $comment->memo_ = $memo_;
            if (!$comment->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Comments')) . CHtml::errorSummary($comment));
            }
        }
    }

    static function add_stock_moves(
        $type, $trans_no, $tran_date, $barang_id, $qty, $reference, $price, $store
    )
    {
        $move = new StockMoves;
        $move->type_no = $type;
        $move->trans_no = $trans_no;
        $move->tran_date = $tran_date;
        $move->price = $price;
        $move->reference = $reference;
        $move->qty = $qty;
//        $move->discount_percent = $discount_percent;
        $move->store = $store;
        $move->barang_id = $barang_id;
        $move->visible = 1;
//        $move->gudang_id = $gudang_id;
        if (!$move->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Stock moves')) . CHtml::errorSummary($move));
        }
        return $move->stock_moves_id;
    }

    static function stockOpname_input($data, $trans_date, $store)
    {
        $command = StockMoves::model()->dbConnection->createCommand("SELECT UUID();");
        $trans_no = $command->queryScalar();
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                $barang = Barang::model()->findByAttributes(array('kode_barang' => $dt['kode_barang']));
                if (!$barang)
                    continue;
//                    throw new Exception("Data was not saved.<br>Kode Barang ".$dt['kode_barang']." has not been registered.");
                $stock_baru = array(
                    'type_no' => -1
                , 'trans_no' => $trans_no
                , 'tran_date' => $trans_date
                , 'price' => 0
                , 'reference' => 'SALDO AWAL'
                , 'qty' => get_number($dt['qty'])
                , 'barang_id' => $barang->barang_id
                , 'store' => $store
                );
                $item = new StockMoves;
                $item->attributes = $stock_baru;
                if (!$item->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Input Stock Opname')) . CHtml::errorSummary($item));
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

    //projection

    //------------- projection report

    //sales override
    static function report_proyeksitransaksioverride_bep($from, $to, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);

        $comm = Yii::app()->db
            ->createCommand("select tc.category_name, SUM(tro.amount) as total from transaksi_override tro 
                                join transaksi_category tc on tro.category_id = tc.category_id
                                WHERE DATE(tro.tdate) >= :from AND DATE(tro.tdate) <= :to AND tro.flag = '1' AND tro.businessunit_id = '$bu'
                                group by tc.category_name");

        /*select tc.category_name, SUM(tro.amount) as total from transaksi_override tro
                                join transaksi_category tc on tro.category_id = tc.category_id
                                WHERE DATE(tro.tdate) >= :from AND DATE(tro.tdate) <= :to AND tro.flag = '1' AND tro.businessunit_id = '$bu'
                                group by tc.category_name*/

        return $comm->queryAll(true, $param);
    }

    //sales override
    static function report_proyeksitransaksioverride($from, $to, $bu, $showdata)
    {
        $query = "";
        $where = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);


        if ($showdata == 'D') {
            $comm = Yii::app()->db
                ->createCommand("select * from transaksi_override
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to AND flag = '1' AND businessunit_id = '$bu'
                                order by docref, tdate DESC");
        } else {
            $comm = Yii::app()->db
                ->createCommand("select transaksi_id, businessunit_id, docref, name, description, DATE_FORMAT(tdate, '%b %Y') as tdate, sum(amount) as amount, flag from transaksi_override
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to AND flag = '1' AND businessunit_id = '$bu'
                                group by MONTH(tdate) order by docref, tdate DESC");
        }

        /*$comm = Yii::app()->db
            ->createCommand("select * from transaksi_override
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to AND flag = '1' AND businessunit_id = '$bu'
                                order by docref, tdate DESC");*/

        /*select transaksi_id, businessunit_id, docref, name, description, DATE_FORMAT(tdate, '%b %Y') as tdate, sum(amount) as amont, flag from transaksi_override
              where businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'
                                group by MONTH(tdate)
                                order by tdate ASC */


        return $comm->queryAll(true, $param);
    }

    //sales
    static function report_proyeksitransaksi($from, $to, $outlet, $grup, $produk, $showby, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);

        if ($showby == "Outlet") {
            $where = "AND kodeoutlet = :outlet ";
            $param[':outlet'] = $outlet;
        }
        if ($showby == "GroupProduct") {
            $where = "AND kodegroup = :kodegroup ";
            $param[':kodegroup'] = $grup;
        }
        if ($showby == "Product") {
            $where = "AND kodeproduk = :kodeproduk ";
            $param[':kodeproduk'] = $produk;
        }

        $comm = Yii::app()->db
            ->createCommand("select * from transaksi_detail_view
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to AND flag = '1' $where
                                AND businessunit_id = '$bu'
                                order by docref, tdate DESC ");

        return $comm->queryAll(true, $param);
    }

    //budget plan
    static function report_budget_plan($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select * from budget_view
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                AND businessunit_id = '$bu'
                                ORDER BY account_code ASC");
        } else {
            $comm = Yii::app()->db->createCommand("select * from budget_view
            WHERE account_id = '$id' 
            AND businessunit_id = '$bu'
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //budget plan permonth
    static function report_budget_plan_permonth($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND a.account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.amount else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.amount else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.amount else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.amount else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.amount else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.amount else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.amount else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.amount else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.amount else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.amount else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.amount else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.amount else 0 end) december,
                                    sum(b.amount) total
                                from budget b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                where DATE(b.tdate) >= :from
                                        and DATE(b.tdate) <= :to
                                        and b.businessunit_id = '$bu' $where
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        } else {
            $comm = Yii::app()->db->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.amount else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.amount else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.amount else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.amount else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.amount else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.amount else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.amount else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.amount else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.amount else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.amount else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.amount else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.amount else 0 end) december,
                                    sum(b.amount) total
                                from budget b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE a.account_id = '$id'
                                    and b.businessunit_id = '$bu'
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }

        return $comm->queryAll(true, $param);
    }

    //budget realization
    static function report_budget_realization($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select * from realization_view
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                AND businessunit_id = '$bu'
                                ORDER BY account_code ASC");
        } else {
            $comm = Yii::app()->db->createCommand("select * from realization_view
            WHERE account_id = '$id' 
            AND businessunit_id = '$bu'
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //budget realization permonth
    static function report_budget_realization_permonth($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND a.account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.amount else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.amount else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.amount else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.amount else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.amount else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.amount else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.amount else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.amount else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.amount else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.amount else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.amount else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.amount else 0 end) december,
                                    sum(b.amount) total
                                from realization b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                where DATE(b.tdate) >= :from
                                        and DATE(b.tdate) <= :to
                                        and b.businessunit_id = '$bu' $where
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        } else {
            $comm = Yii::app()->db->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.amount else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.amount else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.amount else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.amount else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.amount else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.amount else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.amount else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.amount else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.amount else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.amount else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.amount else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.amount else 0 end) december,
                                    sum(b.amount) total
                                from realization b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE a.account_id = '$id'
                                    and b.businessunit_id = '$bu'
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }

        return $comm->queryAll(true, $param);
    }

    //budget analysis
    static function report_budget_analysis($acc, $month, $year, $from, $to, $showby, $bu)
    {

        $join1 = DbCmd::instance()
            ->addSelect("g.account_id , sum(g.amount) as amount")
            ->addFrom("realization g")
            ->addCondition("g.businessunit_id = :businessunit_id")
            ->addGroup("g.account_id");

        $join2 = DbCmd::instance()
            ->addSelect("b.account_id , sum(b.amount) as amount")
            ->addFrom("budget b")
            ->addCondition("b.businessunit_id = :businessunit_id")
            ->addGroup("b.account_id");

        $cmd = DbCmd::instance()
            ->addSelect("a.businessunit_id, a.account_code as 'AccountCode', a.account_name as 'AccountName',
            IFNULL(b.amount,0) as 'AmountBudget', 
            IFNULL(gl.amount,0) as 'AmountRealization' , 
            IFNULL((b.amount - gl.amount),0) as 'Achievement',
            IFNULL(Round((gl.amount/b.amount*100),0),0) as 'AchievementPercent'")
            ->addFrom("account a");


        if ($showby == 'Year') {
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }
        } elseif ($showby == 'Month & Year') {
            if ($month != "") {
                $join1->addCondition("MONTH(g.tdate) = :month");
                $join2->addCondition("MONTH(b.tdate) = :month");
                $cmd->addParams([':month' => $month]);
            }
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }

        } elseif ($showby == 'Account') {
            if ($acc != "") {
                $join1->addCondition("g.account_id = :account_id");
                $join2->addCondition("b.account_id = :account_id");
                $cmd->addCondition("a.account_id = :account_id")
                    ->addParams([':account_id' => $acc]);
            }
        } elseif ($showby == 'Date') {
            if ($from != "") {
                $join1->addCondition("DATE(g.tdate) >= :date_start");
                $join2->addCondition("DATE(b.tdate) >= :date_start");
                $cmd->addParams([':date_start' => $from]);
            }

            if ($to != "") {
                $join1->addCondition("DATE(g.tdate) <= :date_end");
                $join2->addCondition("DATE(b.tdate) <= :date_end");
                $cmd->addParams([':date_end' => $to]);
            }
        }

        $cmd->addLeftJoin($join1->setAs("gl"), "a.account_id = gl.account_id")
            ->addLeftJoin($join2->setAs("b"), "a.account_id = b.account_id")
            ->addCondition("a.businessunit_id = :businessunit_id")
            ->addParams([':businessunit_id' => $bu])
            ->addOrder('a.account_code asc');

        //$cmd->setConnection(Yii::app()->dbprojection);
        return $cmd->queryAll();
    }

    //investment plan
    static function report_investment_plan($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";


        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select *, (select sum(total) from investasi_view ii join account aa on aa.account_id=ii.account_id join `group` gg on gg.group_id = aa.group_id where gg.group_id = a.group_id) as alltotal 
                                from investasi_view i
                                join account a on a.account_id = i.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE DATE(i.tdate) >= :from
                                AND DATE(i.tdate) <= :to
                                AND i.businessunit_id = '$bu'
                                ORDER BY g.group_name, i.account_code ASC");


            /*Select *, (select sum(total) from investasi_view where account_code = i.account_code) as alltotal from investasi_view i
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
            AND businessunit_id = '$bu'
                                ORDER BY account_code ASC*/

        } else {
            $comm = Yii::app()->db->createCommand("select *, (select sum(total) from investasi_view where account_code = i.account_code) as alltotal from investasi_view i
            WHERE account_id = '$id' 
            AND businessunit_id = '$bu'
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //investment plan permonth
    static function report_investment_plan_permonth($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND a.account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.total else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.total else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.total else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.total else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.total else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.total else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.total else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.total else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.total else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.total else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.total else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.total else 0 end) december,
                                    sum(b.total) total
                                from investasi b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                where DATE(b.tdate) >= :from
                                        and DATE(b.tdate) <= :to
                                        and b.businessunit_id = '$bu' $where
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        } else {
            $comm = Yii::app()->db->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.total else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.total else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.total else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.total else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.total else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.total else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.total else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.total else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.total else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.total else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.total else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.total else 0 end) december,
                                    sum(b.total) total
                                from investasi b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE a.account_id = '$id'
                                    and b.businessunit_id = '$bu'
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }

        return $comm->queryAll(true, $param);
    }

    //investment realization
    static function report_investment_realization($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";


        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select *, (select sum(total) from investasi_realisasi_view ii join account aa on aa.account_id=ii.account_id join `group` gg on gg.group_id = aa.group_id where gg.group_id = a.group_id) as alltotal 
                                from investasi_realisasi_view i
                                join account a on a.account_id = i.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE DATE(i.tdate) >= :from
                                AND DATE(i.tdate) <= :to
                                AND i.businessunit_id = '$bu'
                                ORDER BY g.group_name, i.account_code ASC");


            /*Select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal
from investasi_realisasi_view i
join account a on a.account_id = i.account_id
join `group` g on g.group_id = a.group_id
WHERE DATE(i.tdate) >= :from
            AND DATE(i.tdate) <= :to
            AND i.businessunit_id = '$bu'
ORDER BY g.group_name, i.account_code ASC*/

            /*Select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
            AND businessunit_id = '$bu'
                                ORDER BY account_code ASC*/

        } else {
            $comm = Yii::app()->db->createCommand("select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
            WHERE account_id = '$id' 
            AND businessunit_id = '$bu'
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //investment realization permonth
    static function report_investment_realization_permonth($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND a.account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.total else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.total else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.total else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.total else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.total else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.total else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.total else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.total else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.total else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.total else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.total else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.total else 0 end) december,
                                    sum(b.total) total
                                from investasi_realisasi b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                where DATE(b.tdate) >= :from
                                        and DATE(b.tdate) <= :to
                                        and b.businessunit_id = '$bu' $where
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        } else {
            $comm = Yii::app()->db->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.total else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.total else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.total else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.total else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.total else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.total else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.total else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.total else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.total else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.total else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.total else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.total else 0 end) december,
                                    sum(b.total) total
                                from investasi_realisasi b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE a.account_id = '$id'
                                    and b.businessunit_id = '$bu'
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }

        return $comm->queryAll(true, $param);
    }

    //investment analysis
    static function report_investment_analysis($acc, $month, $year, $from, $to, $showby, $bu)
    {

        $join1 = DbCmd::instance()
            ->addSelect("g.account_id , sum(g.total) as amount")
            ->addFrom("investasi_realisasi g")
            ->addCondition("g.businessunit_id = :businessunit_id")
            ->addGroup("g.account_id");

        $join2 = DbCmd::instance()
            ->addSelect("b.account_id , sum(b.total) as amount")
            ->addFrom("investasi b")
            ->addCondition("b.businessunit_id = :businessunit_id")
            ->addGroup("b.account_id");

        $cmd = DbCmd::instance()
            ->addSelect("a.businessunit_id, a.account_code as 'AccountCode', a.account_name as 'AccountName',
            IFNULL(b.amount,0) as 'AmountBudget', 
            IFNULL(gl.amount,0) as 'AmountRealization' , 
            IFNULL((b.amount - gl.amount),0) as 'Achievement',
            IFNULL(Round((gl.amount/b.amount*100),0),0) as 'AchievementPercent'")
            ->addFrom("account a");


        if ($showby == 'Year') {
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }
        } elseif ($showby == 'Month & Year') {
            if ($month != "") {
                $join1->addCondition("MONTH(g.tdate) = :month");
                $join2->addCondition("MONTH(b.tdate) = :month");
                $cmd->addParams([':month' => $month]);
            }
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }

        } elseif ($showby == 'Account') {
            if ($acc != "") {
                $join1->addCondition("g.account_id = :account_id");
                $join2->addCondition("b.account_id = :account_id");
                $cmd->addCondition("a.account_id = :account_id")
                    ->addParams([':account_id' => $acc]);
            }
        } elseif ($showby == 'Date') {
            if ($from != "") {
                $join1->addCondition("DATE(g.tdate) >= :date_start");
                $join2->addCondition("DATE(b.tdate) >= :date_start");
                $cmd->addParams([':date_start' => $from]);
            }

            if ($to != "") {
                $join1->addCondition("DATE(g.tdate) <= :date_end");
                $join2->addCondition("DATE(b.tdate) <= :date_end");
                $cmd->addParams([':date_end' => $to]);
            }
        }

        $cmd->addLeftJoin($join1->setAs("gl"), "a.account_id = gl.account_id")
            ->addLeftJoin($join2->setAs("b"), "a.account_id = b.account_id")
            ->addCondition("a.businessunit_id = :businessunit_id")
            ->addParams([':businessunit_id' => $bu])
            ->addOrder('a.account_code asc');

        //$cmd->setConnection(Yii::app()->dbprojection);
        return $cmd->queryAll();
    }

    //------------------------------------------LABARUGI
    //BEP
    static function report_bep($from, $to, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);


        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("");
        } else {
            $comm = Yii::app()->db->createCommand("");
        }

        return $comm->queryAll(true, $param);
    }

    //budget realization bep
    static function report_budget_realization_bep($from, $to, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);

        $comm = Yii::app()->db
            ->createCommand("select gr.group_name,  a.account_name, SUM(r.amount) total from `group` gr
                                join account a on a.group_id = gr.group_id
                                join realization r on a.account_id = r.account_id
                                WHERE DATE(r.tdate) >= :from
                                AND DATE(r.tdate) <= :to
                                AND r.businessunit_id = '$bu'
                                group by a.account_code, gr.group_code
                                order by gr.group_name asc");


        return $comm->queryAll(true, $param);
    }

    //investment realization bep
    static function report_investment_realization_bep($from, $to, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);

        $comm = Yii::app()->db
            ->createCommand("Select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
                                WHERE DATE(tdate) >= :from 
                                AND DATE(tdate) <= :to 
                                AND businessunit_id = '$bu'
                                ORDER BY account_code ASC");

        /*Select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
                                WHERE YEAR(tdate) = :from
                                AND businessunit_id = '$bu'
                                ORDER BY account_code ASC*/

        return $comm->queryAll(true, $param);
    }

    //-----------------------------------------------ALL BU

    //budget plan
    static function report_budget_plan_all($from, $to, $id)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select * from budget_view
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                ORDER BY account_code ASC");
        } else {
            $comm = Yii::app()->db->createCommand("select * from budget_view
            WHERE account_id = '$id' 
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //budget realization
    static function report_budget_realization_all($from, $to, $id)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select * from realization_view
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                ORDER BY account_code ASC");
        } else {
            $comm = Yii::app()->db->createCommand("select * from realization_view
            WHERE account_id = '$id' 
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //budget analysis
    static function report_budget_analysis_all($acc, $month, $year, $from, $to, $showby)
    {
        $join1 = DbCmd::instance()
            ->addSelect("g.account_id , sum(g.amount) as amount")
            ->addFrom("realization g")
            ->addGroup("g.account_id");

        $join2 = DbCmd::instance()
            ->addSelect("b.account_id , sum(b.amount) as amount")
            ->addFrom("budget b")
            ->addGroup("b.account_id");

        $cmd = DbCmd::instance()
            ->addSelect("a.businessunit_id, a.account_code as 'AccountCode', a.account_name as 'AccountName',
            IFNULL(b.amount,0) as 'AmountBudget', 
            IFNULL(gl.amount,0) as 'AmountRealization' , 
            IFNULL((b.amount - gl.amount),0) as 'Achievement',
            IFNULL(Round((gl.amount/b.amount*100),0),0) as 'AchievementPercent'")
            ->addFrom("account a");


        if ($showby == 'Year') {
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }
        } elseif ($showby == 'Month & Year') {
            if ($month != "") {
                $join1->addCondition("MONTH(g.tdate) = :month");
                $join2->addCondition("MONTH(b.tdate) = :month");
                $cmd->addParams([':month' => $month]);
            }
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }

        } elseif ($showby == 'Account') {
            if ($acc != "") {
                $join1->addCondition("g.account_id = :account_id");
                $join2->addCondition("b.account_id = :account_id");
                $cmd->addCondition("a.account_id = :account_id")
                    ->addParams([':account_id' => $acc]);
            }
        } elseif ($showby == 'Date') {
            if ($from != "") {
                $join1->addCondition("DATE(g.tdate) >= :date_start");
                $join2->addCondition("DATE(b.tdate) >= :date_start");
                $cmd->addParams([':date_start' => $from]);
            }

            if ($to != "") {
                $join1->addCondition("DATE(g.tdate) <= :date_end");
                $join2->addCondition("DATE(b.tdate) <= :date_end");
                $cmd->addParams([':date_end' => $to]);
            }
        }

        $cmd->addLeftJoin($join1->setAs("gl"), "a.account_id = gl.account_id")
            ->addLeftJoin($join2->setAs("b"), "a.account_id = b.account_id")
            ->addOrder('a.account_code asc');

        //$cmd->setConnection(Yii::app()->dbprojection);
        return $cmd->queryAll();
    }

    //investment plan
    static function report_investment_plan_all($from, $to, $id)
    {
        $query = "";
        $where = "";
        $comm = "";


        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select *, (select sum(total) from investasi_view ii join account aa on aa.account_id=ii.account_id join `group` gg on gg.group_id = aa.group_id where gg.group_id = a.group_id) as alltotal 
                                from investasi_view i
                                join account a on a.account_id = i.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE DATE(i.tdate) >= :from
                                AND DATE(i.tdate) <= :to
                                ORDER BY g.group_name, i.account_code ASC");

            /*Select *, (select sum(total) from investasi_view where account_code = i.account_code) as alltotal from investasi_view i
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                ORDER BY account_code ASC*/

        } else {
            $comm = Yii::app()->db->createCommand("select *, (select sum(total) from investasi_view where account_code = i.account_code) as alltotal from investasi_view i
            WHERE account_id = '$id' 
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //investment realization
    static function report_investment_realization_all($from, $to, $id)
    {
        $query = "";
        $where = "";
        $comm = "";


        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select *, (select sum(total) from investasi_realisasi_view ii join account aa on aa.account_id=ii.account_id join `group` gg on gg.group_id = aa.group_id where gg.group_id = a.group_id) as alltotal 
                                from investasi_realisasi_view i
                                join account a on a.account_id = i.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE DATE(i.tdate) >= :from
                                AND DATE(i.tdate) <= :to
                                ORDER BY g.group_name, i.account_code ASC");

            /*Select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                ORDER BY account_code ASC*/

        } else {
            $comm = Yii::app()->db->createCommand("select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
            WHERE account_id = '$id' 
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //investment analysis
    static function report_investment_analysis_all($acc, $month, $year, $from, $to, $showby)
    {

        $join1 = DbCmd::instance()
            ->addSelect("g.account_id , sum(g.amount) as amount")
            ->addFrom("investasi_realisasi g")
            ->addGroup("g.account_id");

        $join2 = DbCmd::instance()
            ->addSelect("b.account_id , sum(b.amount) as amount")
            ->addFrom("investasi b")
            ->addGroup("b.account_id");

        $cmd = DbCmd::instance()
            ->addSelect("a.businessunit_id, a.account_code as 'AccountCode', a.account_name as 'AccountName',
            IFNULL(b.amount,0) as 'AmountBudget', 
            IFNULL(gl.amount,0) as 'AmountRealization' , 
            IFNULL((b.amount - gl.amount),0) as 'Achievement',
            IFNULL(Round((gl.amount/b.amount*100),0),0) as 'AchievementPercent'")
            ->addFrom("account a");


        if ($showby == 'Year') {
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }
        } elseif ($showby == 'Month & Year') {
            if ($month != "") {
                $join1->addCondition("MONTH(g.tdate) = :month");
                $join2->addCondition("MONTH(b.tdate) = :month");
                $cmd->addParams([':month' => $month]);
            }
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }

        } elseif ($showby == 'Account') {
            if ($acc != "") {
                $join1->addCondition("g.account_id = :account_id");
                $join2->addCondition("b.account_id = :account_id");
                $cmd->addCondition("a.account_id = :account_id")
                    ->addParams([':account_id' => $acc]);
            }
        } elseif ($showby == 'Date') {
            if ($from != "") {
                $join1->addCondition("DATE(g.tdate) >= :date_start");
                $join2->addCondition("DATE(b.tdate) >= :date_start");
                $cmd->addParams([':date_start' => $from]);
            }

            if ($to != "") {
                $join1->addCondition("DATE(g.tdate) <= :date_end");
                $join2->addCondition("DATE(b.tdate) <= :date_end");
                $cmd->addParams([':date_end' => $to]);
            }
        }

        $cmd->addLeftJoin($join1->setAs("gl"), "a.account_id = gl.account_id")
            ->addLeftJoin($join2->setAs("b"), "a.account_id = b.account_id")
            ->addOrder('a.account_code asc');

        //$cmd->setConnection(Yii::app()->dbprojection);
        return $cmd->queryAll();
    }

    static function add5digit($counter)
    {
        $no_of_digit = 5;
        $number = $counter;

        $length = strlen((string)$number);
        for ($i = $length; $i < $no_of_digit; $i++) {
            $number = '0' . $number;
        }
        return $number;
    }

    //----------------------------------------------IMPORT DATA PROJECTION

    static function item_import($data, $trans_date, $usr, $bu)
    {
        //$command = Unit::model()->dbConnection->createCommand("SELECT UUID();");
        //$trans_no = $command->queryScalar();
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                //$chck = AssetBarang::model()->findByAttributes(array('kode_barang' => $dt['kode'], 'businessunit_id' => $bu));
                //$chck = AssetBarang::model()->findByAttributes(array('kode_barang' => $dt['kode']));

                //if (!$chck)
                //{
                //continue;
                //                    throw new Exception("Data was not saved.<br>Kode Barang ".$dt['kode_barang']." has not been registered.");
                $item = new AssetBarang;
                /*$item->kode_barang = $dt['kode'];
                $item->nama_barang = $dt['nama'];
                $item->ket = $dt['deskripsi'];
                $item->tipe_barang_id = $dt['tipe'];*/

                $subkode = $dt['subkode'];
                $CategorySub = AssetCategorySub::model()->findByAttributes([
                    'sub_code' => $subkode,
                ]);

                $catcode = $CategorySub->sub_code;
                $catcounter = $CategorySub->counter;
                $last = $catcounter;
                $now = $last + 1;
                $limadigit = U::add5digit($now);
                $buatkodebarang = $catcode . $limadigit;

                $item->kode_barang = $buatkodebarang;
                $item->nama_barang = $dt['nama'];
                $item->ket = $dt['deskripsi'];
                $item->tipe_barang_id = $dt['tipe'];
                $item->category_id = $CategorySub->category_id;
                $item->category_sub_id = $CategorySub->category_sub_id;

                //$item->created_at = new CDbExpression('NOW()');//date('Y-m-d', strtotime($trans_date));
                $item->businessunit_id = $bu;
                //$unit->uploadfile = $usr;
                //$unit->uploaddate = new CDbExpression('NOW()');

                if (!$item->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetBarang')) . CHtml::errorSummary($item));

                //}
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        } finally {
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }


    static function branch_import($data, $trans_date, $usr, $bu)
    {
        //$command = Unit::model()->dbConnection->createCommand("SELECT UUID();");
        //$trans_no = $command->queryScalar();
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                //$chck = AssetBarang::model()->findByAttributes(array('kode_barang' => $dt['kode'], 'businessunit_id' => $bu));
                $chck = Store::model()->findByAttributes(array('store_kode' => $dt['kode'], 'businessunit_id' => $bu));

                if (!$chck) {
                    //continue;
                    //                    throw new Exception("Data was not saved.<br>Kode Barang ".$dt['kode_barang']." has not been registered.");
                    $store = new Store;
                    $store->store_kode = $dt['kode'];
                    $store->nama_store = $dt['nama'];
                    $store->alamat = $dt['alamat'];
                    //$item->created_at = new CDbExpression('NOW()');//date('Y-m-d', strtotime($trans_date));
                    $store->businessunit_id = $bu;
                    //$unit->uploadfile = $usr;
                    //$unit->uploaddate = new CDbExpression('NOW()');

                    if (!$store->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Store')) . CHtml::errorSummary($store));

                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        } finally {
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    static function category_import($data, $trans_date, $usr, $bu)
    {
        //$command = Unit::model()->dbConnection->createCommand("SELECT UUID();");
        //$trans_no = $command->queryScalar();
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                //$chck = AssetBarang::model()->findByAttributes(array('kode_barang' => $dt['kode'], 'businessunit_id' => $bu));
                $chck = AssetCategory::model()->findByAttributes(array('category_code' => $dt['kode']));

                if (!$chck) {
                    //continue;
                    //                    throw new Exception("Data was not saved.<br>Kode Barang ".$dt['kode_barang']." has not been registered.");
                    $cat = new AssetCategory;
                    $cat->category_code = $dt['kode'];
                    $cat->category_name = $dt['nama'];
                    $cat->category_desc = $dt['deskripsi'];
                    //$item->created_at = new CDbExpression('NOW()');//date('Y-m-d', strtotime($trans_date));
                    $cat->businessunit_id = $bu;
                    //$unit->uploadfile = $usr;
                    //$unit->uploaddate = new CDbExpression('NOW()');

                    if (!$cat->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetCategory')) . CHtml::errorSummary($cat));

                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        } finally {
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    static function class_import($data, $trans_date, $usr, $bu)
    {
        //$command = Unit::model()->dbConnection->createCommand("SELECT UUID();");
        //$trans_no = $command->queryScalar();
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                //$chck = AssetBarang::model()->findByAttributes(array('kode_barang' => $dt['kode'], 'businessunit_id' => $bu));
                $chck = AssetGroup::model()->findByAttributes(array('golongan' => $dt['golongan']));

                if (!$chck) {
                    $gol = new AssetGroup;
                    $gol->golongan = $dt['golongan'];
                    $gol->tariff = $dt['tariff'];
                    $gol->period = $dt['period'];
                    $gol->desc = $dt['desc'];
                    $gol->businessunit_id = $bu;

                    if (!$gol->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetGroup')) . CHtml::errorSummary($gol));

                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        } finally {
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    static function businessunit_import($data, $trans_date, $usr)
    {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                $chck = Businessunit::model()->findByAttributes(array('businessunit_code' => $dt['kode']));
                if (!$chck) {
                    //continue;
//                    throw new Exception("Data was not saved.<br>Kode Barang ".$dt['kode_barang']." has not been registered.");

                    $bu = new Businessunit;
                    $bu->businessunit_code = $dt['kode'];
                    $bu->businessunit_name = $dt['nama'];
                    $bu->type = $dt['tipe'];
                    $bu->description = $dt['deskripsi'];
                    $bu->created_at = new CDbExpression('NOW()');
                    $bu->uploadfile = $usr;
                    $bu->uploaddate = new CDbExpression('NOW()');
                    if (!$bu->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Businessunit')) . CHtml::errorSummary($bu));
                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        } finally {
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }


    public function asset_import($data, $trans_date, $usr, $bu, $migrasi)
    {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();

        $dupArray = array();
        $temp = "";

        try {
            foreach ($data as $dt) {
                if ($migrasi) {
                    $chck = AssetDetail::model()->findByAttributes(array('ati' => $dt['activaold'], 'businessunit_id' => $bu, 'active' => '1'));
                } else {
                    $chck = 0;
                }


                if (!$chck) //cek duplikat no activa lama
                {
                    if ($migrasi) {
                        $qty = 1;
                        $activaold = $dt['activaold'];
                    } else {
                        $qty = $dt['qty'];
                        $activaold = "-";
                    }

                    $item = $dt['itemcode'];
                    $branch = $dt['branch'];
                    $tempdate = strtotime($dt['date']);
                    $date = date('Y-m-d', $tempdate);
                    $price = $dt['price'];
                    $class = $dt['class'];
                    $category = $dt['category'];
                    $desc = $dt['desc'];
                    $hide = $dt['hide'];
                    $ppn = $price * 10 / 100;

                    $groupclass = AssetGroup::model()->findByAttributes(array('golongan' => $class));
                    if ($groupclass == "") {
                        $status = false;
                        $msg = "Class '" . $class . "' tidak ada di database";
                    }
                    $barang = AssetBarang::model()->findByAttributes(array('kode_barang' => $item));
                    if ($barang == "") {
                        $status = false;
                        $msg = "Kode barang '" . $item . "' tidak ada di database";
                    }

                    $catid = $barang->category_id;
                    $catsubid = $barang->category_sub_id;

                    $businessunit = Businessunit::model()->findByPk($bu);
                    if ($businessunit == "") {
                        $status = false;
                        $msg = "Business Unit ID '" . $bu . "' tidak ada di database";
                    }

                    $model = new Asset;
                    $model->businessunit_id = $bu;
                    $model->user_id = $usr;
                    $model->asset_group_id = $groupclass->asset_group_id;
                    $model->barang_id = $barang->barang_id;
                    $model->category_id = $catid;

                    $model->asset_name = $barang->nama_barang;
                    $model->date_acquisition = $date;
                    $model->branch = $branch;
                    $model->qty = $qty;
                    $model->price_acquisition = $price;
                    $model->description = $desc;

                    $model->hide = $hide;
                    $model->created_at = new CDbExpression('NOW()');
                    $model->updated_at = new CDbExpression('NOW()');


                    $group = $this->getGolongan($groupclass->asset_group_id);
                    $last = $this->getLastRow($branch) + 1;

                    $cabang = $businessunit->businessunit_code . $branch;
                    $store = Store::model()->findByAttributes(array('store_kode' => $branch));
                    $area = str_pad($store->wilayah_id, 2, '0', STR_PAD_LEFT);
                    $urutan = str_pad($store->store_id, 2, '0', STR_PAD_LEFT);

                    $golongan = $group[0]; //$this->getGolongan($grupid);
                    $tariff = $group[1];
                    $period = $group[2];

                    $year = date('Y', strtotime($date));

                    $docref = $cabang . 'AST' . $year . $last;

                    $ati = $area . $cabang . $urutan . '/' . $golongan;

                    $model->doc_ref = $docref;

                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset')) . CHtml::errorSummary($model));
                    } else {
                        for ($i = 0; $i < $qty; $i++) {
                            $modeldetail = new AssetDetail;
                            $penyusutan = 0;
                            $penyusutantahun = 0;

                            $lastDetail = $this->getLastRowDetail($branch) + 1;

                            $docrefdetail = $docref . '/' . $lastDetail;
                            if ($migrasi) {
                                $modeldetail->ati = $activaold;//$ati.'/'.$lastDetail;
                            } else {
                                $modeldetail->ati = $ati . '/' . $lastDetail;
                            }
                            $modeldetail->businessunit_id = $bu;
                            $modeldetail->user_id = $usr;
                            $modeldetail->category_id = $model->category_id;
                            $modeldetail->category_sub_id = $catsubid;
                            $modeldetail->hide = $model->hide;
                            $modeldetail->asset_id = $model->asset_id;
                            $modeldetail->asset_trans_branch = $branch;
                            $modeldetail->asset_group_id = $groupclass->asset_group_id;
                            $modeldetail->barang_id = $model->barang_id;
                            $modeldetail->docref_other = $model->doc_ref;
                            $modeldetail->docref = $docrefdetail;
                            $modeldetail->asset_trans_name = $model->asset_name;
                            $modeldetail->asset_trans_date = $model->date_acquisition;
                            $modeldetail->asset_trans_price = $model->price_acquisition;
                            $modeldetail->asset_trans_new_price = $model->new_price_acquisition;
                            $modeldetail->description = $model->description;
                            $modeldetail->class = $golongan;
                            $modeldetail->tariff = $tariff;
                            $modeldetail->period = $period;
                            $modeldetail->status = 1;


                            if ($period) {
                                $penyusutan = $modeldetail->asset_trans_price / $period;
                                $penyusutantahun = $penyusutan * $period;
                            }


                            $modeldetail->penyusutanperbulan = $penyusutan;
                            $modeldetail->penyusutanpertahun = $penyusutantahun;

                            $modeldetail->created_at = $model->created_at;
                            $modeldetail->updated_at = $model->updated_at;

                            if (!$modeldetail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset Detail')) . CHtml::errorSummary($modeldetail));
                            } else {
                                $total = $ppn + $modeldetail->asset_trans_price;
                                //GL
                                $gl = new GL();
                                $atiii = $modeldetail->ati;
                                $gl->add_gl_trans(ASSETBELI, $modeldetail->asset_detail_id, $modeldetail->asset_id,
                                    $modeldetail->businessunit_id, $modeldetail->asset_trans_date,
                                    $atiii, "$atiii", $modeldetail->asset_trans_price, $ppn, $total, $modeldetail->asset_trans_price,
                                    $usr, 1, 'beli',
                                    $modeldetail->asset_trans_branch);

                                //ASSET HISTORY
                                $history = new History();
                                $history->add_history_status($bu, $modeldetail->asset_id, $modeldetail->asset_detail_id, $docref,
                                    $modeldetail->ati, $modeldetail->asset_trans_name, $modeldetail->asset_trans_branch,
                                    $modeldetail->asset_trans_price, $modeldetail->asset_trans_new_price,
                                    $modeldetail->class, $modeldetail->tariff, $modeldetail->period,
                                    $modeldetail->penyusutanperbulan, $modeldetail->penyusutanpertahun,
                                    $modeldetail->description, $modeldetail->status);

                                $lastbalance = 0;
                                $lastaccumulated = 0;

                                $days = date_parse_from_format("Y-m-d", $modeldetail->asset_trans_date);
                                $tahun = $days['year'];
                                $bulan = $days['month'] - 1;
                                for ($j = 0; $j < $period; $j++) {
                                    $modelperiode = new AssetPeriode;

                                    $modelperiode->docref = $modeldetail->docref;
                                    $modelperiode->docref_other = $modeldetail->docref_other;
                                    $modelperiode->ati = $modeldetail->ati;
                                    $modelperiode->businessunit_id = $bu;
                                    $modelperiode->asset_detail_id = $modeldetail->asset_detail_id;
                                    $modelperiode->asset_id = $modeldetail->asset_id;
                                    $modelperiode->asset_group_id = $modeldetail->asset_group_id;
                                    $modelperiode->barang_id = $modeldetail->barang_id;
                                    $modelperiode->asset_trans_date = $modeldetail->asset_trans_date;
                                    $modelperiode->asset_trans_price = get_number($modeldetail->asset_trans_price);
                                    $modelperiode->asset_trans_new_price = $modeldetail->asset_trans_new_price;
                                    $modelperiode->asset_trans_name = $modeldetail->asset_trans_name;
                                    $modelperiode->asset_trans_branch = $modeldetail->asset_trans_branch;
                                    $modelperiode->description = $modeldetail->description;

                                    $modelperiode->period = $modeldetail->period;
                                    $modelperiode->class = $modeldetail->class;
                                    $modelperiode->tariff = $modeldetail->tariff;


                                    //$penyusutan = $modelperiode->asset_trans_price * ($modelperiode->tariff/100) / $modelperiode->period;
                                    $penyusutan = $modelperiode->asset_trans_price / $modelperiode->period;

                                    $modelperiode->penyusutanperbulan = $penyusutan;
                                    $lastdeprisiasi = $modelperiode->penyusutanperbulan;

                                    $penyusutantahun = $penyusutan * $modelperiode->period;
                                    $modelperiode->penyusutanpertahun = $penyusutantahun;

                                    if ($lastbalance == 0 || $lastdeprisiasi == 0) {
                                        $modelperiode->balance = $modelperiode->asset_trans_price - $penyusutan;
                                        $lastbalance = $modelperiode->balance;

                                        $modelperiode->akumulasipenyusutan = $penyusutan;
                                        $lastaccumulated = $modelperiode->akumulasipenyusutan;
                                    } else {
                                        $modelperiode->balance = $lastbalance - $lastdeprisiasi;
                                        $lastbalance = $modelperiode->balance;

                                        $modelperiode->akumulasipenyusutan = $lastaccumulated + $penyusutan;
                                        $lastaccumulated = $modelperiode->akumulasipenyusutan;
                                    }
                                    if ($bulan < 12) {
                                        $bulan++;
                                    } else {
                                        $bulan = 1;
                                        $tahun++; // = $tahun + 1;
                                    }

                                    $hari = $days['day'];
                                    $modelperiode->year = $tahun;//($newdate,"Y");//date('Y',$newdate);
                                    $modelperiode->month = $bulan;//date_parse_from_format("m", $newdate);//date('m',$newdate);
                                    $modelperiode->day = $hari;//date_parse_from_format("t", $newdate);//date('t',$newdate);
                                    $modelperiode->tdate = $tahun . '-' . $bulan . '-' . $hari;
                                    $modelperiode->tglpenyusutan = $modelperiode->tdate;


                                    $modelperiode->startdate = $modelperiode->asset_trans_date;
                                    $tmp = $modelperiode->period - 1;
                                    $tmpdate = new DateTime($modelperiode->asset_trans_date);
                                    $tmpstr = '+' . $tmp . 'months';
                                    $end = date_modify($tmpdate, $tmpstr);
                                    $end = $end->format('Y-m-d');
                                    $modelperiode->enddate = $end;

                                    $modelperiode->status = $modeldetail->status;

                                    $modelperiode->created_at = $modeldetail->created_at;
                                    $modelperiode->updated_at = $modeldetail->updated_at;

                                    if (!$modelperiode->save()) {
                                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($modelperiode));
                                    }
                                }
                            }
                        }
                        $status = true;
                        $msg = "Data berhasil di simpan dengan id " . $model->asset_id;
                    }
                } else {
                    array_push($dupArray, $dt['activaold']);//var_dump($msg);
                    $temp = "Dengan data Activa duplikat : ";
                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        } finally {
            $comma_separated = implode("<br>*", $dupArray);
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg . "<br>" . $temp . "<br>*" . $comma_separated));
            Yii::app()->end();
            var_dump($dupArray);
        }
    }

    public function getLastRow($store)
    {
        $businessunit_id = $_COOKIE['businessunitid'];
        $criteria = new CDbCriteria;
        $criteria->compare('active', '1');
        $criteria->compare('branch', $store);
        $criteria->compare('businessunit_id', $businessunit_id);
        $model = Asset::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    public function getLastRowDetail($store)
    {
        $businessunit_id = $_COOKIE['businessunitid'];
        $criteria = new CDbCriteria;
        $criteria->compare('active', '1');
        $criteria->compare('asset_trans_branch', $store);
        $criteria->compare('businessunit_id', $businessunit_id);
        $model = AssetDetail::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    static function getGolongan($id)
    {
        $query = "select golongan as gol, tariff, period, nscc_asset_group.`desc` from nscc_asset_group
                     where asset_group_id = '" . $id . "' limit 1";

        $list = Yii::app()->db->createCommand($query)->queryAll();

        $rs = array();
        foreach ($list as $item) {
            $rs[0] = $item['gol'];
            $rs[1] = $item['tariff'];
            $rs[2] = $item['period'];
            $rs[3] = $item['desc'];
        }
        return $rs;
    }


    static function report_asset($from, $to, $cat, $catsub, $buid, $branch, $status, $showreportasset)
    {

        $comm = "";

        $qwhere = DbCmd::instance();
        $where = $wherecatsub = $wherebranch = $wherestatus = '';
        if ($cat != null) {
            $where = $qwhere->addCondition("ad.category_id = '" . $cat . "'");
        }
        if ($catsub != null) {
            $wherecatsub = $qwhere->addCondition("ad.category_sub_id = '" . $catsub . "' ");
        }
        if ($branch != null) {
            $wherebranch = $qwhere->addCondition("a.branch = '" . $branch . "' ");
        }
        if ($status != null) {
            $wherestatus = $qwhere->addCondition("ad.status = '" . $status . "' ");
        }

        $q1 = DbCmd::instance()->addFrom("nscc_asset_periode aps")
            ->addSelect("aps.balance")
            ->addCondition("aps.asset_detail_id  = ad.asset_detail_id")
            ->addCondition("MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
                                and YEAR(aps.tglpenyusutan) = YEAR(CURDATE())");
        $q1aa = $q1->getQuery();

        $q2 = DbCmd::instance()->addFrom("nscc_asset_detail ad")
            ->addSelect("SUM((" . $q1->getQuery() . ")) as 'balance'")
//            ->addSelect("0 as 'balance'")
            ->addJoin("nscc_asset a", "a.asset_id = ad.asset_id")
            ->addJoin("nscc_asset_category c", "c.category_id = ad.category_id")
            ->addJoin("nscc_asset_category_sub acs", "acs.category_sub_id = ad.category_sub_id")
            ->addCondition("a.businessunit_id = '" . $buid . "' and a.active != 0")
            ->addCondition($where)
            ->addCondition($wherecatsub)
            ->addCondition($wherebranch)
            ->addCondition($wherestatus)
            ->addGroup("a.branch, ad.asset_name")
            ->addOrder("a.branch, ad.ati, ad.asset_name asc");

        if ($showreportasset == 'T') {
            $q2->addSelect("c.category_name as 'category', acs.sub_name,  ad.asset_name as 'name', ad.`desc`,
                        a.branch,
                        SUM(ad.qty) qty,
                        a.date_acquisition as 'acquisitiondate',
                        ad.class,
                        ad.tariff,
                        ad.period, 
                        ad.price_acquisition as 'price'");
        } else {
            $q2->addSelect("ad.ati as 'activa', c.category_name as 'category',  acs.sub_name, ad.asset_name as 'name',ad.desc,
                a.branch,
                a.date_acquisition as 'acquisitiondate',
                ad.class,
                ad.tariff,
                ad.period, ad.status,
                ad.price_acquisition as 'price'");
        }

        if ($from != undefined || $to != undefined) {
            $param = array(':from' => $from, ':to' => $to);
            $q2->addCondition("DATE(a.date_acquisition) >= :from and DATE(a.date_acquisition) <= :to");
        } else
            $q2->addCondition("ad.status != 0");

        $comm = DbCmd::instance()->addFrom("(" . $q2->getQuery() . ") as asset")
            ->addParams($param);

        if ($showreportasset == 'T') {
            $comm->addSelect("category,sub_name, name as 'asset', branch, qty,  price as 'total', ROUND(IFNULL(balance, price), 2) as 'balance'");
        } else {
            $comm->addSelect("activa, category, sub_name,name, status, `desc`,branch,acquisitiondate, class, tariff, period, ROUND(price, 2) as price, ROUND(IFNULL(balance, price))  as 'balance'");
        }

        $aaa = $comm->getQuery();
        return $comm->queryAll();
    }

    static function report_asset_detail($from, $to, $cat, $catsub, $buid, $branch, $status)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $wherestatus = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);
        //$param[':buid'] = $buid;

        if ($cat != null) {
            $where = "AND ad.category_id = '" . $cat . "'";
            //$param[':category_id'] = $cat;
        }
        if ($catsub != null) {
            $wherecatsub = "AND ad.category_sub_id = '" . $catsub . "' ";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "and ad.asset_trans_branch = '" . $branch . "'";
            //$param[':category_id'] = $cat;
        }
        if ($status != null) {
            $wherestatus = "and ad.status = '" . $status . "'";
            //$param[':category_id'] = $cat;
        }

        if ($from != undefined || $to != undefined) {
            /*$comm = Yii::app()->db->createCommand("select a.docref_other as 'docref', a.asset_trans_name as 'asset', count(a.asset_trans_price) as 'qty', sum(a.asset_trans_price) as 'total'
                                                    from nscc_asset_detail_view a
                                                    where a.`status` = 1
                                                    and a.businessunit_id = :buid
                                                    and date(a.asset_trans_date) >= :from
                                                    and date(a.asset_trans_date) <= :to
                                                    $where
                                                    group by a.docref_other
                                                    order by a.asset_trans_name asc
                                                    ");*/

            /*select ad.ati as 'activa', ad.asset_trans_price as 'price',
IF(ap.balance != '0',ap.balance,0) as 'depriciation price'
from nscc_asset_detail ad
join nscc_asset_periode ap on ap.asset_detail_id = ad.asset_detail_id
where MONTH(ap.tglpenyusutan) = MONTH(CURDATE())
            AND YEAR(ap.tglpenyusutan) = YEAR(CURDATE())
            AND ad.businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'
            AND ap.businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'*/

            /* select ad.ati as 'activa',  ad.asset_trans_name as 'name', ad.description,
 ad.asset_trans_date as 'acquisitiondate',
 ad.asset_trans_price as 'price',
 IF(MONTH(ap.asset_trans_date) = MONTH(CURDATE())  and  YEAR(ap.asset_trans_date) = YEAR(CURDATE()), ap.asset_trans_price ,ap.balance) as 'depriciationprice'
 from nscc_asset_detail ad
 join nscc_asset_periode ap on ap.asset_detail_id = ad.asset_detail_id
 where MONTH(ap.tglpenyusutan) = MONTH(CURDATE())
             AND YEAR(ap.tglpenyusutan) = YEAR(CURDATE())*/
            /*            select ad.ati as 'activa', c.category_name as 'category',  ad.asset_trans_name as 'name',
            ad.asset_trans_branch as 'branch',
            ad.asset_trans_date as 'acquisitiondate',
            ad.class,
            ad.period,
            ad.asset_trans_price as 'price',
            (select aps.balance from nscc_asset_periode aps
            where aps.asset_detail_id  = ad.asset_detail_id
                        and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())
                        and YEAR(aps.tglpenyusutan) = YEAR(CURDATE())) as 'balance'
            from nscc_asset_detail ad
            join nscc_asset_category c on c.category_id = ad.category_id
            join nscc_asset_barang b on b.barang_id = ad.barang_id
            order by ad.asset_trans_name, ad.ati asc*/

            $comm = Yii::app()->db->createCommand("select activa, category, sub_name,name, kode, status, description,branch,acquisitiondate, class, tariff, period, ROUND(price, 2) as price, ROUND(IFNULL(balance, price))  as 'balance' from (

    select ad.ati as 'activa', c.category_name as 'category',  acs.sub_name, ad.asset_trans_name as 'name', b.kode_barang as 'kode',ad.description,
    ad.asset_trans_branch as 'branch',
    ad.asset_trans_date as 'acquisitiondate',
    ad.class,
    ad.tariff,
    ad.period, ad.status,
    ad.asset_trans_price as 'price',
    (select aps.balance from nscc_asset_periode aps
    where aps.asset_detail_id  = ad.asset_detail_id
    and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
    and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_detail_id) as 'balance'
    from nscc_asset_detail ad
    join nscc_asset_category c on c.category_id = ad.category_id
    join nscc_asset_barang b on b.barang_id = ad.barang_id
    join nscc_asset_category_sub acs on acs.category_sub_id = ad.category_sub_id
    where ad.businessunit_id = '" . $buid . "'
                            and ad.active != 0
    and DATE(ad.asset_trans_date) >= :from
    and DATE(ad.asset_trans_date) <= :to
    $where
    $wherecatsub
    $wherebranch
    $wherestatus
    order by ad.asset_trans_name, ad.ati asc ) as asset order by activa, name asc
    "); //and ad.status != 0
        } else {
            $comm = Yii::app()->db->createCommand("select activa, category, sub_name, name, kode, status, description,branch,acquisitiondate, class, tariff, period, ROUND(price, 2) as price, ROUND(IFNULL(balance, price)) as 'balance' from 
( select ad.ati as 'activa', c.category_name as 'category',  acs.sub_name, ad.asset_trans_name as 'name', b.kode_barang as 'kode',ad.description,
ad.asset_trans_branch as 'branch',
ad.asset_trans_date as 'acquisitiondate',
ad.class,
ad.period, ad.status,
ad.tariff,
ad.asset_trans_price as 'price',
(select aps.balance from nscc_asset_periode aps
where aps.asset_detail_id  = ad.asset_detail_id
and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_detail_id) as 'balance'
from nscc_asset_detail ad
join nscc_asset_category c on c.category_id = ad.category_id
join nscc_asset_barang b on b.barang_id = ad.barang_id
join nscc_asset_category_sub acs on acs.category_sub_id = ad.category_sub_id
where ad.businessunit_id = '" . $buid . "'
                            and ad.active != 0
$where
$wherecatsub
$wherebranch
$wherestatus
order by ad.asset_trans_name, ad.ati asc ) as asset order by activa, name asc
"); //and ad.status != 0
        }

        return $comm->queryAll(true, $param);
    }

    static function report_asset_nonactive($from, $to, $cat, $buid, $branch)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);
        //$param[':buid'] = $buid;

        if ($cat != null) {
            $where = "AND ad.category_id = '" . $cat . "' ";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "and ass.branch = '" . $branch . "'";
            //$param[':category_id'] = $cat;
        }

        $query = "select  category, name as 'asset', kode, branch, qty,  price as 'total', ROUND(IFNULL(balance, price), 2) as 'balance' from (
                    
                        select c.category_name as 'category',  ad.asset_name as 'name', b.kode_barang as 'kode',ass.description,
                        ass.branch as 'branch',
                        COUNT(ad.asset_name) as 'qty',
                        ass.date_acquisition as 'acquisitiondate',
                        ad.class,
                        ad.tariff,
                        ad.period, 
                        SUM(ad.price_acquisition) as 'price',
                        SUM((select aps.balance from nscc_asset_periode aps
                        where aps.asset_detail_id  = ad.asset_detail_id
                        and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
                        and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()))) as 'balance'
                        from nscc_asset_detail ad
                        join nscc_asset_category c on c.category_id = ad.category_id
                        join nscc_asset_barang b on b.barang_id = ad.barang_id
                        join nscc_asset ass on ass.asset_id = ad.asset_id";
        if ($from != undefined || $to != undefined) {
            /*$comm = Yii::app()->db->createCommand("select a.docref_other as 'docref', a.asset_trans_name as 'asset', count(a.asset_trans_price) as 'qty', sum(a.asset_trans_price) as 'total'
                                                    from nscc_asset_detail_view a
                                                    where a.`status` = 1
                                                    and a.businessunit_id = :buid
                                                    and date(a.asset_trans_date) >= :from
                                                    and date(a.asset_trans_date) <= :to
                                                    $where
                                                    group by a.docref_other
                                                    order by a.asset_trans_name asc
                                                    ");*/

            /*select b.nama_barang as 'asset', c.category_name as 'category',  COUNT(ad.asset_trans_name) as 'qty', SUM(ad.asset_trans_price) as 'total' from nscc_asset_detail ad
                                                        join nscc_asset_category c on c.category_id = ad.category_id
                                                        join nscc_asset_barang b on b.barang_id = ad.barang_id
                                                        where ad.businessunit_id = :buid
            and DATE(ad.asset_trans_date) >= :from
            and DATE(ad.asset_trans_date) <= :to
                                                        $where
                                                        group by ad.asset_trans_name
                                                        order by ad.asset_trans_name asc*/


            $comm = Yii::app()->db->createCommand("
                        $query
                            where ass.businessunit_id = '" . $buid . "'
                            and ass.active = '0'
                            and DATE(ass.date_acquisition) >= :from
                            and DATE(ass.date_acquisition) <= :to
                            $where
                            $wherebranch
                        group by b.kode_barang
                        order by ad.asset_name, ad.ati asc ) as asset");
        } else {
            $comm = Yii::app()->db->createCommand("
                        $query
                            where ass.businessunit_id = '" . $buid . "'
                            and ass.active = '0'
                            $where
                            $wherebranch
                        group by b.kode_barang
                        order by ad.asset_name, ad.ati asc ) as asset");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_asset_detail_nonactive($from, $to, $cat, $buid, $branch)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);
        //$param[':buid'] = $buid;

        if ($cat != null) {
            $where = "AND ad.category_id = '" . $cat . "'";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "and ass.branch = '" . $branch . "'";
            //$param[':category_id'] = $cat;
        }

        $query = "select activa, category, name, kode, status, description,branch,acquisitiondate, class, tariff, period, ROUND(price, 2) as price, ROUND(IFNULL(balance, price))  as 'balance' from (
        select ad.ati as 'activa', c.category_name as 'category',  ad.asset_name as 'name', b.kode_barang as 'kode',ass.description,
        ass.branch as 'branch',
        ass.date_acquisition as 'acquisitiondate',
        ad.class,
        ad.tariff,
        ad.period, ad.status,
        ad.price_acquisition as 'price',
        (select aps.balance from nscc_asset_periode aps
        where aps.asset_detail_id  = ad.asset_detail_id
        and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
        and YEAR(aps.tglpenyusutan) = YEAR(CURDATE())) as 'balance'
        from nscc_asset_detail ad
        join nscc_asset_category c on c.category_id = ad.category_id
        join nscc_asset_barang b on b.barang_id = ad.barang_id
        join nscc_asset ass on ass.asset_id = ad.asset_id";

        if ($from != undefined || $to != undefined) {
            /*$comm = Yii::app()->db->createCommand("select a.docref_other as 'docref', a.asset_trans_name as 'asset', count(a.asset_trans_price) as 'qty', sum(a.asset_trans_price) as 'total'
                                                    from nscc_asset_detail_view a
                                                    where a.`status` = 1
                                                    and a.businessunit_id = :buid
                                                    and date(a.asset_trans_date) >= :from
                                                    and date(a.asset_trans_date) <= :to
                                                    $where
                                                    group by a.docref_other
                                                    order by a.asset_trans_name asc
                                                    ");*/

            /*select ad.ati as 'activa', ad.asset_trans_price as 'price',
IF(ap.balance != '0',ap.balance,0) as 'depriciation price'
from nscc_asset_detail ad
join nscc_asset_periode ap on ap.asset_detail_id = ad.asset_detail_id
where MONTH(ap.tglpenyusutan) = MONTH(CURDATE())
            AND YEAR(ap.tglpenyusutan) = YEAR(CURDATE())
            AND ad.businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'
            AND ap.businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'*/

            /* select ad.ati as 'activa',  ad.asset_trans_name as 'name', ad.description,
 ad.asset_trans_date as 'acquisitiondate',
 ad.asset_trans_price as 'price',
 IF(MONTH(ap.asset_trans_date) = MONTH(CURDATE())  and  YEAR(ap.asset_trans_date) = YEAR(CURDATE()), ap.asset_trans_price ,ap.balance) as 'depriciationprice'
 from nscc_asset_detail ad
 join nscc_asset_periode ap on ap.asset_detail_id = ad.asset_detail_id
 where MONTH(ap.tglpenyusutan) = MONTH(CURDATE())
             AND YEAR(ap.tglpenyusutan) = YEAR(CURDATE())*/
            /*            select ad.ati as 'activa', c.category_name as 'category',  ad.asset_trans_name as 'name',
            ad.asset_trans_branch as 'branch',
            ad.asset_trans_date as 'acquisitiondate',
            ad.class,
            ad.period,
            ad.asset_trans_price as 'price',
            (select aps.balance from nscc_asset_periode aps
            where aps.asset_detail_id  = ad.asset_detail_id
                        and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())
                        and YEAR(aps.tglpenyusutan) = YEAR(CURDATE())) as 'balance'
            from nscc_asset_detail ad
            join nscc_asset_category c on c.category_id = ad.category_id
            join nscc_asset_barang b on b.barang_id = ad.barang_id
            order by ad.asset_trans_name, ad.ati asc*/

        $comm = Yii::app()->db->createCommand("
            $query            
            where ass.businessunit_id = '" . $buid . "'
            and ass.active = '0'
            and DATE(ass.date_acquisition) >= :from 
            and DATE(ass.date_acquisition) <= :to 
            $where 
            $wherebranch 
            order by ad.asset_name, ad.ati asc ) as asset order by activa, name asc
    ");
        } else {
            $comm = Yii::app()->db->createCommand("
                $query 
                where ass.businessunit_id = '" . $buid . "'
                and ass.active = '0'
                $where 
                $wherebranch 
                order by ad.asset_name, ad.ati asc ) as asset order by activa, name asc
            ");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_asset_depriciation($from, $to, $ati = "", $id)
    {
        $comm = "";
        $where = "";
        $param = [];
        if ($ati != null) {
            $where .= "ati = :ati";
            $param[':ati'] = $ati;
        }

        $query = "select doc_ref, ati, asset_name, 
store_kode,price_acquisition, new_price_acquisition, 
DATE_FORMAT(date_acquisition, '%d %M %Y') as asset_trans_date, description, class,
 period, tariff, DATE_FORMAT(tglpenyusutan, '%d %M %Y') as tglpenyusutan, 
 ROUND(ap.penyusutanperbulan, 2) penyusutanperbulan, 
 ROUND(ap.akumulasipenyusutan, 2) akumulasipenyusutan, ap.penyusutanpertahun, 
 ROUND(ap.balance, 2) balance, status from nscc_asset_periode ap
left join nscc_asset_detail ad on ad.asset_detail_id = ap.asset_detail_id
left join nscc_asset ass on ass.asset_id= ap.asset_id";

        if ($from != undefined || $to != undefined) {
            $param[':from'] = $from;
            $param[':to'] = $to;
            $comm = Yii::app()->db->createCommand("
            $query 
            WHERE DATE(ap.tglpenyusutan) >= :from AND DATE(ap.tglpenyusutan) <= :to 
            AND $where 
            ORDER BY STR_TO_DATE(ap.tglpenyusutan,'%d %M %Y') ASC");
        } else {
            $comm = Yii::app()->db->createCommand("
            $query 
            WHERE #ap.asset_detail_id = '$id' 
            $where
            ORDER BY STR_TO_DATE(ap.tglpenyusutan,'%d %M %Y') ASC");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_jurnalasset($from, $to, $buid, $branch)
    {
        $query = "";
        $where = "";
        $comm = "";

        if ($branch != null) {
            $where = "and gl.store = '" . $branch . "'";
            //$param[':category_id'] = $cat;
        }
        $param = array(':from' => $from, ':to' => $to);
        //$param[':buid'] = $buid;

        /*if ($cat != null) {
            $where = "AND ad.category_id = :category_id ";
            $param[':category_id'] = $cat;
        }*/

        if ($from != 'undefined' || $to != 'undefined') {

            /*$comm = Yii::app()->db->createCommand("select a.docref_other as 'docref', a.asset_trans_name as 'asset', count(a.asset_trans_price) as 'qty', sum(a.asset_trans_price) as 'total'
                                                    from nscc_asset_detail_view a
                                                    where a.`status` = 1
                                                    and a.businessunit_id = :buid
                                                    and date(a.asset_trans_date) >= :from
                                                    and date(a.asset_trans_date) <= :to
                                                    $where
                                                    group by a.docref_other
                                                    order by a.asset_trans_name asc
                                                    ");*/
            /*select
gl.memo_, gl.tran_date,
gl.desc as 'status' ,gl.amount as price, gl.depreciation_price as 'depreciationprice',
if(gl.amount >= 0,gl.amount,0) debit,
if(gl.amount < 0,-gl.amount,0) kredit

from nscc_gl_trans gl
where gl.businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'
order by gl.memo_ asc*/
            /*$comm = Yii::app()->db->createCommand("select b.nama_barang as 'asset', c.category_name as 'category',  COUNT(ad.asset_trans_name) as 'qty', SUM(ad.asset_trans_price) as 'total' from nscc_asset_detail ad
                                                        join nscc_asset_category c on c.category_id = ad.category_id
                                                        join nscc_asset_barang b on b.barang_id = ad.barang_id
                                                        where ad.businessunit_id = :buid
                                                        and DATE(ad.asset_trans_date) >= :from
                                                        and DATE(ad.asset_trans_date) <= :to
                                                        $where
                                                        group by ad.asset_trans_name
                                                        order by ad.asset_trans_name asc");*/

            /*select
                                                        gl.memo_, ad.asset_trans_name, ab.kode_barang, ad.description, gl.tran_date, gl.tdate,
                                                        gl.desc as 'status' ,gl.amount as price, gl.depreciation_price as 'depreciationprice',
                                                        if(gl.amount >= 0,gl.amount,0) debit,
                                                        if(gl.amount < 0,-gl.amount,0) kredit,
                                                        gl.depreciation_price - gl.amount as balance
                                                        from nscc_gl_trans gl
                                                        join nscc_asset_detail ad on ad.asset_detail_id = gl.type_no
                                                        join nscc_asset_barang ab on ab.barang_id = ad.barang_id
                                                        where gl.businessunit_id = '".$buid."'
            and date(gl.tran_date) >= :from
            and date(gl.tran_date) <= :to
                                                        order by gl.memo_,gl.tdate asc*/

            $query = "select *, 
if((kredit - depreciationprice) < 0, 0, ROUND((kredit - depreciationprice),2)  ) as 'profit',
if((kredit - depreciationprice) >= 0, 0, if(kredit != 0,ROUND(-(kredit - depreciationprice),2),0)  ) as 'loss'
from (
	select  
	gl.memo_, ad.asset_name, ab.kode_barang,   gl.tran_date, DATE(gl.tdate) as tdate,
	gl.desc as 'status' ,gl.amount as price, gl.ppn, gl.total, gl.depreciation_price as 'depreciationprice', 
	if(gl.amount >= 0,gl.amount + gl.ppn,0) debit,
	if(gl.amount < 0,-gl.amount,0) kredit

	from nscc_gl_trans gl
	join nscc_asset_detail ad on ad.asset_detail_id = gl.type_no
	join nscc_asset_barang ab on ab.barang_id = ad.barang_id
    join nscc_asset ass on ass.asset_id = ad.asset_id	
	where gl.businessunit_id = '" . $buid . "'";

            $comm = Yii::app()->db->createCommand("
            $query 
	        $where
                    and date(gl.tran_date) >= :from
                    and date(gl.tran_date) <= :to
                    and gl.visible != 0
                    order by gl.memo_,gl.tdate asc
            ) 
            as jurnal");
        } else {
            $comm = Yii::app()->db->createCommand("
                $query 
                $where
                and gl.visible != 0
                        order by gl.memo_,gl.tdate asc
                ) 
            as jurnal");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_rentasset($from, $to, $cat, $buid, $branch)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $comm = "";

        //$param[':buid'] = ;
        $param = array(':from' => $from, ':to' => $to);
        if ($cat != null) {
            $where = "AND rt.category = '" . $cat . "'";
            //$param[':category_id'] = $cat;
        }
        /*if ($cat != null) {
            $where = "AND rt.category = '".$cat."'";
            //$param[':category_id'] = $cat;
        }*/
        if ($branch != null) {
            $wherebranch = "and rt.store = '" . $branch . "'";
            //$param[':category_id'] = $cat;
        }
        $assettype = ASSETSEWA;

        if ($from != 'undefined' || $to != 'undefined') {
            //$param = array(':from' => $from, ':to' => $to);

            $comm = Yii::app()->db->createCommand("select rt.*, DATE(rt.tdate) as 'ttdate' from nscc_rent_trans rt
                                                where rt.businessunit_id = '" . $buid . "'
                                                and rt.`type` = '" . $assettype . "'
                                                and date(rt.tdate) >= :from
                                                and date(rt.tdate) <= :to
                                                $where 
                                                $wherebranch
                                                ");
        } else {
            $comm = Yii::app()->db->createCommand("select rt.*, DATE(rt.tdate) as 'ttdate' from nscc_rent_trans rt
                                                where rt.businessunit_id = '" . $buid . "'
                                                and rt.`type` = '" . $assettype . "'
                                                $where
                                                $wherebranch
                                                ");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_deprisiasiasset($from, $to, $buid, $branch, $cat_id, $cat_sub_id)
    {
        $query = "";
        $where = "";
        $wherecat = "";
        $wheresub = "";
        $comm = "";

        if ($branch != null) {
            $where = "and apv.asset_trans_branch = '" . $branch . "'";
            //$param[':category_id'] = $cat;
        }
        if ($cat_id != null) {
            $wherecat = "and adv.category_id = '" . $cat_id . "'";
            //$param[':category_id'] = $cat;
        }
        if ($cat_sub_id != null) {
            $wheresub = "and adv.category_sub_id = '" . $cat_sub_id . "'";
            //$param[':category_id'] = $cat;
        }
        $param = array(':from' => $from, ':to' => $to);

        if ($from != 'undefined' || $to != 'undefined') {
            $comm = Yii::app()->db->createCommand("select activa, asset, period, tdate, depreciationstart, depreciationend, acquisitionprice, ROUND(IFNULL(depreciationpermonth, 0),2) as depreciationpermonth, ROUND(IFNULL(depreciationaccumulation, 0),2) as depreciationaccumulation, ROUND(IFNULL(nilaibuku, 0),2) as nilaibuku, ROUND(IFNULL(depreciationaccumulationbyparam, 0),2) as depreciationaccumulationbyparam, ROUND(IFNULL(nilaibukubyparam, 0),2) as nilaibukubyparam, ROUND(IFNULL(akumulasibyparam, 0),2) as akumulasibyparam from
(select 
 apv.ati as activa,
 apv.asset_trans_name as asset, 
 apv.period,
 date(apv.asset_trans_date) tdate, 
 date(apv.startdate) as depreciationstart,
 date(apv.enddate) as depreciationend,
 apv.asset_trans_price as acquisitionprice, 
 apv.penyusutanperbulan as depreciationpermonth, 
  (select aps.akumulasipenyusutan from nscc_asset_periode aps
    where aps.asset_detail_id  = apv.asset_detail_id
    and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
    and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_detail_id) as 'depreciationaccumulation',
  (select aps.balance from nscc_asset_periode aps
    where aps.asset_detail_id  = apv.asset_detail_id
    and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
    and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_detail_id) as 'nilaibuku',
sum(apv.penyusutanperbulan) as 'depreciationaccumulationbyparam',
(select aps.balance from nscc_asset_periode aps
    where aps.asset_detail_id  = apv.asset_detail_id
    and MONTH(aps.tglpenyusutan) = MONTH(:to)  
    and YEAR(aps.tglpenyusutan) = YEAR(:to) group by aps.asset_detail_id) as 'nilaibukubyparam',
        (select aps.akumulasipenyusutan from nscc_asset_periode aps
    where aps.asset_detail_id  = apv.asset_detail_id
    and MONTH(aps.tglpenyusutan) = MONTH(:to)  
    and YEAR(aps.tglpenyusutan) = YEAR(:to) group by aps.asset_detail_id) as 'akumulasibyparam'
from nscc_asset_periode apv
join nscc_asset_detail_view adv on apv.asset_detail_id = adv.asset_detail_id
where date(apv.tglpenyusutan) >= :from
and date(apv.tglpenyusutan) <= :to
and apv.businessunit_id = '" . $buid . "'
$where $wherecat $wheresub
group by apv.ati
order by apv.asset_trans_name, apv.asset_trans_date) as deprisiasi
ORDER BY CAST((SUBSTRING_INDEX(activa, '/', -1)) AS UNSIGNED), activa asc");
        } else {
            $comm = Yii::app()->db->createCommand("select activa, asset, period, tdate, depreciationstart, depreciationend, acquisitionprice, ROUND(IFNULL(depreciationpermonth, 0),2) as depreciationpermonth, ROUND(IFNULL(depreciationaccumulation, 0),2) as depreciationaccumulation, ROUND(IFNULL(nilaibuku, 0),2) as nilaibuku, ROUND(IFNULL(depreciationaccumulationbyparam, 0),2) as depreciationaccumulationbyparam, ROUND(IFNULL(nilaibukubyparam, 0),2) as nilaibukubyparam, ROUND(IFNULL(akumulasibyparam, 0),2) as akumulasibyparam from
(select 
 apv.ati as activa,
 apv.asset_trans_name as asset, 
 apv.period,
 date(apv.asset_trans_date) tdate, 
 date(apv.startdate) as depreciationstart,
 date(apv.enddate) as depreciationend,
 apv.asset_trans_price as acquisitionprice, 
 apv.penyusutanperbulan as depreciationpermonth, 
  (select aps.akumulasipenyusutan from nscc_asset_periode aps
    where aps.asset_detail_id  = apv.asset_detail_id
    and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
    and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_detail_id) as 'depreciationaccumulation',
  (select aps.balance from nscc_asset_periode aps
    where aps.asset_detail_id  = apv.asset_detail_id
    and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
    and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_detail_id) as 'nilaibuku',
    sum(apv.penyusutanperbulan) as 'depreciationaccumulationbyparam',
    (select aps.balance from nscc_asset_periode aps
    where aps.asset_detail_id  = apv.asset_detail_id
    and MONTH(aps.tglpenyusutan) = MONTH(:to)  
    and YEAR(aps.tglpenyusutan) = YEAR(:to) group by aps.asset_detail_id) as 'nilaibukubyparam',
    (select aps.akumulasipenyusutan from nscc_asset_periode aps
    where aps.asset_detail_id  = apv.asset_detail_id
    and MONTH(aps.tglpenyusutan) = MONTH(:to)  
    and YEAR(aps.tglpenyusutan) = YEAR(:to) group by aps.asset_detail_id) as 'akumulasibyparam'
from nscc_asset_periode apv
join nscc_asset_detail_view adv on apv.asset_detail_id = adv.asset_detail_id
where date(apv.tglpenyusutan) >= :from
and date(apv.tglpenyusutan) <= :to
and apv.businessunit_id = '" . $buid . "'
$where $wherecat $wheresub
group by apv.ati
order by apv.asset_trans_name, apv.asset_trans_date) as deprisiasi
ORDER BY CAST((SUBSTRING_INDEX(activa, '/', -1)) AS UNSIGNED), activa asc");
        }

        return $comm->queryAll(true, $param);
    }

    //untuk cabang
    static function report_showasset($from, $to, $buid, $branch, $cat, $catsub)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $comm = "";

        if ($cat != null) {
            $where = "AND ad.category_id = '" . $cat . "'";
            //$param[':category_id'] = $cat;
        }
        if ($catsub != null) {
            $wheresub = "AND ad.category_sub_id = '" . $catsub . "'";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "and ad.asset_trans_branch = '" . $branch . "'";
            //$param[':category_id'] = $cat;
        }
        $param = array(':from' => $from, ':to' => $to);

        if ($from != 'undefined' || $to != 'undefined') {
            $comm = Yii::app()->db->createCommand("select ad.ati as 'activa', ad.asset_trans_name as 'assetname', ab.kode_barang as 'assetcode' from nscc_asset_detail ad
join nscc_asset_barang ab 
on ab.barang_id = ad.barang_id
where date(ad.asset_trans_date) >= :from
and date(ad.asset_trans_date) <= :to
and ad.businessunit_id  = '" . $buid . "'
and ad.active = '1'
$where
$wheresub
$wherebranch
order by ad.asset_trans_name asc");
        } else {
            $comm = Yii::app()->db->createCommand("select ad.ati as 'activa', ad.asset_trans_name as 'assetname', ab.kode_barang as 'assetcode' from nscc_asset_detail ad
join nscc_asset_barang ab 
on ab.barang_id = ad.barang_id
where ad.businessunit_id  = '" . $buid . "'
and ad.active = '1'
$where
$wheresub
$wherebranch
order by ad.asset_trans_name asc");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_asset_non_aktiva($from, $to, $cat, $subcat, $buid, $branch)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);
        //$param[':buid'] = $buid;

        if ($cat != null) {
            $wherecat = "AND ana.category_id = '" . $cat . "' ";
            //$param[':category_id'] = $cat;
        }
        if ($subcat != null) {
            $wheresubcat = "AND ana.category_sub_id = '" . $subcat . "' ";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "AND ana.store_kode = '" . $branch . "'";
            //$param[':category_id'] = $cat;
        }

        if ($from != undefined || $to != undefined) {

            $comm = Yii::app()->db->createCommand("select ab.kode_barang, ab.nama_barang, ac.category_name, acs.sub_name, sum(ana.qty) as 'qty',sum(ana.price) as 'price', ana.note as 'description' from nscc_asset_non_ati ana
join nscc_asset_barang ab on ab.barang_id = ana.barang_id
join nscc_asset_category ac on ac.category_id = ana.category_id
join nscc_asset_category_sub acs on acs.category_sub_id = ana.category_sub_id
where DATE(ana.tgl) >= :from
and DATE(ana.tgl) <= :to
and ana.businessunit_id = '" . $buid . "'
$wherecat
$wheresubcat
$wherebranch
and ana.visible = 1");
        } else {
            $comm = Yii::app()->db->createCommand("select ab.kode_barang, ab.nama_barang, ac.category_name, acs.sub_name, sum(ana.qty) as 'qty',sum(ana.price) as 'price', ana.note as 'description' from nscc_asset_non_ati ana
join nscc_asset_barang ab on ab.barang_id = ana.barang_id
join nscc_asset_category ac on ac.category_id = ana.category_id
join nscc_asset_category_sub acs on acs.category_sub_id = ana.category_sub_id
where ana.businessunit_id = '" . $buid . "'
$wherecat
$wheresubcat
$wherebranch
and ana.visible = 1");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_asset_detail_non_aktiva($from, $to, $cat, $subcat, $buid, $branch)
    {
        $query = "";
        $wherecat = "";
        $wheresubcat = "";
        $wherebranch = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);
        //$param[':buid'] = $buid;

        if ($cat != null) {
            $wherecat = "AND ana.category_id = '" . $cat . "' ";
            //$param[':category_id'] = $cat;
        }
        if ($subcat != null) {
            $wheresubcat = "AND ana.category_sub_id = '" . $subcat . "' ";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "AND ana.store_kode = '" . $branch . "'";
            //$param[':category_id'] = $cat;
        }

        if ($from != undefined || $to != undefined) {

            $comm = Yii::app()->db->createCommand("select ana.doc_ref, ab.kode_barang, ab.nama_barang, ac.category_name, acs.sub_name, ana.qty, ana.price, ana.note as 'description' from nscc_asset_non_ati ana
join nscc_asset_barang ab on ab.barang_id = ana.barang_id
join nscc_asset_category ac on ac.category_id = ana.category_id
join nscc_asset_category_sub acs on acs.category_sub_id = ana.category_sub_id
where DATE(ana.tgl) >= :from
and DATE(ana.tgl) <= :to
and ana.businessunit_id = '" . $buid . "'
$wherecat
$wheresubcat
$wherebranch
and ana.visible = 1");
        } else {
            $comm = Yii::app()->db->createCommand("select ana.doc_ref, ab.kode_barang, ab.nama_barang, ac.category_name, acs.sub_name, ana.qty, ana.price, ana.note as 'description' from nscc_asset_non_ati ana
join nscc_asset_barang ab on ab.barang_id = ana.barang_id
join nscc_asset_category ac on ac.category_id = ana.category_id
join nscc_asset_category_sub acs on acs.category_sub_id = ana.category_sub_id
where  DATE(ana.tgl) >= :from
and DATE(ana.tgl) <= :to
and ana.businessunit_id = '" . $buid . "'
$wherecat
$wheresubcat
$wherebranch
and ana.visible = 1");
        }

        return $comm->queryAll(true, $param);
    }

    public static function getQuery($params = [], $query = '') {
        for($i=0;$i<count($params);$i++) {
            if($params[$i] === '')
                $query .= "NULL,";
            else
                $query .= "'" . $params[$i] . "',";
        }
        $query = substr($query, 0, -1);
        $query = "(" . $query . "),";
        return $query;
    }
}



