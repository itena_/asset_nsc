<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 27/12/2014
 * Time: 10:30
 */
class GL
{
    private $_total_amount = 0;
    public $_id = array();

    public function add_gl_trans($type, $trans_id, $masterassetid, $bu, $date_, $ref, $memo_, $amount, $ppn, $total, $depprice, $person_id, $inout, $desc, $store = null)
    {
        $gl_trans = new GlTrans();
        $gl_trans->type = $type;
        $gl_trans->type_no = $trans_id;
        $gl_trans->masterassetid = $masterassetid;
        $gl_trans->tran_date = $date_;
        $gl_trans->businessunit_id = $bu;
        $gl_trans->memo_ = $memo_;
        $gl_trans->id_user = $person_id;
        $gl_trans->amount = $amount;
        $gl_trans->ppn = $ppn;
        $gl_trans->total = $total;
        $gl_trans->depreciation_price = $depprice;
        $gl_trans->tdate = date('Y-m-d H:i:s');
        $gl_trans->inout = $inout;
        $gl_trans->desc = $desc;
        $gl_trans->store = $store;
        if (!$gl_trans->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => "General Ledger $trans_id")) . CHtml::errorSummary($gl_trans));
        }
    }

    public function setGLNonActive($trans_id)
    {
        $gl = GlTrans::model()->findByAttributes(array('type_no' => $trans_id));
        $gl->visible = 0;
        if (!$gl->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => "General Ledger $trans_id")) . CHtml::errorSummary($gl));
        }
    }

    public function editGL($trans_id, $amount, $ppn, $depprice)
    {
        $gl = GlTrans::model()->findByAttributes(array('type_no' => $trans_id));
        $gl->amount = $amount;
        $gl->ppn = $ppn;
        $gl->total = $amount+$ppn;
        $gl->depreciation_price = $depprice;
        if (!$gl->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => "General Ledger $trans_id")) . CHtml::errorSummary($gl));
        }
    }

    public function add_rent_trans($type, $trans_id, $bu, $date_, $ati, $name, $category, $price, $depprice, $amount,  $person_id, $desc, $store = null, $tostore, $tobu )
    {
        $rent_trans = new RentTrans();
        $rent_trans->type = $type;
        $rent_trans->type_no = $trans_id;
        $rent_trans->tran_date = $date_;
        $rent_trans->businessunit_id = $bu;
        $rent_trans->ati = $ati;
        $rent_trans->name = $name;
        $rent_trans->category = $category;
        $rent_trans->id_user = $person_id;
        $rent_trans->price = $price;
        $rent_trans->depreciation_price = $depprice;
        $rent_trans->amount = $amount;
        $rent_trans->tdate = date('Y-m-d H:i:s');
        $rent_trans->desc = $desc;
        $rent_trans->store = $store;
        $rent_trans->tobu = $tobu;
        $rent_trans->tostore = $tostore;
        if (!$rent_trans->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => "Rent Trans")) . CHtml::errorSummary($rent_trans));
        }
    }

    public function add_gl($type, $trans_id, $date_, $ref, $account, $memo_, $comment_,$amount, $cf, $store = null, $person_id = null)
    {
        if ($person_id == null) {
            $person_id = Yii::app()->user->getId();
        }
//        $is_bank_to = $this->is_bank_account($account);
        if ($amount != 0) {
            $this->add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount,
                $person_id, $cf, $store);
        }
//        if ($is_bank_to) {
//            $bank = Bank::model()->find("account_code = :account_code",
//                array(":account_code" => $account));
//            if ($bank == null) {
//                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bank transaction')));
//            }
//            $this->add_bank_trans($type, $trans_id, $bank->bank_id, $ref, $date_,
//                $amount, $person_id, $store);
//        }
        if (strlen($comment_) > 0) {
            $this->add_comments($type, $trans_id, $date_, $comment_);
        }
        $this->_total_amount += $amount;
        $this->_id[] = [$account, $amount];
    }

    public function validate()
    {
//        $this->_total_amount = array_sum(array_column($this->detil,'amount'));
        if (round($this->_total_amount, 2) != 0.00)
            throw new Exception("Gagal menyimpan jurnal karena tidak balance. Total GL = " .
                number_format($this->_total_amount, 2) . " \n" . CJSON::encode($this->_id));
    }

    public function add_comments($type, $type_no, $date_, $memo_)
    {
        if ($memo_ != null && $memo_ != "") {
            $comment = Comments::model()->findByAttributes([
                'type' => $type,
                'type_no' => $type_no
            ]);
            if ($comment == null) {
                $comment = new Comments();
            }
            $comment->type = $type;
            $comment->type_no = $type_no;
            $comment->date_ = $date_;
            $comment->memo_ = $memo_;
            if (!$comment->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Comments')) . CHtml::errorSummary($comment));
        }
    }

    public static function saveGl($model, $modeldetail, $import = 0, $params = []) {
        $total = $_POST['ppnasset'] + $modeldetail->price_acquisition;
        $ati   = $modeldetail->ati;

        if($import == 0) {
            $gl = new GL();
            $gl->add_gl_trans(ASSETBELI,
                $modeldetail->asset_detail_id,
                $modeldetail->asset_id,
                $model->businessunit_id,
                $model->date_acquisition,
                $ati, "$ati",
                $modeldetail->price_acquisition,
                $_POST['ppnasset'],
                $total,
                $modeldetail->price_acquisition,
                $model->user_id, 1, 'beli',
                $model->store_kode);
        } else {
            return U::getQuery([
                DbCmd::uuid(),
                $model->businessunit_id,
                $params['asset_id'],
                ASSETBELI,
                $params['assetDetail_id'],
                $model->date_acquisition,
                "$ati",
                $modeldetail->price_acquisition,
                $_POST['ppnasset'],
                $total,
                $modeldetail->price_acquisition,
                $params['asset_id'],
                $model->store_kode,
                date('Y-m-d H:i:s'),
                1,
                1,
                1,
                'beli'
            ]);
        }
    }
}