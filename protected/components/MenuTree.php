<?php
class MenuTree
{
    var $security_role;
    var $security_roles_id;

    var $menu_users = array(
        'text' => 'User Manajement',
        'id' => 'jun.UsersGrid',
        'leaf' => true
    );
    var $security = array(
        'text' => 'Security Roles',
        'id' => 'jun.SecurityRolesGrid',
        'leaf' => true
    );
    function __construct($id)
    {
        $role = SecurityRoles::model()->findByPk($id);
        $this->security_role = explode(",", $role->sections);
        $this->security_roles_id = $role->security_roles_id;
    }
    function getChildMaster()
    {
        /** @var TODO 112 kosong */
        $child = array();

        if (in_array(504, $this->security_role)) {
            $child[] = array(
                'text' => 'Business Unit',
                'id' => 'jun.BusinessunitGrid',
                'leaf' => true
            );
        }
        if (in_array(605, $this->security_role)) {
            $child[] = array(
                'text' => 'Items',
                'id' => 'jun.AssetBarangGrid',
                'leaf' => true
            );
        }
        if (in_array(504, $this->security_role)) {
            $child[] = array(
                'text' => 'Group',
                'id' => 'jun.AssetGroupGrid',
                'leaf' => true
            );
        }
        if (in_array(509, $this->security_role)) {
            $child[] = array(
                'text' => 'Categories',
                'id' => 'jun.AssetCategoryGrid',
                'leaf' => true
            );
        }
        if (in_array(510, $this->security_role)) {
            $child[] = array(
                'text' => 'Sub Categories',
                'id' => 'jun.AssetCategorySubGrid',
                'leaf' => true
            );
        }
        if (in_array(116, $this->security_role)) {
            $child[] = array(
                'text' => 'Region / Area',
                'id' => 'jun.WilayahGrid',
                'leaf' => true
            );
        }
        if (in_array(115, $this->security_role)) {
            $child[] = array(
                'text' => 'Branch / Store',
                'id' => 'jun.StoreGrid',
                'leaf' => true
            );
        }

        return $child;
    }
    function getMaster($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Master',
            'expanded' => false,
            'children' => $child
        );
    }
    function getTransaction($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Transaction',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildTransaction()
    {
        $child = array();
        if (in_array(506, $this->security_role)) {
            $child[] = array(
                'text' => 'Assets Acquisition',
                'id' => 'jun.AssetGrid',
                'leaf' => true
            );
        }
        if (in_array(506, $this->security_role)) {
            $child[] = array(
                'text' => 'Compare Assets',
                'id' => 'jun.AssetComparationGrid',
                'leaf' => true
            );
        }
        if (in_array(704, $this->security_role)) {
            $child[] = array(
                'text' => 'Show Assets Acquisition',
                'id' => 'jun.ShowAssetDetailGrid',
                'leaf' => true
            );
        }
        if (in_array(513, $this->security_role)) {
            $child[] = array(
                'text' => 'Asset Request Item Code',
                'id' => 'jun.AssetRequestCodeGrid',
                'leaf' => true
            );
        }
        if (in_array(513, $this->security_role)) {
            $child[] = array(
                'text' => 'Asset Requests',
                'id' => 'jun.AssetRequestGrid',
                'leaf' => true
            );
        }
        if (in_array(511, $this->security_role)) {
            $child[] = array(
                'text' => 'Asset Non Activa ',
                'id' => 'jun.AssetNonAtiGrid',
                'leaf' => true
            );
        }
        if (in_array(512, $this->security_role)) {
            $child[] = array(
                'text' => 'Asset Request Approval',
                'id' => 'jun.AssetRequestApprovalGrid',
                'leaf' => true
            );
        }
        if (in_array(514, $this->security_role)) {
            $child[] = array(
                'text' => 'Asset Transfer',
                'id' => 'jun.AssetTransGrid',
                'leaf' => true
            );
        }
        return $child;
    }
    function getReport($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Report',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildReport()
    {
        $child = array();

        if (in_array(700, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Assets',
                'id' => 'jun.ReportAssetTotal',
                'leaf' => true
            );
        }
        if (in_array(700, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Non Activa',
                'id' => 'jun.ReportAssetNonAktiva',
                'leaf' => true
            );
        }
        if (in_array(700, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Asset List',
                'id' => 'jun.ReportAssetList',
                'leaf' => true
            );
        }
        if (in_array(701, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Assets (NonActive)',
                'id' => 'jun.ReportAssetTotalNonActive',
                'leaf' => true
            );
        }
        if (in_array(702, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Rent Asset',
                'id' => 'jun.ReportRentAsset',
                'leaf' => true
            );
        }
        if (in_array(703, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Jurnal Asset',
                'id' => 'jun.ReportJurnalAsset',
                'leaf' => true
            );
        }

        return $child;
    }
    function getAdministration($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Administration',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildAdministration()
    {
        $child = array();
        if (in_array(400, $this->security_role)) {
            $child[] = array(
                'text' => 'User Management',
                'id' => 'jun.UsersGrid',
                'leaf' => true
            );
        }
        if (in_array(800, $this->security_role)) {
            $child[] = array(
                'text' => 'Business Units',
                'id' => 'jun.BusinessunitGrid',
                'leaf' => true
            );
        }
        if (in_array(401, $this->security_role)) {
            $child[] = array(
                'text' => 'Security Roles',
                'id' => 'jun.SecurityRolesGrid',
                'leaf' => true
            );
        }
        if (in_array(402, $this->security_role)) {
            $child[] = array(
                'text' => 'Backup / Restore',
                'id' => 'jun.BackupRestoreWin',
                'leaf' => true
            );
        }
        if (in_array(403, $this->security_role)) {
            $child[] = array(
                'text' => 'Import',
                'id' => 'jun.ImportXlsx',
                'leaf' => true
            );
        }
        if (in_array(404, $this->security_role)) {
            $child[] = array(
                'text' => 'Setting Client',
                'id' => 'jun.SettingsClientsGrid',
                'leaf' => true
            );
        }
        if (in_array(405, $this->security_role)) {
            $child[] = array(
                'text' => 'Preferences',
                'id' => 'jun.SysPrefsWin',
                'leaf' => true
            );
        }
        if (in_array(124, $this->security_role)) {
            $child[] = array(
                'text' => 'Employee',
                'id' => 'jun.EmployeeGrid',
                'leaf' => true
            );
        }
        if (in_array(406, $this->security_role)) {
            $child[] = array(
                'text' => 'User Employee',
                'id' => 'jun.UserEmployeeGrid',
                'leaf' => true
            );
        }
        if (in_array(502, $this->security_role)) {
            $child[] = array(
                'text' => 'Restrict Date User',
                'id' => 'jun.RestrictDateGrid',
                'leaf' => true
            );
        }
        if (in_array(503, $this->security_role)) {
            $child[] = array(
                'text' => 'Upload History',
                'id' => 'jun.UploadHistoryManual',
                'leaf' => true
            );
        }
        if (in_array(508, $this->security_role)) {
            $child[] = array(
                'text' => 'Sync',
                'id' => 'jun.SyncStatusGrid',
                'leaf' => true
            );
        }
        if (in_array(817, $this->security_role)) {
            $child[] = array(
                'text' => 'Import Asset',
                'id' => 'jun.ImportDataAsset',
                'leaf' => true
            );
        }
        if (in_array(817, $this->security_role)) {
            $child[] = array(
                'text' => 'Import Data Real',
                'id' => 'jun.ImportData',
                'leaf' => true
            );
        }
        return $child;
    }

    function getGeneral()
    {
        $username = Yii::app()->user->name;
        $child = array();
        if (in_array(000, $this->security_role)) {
            $child[] = array(
                'text' => 'Change Password',
                'id' => 'jun.PasswordWin',
                'leaf' => true
            );
        }
        if (in_array(001, $this->security_role)) {
            $child[] = array(
                'text' => "Logout ($username)",
                'id' => 'logout',
                'leaf' => true
            );
        }
        return $child;
    }
    public function get_menu()
    {
        $data = array();
        $master = self::getMaster(self::getChildMaster());
        if (!empty($master)) {
            $data[] = $master;
        }
        $trans = self::getTransaction(self::getChildTransaction());
        if (!empty($trans)) {
            $data[] = $trans;
        }
        $report = self::getReport(self::getChildReport());
        if (!empty($report)) {
            $data[] = $report;
        }
        $adm = self::getAdministration(self::getChildAdministration());
        if (!empty($adm)) {
            $data[] = $adm;
        }

        $username = Yii::app()->user->name;

        if (in_array(000, $this->security_role)) {
            $data[] = array(
                'text' => 'Change Password',
                'id' => 'jun.PasswordWin',
                'leaf' => true
            );
        }

        if($this->security_roles_id == '2' || $this->security_roles_id == '5')
        {
            $bc = $_COOKIE['businessunitcode'];
            if (in_array(001, $this->security_role)) {
                $data[] = array(
                    'text' => "Logout ($username) - ($bc)",
                    'id' => 'logout',
                    'leaf' => true
                );
            }
        }
        else{
            if (in_array(001, $this->security_role)) {
                $data[] = array(
                    'text' => "Logout ($username)",
                    'id' => 'logout',
                    'leaf' => true
                );
            }
        }
        return CJSON::encode($data);
    }
    public function getState($section)
    {

        return in_array($section, $this->security_role);
    }
}
