<?php

class AssetBarangController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) return;

        $status = true;
        $msg = 'Data berhasil disimpan.';
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model = new AssetBarang;
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['AssetBarang'][$k] = $v;
            }
            $model->attributes = $_POST['AssetBarang'];

            if (AssetBarang::model()->findByAttributes(['kode_barang' => $_POST['AssetBarang']['kode_barang']])) {
                $status = false;
                $msg = "Kode Barang sudah terdaftar";
                SystemNotification::generateMessage($status, $msg);
            }

            $CategorySub = AssetCategorySub::model()->findByAttributes([
                'category_sub_id' => $_POST['category_sub_id'],
            ]);
            $CategorySub->counter = $CategorySub->counter + 1;
            if (!$CategorySub->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetCategorySub')) . CHtml::errorSummary($CategorySub));

            $model->businessunit_id = $_COOKIE['businessunitid'];
            $model->kode_barang = $CategorySub->sub_code . $this->add5digit($CategorySub->counter);

            if (isset($_POST['store']) && $_POST['store'] != null) {
                $model->owner = $_POST['store'];
            }

            if (!$model->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetBarang')) . CHtml::errorSummary($model));

            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        SystemNotification::generateMessage($status, $msg);
    }

    public function add5digit($counter)
    {
        return substr("00000", 0, 5-strlen($counter)) . $counter;
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'AssetBarang');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['AssetBarang'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['AssetBarang'];
            $model->tipe_barang_id = $_POST['tipe_barang_id'];
            $model->businessunit_id = $_COOKIE['businessunitid'];

            if (isset($_POST['store']) && $_POST['store'] != null) {
                $model->owner = $_POST['store'];
            }

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->barang_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->barang_id));
            }
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                //$this->loadModel($id, 'AssetBarang')->delete();
                $model = $this->loadModel($id, 'AssetBarang');
                $model->active = 0;
                if ($model->save()) {

                    $status = true;
                    $msg = "Data berhasil di hapus.";
                } else {
                    $msg .= " " . implode(", ", $model->getErrors());
                    $status = false;
                }
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex()
    {
        $limit = isset($_POST['limit']) ? $_POST['limit'] : 20;
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $param = array();

        $criteria = DbCmd::instance()->addFrom("{{asset_barang}}")
            ->addCondition('active = 1');
        if (isset($_POST['tipe_barang_id']) && $_POST['tipe_barang_id'] != 'all') {
            $criteria->addCondition('tipe_barang_id = :tipe_barang_id');
            $param[':tipe_barang_id'] = $_POST['tipe_barang_id'];
        }
        if (isset($_POST['kode_barang'])) {
            $criteria->addCondition('kode_barang like :kode_barang');
            $param[':kode_barang'] = "%" . $_POST['kode_barang'] . "%";
        }
        if (isset($_POST['nama_barang'])) {
            $criteria->addCondition('nama_barang like :nama_barang');
            $param[':nama_barang'] = "%" . $_POST['nama_barang'] . "%";
        }
        if (isset($_POST['ket'])) {
            $criteria->addCondition('ket like :ket');
            $param[':ket'] = "%" . $_POST['ket'] . "%";
        }

        if (isset($_POST['loadati'])) $criteria->addCondition('tipe_barang_id = 1');
        if (isset($_POST['loadnonati'])) $criteria->addCondition('tipe_barang_id = 0');

        $role = Users::get_security_role_id();
        if ($role != 2) {
            $criteria->addCondition("owner is null OR owner = '" . Users::getStoreIdUser() . "'");
        }
        $criteria->addParams($param);
        $total = $criteria->queryCount();

        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->setLimit($limit, $start);
        }

        $criteria->addOrder("kode_barang");
        $model = $criteria->getQuery();
        $model = $criteria->queryAll();

        $this->renderJsonArrWithTotal($model, $total);
    }

}