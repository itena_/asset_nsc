<?php
class AssetUserController extends GxController
{
    public function actionCreate()
    {
        $model = AssetUser::saveData();
        echo CJSON::encode(array(
            'success' => true,
            'msg' => "Data berhasil disimpan dengan id $model"));
        Yii::app()->end();
    }
    public function actionUpdate($key)
    {
        AssetUser::saveData($key);
        echo CJSON::encode(array(
            'success' => true,
            'msg' => "Data berhasil disimpan dengan id $model"));
        Yii::app()->end();
    }
    public function actionIndex()
    {
        $data = AssetUser::getData();
        $model = $data->queryAll();
        $count = count($model);
        return $this->renderJsonArrWithTotal($model, $count);
    }
}