<?php
Yii::import('application.components.U');
Yii::import("application.components.tbs_class_php5", true);
Yii::import("application.components.tbs_plugin_excel", true);
Yii::import("application.components.tbs_plugin_opentbs", true);
Yii::import("application.components.tbs_plugin_aggregate", true);
Yii::import("application.extensions.PHPExcel", true);
Yii::import('application.models.*');

class ReportController extends GxController
{
    private $TBS;
    private $logo;
    private $format;
    public $is_excel;

    public function init()
    {
        parent::init();
        if (!isset($_POST) || empty($_POST)) {
            $this->redirect(url('/'));
        }
        $this->layout = "report";
        $this->logo = bu() . app()->params['url_logo'];
        $this->TBS = new clsTinyButStrong;
        $this->format = $_POST['format'];
        if ($this->format == 'excel') {
            $this->TBS->PlugIn(TBS_INSTALL, TBS_EXCEL);
            $this->is_excel = true;
        } elseif ($this->format == 'openOffice') {
            $this->TBS->PlugIn(TBS_INSTALL, OPENTBS_PLUGIN);
        }
        error_reporting(E_ALL & ~E_NOTICE);
    }

    public function actionAssetDepriciation()
    {
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $ati = $_POST['ati'];
            $id = $_POST['asset_detail_id'];
            $summary = U::report_asset_depriciation($from, $to, $ati, $id);

            $total = array_sum(array_column($summary, 'penyusutanperbulan'));
            $totalprice = array_sum(array_column($summary, 'price_acquisition'));
            $totalnewprice = array_sum(array_column($summary, 'new_price_acquisition'));
            if ($total != 0) {
                $totalbalance = min(array_column($summary, 'balance'));
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=AssetDetails$from-$to.xls");
                echo $this->render('AssetDepriciation',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'total' => $total,
                        'totalprice' => $totalprice,
                        'totalnewprice' => $totalnewprice,
                        'totalbalance' => $totalbalance,
                        'ati' => $ati
                    ), true);
            } else {
                $this->render('AssetDepriciation',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'total' => $total,
                        'totalprice' => $totalprice,
                        'totalnewprice' => $totalnewprice,
                        'totalbalance' => $totalbalance,
                        'ati' => $ati
                    ));
            }
        }
    }

    public function actionAssetList()
    {
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $cat_sub_id = $_POST['category_sub_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];
            $status = $_POST['statusasset'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name . ' (' . $category->category_code . ')' : 'All Category';
            $summary = Asset::getAssetList($from, $to, $cat_id, $cat_sub_id, $businessunit_id, $branch, $status);

            if ($branch == "") {
                $branch = 'All Branch/Store';
            }
            if ($status == "") {
                $status = 'All Status';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('AssetList',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'status' => $status,
                        'category' => $categoryname
                    ), true);
            } else {
                $this->render('AssetList',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'status' => $status,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionAssetTotal()
    {
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $cat_sub_id = $_POST['category_sub_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];
            $status = $_POST['statusasset'];
            $showreportasset = $_POST['showreportasset'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name . ' (' . $category->category_code . ')' : 'All Category';
            $summary = U::report_asset($from, $to, $cat_id, $cat_sub_id, $businessunit_id, $branch, $status, $showreportasset);

            $total = array_sum(array_column($summary, 'total'));
            $totalprice = array_sum(array_column($summary, 'price'));
            $totalbalance = array_sum(array_column($summary, 'balance'));
            if ($branch == "") {
                $branch = 'All Branch/Store';
            }
            if ($status == "") {
                $status = 'All Status';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render($showreportasset == 'T' ? 'Asset' : 'AssetTotalDetail',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'status' => $status,
                        'total' => $total,
                        'totalprice' => $totalprice,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ), true);
            } else {
                $this->render($showreportasset == 'T' ? 'Asset' : 'AssetTotalDetail',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'status' => $status,
                        'total' => $total,
                        'totalprice' => $totalprice,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionAssetTotalDetail()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $cat_sub_id = $_POST['category_sub_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];
            $status = $_POST['statusasset'];
            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name . ' (' . $category->category_code . ')' : 'All Category';
            $summary = U::report_asset_detail($from, $to, $cat_id, $cat_sub_id, $businessunit_id, $branch, $status);

            $totalprice = array_sum(array_column($summary, 'price'));
            $totalbalance = array_sum(array_column($summary, 'balance'));

            if ($branch == "") {
                $branch = 'All Branch/Store';
            }
            if ($status == "") {
                $status = 'All Status';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('AssetTotalDetail',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'status' => $status,
                        'totalprice' => $totalprice,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ), true);
            } else {
                $this->render('AssetTotalDetail',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'status' => $status,
                        'totalprice' => $totalprice,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionAssetTotalNonActive()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name . ' (' . $category->category_code . ')' : 'All Category';
            $summary = U::report_asset_nonactive($from, $to, $cat_id, $businessunit_id, $branch);

            $total = array_sum(array_column($summary, 'total'));
            $totalbalance = array_sum(array_column($summary, 'balance'));
            //$totalprice = array_sum(array_column($summary, 'asset_trans_price'));
            //$totalnewprice = array_sum(array_column($summary, 'asset_trans_new_price'));

            if ($branch == "") {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('AssetNonActive',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'total' => $total,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ), true);
            } else {
                $this->render('AssetNonActive',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'total' => $total,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionAssetTotalDetailNonActive()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name . ' (' . $category->category_code . ')' : 'All Category';
            $summary = U::report_asset_detail_nonactive($from, $to, $cat_id, $businessunit_id, $branch);

            //$total = array_sum(array_column($summary, 'total'));
            $totalprice = array_sum(array_column($summary, 'price'));
            $totalbalance = array_sum(array_column($summary, 'balance'));

            if ($branch == "") {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('AssetTotalDetailNonActive',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totalprice' => $totalprice,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ), true);
            } else {
                $this->render('AssetTotalDetailNonActive',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totalprice' => $totalprice,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionJurnalAsset()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $branch = $_POST['branch'];
            //$cat_id = $_POST['category_id'];
            $businessunit_id = $_COOKIE['businessunitid'];

            //$category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            //$categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';
            $summary = U::report_jurnalasset($from, $to, $businessunit_id, $branch);

            //$total = array_sum(array_column($summary, 'total'));
            $totaldebit = array_sum(array_column($summary, 'debit'));
            $totalkredit = array_sum(array_column($summary, 'kredit'));
            $totalprofit = array_sum(array_column($summary, 'profit'));
            $totalloss = array_sum(array_column($summary, 'loss'));
            $totalppn = array_sum(array_column($summary, 'ppn'));
            $totalprice = array_sum(array_column($summary, 'price'));


            if ($branch == "") {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('JurnalAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totaldebit' => $totaldebit,
                        'totalkredit' => $totalkredit,
                        'totalprofit' => $totalprofit,
                        'totalloss' => $totalloss,
                        'totalppn' => $totalppn,
                        'totalprice' => $totalprice
                    ), true);
            } else {
                $this->render('JurnalAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totaldebit' => $totaldebit,
                        'totalkredit' => $totalkredit,
                        'totalprofit' => $totalprofit,
                        'totalloss' => $totalloss,
                        'totalppn' => $totalppn,
                        'totalprice' => $totalprice
                    ));
            }
        }
    }

    public function actionRentAsset()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name . ' (' . $category->category_code . ')' : 'All Category';
            $summary = U::report_rentasset($from, $to, $cat_id, $businessunit_id, $branch);

            //$total = array_sum(array_column($summary, 'total'));
            $totalamount = array_sum(array_column($summary, 'amount'));

            if ($branch == "") {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('RentAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totalamount' => $totalamount,
                        'category' => $categoryname
                    ), true);
            } else {
                $this->render('RentAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totalamount' => $totalamount,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionPrintAsset()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {


            $ti = AssetDetail::model()->find('asset_id = :asset_id', array(':asset_id' => $_POST['asset_id']));
            $tid = AssetDetail::get_details_to_print($_POST['asset_id']);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'asset_detail.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $header_data = array(
                'title' => 'ASSETS',
                'doc_ref' => $ti->docref_other,
                'tgl' => date_format(date_create($ti->asset_trans_date), 'd F Y'),
                'branch' => $ti->asset_trans_branch,
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $tid);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "ASSET $ti->docref_other $ti->asset_trans_branch.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }

    public function actionPrintFakturJualAsset()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {


            $ti = AssetDetail::model()->find('asset_id = :asset_id', array(':asset_id' => $_POST['asset_id']));
            $tid = AssetDetail::get_details_to_print($_POST['asset_id']);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'fakturjualasset.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $header_data = array(
                'title' => 'ASSETS',
                'doc_ref' => $ti->docref_other,
                'tgl' => date_format(date_create($ti->asset_trans_date), 'd F Y'),
                'branch' => $ti->asset_trans_branch,
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $tid);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "ASSET $ti->docref_other $ti->asset_trans_branch.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }

    public function actionDeprisiasiAktif()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $branch = $_POST['branch'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $cat_id = $_POST['category_id'];
            $cat_sub_id = $_POST['category_sub_id'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name . ' (' . $category->category_code . ')' : 'All Category';


            //$category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            //$categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';
            $summary = U::report_deprisiasiasset($from, $to, $businessunit_id, $branch, $cat_id, $cat_sub_id);

            //$total = array_sum(array_column($summary, 'total'));
            $totaldep = array_sum(array_column($summary, 'depreciationpermonth'));
            $totalacc = array_sum(array_column($summary, 'depreciationaccumulation'));
            $totalnilaibuku = array_sum(array_column($summary, 'nilaibuku'));
            $totaldepbyparam = array_sum(array_column($summary, 'depreciationaccumulationbyparam'));
            $totalnilaibukubyparam = array_sum(array_column($summary, 'nilaibukubyparam'));
            $totalakumulasibyparam = array_sum(array_column($summary, 'akumulasibyparam'));

            if ($branch == "") {
                $branch = 'All Branch/Store';
            }
            if ($cat_id == "") {
                $categoryname = 'All Category';
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('AssetReportDepreciation',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'categoryname' => $categoryname,
                        'totaldep' => $totaldep,
                        'totalacc' => $totalacc,
                        'totalnilaibuku' => $totalnilaibuku,
                        'totaldepbyparam' => $totaldepbyparam,
                        'totalnilaibukubyparam' => $totalnilaibukubyparam,
                        'totalakumulasibyparam' => $totalakumulasibyparam,
                    ), true);
            } else {
                $this->render('AssetReportDepreciation',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'categoryname' => $categoryname,
                        'totaldep' => $totaldep,
                        'totalacc' => $totalacc,
                        'totalnilaibuku' => $totalnilaibuku,
                        'totaldepbyparam' => $totaldepbyparam,
                        'totalnilaibukubyparam' => $totalnilaibukubyparam,
                        'totalakumulasibyparam' => $totalakumulasibyparam,
                    ));
            }
        }
    }

    public function actionShowAsset()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $branch = $_POST['branch'];
            $categoryid = $_POST['category_id'];
            $cat_sub_id = $_POST['category_sub_id'];
            $businessunit_id = $_COOKIE['businessunitid'];

            $user_id = Yii::app()->user->getId();
            $Users = Users::model()->findByPk($user_id);

            if ($Users->security_roles_id == 4) {
                $summary = U::report_showasset($from, $to, $businessunit_id, $Users->store, $categoryid, $cat_sub_id);
                if ($branch == "") {
                    $branch = $Users->store;
                }
            } else {
                $summary = U::report_showasset($from, $to, $businessunit_id, $branch, $categoryid, $cat_sub_id);
                if ($branch == "") {
                    $branch = 'All Branch/Store';
                }
            }
            //$category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            //$categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';


            //$total = array_sum(array_column($summary, 'total'));


            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=ShowAsset$branch$from-$to.xls");
                echo $this->render('ShowAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                    ), true);
            } else {
                $this->render('ShowAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                    ));
            }
        }
    }

    public function actionAssetTotalNonAktiva()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $cat_sub_id = $_POST['category_sub_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categorysub = $cat_sub_id ? AssetCategorySub::model()->findByPk($cat_sub_id) : 'All Sub Category';
            $categoryname = $cat_id ? $category->category_name . ' (' . $category->category_code . ')' : 'All Category';
            $summary = U::report_asset_non_aktiva($from, $to, $cat_id, $cat_sub_id, $businessunit_id, $branch);

            $total = array_sum(array_column($summary, 'price'));
            //$totalbalance = array_sum(array_column($summary, 'balance'));
            //$totalprice = array_sum(array_column($summary, 'asset_trans_price'));
            //$totalnewprice = array_sum(array_column($summary, 'asset_trans_new_price'));
            if ($branch == "") {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('AssetNonAktiva',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'total' => $total,
                        //'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ), true);
            } else {
                $this->render('AssetNonAktiva',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'total' => $total,
                        //'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionAssetTotalDetailNonAktiva()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $cat_sub_id = $_POST['category_sub_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name . ' (' . $category->category_code . ')' : 'All Category';
            $summary = U::report_asset_detail_non_aktiva($from, $to, $cat_id, $cat_sub_id, $businessunit_id, $branch);

            //$total = array_sum(array_column($summary, 'total'));
            $total = array_sum(array_column($summary, 'price'));
            //$totalbalance = array_sum(array_column($summary, 'balance'));

            if ($branch == "") {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('AssetNonAktivaDetail',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'total' => $total,
                        'category' => $categoryname
                    ), true);
            } else {
                $this->render('AssetNonAktivaDetail',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'total' => $total,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionAssetDetails()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $ati = $_POST['ati'];
            $id = $_POST['asset_detail_id'];

            /* @var $sales_trans AssetDetail */
            //$sales_trans = AssetDetail::model()->findByPk($id);


            /*if ($docref != "") {
                $doc = AssetPeriode::model()->find(array('docref' => $docref));
                $docrefid = $doc->asset_detail_id;
            } else $docrefid = "";*/

            $summary = U::report_asset_details($from, $to, $ati, $id);

            $total = array_sum(array_column($summary, 'penyusutanperbulan'));
            $totalprice = array_sum(array_column($summary, 'asset_trans_price'));
            $totalnewprice = array_sum(array_column($summary, 'asset_trans_new_price'));
            if ($total != 0) {
                $totalbalance = min(array_column($summary, 'balance'));
            }


            /*if ($docref == "") {
                $docref = "All Customers";
            } else $customer = $cust->no_customer . " - " . $cust->nama_customer;*/

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=AssetDetails$from-$to.xls");
                echo $this->render('AssetDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'total' => $total,
                        'totalprice' => $totalprice,
                        'totalnewprice' => $totalnewprice,
                        'totalbalance' => $totalbalance,
                        'ati' => $ati
                    ), true);
            } else {
                $this->render('AssetDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'total' => $total,
                        'totalprice' => $totalprice,
                        'totalnewprice' => $totalnewprice,
                        'totalbalance' => $totalbalance,
                        'ati' => $ati
                    ));
            }
        }
    }

}

