<?php

class BusinessunitController extends GxController
{
    public function actionCreate()
    {
        if (isset($_POST) && !empty($_POST)) {
            $is_new = $_POST['mode'] == 0;
            $bu_id = $_POST['id'];
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new Businessunit : $this->loadModel($bu_id, "Businessunit");
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Businessunit'][$k] = $v;
                }
                $check = Businessunit::model()->findByAttributes(['businessunit_code' => $_POST['Businessunit']['businessunit_code']]);
                if ($is_new) {
                    if ($check) {
                        $msg = "Data sudah ada di database.";
                        $status = false;
                        return;
                    }
                }
                $model->attributes = $_POST['Businessunit'];
                $model->created_at = new CDbExpression('NOW()');
                $msg = "Data berhasil di simpan.";
                $status = true;
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Businessunit')) . CHtml::errorSummary($model));

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            } finally {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg));
                Yii::app()->end();

            }
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Businessunit');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Businessunit'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Businessunit'];
            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->businessunit_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->businessunit_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];

        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
            $criteria->order = 'businessunit_code asc';
        }
        $model = Businessunit::model()->findAll($criteria);
        $total = Businessunit::model()->count($criteria);

        $this->renderJson($model, $total);
    }
}