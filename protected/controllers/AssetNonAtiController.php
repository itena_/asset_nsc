<?php

class AssetNonAtiController extends GxController {

    public function actionCreate() {

        if (!Yii::app()->request->isAjaxRequest)
        return;
        if (isset($_POST) && !empty($_POST)) {
            $user_id = Yii::app()->user->getId();
            $Users = Users::model()->findByPk($user_id);
            $businessunit_id = $_COOKIE['businessunitid'];
            $qty = $_POST['qty'];
            try{
                foreach($_POST as $k=>$v){
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['AssetNonAti'][$k] = $v;
                }
                for ($i=0;$i<$qty;$i++) {
                    $model = new AssetNonAti;
                    $last = $this->getLastRow() + 1;
                    $model->attributes = $_POST['AssetNonAti'];
                    //$model->doc_ref = 'ASTNONATI-' . $Users->store . '-' . $last;
                    $model->doc_ref = 'ASTNONATI-' . STOREID . '-' . $last;
                    $model->barang_id = $_POST['asset_barang_id'];
                    $model->category_id = $_POST['category_id'];
                    $model->category_sub_id = $_POST['category_sub_id'];
                    $model->location = $_POST['location'];
                    $model->merk = $_POST['merk'];
                    $catSub = AssetCategorySub::model()->findByPk($_POST['category_sub_id']);
                    $model->group_id = $catSub->asset_group_id;
                    $model->price = get_number($_POST['price']);
                    $model->qty = 1;
                    $model->tgl = $_POST['tgl'];
                    $model->tdate = new CDbExpression('NOW()');
                    //$model->store_kode = STOREID;
                    $model->store_kode = $Users->store;
                    $model->businessunit_id = $businessunit_id;
                    $model->user_id = $user_id;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetNonAti')) . CHtml::errorSummary($model));
                    }
                }
                $msg = "Data berhasil disimpan.";
                $status = true;
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg));
                Yii::app()->end();
            }
            catch (Exception $ex)
            {
                $status = false;
                $msg = $ex->getMessage();
                echo CJSON::encode(array(
                    'success' => 'failed',
                    'msg' => $msg
                ));
            }


        }

    }

    public function actionStatus() {

        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $user_id = Yii::app()->user->getId();
            $businessunit_id = $_COOKIE['businessunitid'];
            //$qty = $_POST['qty'];
            $id = $_POST['nonati_id'];
            try
            {
                $model = $this->loadModel($id, 'AssetNonAti');
                $model->status = $_POST['statusasset'];

                $model->status_note = $_POST['ket'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetNonAti')) . CHtml::errorSummary($model));
                }

                $msg = "Data berhasil disimpan.";
                $status = true;
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg));
                Yii::app()->end();
            }
            catch (Exception $ex)
            {
                $status = false;
                $msg = $ex->getMessage();
                echo CJSON::encode(array(
                    'success' => 'failed',
                    'msg' => $msg
                ));
            }


        }

    }

    public function getLastRow()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('businessunit_id',$_COOKIE['businessunitid']);
        $criteria->compare('store_kode',STOREID);
        $model = AssetNonAti::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

public function actionUpdate($id) {
$model = $this->loadModel($id, 'AssetNonAti');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetNonAti'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['AssetNonAti'];
$model->location = $_POST['AssetNonAti']['location'];


    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->nonati_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->nonati_id));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'AssetNonAti')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}
    $businessunit_id = $_COOKIE['businessunitid'];
    $param = array();
    $criteria = new CDbCriteria();

if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}

    $user_id = Yii::app()->user->getId();
    $Users = Users::model()->findByPk($user_id);

    if($Users->security_roles_id == 4)
    {
        $criteria->addCondition('store_kode = :store_kode');
        $param[':store_kode'] = $Users->store;
    }
    $criteria->addCondition('businessunit_id = :businessunit_id');
    $param[':businessunit_id'] = $businessunit_id;

    $criteria->params = $param;
$model = AssetNonAtiView::model()->findAll($criteria);
$total = AssetNonAtiView::model()->count($criteria);

$this->renderJson($model, $total);

}

}