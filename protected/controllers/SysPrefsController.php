<?php

class SysPrefsController extends GxController {

    public function actionCreate() {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $filter_payments = CJSON::decode($_POST['filter_payment']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                foreach ($_POST as $k => $v) {
                    if ($k == 'filter_payment') {
                        $v = implode(",", array_column($filter_payments, 'bank_id'));
                    } else {
                        if (is_angka($v))
                            $v = get_number($v);
                    }
                    SysPrefs::model()->updateAll(['value_' => $v], 'name_ = :name_', [':name_' => $k]);
                }
                $transaction->commit();
                $status = true;
                $msg = 'Preferences berhasil diupdate.';
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'SysPrefs');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['SysPrefs'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['SysPrefs'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->sys_prefs_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->sys_prefs_id));
            }
        }
    }
    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        /** @var SysPrefs[] $model */
        $model = SysPrefs::model()->findAll($criteria);
//        $total = SysPrefs::model()->count($criteria);
        $data = [];
        foreach ($model as $d) {
            switch ($d->name_) {
                case 'filter_payment' :
                    $arr = explode(',', $d->value_);
                    $filter = [];
                    foreach ($arr as $r) {
                        $filter[] = ['bank_id' => $r];
                    };
                    $data[$d->name_] = '{"results":' . json_encode($filter) . '}';
                    break;
                default:
                    $data[$d->name_] = $d->value_;
                    break;
            }
        }
        $jsonresult = '{"total":"1","data":' . json_encode($data) . '}';
        Yii::app()->end($jsonresult);
    }
    public function actionReferral() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("name_ = 'referral_fee'");
        $criteria->addCondition("store = :store");
        $param[':store'] = STOREID;
        $criteria->params = $param;
        $model = SysPrefs::model()->find($criteria);
//        $total = SysPrefs::model()->count($criteria);
//        $this->renderJson($model);
        $jsonresult = '{"sys_prefs_id":"' . $model["sys_prefs_id"] . '", "value_":"' . $model["value_"] . '" , "store":"' . $model["store"] . '"}';
        Yii::app()->end($jsonresult);
    }
    public function actionUpdateReferral($id) {
        $model = $this->loadModel($id, 'SysPrefs');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['SysPrefs'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['SysPrefs'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->sys_prefs_id;
            } else {
                $msg .= CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->sys_prefs_id));
            }
        }
    }

    public function actionGet() {
        if (isset($_POST) && !empty($_POST)) {
            $val = SysPrefs::get_val($_POST['val']);
            echo CJSON::encode(array(
                'success' => true,
                'msg' => $val
            ));
        }
    }

    /*
     * Untuk memanage data 'employe_id' di table 'nscc_sys_pref'
     */

    public function actionEmployee() {
        $criteria = new CDbCriteria();

        $criteria->addInCondition("name_", array(
            'apoteker', 'stocker', 'admin_cabang', 'pengelola_cabang'
        ));

        if (!HEADOFFICE) {
            $criteria->addCondition("store = :store");
            $criteria->params = array();
            $criteria->params[':store'] = STOREID;
        }

        $model = SysPrefs::model()->findAll($criteria);
        $total = SysPrefs::model()->count($criteria);
        $this->renderJson($model, $total);
    }

    public function actionEmployeeUpdate() {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                foreach ($_POST as $k => $v) {
                    $model = SysPrefs::model()->find('name_ = :name_ AND store = :store', array(':name_' => $k, ':store' => STOREID));
                    if ($model == NULL) {
                        $model = new SysPrefs;
                        $model->name_ = $k;
                        $model->store = STOREID;
                    }
                    $model->value_ = $v;

                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'SysPrefs')) . CHtml::errorSummary($model));
                    }
                }
                $transaction->commit();
                $status = true;
                $msg = 'Preferences berhasil diupdate.';
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

}
