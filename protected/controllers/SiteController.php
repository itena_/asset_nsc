<?php
Yii::import( 'application.components.U' );
class SiteController extends GxController {
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class'     => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'    => array( 'class' => 'CViewAction', ),
		);
	}
	public function actionIndex() {


		$this->layout = 'main';
		if ( ! isset( $_POST['app'] ) ) {

			$user = Users::model()->findByPk( user()->getId() );
            //$bsu = Businessunit::model()->findByPk( $user->businessunit_id );
			$tree = new MenuTree( $user->security_roles_id );
/*			if ( $tree->getState( 9996 ) ) {
				$this->redirect( url( 'site/counter' ) );
			} elseif ( $tree->getState( 9997 ) ) {
				$this->redirect( url( 'site/dokter' ) );
			} elseif ( $tree->getState( 9998 ) ) {
				$this->redirect( url( 'site/perawatan' ) );
			} */
			if ( $tree->getState( 9999 ) ) {
				$this->render( 'sales', array( 'state' => 0, 'username' => $user->name ) );
			} else {
                setcookie('businessunitid', $user->businessunit_id, time() + (86400 * 30), "/");
				$this->render( 'index' );
			}
		} else {
			$this->render( 'index' );
		}
	}
	public function actionGetDateTime() {
		if ( Yii::app()->request->isAjaxRequest ) {
			echo CJSON::encode( array(
				'success'  => true,
				'datetime' => date( 'Y-m-d H:i:s' )
			) );
			Yii::app()->end();
		}
	}
	public function actionError() {
		if ( $error = Yii::app()->errorHandler->error ) {
			if ( Yii::app()->request->isAjaxRequest ) {
//                if (is_array($error)) {
//                    $error = implode("\n", $error);
//                }
				throw new Exception( $error['message'] );
//                echo CJSON::encode(array(
//                    'success' => false,
//                    'msg' => $error
//                ));
//                Yii::app()->end();
			} else {
				$this->render( 'error', $error );
			}
		}
	}
	public function actionLogout()
    {
        if (isset($_COOKIE['businessunitid'])) {
            unset($_COOKIE['businessunitid']);
            setcookie('businessunitid', '', time() - 3600, '/'); // empty value and old timestamp
        }
        if (isset($_COOKIE['businessunitcode'])) {
            unset($_COOKIE['businessunitcode']);
            setcookie('businessunitcode', '', time() - 3600, '/'); // empty value and old timestamp
        }
		Yii::app()->user->logout();
		$this->redirect( Yii::app()->homeUrl );
	}
    public function actionLogin()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $url_ = reset($_SESSION);
            if ($url_ == bu('login')) {
                $url_ = bu();
            }
            $this->layout = 'login';
            $this->render('login', array('url_' => $url_));
        } else {
            $model = new LoginForm;
            $loginUsername = isset($_POST["loginUsername"]) ? $_POST["loginUsername"]
                : "";
            $loginPassword = isset($_POST["loginPassword"]) ? $_POST["loginPassword"]
                : "";
            if ($loginUsername != "") {
                //$model->attributes = $_POST['LoginForm'];
                $model->username = $loginUsername;
                $model->password = $loginPassword;
                // validate user input and redirect to the previous page if valid
                $x = $model->validate();
                if ($model->validate() && $model->login()) {
                    echo "{success: true}";
                } else {
                    echo "{success: false, errors: { reason: 'Login failed. Try again.' }}";
                }
            } else {
                echo "{success: false, errors: { reason: 'Login failed. Try again' }}";
            }
        }
    }
	public function actionLoginOverride() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			$this->redirect( '/' );
		} else {
			$count = Users::get_override( $_POST["loginUsername"], $_POST["loginPassword"] );
			if ( $count ) {
				echo CJSON::encode( array(
					'success' => true,
					'msg'     => $count
				) );
			} else {
				echo CJSON::encode( array(
					'success' => false,
					'msg'     => "You don't have permission."
				) );
			}
		}
	}
	public function actionTree() {
		$user = Users::model()->findByPk( user()->getId() );
		$menu = new MenuTree( $user->security_roles_id );
		$data = $menu->get_menu();
		Yii::app()->end( $data );
	}
}
