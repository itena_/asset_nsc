<?php

class AssetComparationDetailController extends GxController
{
    public function actionIndex()
    {
        $cmd = DbCmd::instance()
            ->addFrom('{{asset_comparation_detail}} t')
            ->addSelect('t.*')
//            ->addLeftJoin('{{pegawai}} p','p.pegawai_id = t.pegawai_id')
            ->addCondition('visible = 1 AND tipe = "'.$_POST['tipe'].'"')
            ->orderBy('ati')
        ;

        $cmd->addCondition('asset_comparation_id = :id')->addParam(':id', $_POST['asset_comparation_id']);

//        if (isset($_POST['masalah_detail_id'])) {
//            $cmd->addCondition('masalah_detail_id = :masalah_detail_id')->addParam(':masalah_detail_id', $_POST['masalah_detail_id']);
//        }

        $count = $cmd->queryCount();
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $cmd->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }

        $model = $cmd->queryAll();
        $this->renderJsonArrWithTotal($model, $count);
    }

}