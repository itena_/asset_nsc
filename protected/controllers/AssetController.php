<?php

class AssetController extends GxController
{

    public function actionCreate() {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model = Asset::saveAsset();

            $status = true;
            $msg = "Data berhasil di simpan dengan id " . $model->asset_id;
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $Asset = $this->loadModel($id, 'Asset');
                $Asset->active = '0';
                if (!$Asset->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset')) . CHtml::errorSummary($Asset));
                }
                AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $id));
                AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $id));
                AssetHistory::model()->deleteAll("masterassetid = :asset_id", array(':asset_id' => $id));
                GlTrans::model()->deleteAll("masterassetid = :asset_id", array(':asset_id' => $id));
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex()
    {
        $dbcmd = Asset::getData();

//        if(!Users::is_Administrator())
        $dbcmd->addCondition('a.businessunit_id = :bu_id')->addParam(':bu_id', $_COOKIE['businessunitid']);

        $count = count($dbcmd->queryAll());
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $dbcmd->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }
        $model = $dbcmd->getQuery();
        $model = $dbcmd->queryAll();
        return $this->renderJsonArrWithTotal($model, $count);

    }

    public function actionGetExpiredAssets()
    {
        $msg = 'Tidak ada.';
        $store_id = Store::model()->checkCabangId();

        $db = DbCmd::instance()->addFrom("{{asset}} a")
            ->addSelect("ad.expdate, ab.kode_barang, ab.nama_barang, DATEDIFF(ad.expdate, DATE_FORMAT(now(),'%Y-%m-%d')) sisa")
            ->addLeftJoin("{{asset_detail}} ad", "ad.asset_id = a.asset_id")
            ->addLeftJoin("{{asset_barang}} ab", "ab.barang_id = ad.barang_id")
            ->addCondition("DATEDIFF(ad.expdate, DATE_FORMAT(now(),'%Y-%m-%d')) < 30 and ad.expdate is not null")
            ->addCondition("a.store_id = '" . $store_id . "'");

        $data = $db->queryAll();
        if (count($data) > 0) {
            $msg = 'Terdapat Asset yang akan expired 1 bulan kedepan :<br />';
            $i = 1;
            foreach ($data as $row) {
                $ed = new DateTime($row['expdate']);
                $msg .= $i . '. ' . $row['kode_barang'] . '  ' . $row['nama_barang'] . '   : ' . '  ' . $ed->format('d M Y') . '<br />';
                $i++;
            }
        }

        echo $msg;
    }

    public function actionPrintAsset()
    {
        $tdate = $_POST['date'];
        $date = date('Y-m-d', strtotime($tdate));
        $data = array();

        $user_id = Yii::app()->user->getId();
        $Users = Users::model()->findByPk($user_id);
        $businessunit_id = $_COOKIE['businessunitid'];

        $criteria = new CDbCriteria;
        $criteria->compare('asset_trans_date', $date);
        $criteria->compare('asset_trans_branch', $Users->store);
        $criteria->compare('businessunit_id', $businessunit_id);
        $criteria->order = "asset_trans_name, ati ASC";
        $model = AssetDetail::model()->findAll($criteria);

        //$model = AssetDetail::model()->findByAttributes(array('asset_trans_date' => $date, 'asset_trans_branch' => STOREID));

        if ($model != null) {

            if ($model != null) {
                foreach ($model as $val) {
                    $attribDetil = $val->getAttributes();
                    //$data[] = json_encode($attribDetil);
                    $data[] = $attribDetil;
                }
            }

            echo CJSON::encode(array(
                'success' => 'success',
                'data' => $data,
                'msg' => 'success'
            ));

        } else {
            echo CJSON::encode(array(
                'success' => false,
                'msg' => 'data tidak ditemukan'
            ));
        }
    }

    public function actionImport() {
        try {
            Yii::app()->db->createCommand("
                    TRUNCATE TABLE importData;
                ")->execute();

            $query = '';
            $details = CJSON::decode( $_POST['detil'] );
            foreach ($details as $det) {
                $No = $det['No'];
                $ket = str_replace("'","", $det['Keterangan']);
                $ket = str_replace('"',"", $ket);
                $Keterangan = $ket;
                $Cabang = $det['Cabang'];
                $Tgl = $det['Tgl'];
                $Bulan = $det['Bulan'];
                $Tahun = $det['Tahun'];
                $Qty = $det['Qty'];
                $Harga = $det['Harga'];
                $Location       = isset($det['Location']) ? $det['Location'] : NULL;
                $User           = isset($det['User']) ? $det['User'] : NULL;
                $category       = isset($det['category']) ? $det['category'] : NULL;
                $sub_category   = isset($det['sub_category']) ? $det['sub_category'] : NULL;
                $Golongan       = isset($det['Golongan']) ? $det['Golongan'] : NULL;
                $query .= "('$No', '$Keterangan', '$Cabang', '$Tgl', '$Bulan', '$Tahun', '$Qty', '$Harga', '$Location', '$User', '$category', '$sub_category', '$Golongan'),";
            }
            $query = substr($query, 0, -1);
            $query = $query.';';
            Yii::app()->db->createCommand("
                    INSERT INTO `importData` (`No`, `Keterangan`, `Cabang`, `Tgl`, `Bulan`, `Tahun`, `Qty`, `Harga`, `Location`, `User`, `category`, `sub_category`, `Golongan`) VALUES 
                    $query
            ")->execute();

            $status = true;
            $message = "Data berhasil di-import.";
        } catch (Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $message));
        Yii::app()->end();
    }

    public function actionImportData() {
        try {
            ini_set('max_execution_time', 300);
            $count = DbCmd::instance()->addFrom("importData")->addSelect("count(*)")->queryScalar();
            $countDatas = $count > 1000 ? 1000 : $count;
            $loop = round($count / $countDatas);

            $counterStore = [];
            $arr['bu_name'] = Businessunit::model()->findByPk($_COOKIE['businessunitid'])->businessunit_name;
            for($counter=0; $counter < $loop; $counter++) {
                $details = DbCmd::instance()->addFrom("importData")->
                    setLimit($countDatas, $counter*$countDatas)->queryAll();
                $x = 0;
                $query = [];
                $query['asset'] = '';
                $query['assetDetail'] = '';
                $query['gl'] = '';
                $query['history'] = '';
                $query['assetPeriode'] = '';
                foreach ($details as $det) {
                    if(!isset($det['Golongan']) || $det['Golongan'] == '')
                        continue;

                    if(!AssetGroup::model()->findByAttributes(['golongan' => $det['Golongan']]))
                        continue;

                    if(is_numeric($det['Bulan']))
                        if(strlen($det['Bulan']) == 1)
                            $month = '0'.$det['Bulan'];
                        else
                            $month = $det['Bulan'];
                    else
                        $month = Other::getMonth($det['Bulan']);

                    $date   = $det['Tahun'] . '-' . $month .'-' . $det['Tgl'];
                    $branch = Store::saveStore($det);

                    $arr['asset_group_id']   = AssetGroup::model()->findByAttributes(['golongan' => $det['Golongan']])->asset_group_id;

                    $arr['branch']           = $branch;
                    if (!array_key_exists($branch, $counterStore)){
                        $lastRowBranch = Asset::getLastRow($branch);
                        if($lastRowBranch > 0)
                            $counterStore[$branch] = $lastRowBranch + 1;
                        else
                            $counterStore[$branch] = 1;
                    }
                    else
                        $counterStore[$branch] += 1;

                    $arr['mode']             = 0;
                    $arr['qty']              = $det['Qty'];
                    $arr['ppnasset']         = 0;
                    $arr['date_acquisition'] = $date;
                    $arr['description']      = strtoupper($det['Keterangan']);

                    $_POST['Detail']['price_acquisition']     = (float)$det['Harga'];
                    $_POST['Detail']['new_price_acquisition'] = 0;
                    $_POST['Detail']['conditions']            = 0;
                    $_POST['Detail']['category_id']           = AssetCategory::model()->findByAttributes(['category_code' => $det['category']])->category_id;
                    $_POST['Detail']['category_sub_id']       = AssetCategorySub::model()->findByAttributes(['sub_code' => $det['sub_category']])->category_sub_id;

                    $asset_user = isset($det['User']) && $det['User'] != '' ? AssetUser::saveData($det['User']) : null;
                    $location   = isset($det['Location']) && $det['Location'] != ''  ? Location::saveData($det['Location']) : null;

                    $_POST['Detail']['asset_user'] = $asset_user;
                    $_POST['Detail']['location'] = $location;

                    $_POST = array_merge($_POST, $arr);

                    $result = Asset::saveAsset(1, $counterStore);
                    $query['asset'] .= $result['asset'];
                    $query['assetDetail'] .= $result['assetDetail'];
                    $query['gl'] .= $result['gl'];
                    $query['history'] .= $result['history'];
                    $query['assetPeriode'] .= $result['assetPeriode'];
                    $x++;
                }
                foreach ($query as $k => $v) {
                    $query = substr($v, 0, -1);
                    $query = $query.';';

                    switch ($k) {
                        case 'asset':
                            $query = "INSERT INTO `nscc_asset` (`asset_id`, `asset_group_id`, `user_id`, `businessunit_id`, `doc_ref`, `branch`, `description`, `date_acquisition`, `hide`, `active`, `created_at`, `updated_at`) VALUES ".$query;
                            break;
                        case 'assetDetail':
                            $query = "INSERT INTO `nscc_asset_detail` (`asset_detail_id`, `asset_id`, `ati`, `non_ati`, `category_id`, `category_sub_id`, `barang_id`, `asset_name`, `qty`, `price_acquisition`, `new_price_acquisition`, `class`, `period`, `tariff`, `desc`, `penyusutanperbulan`, `penyusutanpertahun`, `status`, `statusdesc`, `serialnumber`, `conditions`, `merk`, `docnumber`, `expdate`, `asset_user`, `location`) VALUES ".$query;
                            break;
                        case 'gl':
                            $query = "INSERT INTO `nscc_gl_trans` (`gl_trans_id`, `businessunit_id`, `masterassetid`, `type`, `type_no`, `tran_date`, `memo_`, `amount`, `ppn`, `total`, `depreciation_price`, `id_user`, `store`, `tdate`, `up`, `visible`, `inout`, `desc`) VALUES ".$query;
                            break;
                        case 'history':
                            $query = "INSERT INTO `nscc_asset_history` (`asset_history_id`, `masterassetid`, `asset_id`, `businessunit_id`, `docref`, `ati`, `name`, `businessunit_name`, `branch`, `price`, `newprice`, `class`, `tariff`, `period`, `penyusutanperbulan`, `penyusutanpertahun`, `desc`, `status`, `tobu`, `tobranch`, `amount`, `sdate`, `created_at`, `updated_at`) VALUES ".$query;
                            break;
                        case 'assetPeriode':
//                            $query = "INSERT INTO `nscc_asset_periode` (`asset_periode_id`, `businessunit_id`, `asset_id`, `asset_detail_id`, `asset_group_id`, `barang_id`, `docref`, `docref_other`, `asset_trans_name`, `ati`, `asset_trans_branch`, `asset_trans_price`, `asset_trans_new_price`, `asset_trans_date`, `description`, `class`, `period`, `tariff`, `tglpenyusutan`, `penyusutanperbulan`, `penyusutanpertahun`, `balance`, `akumulasipenyusutan`, `status`, `startdate`, `enddate`, `created_at`, `updated_at`, `year`, `month`, `day`, `tdate`, `visible`) VALUES ".$query;
                            $query = "INSERT INTO `nscc_asset_periode` (`asset_periode_id`, `asset_id`, `asset_detail_id`, `tglpenyusutan`, `penyusutanperbulan`, `penyusutanpertahun`, `balance`, `akumulasipenyusutan`, `tdate`, `visible`) VALUES ".$query;
                            break;
                    }
                    Yii::app()->db->createCommand($query)->execute();
                }
            }
            $status = true;
            $message = "Data berhasil di-import.";
            ini_set('max_execution_time', 30);
        } catch (Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $message));
        Yii::app()->end();
    }
}