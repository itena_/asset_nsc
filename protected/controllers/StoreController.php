<?php

class StoreController extends GxController
{
    public function actionCreate()
    {
        $model = new Store;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Store'][$k] = $v;
            }
            $businessunit_id = $_COOKIE['businessunitid'];
            $model->attributes = $_POST['Store'];
            $model->nama_store = $_POST['Store']['nama_store'];
            $model->store_kode = $_POST['Store']['store_kode'];
            $model->businessunit_id = $businessunit_id;
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan store " . $model->store_kode;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id)
    {
        $businessunit_id = $_COOKIE['businessunitid'];
        $model = Store::model()->findByAttributes(array('store_kode' => $id, 'businessunit_id' => $businessunit_id));

        //$model = $this->loadModel($id, 'Store');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Store'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->nama_store = $_POST['Store']['nama_store'];
            $model->store_kode = $_POST['Store']['store_kode'];
            $model->businessunit_id = $businessunit_id;
            if ($model->save()) {
                $status = true;
                $msg = $model->store_kode . " berhasil di edit. ";
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->store_kode));
            }
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Store')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex(){
        $limit = isset($_POST['limit']) ? $_POST['limit'] : 20;
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $param = array();

        $criteria = DbCmd::instance()->addFrom("{{store}} s")
            ->addSelect("s.*")
        ;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->setLimit($limit, $start);
        }

        $businessunit_id = $_COOKIE['businessunitid'];
        $criteria->addLeftJoin("businessunit b", "b.businessunit_id = s.businessunit_id")
            ->addSelect("CONCAT(store_kode, ' - ', businessunit_code) bu_mix")
        ;

        if(!isset($_POST['param']) || $_POST['param'] !== 'all'){
            $criteria->addCondition('s.businessunit_id = :businessunit_id')
                ->addParam(':businessunit_id', $businessunit_id);
        }

        if ((isset ($_POST['store_id']) && $_POST['store_id'] != 99)){
            $criteria->addCondition('s.store_id = :store_id')
                ->addParam(':store_id', $_POST['store_id']);
        }

        if (isset($_POST['businessunit_id'])) {
            $criteria->addCondition('s.businessunit_id = :businessunit_id')
                ->addParam(':businessunit_id', $_POST['businessunit_id']);
        }

        $criteria->addOrder("store_kode");
        $criteria->params = $param;
        $model = $criteria->queryAll();
        $total = $criteria->queryCount();

        $this->renderJsonArrWithTotal($model, $total);
    }

    public function actionGetUserCabang()
    {
        $cabang = Store::model()->checkCabangId();
//        $security_role  = SecurityRoles::model()->checkSecurityRoleId();
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $cabang,
//            'sec' => $security_role
        ));
        Yii::app()->end();
    }
}