<?php

class AssetTransController extends GxController
{

    public function actionCreate()
    {
        $model = new AssetTrans;
        if (!Yii::app()->request->isAjaxRequest)
            return;

        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['AssetTrans'][$k] = $v;
            }

            $msg = "Data gagal disimpan.";
            $status = 'false';
            $detils = CJSON::decode($_POST['detil']);


            //$model->bu_pengirim =
            $model->bu_penerima = $_POST['businessunit_id'];
            $model->store_penerima = $_POST['branch'];
            $model->status = $_POST['statusasset'];
            $model->note = $_POST['ket'];
            $model->tdate = $_POST['tdate'];
            $model->approval = 2;
            $model->visible = 1;


            $msg = "Data berhasil disimpan.";
            $status = 'true';
            if (!$model->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetTrans')) . CHtml::errorSummary($model));


            foreach ($detils as $detil) {

                $dtl = $this->loadModel($detil['asset_id'], 'AssetDetail');


                $asst_details = new AssetTransDetail;
                $_POST['AssetTransDetail']['trans_id'] = $model->trans_id;
                $_POST['AssetTransDetail']['asset_id'] = $detil['asset_id'];
                $_POST['AssetTransDetail']['ati'] = get_number($detil['ati']);
                $_POST['AssetTransDetail']['asset_name'] = $detil['asset_name'];
                $_POST['AssetTransDetail']['amount'] = get_number($detil['amount']);
                $_POST['AssetTransDetail']['bookvalue'] = get_number($detil['bookvalue']);
                $_POST['AssetTransDetail']['ppn'] = get_number($detil['ppn']);
                $_POST['AssetTransDetail']['note'] = $detil['note'];

                $_POST['AssetTransDetail']['bu_penerima'] = $_POST['businessunit_id'];
                $_POST['AssetTransDetail']['store_penerima'] = $_POST['branch'];
                $_POST['AssetTransDetail']['bu_pengirim'] = $dtl->businessunit_id;
                $_POST['AssetTransDetail']['store_pengirim'] = $dtl->asset_trans_branch;
                $_POST['AssetTransDetail']['tdate'] = $_POST['tdate'];
                $_POST['AssetTransDetail']['approval'] = 2;
                $_POST['AssetTransDetail']['visible'] = 1;
                $_POST['AssetTransDetail']['status'] = $_POST['statusasset'];

                $asst_details->attributes = $_POST['AssetTransDetail'];
                if (!$asst_details->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetTransDetail')) . CHtml::errorSummary($asst_details));
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();

        }

    }

    public function actionApprove($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil di Approve.';
            $status = true;
            try {
                $dtl = AssetTransDetail::model()->findByAttributes(array('trans_id' => $id));
                //$dtl = $this->loadModel($id, 'AssetTransDetail');
                $dtl->approval = 1;
                if (!$dtl->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetTransDetail')) . CHtml::errorSummary($dtl));


            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionDisApprove($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil di Tolak.';
            $status = true;
            try {
                $dtl = AssetTransDetail::model()->findByAttributes(array('trans_id' => $id));
                //$dtl = $this->loadModel($id, 'AssetTransDetail');
                $dtl->approval = 0;
                if (!$dtl->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetTransDetail')) . CHtml::errorSummary($dtl));


            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionGenerate($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil di Generate.';
            $status = true;
            try {
                $dtl = AssetTransDetail::model()->findByAttributes(array('trans_id' => $id));
                //$dtl = $this->loadModel($id, 'AssetTransDetail');
                $dtl->approval = 3;
                if (!$dtl->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetTransDetail')) . CHtml::errorSummary($dtl));


            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionTranferAsset($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Asset Status berhasil di ubah.';
            $status = true;
            try {
                $dtl = AssetTransDetail::model()->findByAttributes(array('trans_id' => $id));

                $asset_detail_id = $dtl->asset_id;//$_POST['asset_detail_id'];

                $amountstatus = get_number($dtl->amount);
                $ppnstatus = get_number($dtl->ppn);
                $statusasset = $dtl->status;
                $assetvalue = get_number($dtl->bookvalue);

                $user_id = Yii::app()->user->getId();

                $ket = $dtl->note;

                $model = $this->loadModel($asset_detail_id, 'AssetDetail');


                $sellbranch = $dtl->store_penerima;

                $tobusinessunit_id = $dtl->bu_penerima;
                if ($tobusinessunit_id == "") {
                    $businessunit_id = $_COOKIE['businessunitid'];
                    $tobusinessunit_id = $businessunit_id;
                }

                $businessunit = $this->loadModel($tobusinessunit_id, 'Businessunit');
                $tobu = $businessunit->businessunit_code;

                if ($tobu == 'OTHER') {
                    $tobranch = '';
                } else {
                    $tobranch = $sellbranch;
                }


                if ($statusasset == '3') {
                    if ($tobusinessunit_id != '') {
                        $penyusutan = 0;
                        $penyusutantahun = 0;

                        //ati dan cabang lama sebelum aktiva berubah
                        $atiii = $dtl->ati;
                        $cablama = $dtl->store_pengirim;
                        $buidlama = $dtl->bu_pengirim;
                        $transdatelama = $model->asset_trans_date;

                        $model->businessunit_id = $tobusinessunit_id;
                        $model->status = '1';
                        $model->asset_trans_price = $amountstatus;
                        $model->asset_trans_branch = $sellbranch;
                        $model->asset_trans_date = new CDbExpression('NOW()');

                        if ($model->period) {
                            $penyusutan = $model->asset_trans_price / $model->period;
                            $penyusutantahun = $penyusutan * $model->period;
                        }

                        $model->penyusutanperbulan = $penyusutan;
                        $model->penyusutanpertahun = $penyusutantahun;

                        //rubah activa
                        $cabang = $businessunit->businessunit_code . $sellbranch;
                        $store = Store::model()->findByAttributes(array('store_kode' => $sellbranch, 'businessunit_id' => $tobusinessunit_id));
                        $area = str_pad($store->wilayah_id, 2, '0', STR_PAD_LEFT);
                        $urutan = str_pad($store->store_id, 2, '0', STR_PAD_LEFT);
                        $golongan = $model->class;

                        $lastDetail = $this->getLastRowDetail($sellbranch) + 1;

                        $ati = $area . $cabang . $urutan . '/' . $golongan . '/' . $lastDetail;

                        $model->ati = $ati;
                    } else
                        return;

                } else {
                    $model->status = $statusasset;

                    /*
                     * cek jika status 0 / non active update db field status dan active
                     */
                    if ($statusasset == '0') {
                        $model->active = 0;
                    } else
                        $model->active = 1;
                }

                if ($ket != "") {
                    $model->statusdesc = $ket;//$model->description;
                } else {
                    if ($statusasset == '3') {
                        $model->statusdesc = "Di beli dari " . $cablama;
                    } elseif ($statusasset == '4') {
                        $model->statusdesc = "Di sewa oleh " . $tobu . " - " . $tobranch;
                    }
                }

                //$model->statusdesc = $ket;
                $model->updated_at = new CDbExpression('NOW()');
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetDetail')) . CHtml::errorSummary($model));


                if ($statusasset == '3') {
                    $total = $ppnstatus + $model->asset_trans_price;
                    //gl penjual
                    $gl = new GL();
                    $gl->add_gl_trans(ASSETJUAL, $model->asset_detail_id, $model->asset_id,
                        $buidlama, $transdatelama, $atiii,
                        "$atiii", -$model->asset_trans_price, $ppnstatus, $total, $assetvalue,
                        $user_id, 0, 'jual', $cablama);

                    //reset deprisiasi
                    $this->rubahDeprisiasi($model->asset_detail_id, 0);

                    //gl pembeli
                    $gl = new GL();
                    $gl->add_gl_trans(ASSETBELI, $model->asset_detail_id, $model->asset_id,
                        $model->businessunit_id, $model->asset_trans_date, $model->ati,
                        "$model->ati", $model->asset_trans_price, $ppnstatus, $total, $model->asset_trans_price,
                        $user_id, 1, 'beli', $model->asset_trans_branch);


                    //$this->PrintFakturJualAsset($model->asset_detail_id);

                }

                if ($statusasset == '4') {
                    //$type, $trans_id, $bu, $date_, $memo_, $price, $depprice, $amount,  $person_id, $desc, $store, $tostore, $tobu
                    $gl = new GL();
                    $gl->add_rent_trans(ASSETSEWA, $model->asset_detail_id,
                        $model->businessunit_id, $model->asset_trans_date, $model->ati,
                        $model->asset_trans_name, $model->category_id,
                        $model->asset_trans_price, $assetvalue, $amountstatus, $user_id, $ket, $model->asset_trans_branch, $tobranch ? $tobranch : 'OTHER', $tobu);
                }

                //ASSET HISTORY
                $history = new AssetHistory;
                $history->businessunit_id = $model->businessunit_id;
                $history->masterassetid = $model->asset_id;
                $history->asset_id = $model->asset_detail_id;
                $history->docref = $model->docref;
                $history->ati = $model->ati;
                $history->name = $model->asset_trans_name;
                $history->branch = $model->asset_trans_branch;
                $history->price = $model->asset_trans_price;
                $history->newprice = $model->asset_trans_new_price;
                $history->class = $model->class;
                $history->tariff = $model->tariff;
                $history->period = $model->period;
                $history->penyusutanperbulan = $model->penyusutanperbulan;
                $history->penyusutanpertahun = $model->penyusutanpertahun;

                if ($ket != "") {
                    $history->desc = $ket;//$model->description;
                } else {
                    if ($statusasset == '3') {
                        $history->desc = "Di beli dari " . $cablama;
                    } elseif ($statusasset == '4') {
                        $history->desc = "Di sewa oleh " . $tobu . " - " . $tobranch;
                    }
                }

                $history->status = $model->status;
                $history->tobu = $tobu;
                $history->tobranch = $tobranch ? $tobranch : 'OTHER';
                $history->amount = $amountstatus;
                $history->sdate = new CDbExpression('NOW()');
                $history->amount = $amountstatus ? $amountstatus : 0;
                $history->created_at = new CDbExpression('NOW()');
                $history->updated_at = new CDbExpression('NOW()');

                if (!$history->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetHistory')) . CHtml::errorSummary($history));
                }


                //$dtl = $this->loadModel($id, 'AssetTransDetail');
                $dtl->approval = 3;
                if (!$dtl->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetTransDetail')) . CHtml::errorSummary($dtl));

            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'AssetTrans')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function getLastRowDetail($store)
    {
        $businessunit_id = $_COOKIE['businessunitid'];
        $criteria = new CDbCriteria;
        $criteria->compare('active', '1');
        $criteria->compare('asset_trans_branch', $store);
        $criteria->compare('businessunit_id', $businessunit_id);
        $model = AssetDetail::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    public function rubahDeprisiasi($assedetailid, $new)
    {
        $lastdeprisiasi = 0;
        $lastbalance = 0;
        $lastaccumulated = 0;

        $modeldetail = $this->loadModel($assedetailid, "AssetDetail");
        //$modelperiode = $new ? new AssetPeriode : $this->loadModel($assedetailid, "AssetPeriode");
        AssetPeriode::model()->deleteAll("asset_detail_id = :asset_detail_id", array(':asset_detail_id' => $modeldetail->asset_detail_id));
        //$modelperiode = AssetPeriode::model()->findByAttributes(array('asset_detail_id' => $assedetailid));
        //$modelperiode = new AssetPeriode;

        $days = date_parse_from_format("Y-m-d", $modeldetail->asset_trans_date);
        $tahun = $days['year'];
        $bulan = $days['month'] - 1;
        for ($j = 0; $j < $modeldetail->period; $j++) {
            $modelperiode = new AssetPeriode;
            $modelperiode->docref = $modeldetail->docref;
            $modelperiode->docref_other = $modeldetail->docref_other;
            $modelperiode->ati = $modeldetail->ati;
            $modelperiode->businessunit_id = $modeldetail->businessunit_id;
            $modelperiode->asset_detail_id = $modeldetail->asset_detail_id;
            $modelperiode->asset_id = $modeldetail->asset_id;
            $modelperiode->asset_group_id = $modeldetail->asset_group_id;
            $modelperiode->barang_id = $modeldetail->barang_id;
            $modelperiode->asset_trans_date = $modeldetail->asset_trans_date;
            $modelperiode->asset_trans_price = $modeldetail->asset_trans_price;
            $modelperiode->asset_trans_new_price = $modeldetail->asset_trans_new_price;
            $modelperiode->asset_trans_name = $modeldetail->asset_trans_name;
            $modelperiode->asset_trans_branch = $modeldetail->asset_trans_branch;
            $modelperiode->description = $modeldetail->description;

            $modelperiode->period = $modeldetail->period;
            $modelperiode->class = $modeldetail->class;
            $modelperiode->tariff = $modeldetail->tariff;

            //$penyusutan = $modelperiode->asset_trans_price * ($modelperiode->tariff/100) / $modelperiode->period;
            $penyusutan = $modelperiode->asset_trans_price / $modelperiode->period;

            $modelperiode->penyusutanperbulan = $penyusutan;
            $lastdeprisiasi = $modelperiode->penyusutanperbulan;

            $penyusutantahun = $penyusutan * $modelperiode->period;
            $modelperiode->penyusutanpertahun = $penyusutantahun;

            if ($lastbalance == 0 || $lastdeprisiasi == 0) {
                $modelperiode->balance = $modelperiode->asset_trans_price - $penyusutan;
                $lastbalance = $modelperiode->balance;

                $modelperiode->akumulasipenyusutan = $penyusutan;
                $lastaccumulated = $modelperiode->akumulasipenyusutan;
            } else {
                $modelperiode->balance = $lastbalance - $lastdeprisiasi;
                $lastbalance = $modelperiode->balance;

                $modelperiode->akumulasipenyusutan = $lastaccumulated + $penyusutan;
                $lastaccumulated = $modelperiode->akumulasipenyusutan;
            }

            //counter bulan
            /*$x = $j;
            $d = strtotime("$x months",strtotime($modeldetail->asset_trans_date));
            $newdate = date('Y-m-t',$d);*/
            /////////////////////////////////////////////
            ///
            if ($bulan < 12) {
                $bulan++;
                //$bulan = $bulan + $x;
            } else {
                $bulan = 1;
                $tahun++; // = $tahun + 1;
            }

            $hari = $days['day'];
            //$days->modify('last day of this month');
            $modelperiode->year = $tahun;//($newdate,"Y");//date('Y',$newdate);
            $modelperiode->month = $bulan;//date_parse_from_format("m", $newdate);//date('m',$newdate);
            $modelperiode->day = $hari;//date_parse_from_format("t", $newdate);//date('t',$newdate);
            $modelperiode->tdate = $tahun . '-' . $bulan . '-' . $hari;
            $modelperiode->tglpenyusutan = $modelperiode->tdate;

            //$modelperiode->tglpenyusutan = $newdate;

            $modelperiode->startdate = $modelperiode->asset_trans_date;
            $tmp = $modelperiode->period - 1;
            $tmpdate = new DateTime($modelperiode->asset_trans_date);
            $tmpstr = '+' . $tmp . 'months';
            $end = date_modify($tmpdate, $tmpstr);
            //$modelperiode->enddate = $newdate = date('Y-m-t',strtotime("+$tmp months",strtotime($modeldetail->asset_trans_date)));
            $end = $end->format('Y-m-d');
            $modelperiode->enddate = $end;
            //$tmp = $modelperiode->period - 1;
            //$modelperiode->enddate = $newdate = date('Y-m-t',strtotime("+$tmp months",strtotime($modeldetail->asset_trans_date)));

            //$modelperiode->enddate = $newdate = date('Y-m-t',strtotime("+$modelperiode->period months",strtotime($modeldetail->asset_trans_date)));

            $modelperiode->status = $modeldetail->status;

            $modelperiode->created_at = $modeldetail->created_at;
            $modelperiode->updated_at = $modeldetail->updated_at;

            if (!$modelperiode->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'modelperiode')) . CHtml::errorSummary($modelperiode));
            }


        }
    }

    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if (isset($_POST['start'])) {
            $start = $_POST['start'];

        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        $param = [];
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tdate'])) {
            $asset_trans_date = $_POST['asset_trans_date'];
            $criteria->addCondition('asset_trans_date = date(:asset_trans_date)');
            $param[':asset_trans_date'] = "$asset_trans_date";
        }
        if (isset($_POST['status']) && $_POST['status'] != 'all') {
            $status = $_POST['status'];
            $criteria->addCondition('status = :status');
            $param[':status'] = $status;
        }
        if (isset($_POST['approval']) && $_POST['approval'] != 'all') {
            $approval = $_POST['approval'];
            $criteria->addCondition('approval = :approval');
            $param[':approval'] = $approval;
        }

        $businessunit_id = $_COOKIE['businessunitid'];

        $criteria->addCondition('visible = 1');
        /*$criteria->addCondition('bu_pengirim = :businessunit_id');
        $param[':businessunit_id'] = $businessunit_id;
        $criteria->params = $param;*/

        $criteria->params = $param;

        $model = AssetTransferView::model()->findAll($criteria);
        $total = AssetTransferView::model()->count($criteria);

        /*$model = AssetTrans::model()->findAll($criteria);
        $total = AssetTrans::model()->count($criteria);*/

        $this->renderJson($model, $total);

    }

}