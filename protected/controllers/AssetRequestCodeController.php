<?php

class AssetRequestCodeController extends GxController {

public function actionCreate() {
$model = new AssetRequestCode;
if (!Yii::app()->request->isAjaxRequest)
return;
if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetRequestCode'][$k] = $v;
}
$model->attributes = $_POST['AssetRequestCode'];
    $model->timestamps = new CDbExpression('NOW()');
$msg = "Data gagal disimpan.";

    if ($model->save()) {
$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->request_code_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg));
Yii::app()->end();

}

}
    public function actionSetCode($id) {

        $user_id = Yii::app()->user->getId();
        $Users = Users::model()->findByPk($user_id);

        if($Users->security_roles_id == 4)
        {
            echo CJSON::encode(array(
                'success'=>'Access Denied',
                'msg'=>'Access Denied'
            ));
            Yii::app()->end();
        }

        $model = $this->loadModel($id, 'AssetRequestCode');


        if (isset($_POST) && !empty($_POST)) {
            foreach($_POST as $k=>$v){
                if (is_angka($v)) $v = get_number($v);
                $_POST['AssetRequestCode'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['AssetRequestCode'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->request_code_id;

                $_POST['nama_barang'] = $_POST['name'];
                $_POST['kode_barang'] = $_POST['code'];
                $_POST['ket'] = $_POST['desc'];
                $_POST['tipe_barang_id'] = $_POST['tipe'];
                Yii::app()->runController('asset/Create');
            } else {
                $msg .= " ".implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest)
            {
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg
                ));
                Yii::app()->end();
            } else
            {
                $this->redirect(array('view', 'id' => $model->request_code_id));
            }
        }
    }
public function actionUpdate($id) {
$model = $this->loadModel($id, 'AssetRequestCode');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetRequestCode'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['AssetRequestCode'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->request_code_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->request_code_id));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'AssetRequestCode')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}
    $param = array();

$criteria = new CDbCriteria();
if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}
    if (isset($_POST['name'])) {
        $name = $_POST['name'];
        $criteria->addCondition('name like :name');
        $param[':name'] = "%$name%";
    }
    if (isset($_POST['code'])) {
        $code = $_POST['code'];
        $criteria->addCondition('code like :code');
        $param[':code'] = "%$code%";
    }
    if (isset($_POST['desc'])) {
        $desc = $_POST['desc'];
        $criteria->addCondition('desc like :desc');
        $param[':desc'] = "%$desc%";
    }
    if (isset($_POST['tipe']) && $_POST['tipe'] != 'all') {
        $tipe = $_POST['tipe'];
        $criteria->addCondition('tipe like :tipe');
        $param[':tipe'] = "%$tipe%";
    }
    $criteria->order = 'code';
    $criteria->params = $param;
$model = AssetRequestCode::model()->findAll($criteria);
$total = AssetRequestCode::model()->count($criteria);

$this->renderJson($model, $total);

}

}