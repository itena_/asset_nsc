<?php

class AssetCategoryController extends GxController
{

    public function actionCreate()
    {
        $model = new AssetCategory;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['AssetCategory'][$k] = $v;
            }
            $model->attributes = $_POST['AssetCategory'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $model->businessunit_id = $businessunit_id;
            $msg = "Data gagal disimpan.";

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->category_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();

        }

    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'AssetCategory');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['AssetCategory'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['AssetCategory'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->category_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->category_id));
            }
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'AssetCategory')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }


    public function actionIndex()
    {
        $limit = isset($_POST['limit']) ? $_POST['limit'] : 20;
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $param = array();

        $criteria = DbCmd::instance()->addFrom("{{asset_category}}");
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->setLimit($limit, $start);
        }

        $criteria->addOrder("category_code");
        $criteria->params = $param;
        $model = $criteria->queryAll();
        $total = $criteria->queryCount();

        $this->renderJsonArrWithTotal($model, $total);
    }

}