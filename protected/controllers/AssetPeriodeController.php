<?php

class AssetPeriodeController extends GxController
{
    public function actionIndex() {
        $cmd = DbCmd::instance()->addFrom("{{asset_periode}} ap")
            ->addSelect("a.businessunit_id, a.asset_group_id, a.doc_ref, a.description as asset_trans_name")
            ->addSelect("ad.ati, 	ad.class, ad.period, ad.tariff, ad.desc")
            ->addSelect("
                asset_periode_id,
                ap.asset_id,
                ap.asset_detail_id,
                tglpenyusutan,       
                ap.penyusutanpertahun,
                format(balance,2) AS balance,
                format(ap.akumulasipenyusutan,2) AS akumulasipenyusutan,
                format(price_acquisition,2) AS price_acquisition,
                format(ap.penyusutanperbulan,2) AS penyusutanperbulan,
                tdate,
                visible
            ")
            ->addLeftJoin("{{asset}} a","a.asset_id = ap.asset_id")
            ->addLeftJoin("{{asset_detail}} ad","ad.asset_detail_id = ap.asset_detail_id")
        ;

        if($_POST['asset_detail_id']){
            $cmd->addCondition('ap.asset_detail_id = :asset_detail_id')
                ->addParam(':asset_detail_id', $_POST['asset_detail_id'])
            ;
        }

        $model = $cmd->getQuery();
        $model = $cmd->queryAll();
        $total = count($model);

        $this->renderJsonArrWithTotal($model, $total);
    }

}