<?php
class LocationController extends GxController
{
    public function actionCreate()
    {
        $model = Location::saveData();
        echo CJSON::encode(array(
            'success' => true,
            'msg' => "Data berhasil disimpan dengan id $model"));
        Yii::app()->end();
    }
    public function actionUpdate($key)
    {
        $model = Location::saveData($key);
        echo CJSON::encode(array(
            'success' => true,
            'msg' => "Data berhasil disimpan dengan id $model"));
        Yii::app()->end();
    }
    public function actionIndex()
    {
        $data = Location::getData();
        $model = $data->queryAll();
        $count = count($model);
        return $this->renderJsonArrWithTotal($model, $count);
    }
}