<?php

class AssetCategorySubController extends GxController
{

    public function actionCreate()
    {
        $model = new AssetCategorySub;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            try {
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['AssetCategorySub'][$k] = $v;
                }
                $businessunit_id = $_COOKIE['businessunitid'];

                $model->attributes = $_POST['AssetCategorySub'];
                $model->asset_group_id = $_POST['asset_group_id'];
                $model->businessunit_id = $businessunit_id;
                $model->category_id = $_POST['category_id'];
                $model->timestamp = new CDbExpression('NOW()');
                $msg = "Data gagal disimpan.";

                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetCategorySub')) . CHtml::errorSummary($model));
                }
                $status = true;
                $msg = "Data berhasil di simpan.";
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg));
                Yii::app()->end();
            } catch (Exception $ex) {
                //->rollback();
                $status = false;
                $msg = $ex->getMessage();
                echo CJSON::encode(array(
                    'success' => 'failed',
                    'msg' => $msg
                ));
            }

        }
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'AssetCategorySub');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['AssetCategorySub'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['AssetCategorySub'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->category_sub_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->category_sub_id));
            }
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'AssetCategorySub')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }


    public function actionIndex()
    {
        $limit = isset($_POST['limit']) ? $_POST['limit'] : 20;
        $start = isset($_POST['start']) ? $_POST['start'] : 0;

        $criteria = DbCmd::instance()->addFrom("{{asset_category_sub}} acs")
            ->addSelect("acs.*, ac.category_name, ac.category_desc")
            ->addLeftJoin("{{asset_category}} ac","ac.category_id = acs.category_id")
        ;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->setLimit($limit, $start);
        }

        if (isset($_POST['category_id'])) {
            $criteria->addCondition('acs.category_id = :category_id')
                ->addParam(':category_id', $_POST['category_id'])
            ;
        }

        $criteria->addOrder("sub_code");
        $model = $criteria->queryAll();
        $total = $criteria->queryCount();

        $this->renderJsonArrWithTotal($model, $total);
    }

}