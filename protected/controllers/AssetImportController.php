<?php
/**
 * Created by PhpStorm.
 * User: wisnu
 * Date: 07/07/2018
 * Time: 10:13
 */

class AssetImportController extends GxController
{
    public function actionImport()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {

            $user_id = Yii::app()->user->getId();
            $users = UserView::model()->findByAttributes(['id' => $user_id]);
            $businessunit_id = $_COOKIE['businessunitid'];
            $bu = Businessunit::model()->findByPk( $businessunit_id );


            $businessunitcode = $bu->businessunit_code;
            $username = $users->user_id;

            $sheets = CJSON::decode($_POST['data']);
            $trans_date = $_POST['tgl'];
            $importtype = $_POST['importtype'];

            foreach ($sheets as $sheet)
            {
                if($importtype == "I")
                {
                    U::item_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "B")
                {
                    U::branch_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "C")
                {
                    U::category_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "CL")
                {
                    U::class_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "BU")
                {
                    U::businessunit_import($sheet, $trans_date, $username);
                }
                elseif ($importtype == "A")
                {
                    $migrasi = 0;
                    $u = new U();
                    $u->asset_import($sheet, $trans_date, $user_id, $businessunit_id, $migrasi);
                }
                elseif ($importtype == "AM")
                {
                    $migrasi = 1;
                    $u = new U();
                    $u->asset_import($sheet, $trans_date, $user_id, $businessunit_id, $migrasi);
                }

            }
        }
    }
}