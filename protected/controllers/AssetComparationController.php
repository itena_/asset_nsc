<?php

class AssetComparationController extends GxController
{
    public function actionCreate()
    {
        $message = "Data saved.";
        $status  = true;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if($_POST['mode'] == 0)
                $model = new AssetComparation;
            else
                $model = AssetComparation::model()->findByPk($_POST['id']);

            $model->tgl = $_POST['tgl'];
            $model->store_id = $_POST['store_id'];
            $model->status = $_POST['status'];
            $model->version = $_POST['mode'] == 0 ? 1 : $model->version + 1;
            if (!$model->save()) {
                $message = CHtml::errorSummary($model);
                SysPrefs::generateErrorMessage(false, $message);
            }

            AssetComparationDetail::model()->updateAll(['visible' => 0],"asset_comparation_id = :asset_comparation_id", array(':asset_comparation_id' => $model->asset_comparation_id));

            $detils = CJSON::decode($_POST['detil']);
            foreach ($detils as $detil) {
                $modelDetail = new AssetComparationDetail;
                foreach ($detil as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST["AssetDetail"][$k] = $v;
                }
                $modelDetail->attributes = $_POST["AssetDetail"];
                $modelDetail->asset_comparation_id = $model->asset_comparation_id;
                $modelDetail->tipe = 'new';
                if (!$modelDetail->save()) {
                    $message = CHtml::errorSummary($modelDetail);
                }
            }

            $detilsExisting = CJSON::decode($_POST['detilExisting']);
            foreach ($detilsExisting as $detil) {
                $modelDetail = new AssetComparationDetail;
                foreach ($detil as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST["AssetDetailExisting"][$k] = $v;
                }
                $modelDetail->attributes = $_POST["AssetDetailExisting"];
                $modelDetail->asset_comparation_id = $model->asset_comparation_id;
                $modelDetail->tipe = 'existing';
                if (!$modelDetail->save()) {
                    $message = CHtml::errorSummary($modelDetail);
                }
            }

            AssetComparationDetail::model()->deleteAll("asset_comparation_id = :asset_comparation_id AND visible = 0", array(':asset_comparation_id' => $model->asset_comparation_id));

            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $message = $ex->getMessage();

            if (strpos($message, 'Cannot delete or update a parent row: a foreign key constraint fails') !== false) {
                $message = 'This problem already has solution(s), so it cannot be deleted.';
            }

            $status  = false;
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $message));
        Yii::app()->end();
    }

    public function actionIndex()
    {
        $cmd = DbCmd::instance()
            ->addFrom('{{asset_comparation}} t')
            ->addSelect('t.*, s.nama_store, u.name nama_user')
            ->addLeftJoin('{{store}} s','s.store_id = t.store_id')
            ->addLeftJoin('{{users}} u','u.id = t.user_id')
            ->addOrder("tdate desc");

        $count = count($cmd->queryAll());
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $cmd->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }
//        $model = $cmd->getQuery();
        $model = $cmd->queryAll();
        return $this->renderJsonArrWithTotal($model, $count);
    }
}