<?php

class AssetRequestController extends GxController {

    public function actionCreate() {

        if (!Yii::app()->request->isAjaxRequest)
        return;
        if (isset($_POST) && !empty($_POST))
        {
            $is_new = $_POST['mode'] == 0;
            $req_id = $_POST['id'];

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try{
                $qty = $_POST['qty'];
                foreach($_POST as $k=>$v){
                if (is_angka($v)) $v = get_number($v);
                    $_POST['AssetRequest'][$k] = $v;
                }
                $user_id = Yii::app()->user->getId();
                $Users = Users::model()->findByPk($user_id);
                $businessunit_id = $_COOKIE['businessunitid'];



                for ($i=0;$i<$qty;$i++)
                {
                    $last = $this->getLastRow() + 1;
                    $model = $is_new ? new AssetRequest : $this->loadModel($req_id, "AssetRequest");
                    //$model = new AssetRequest;
                    $model->attributes = $_POST['AssetRequest'];
                    $model->doc_ref = 'ASTREQ-'.$Users->store.'-'.$last;
                    $model->tgl = $_POST['tgl'];
                    $model->barang_id = $_POST['barang_id'];
                    $model->merk = $_POST['merk'];
                    $model->category_id = $_POST['category_id'];
                    $model->category_sub_id = $_POST['category_sub_id'];
                    $catSub = AssetCategorySub::model()->findByPk( $_POST['category_sub_id'] );
                    $model->group_id = $catSub->asset_group_id;
                    $model->qty = 1;
                    $harga = get_number($_POST['price']);
                    $model->price = $harga;
                    $model->serialnumber = $_POST['serialnumber'];
                    $model->conditions = $_POST['conditions'];
                    $model->location = $_POST['location'];
                    $model->tdate = new CDbExpression('NOW()');
                    $model->store_kode = $Users->store;
                    $model->businessunit_id = $businessunit_id;
                    $model->user_id = $user_id;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetRequest')) . CHtml::errorSummary($model));
                    }


                }
                $msg = "Data berhasil disimpan.";
                $status = true;

                $transaction->commit();
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg));
                Yii::app()->end();
            }
            catch (Exception $ex)
            {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
                echo CJSON::encode(array(
                    'success' => 'failed',
                    'msg' => $msg
                ));
            }


        }

    }

    public function getLastRow()
    {
        $user_id = Yii::app()->user->getId();
        $Users = Users::model()->findByPk($user_id);
        $criteria = new CDbCriteria;
        $criteria->compare('businessunit_id',$_COOKIE['businessunitid']);
        $criteria->compare('store_kode',$Users->store);
        $model = AssetRequest::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    public function actionApprove()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        $model = $this->loadModel($_POST['id'], 'AssetRequest');
        $userid = Yii::app()->user->getId();
        $user = Users::model()->findByPk($userid);
        $creator = $model->store_kode == STOREID;
        //$approver = $user->is_available_role(257);
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            /*if (!$approver || !$creator) {
                throw new Exception("Anda tidak diperbolehkan mengubah data ini.");
                $msg = "Anda tidak diperbolehkan mengubah data ini.";
            }*/

            if($model->approved == 1)
            {
                throw new Exception("Data ini telah di approve.");
                $msg = "Data ini telah di approve.";
            }

            $model->approved = 1;
            $model->approved_date = new CDbExpression('NOW()');
            $model->approved_user = $userid;

            if (!$model->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetRequest')) . CHtml::errorSummary($model));

            $_POST['mode'] = 0;
            $_POST['Asset']['branch'] = $model->store_kode;
            $_POST['Asset']['asset_group_id'] = $model->group_id;
            $_POST['Asset']['qty'] = $model->qty;
            $_POST['Asset']['ppnasset'] = $model->price*10/100;
            $_POST['Asset']['hide'] = 0;
            $_POST['Asset']['date_acquisition'] = $model->tgl;
            $_POST['Asset']['asset_barang_id'] = $model->barang_id;
            $_POST['Asset']['new_price_acquisition'] = 0;
            $_POST['Asset']['price_acquisition'] = $model->price;
            $_POST['Asset']['category_id'] = $model->category_id;
            $_POST['Asset']['category_sub_id'] = $model->category_sub_id;
            $_POST['Asset']['description'] = $model->note;
            $_POST['Asset']['serialnumber'] = $model->serialnumber;
            $_POST['Asset']['conditions'] = $model->conditions;
            $_POST['Asset']['location'] = $model->location;
            $_POST['Asset']['merk'] = $model->merk;

            $transaction->commit();
            Yii::app()->runController('asset/Create');

            $msg = t('save.success', 'app');
            $status = true;
            $transaction->commit();
        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }

        app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'success' => $status,
            'id' => $model->doc_ref,
            'msg' => $msg));
        Yii::app()->end();
    }

    public function actionDeclined()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        $model = $this->loadModel($_POST['id'], 'AssetRequest');
        $userid = Yii::app()->user->getId();
        $user = Users::model()->findByPk($userid);
        $creator = $model->store_kode == STOREID;
        $approver = $user->is_available_role(257);

        try{
            //$this->redirect(array('asset/Create'));
            //AssetController::class()

            if (!$approver || !$creator) {
                throw new Exception("Anda tidak diperbolehkan mengubah data ini.");
                $msg = "Anda tidak diperbolehkan mengubah data ini.";
            }

            if($model->approved == 1)
            {
                throw new Exception("Data ini telah di approve.");
                $msg = "Data ini telah di approve.";
            }

            $model->approved = 2;
            $model->approved_date = new CDbExpression('NOW()');
            $model->approved_user = Yii::app()->user->getId();

            if (!$model->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetRequest')) . CHtml::errorSummary($model));
            $msg = t('save.success', 'app');
            $status = true;
        }
        catch (Exception $ex)
        {
            //$transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'success' => $status,
            'id' => $model->doc_ref,
            'msg' => $msg));
        Yii::app()->end();


    }

public function actionUpdate($id) {
$model = $this->loadModel($id, 'AssetRequest');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetRequest'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['AssetRequest'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->request_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->request_id));
}
}
}

    public function actionDelete($id) {
    if (Yii::app()->request->isPostRequest) {
    $msg = 'Data berhasil dihapus.';
    $status = true;
    try {
    $this->loadModel($id, 'AssetRequest')->delete();
    } catch (Exception $ex) {
    $status = false;
    $msg = $ex;
    }
    echo CJSON::encode(array(
    'success' => $status,
    'msg' => $msg));
    Yii::app()->end();
    } else
    throw new CHttpException(400,
    Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }


    public function actionIndex() {
        if(isset($_POST['limit'])) {
        $limit = $_POST['limit'];
        } else {
        $limit = 20;
        }

        if(isset($_POST['start'])){
        $start = $_POST['start'];

        } else {
        $start = 0;
        }
        $businessunit_id = $_COOKIE['businessunitid'];
        $user_id = Yii::app()->user->getId();
        $Users = Users::model()->findByPk($user_id);

        $param = array();
        $criteria = new CDbCriteria();

        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
        (isset($_POST['limit']) && isset($_POST['start']))) {
        $criteria->limit = $limit;
        $criteria->offset = $start;
        }

        if (isset($_POST['doc_ref'])) {
            $doc_ref = $_POST['doc_ref'];
            $criteria->addCondition('doc_ref like :doc_ref');
            $param[':doc_ref'] = "%$doc_ref%";
        }
        if (isset($_POST['price'])) {
            $price = $_POST['price'];
            $criteria->addCondition('price like :price');
            $param[':price'] = "%$price%";
        }
        if (isset($_POST['serialnumber'])) {
            $serialnumber = $_POST['serialnumber'];
            $criteria->addCondition('serialnumber like :serialnumber');
            $param[':serialnumber'] = "%$serialnumber%";
        }
        if (isset($_POST['location'])) {
            $location = $_POST['location'];
            $criteria->addCondition('location like :location');
            $param[':location'] = "%$location%";
        }
        if (isset($_POST['note'])) {
            $note = $_POST['note'];
            $criteria->addCondition('note like :note');
            $param[':note'] = "%$note%";
        }
        if (isset($_POST['nama_barang'])) {
            $nama_barang = $_POST['nama_barang'];
            $criteria->addCondition('nama_barang like :nama_barang');
            $param[':nama_barang'] = "%$nama_barang%";
        }
        if (isset($_POST['kode_barang'])) {
            $kode_barang = $_POST['kode_barang'];
            $criteria->addCondition('kode_barang like :kode_barang');
            $param[':kode_barang'] = "%$kode_barang%";
        }
        if (isset($_POST['tgl'])) {
            $tgl = date('Y-m-d', strtotime($_POST['tgl']));
            $criteria->addCondition('tgl = date(:tgl)');
            $param[':tgl'] = "$tgl";
        }
        if (isset($_POST['merk'])) {
            $merk = $_POST['merk'];
            $criteria->addCondition('merk like :merk');
            $param[':merk'] = "%$merk%";
        }
        if (isset($_POST['approved']) && $_POST['approved'] != 'all') {
            $criteria->addCondition('approved = :approved');
            $param[':approved'] = $_POST['approved'];
        }
        if (isset($_POST['conditions']) && $_POST['conditions'] != 'all') {
            $criteria->addCondition('conditions = :conditions');
            $param[':conditions'] = $_POST['conditions'];
        }



        if($Users->security_roles_id == 4)
        {
            $criteria->addCondition('store_kode = :store_kode');
            $param[':store_kode'] = $Users->store;
        }

        $criteria->addCondition('businessunit_id = :businessunit_id');
        $param[':businessunit_id'] = $businessunit_id;

        $criteria->params = $param;
        $model = AssetRequestView::model()->findAll($criteria);
        $total = AssetRequestView::model()->count($criteria);

        $this->renderJson($model, $total);

    }

}