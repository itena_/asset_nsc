<?php

class importCommand extends CConsoleCommand
{
    public function actionImport($query)
    {
        ini_set('max_execution_time', -1);

        Yii::app()->db->createCommand($query)->execute();

        ini_set('max_execution_time', 30);
    }
}