<?php
Yii::import('application.models._base.BaseAssetDetailView');

class AssetDetailView extends BaseAssetDetailView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->asset_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}