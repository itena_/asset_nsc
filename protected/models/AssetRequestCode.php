<?php
Yii::import('application.models._base.BaseAssetRequestCode');

class AssetRequestCode extends BaseAssetRequestCode
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->request_code_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->request_code_id = $uuid;
        }
        return parent::beforeValidate();
    }
}