<?php
Yii::import('application.models._base.BaseAssetRequestView');

class AssetRequestView extends BaseAssetRequestView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->request_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->request_id = $uuid;
        }
        return parent::beforeValidate();
    }
}