<?php

Yii::import('application.models._base.BaseSysPrefs');
class SysPrefs extends BaseSysPrefs
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->sys_prefs_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sys_prefs_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public static function get_val($index)
    {
        $pref = SysPrefs::model()->find('name_ = :name_',
            array(':name_' => $index));
        if($pref == null){
            return "";
        }
        return $pref->value_;
    }
    public static function save_value($name_, $value_, $store)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO nscc_sys_prefs (name_, value_, store, up)
                VALUES (:name_, :value_, :store, 0)"
        );
        return $comm->execute(array(':name_' => $name_, ':value_' => $value_, ':store' => $store));
    }
    public static function generateErrorMessage($status, $msg, $important = null) {
        if($important) {
            for($i=0;$i<count($important);$i++) {
                $msg = str_replace($important[$i], '<b style="color:red;">'.$important[$i].'</b>', $msg);
            }
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
//        logToFile($msg);
        Yii::app()->end();
    }
}