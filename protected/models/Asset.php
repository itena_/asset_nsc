<?php
Yii::import('application.models._base.BaseAsset');

class Asset extends BaseAsset
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->asset_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_id = $uuid;
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        if ($this->created_at == null) {
            $this->created_at = new CDbExpression('NOW()');
        }
        $this->updated_at = new CDbExpression('NOW()');
        return parent::beforeValidate();
    }
    public static function saveAsset($import = 0, $counterStore = []) {
        $businessunit_id = $_COOKIE['businessunitid'];
        $businessunit    = Businessunit::model()->findByPk($businessunit_id);

        $is_new  = $_POST['mode'] == 0;
        $model = $is_new ? new Asset : Asset::model()->findByPk($_POST['id']);

        foreach ($_POST as $k => $v) {
            if (is_angka($v)) $v = get_number($v);
            $_POST['Asset'][$k] = $v;
        }
        $model->attributes = $_POST['Asset'];
        $model->businessunit_id = $businessunit_id;
        $model->hide = isset($_POST['Asset']['hide']) ? 1 : 0;

        $ag = AssetGroup::model()->findByPk($model->asset_group_id);
        $golongan = $ag->golongan;
        $tariff   = $ag->tariff;
        $period   = $ag->period;
        $desc     = $ag->desc;

        $store_kode = Store::model()->findByPk($model->store_id)->store_kode;

        $model->store_kode = $store_kode;

        if(count($counterStore) > 0)
            $last   = $counterStore[$store_kode];
        else
            $last   = self::getLastRow($store_kode) + 1;

        $cabang = $businessunit->businessunit_code . '/' . $store_kode;

        $store  = Store::model()->findByAttributes(array('store_kode' => $store_kode, 'businessunit_id' => $businessunit_id));
        $area   = str_pad($store->wilayah_id, 2, '0', STR_PAD_LEFT);
        $urutan = $store->tipe;

        $year = date('Y', strtotime($model->date_acquisition));

        if ($is_new) {
            $docref = $cabang . '/AST/' . $year . "/" . $last;
        } else {
            $docref = $model->doc_ref;
            AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $model->asset_id));
            AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $model->asset_id));
            GlTrans::model()->deleteAll("masterassetid = :masterassetid", array(':masterassetid' => $model->asset_id));
        }

        $model->doc_ref = $docref;
        $ati = $area . '/' . $cabang . '/' . $urutan . '/' . $golongan . '/' . $last;

        $params['ati'] = $ati;
        $params['grupid'] = $model->asset_group_id;
        $params['golongan'] = $golongan;
        $params['tariff'] = (float)$tariff;
        $params['period'] = $period;
        $params['desc'] = $desc;
        $params['isNew'] = $is_new;

        if($import == 0) {
            if (!$model->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset')) . CHtml::errorSummary($model));
            } else {
                AssetDetail::saveAssetDetail($model, $params);
            }
        } else {
            $asset_id = DbCmd::uuid();
            $params['asset_id'] = $asset_id;
            $params['user_id'] = Yii::app()->user->getId();
            $query['asset'] = U::getQuery([
                $asset_id
                , $model->asset_group_id
                , Yii::app()->user->getId()
                , $model->businessunit_id
                , $model->doc_ref
                , $model->branch
                , $model->description
                , $model->date_acquisition
                , $model->hide
                , $model->active
                , date('Y-m-d h:i:s')
                , date('Y-m-d h:i:s')]);

            $result = AssetDetail::saveAssetDetail($model, $params, 1);
            $query['assetDetail'] = $result['assetDetail'];
            $query['gl'] = $result['gl'];
            $query['history'] = $result['history'];
            $query['assetPeriode'] = $result['assetPeriode'];

            return $query;
        }

        return $model;
    }
    public static function getData() {
        $dbcmd = DbCmd::instance()->addFrom("{{asset}} a")
            ->addSelect("a.*, ad.*, category_code, category_name, category_desc")
            ->addSelect("sub_code, sub_name, sub_desc, businessunit_code, s.store_kode")
            ->addLeftJoin("{{asset_detail}} ad","ad.asset_id = a.asset_id")
            ->addLeftJoin("{{asset_category}} ac", "ac.category_id = ad.category_id")
            ->addLeftJoin("{{asset_category_sub}} acs", "acs.category_sub_id = ad.category_sub_id")
            ->addLeftJoin("businessunit b", "b.businessunit_id = a.businessunit_id")
            ->addLeftJoin("{{store}} s", "s.store_id = a.store_id")
        ;
        $dbcmd->addOrder('a.doc_ref');
        return $dbcmd;
    }
    public static function getLastRow($store) {
        $criteria = new CDbCriteria;
        $criteria->compare('store_kode', $store);
        $criteria->compare('active', '1');
        $criteria->compare('businessunit_id', $_COOKIE['businessunitid']);

        return count(Asset::model()->findAll($criteria));
    }
    public static function getAssetList($from, $to, $cat_id, $cat_sub_id, $businessunit_id, $branch, $status) {
	    $db = new DbCmd();
	    $db->addSelect("b.kode_barang, b.nama_barang, b.ket, ad.ati,
        ass.branch AS asset_trans_branch, ass.date_acquisition AS asset_trans_date, g.golongan,
        ad.location, ass.description, ad.price_acquisition AS asset_trans_price, ad.penyusutanperbulan,
        ac.category_name")
            ->addFrom("nscc_asset_detail ad")
            ->addLeftJoin("nscc_asset_barang b","b.barang_id = ad.barang_id")
            ->addLeftJoin("nscc_asset ass", "ass.asset_id = ad.asset_id")
            ->addLeftJoin("nscc_asset_group g","g.asset_group_id = ass.asset_group_id")
            ->addLeftJoin("nscc_asset_category ac","ac.category_id = ad.category_id")
        ;

        if ($from != undefined || $to != undefined) {
            $db->addCondition("DATE(ass.date_acquisition) >= :from and DATE(ass.date_acquisition) <= :to")
                ->addParams(array(':from' => $from, ':to' => $to))
            ;
        } else
            $db->addCondition("ad.status != 0");

        if ($businessunit_id != null) {
            $db->addCondition("ass.businessunit_id = '" . $businessunit_id . "'");
        }
        if ($cat_id != null) {
            $db->addCondition("ad.category_id = '" . $cat_id . "'");
        }
        if ($cat_sub_id != null) {
            $db->addCondition("ad.category_sub_id = '" . $cat_sub_id . "' ");
        }
        if ($branch != null) {
            $db->addCondition("ass.branch = '" . $branch . "' ");
        }
        if ($status != null) {
            $db->addCondition("ad.status = '" . $status . "' ");
        }

        $aaa = $db->getQuery();
        return $db->queryAll();
    }
}