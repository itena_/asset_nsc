<?php

/**
 * This is the model class for table "{{asset_transfer_view}}".
 *
 * The followings are the available columns in table '{{asset_transfer_view}}':
 * @property string $trans_id
 * @property string $bu_pengirim
 * @property string $bu_penerima
 * @property string $store_penerima
 * @property string $store_pengirim
 * @property string $ati
 * @property string $asset_name
 * @property double $bookvalue
 * @property double $amount
 * @property double $ppn
 * @property string $note
 * @property string $tdate
 * @property integer $approval
 * @property integer $status
 * @property integer $visible
 */
class AssetTransferView extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{asset_transfer_view}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trans_id, bu_pengirim, bu_penerima, visible', 'required'),
			array('approval, status, visible', 'numerical', 'integerOnly'=>true),
			array('bookvalue, amount, ppn', 'numerical'),
			array('trans_id', 'length', 'max'=>50),
			array('bu_pengirim, bu_penerima', 'length', 'max'=>20),
			array('store_penerima, store_pengirim, ati, asset_name', 'length', 'max'=>255),
			array('note, tdate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('trans_id, bu_pengirim, bu_penerima, store_penerima, store_pengirim, ati, asset_name, bookvalue, amount, ppn, note, tdate, approval, status, visible', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'trans_id' => 'Trans',
			'bu_pengirim' => 'Bu Pengirim',
			'bu_penerima' => 'Bu Penerima',
			'store_penerima' => 'Store Penerima',
			'store_pengirim' => 'Store Pengirim',
			'ati' => 'Ati',
			'asset_name' => 'Asset Name',
			'bookvalue' => 'Bookvalue',
			'amount' => 'Amount',
			'ppn' => 'Ppn',
			'note' => 'Note',
			'tdate' => 'Tdate',
			'approval' => 'Approval',
			'status' => 'Status',
			'visible' => 'Visible',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('trans_id',$this->trans_id,true);
		$criteria->compare('bu_pengirim',$this->bu_pengirim,true);
		$criteria->compare('bu_penerima',$this->bu_penerima,true);
		$criteria->compare('store_penerima',$this->store_penerima,true);
		$criteria->compare('store_pengirim',$this->store_pengirim,true);
		$criteria->compare('ati',$this->ati,true);
		$criteria->compare('asset_name',$this->asset_name,true);
		$criteria->compare('bookvalue',$this->bookvalue);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('ppn',$this->ppn);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('tdate',$this->tdate,true);
		$criteria->compare('approval',$this->approval);
		$criteria->compare('status',$this->status);
		$criteria->compare('visible',$this->visible);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AssetTransferView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
