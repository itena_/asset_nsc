<?php
Yii::import('application.models._base.BaseAssetDetail');

class AssetDetail extends BaseAssetDetail
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->asset_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_detail_id = $uuid;
        }
        if ($this->status == null) {
            $this->status = 1;
        }
        return parent::beforeValidate();
    }

    public static function saveAssetDetail($model, $params, $import = 0)
    {
        $penyusutan = $penyusutantahun = 0;

        $modeldetail = new AssetDetail;
        foreach ($model as $k => $v) {
            if (is_angka($v)) $v = get_number($v);
            $_POST['Detail'][$k] = $v;
        }
        $modeldetail->attributes = $_POST['Asset'];
        $modeldetail->attributes = $_POST['Detail'];
        $modeldetail->asset_detail_id = $params['isNew'] ? null : $modeldetail->asset_detail_id;
        $modeldetail->ati = $params['ati'];

        $modeldetail->class = $params['golongan'];
        $modeldetail->tariff = $params['tariff'];
        $modeldetail->period = $params['period'];
        $modeldetail->desc = $params['desc'];

        if ($params['period']) {
            $penyusutan = $modeldetail->price_acquisition / $params['period'];
            $penyusutantahun = $penyusutan * $params['period'];
        }

        $modeldetail->penyusutanperbulan = $penyusutan;
        $modeldetail->penyusutanpertahun = $penyusutantahun;

        if ($import == 0) {
            if (!$modeldetail->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset Detail')) . CHtml::errorSummary($modeldetail));
            } else {
                if($model->active = 1)
                    Gl::saveGl($model, $modeldetail);
                History::saveHistory($model, $modeldetail);
                AssetPeriode::saveAssetPeriode($model, $modeldetail, $params);
            }
        } else {
            $assetDetail_id = DbCmd::uuid();
            $params['assetDetail_id'] = $assetDetail_id;
            $query['assetDetail'] = U::getQuery([
                $assetDetail_id,
                $params['asset_id'],
                $modeldetail->ati,
                $modeldetail->non_ati,
                $modeldetail->category_id,
                $modeldetail->category_sub_id,
                NULL,
                $_POST['description'],
                $modeldetail->qty,
                $modeldetail->price_acquisition,
                $modeldetail->new_price_acquisition,
                $modeldetail->class,
                $modeldetail->period,
                $modeldetail->tariff,
                $modeldetail->desc,
                $modeldetail->penyusutanperbulan,
                $modeldetail->penyusutanpertahun,
                1,
                NULL,
                NULL,
                $modeldetail->conditions,
                NULL,
                NULL,
                $modeldetail->expdate,
                $modeldetail->asset_user,
                $modeldetail->location
            ]);
            $query['gl'] = Gl::saveGl($model, $modeldetail, 1, $params);
            $query['history'] = History::saveHistory($model, $modeldetail, 1, $params);
            $query['assetPeriode'] = AssetPeriode::saveAssetPeriode($model, $modeldetail, $params, 1);

            return $query;
        }
    }

    public static function getData()
    {
        $cmd = DbCmd::instance()->addFrom("{{asset_detail}} ad")
            ->addSelect("a.businessunit_id, a.description, ad.*")
            ->addLeftJoin("{{asset}} a", "a.asset_id = ad.asset_id")
            ->addCondition("businessunit_id = :businessunitid")
            ->addParams([':businessunitid' => $_COOKIE['businessunitid']]);

        if (isset($_POST['date_acquisition'])) {
            $cmd->addCondition('date_acquisition = date(:date_acquisition)')
                ->addParam(':date_acquisition', $_POST['date_acquisition']);
        }
        if (isset($_POST['ati'])) {
            $cmd->addCondition('ati like :ati')
                ->addParam(':ati', "%" . $_POST['ati'] . "%");
        }
        if (isset($_POST['store_id'])) {
            $cmd->addCondition('store_id like :store_id')
                ->addParam(':store_id', "%" . $_POST['store_id'] . "%");
        }
        if (isset($_POST['asset_name'])) {
            $cmd->addCondition('asset_name like :asset_name')
                ->addParam(':asset_name', "%" . $_POST['asset_name'] . "%");
        }
        if (isset($_POST['price_acquisition'])) {
            $cmd->addCondition('price_acquisition like :price_acquisition')
                ->addParam(':price_acquisition', "%" . $_POST['price_acquisition'] . "%");
        }
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $cmd->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }
        $cmd->addOrder('ad.ati');

        return $cmd;
    }
}