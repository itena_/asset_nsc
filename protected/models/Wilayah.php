<?php
Yii::import('application.models._base.BaseWilayah');

class Wilayah extends BaseWilayah
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->wilayah_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->wilayah_id = $uuid;
        }
        return parent::beforeValidate();
    }
}