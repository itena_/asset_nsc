<?php
Yii::import('application.models._base.BaseGlTrans');

class GlTrans extends BaseGlTrans
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->gl_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->gl_trans_id = $uuid;
        }
        return parent::beforeValidate();
    }
}