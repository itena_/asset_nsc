<?php

/**
 * This is the model class for table "{{asset_trans_detail}}".
 *
 * The followings are the available columns in table '{{asset_trans_detail}}':
 * @property string $trans_detail_id
 * @property string $trans_id
 * @property string $asset_id
 * @property string $ati
 * @property string $asset_name
 * @property double $amount
 * @property double $bookvalue
 * @property double $ppn
 * @property string $note
 * @property string $tdate
 * @property string $bu_penerima
 * @property string $bu_pengirim
 * @property string $store_penerima
 * @property string $store_pengirim
 * @property integer $visible
 * @property integer $approval
 * @property integer $status
 */
class BaseAssetTransDetail extends GxActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{asset_trans_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('trans_id, asset_id, visible', 'required'),
            array('visible, approval, status', 'numerical', 'integerOnly'=>true),
            array('amount, bookvalue, ppn', 'numerical'),
            array('trans_detail_id, trans_id, asset_id, ati, asset_name, bu_penerima, bu_pengirim, store_penerima, store_pengirim', 'length', 'max'=>255),
            array('note, tdate', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('trans_detail_id, trans_id, asset_id, ati, asset_name, amount, bookvalue, ppn, note, tdate, bu_penerima, bu_pengirim, store_penerima, store_pengirim, visible, approval, status', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'trans_detail_id' => 'Trans Detail',
            'trans_id' => 'Trans',
            'asset_id' => 'Asset',
            'ati' => 'Ati',
            'asset_name' => 'Asset Name',
            'amount' => 'Amount',
            'bookvalue' => 'Bookvalue',
            'ppn' => 'Ppn',
            'note' => 'Note',
            'tdate' => 'Tdate',
            'bu_penerima' => 'Bu Penerima',
            'bu_pengirim' => 'Bu Pengirim',
            'store_penerima' => 'Store Penerima',
            'store_pengirim' => 'Store Pengirim',
            'visible' => 'Visible',
            'approval' => 'Approval',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('trans_detail_id',$this->trans_detail_id,true);
        $criteria->compare('trans_id',$this->trans_id,true);
        $criteria->compare('asset_id',$this->asset_id,true);
        $criteria->compare('ati',$this->ati,true);
        $criteria->compare('asset_name',$this->asset_name,true);
        $criteria->compare('amount',$this->amount);
        $criteria->compare('bookvalue',$this->bookvalue);
        $criteria->compare('ppn',$this->ppn);
        $criteria->compare('note',$this->note,true);
        $criteria->compare('tdate',$this->tdate,true);
        $criteria->compare('bu_penerima',$this->bu_penerima,true);
        $criteria->compare('bu_pengirim',$this->bu_pengirim,true);
        $criteria->compare('store_penerima',$this->store_penerima,true);
        $criteria->compare('store_pengirim',$this->store_pengirim,true);
        $criteria->compare('visible',$this->visible);
        $criteria->compare('approval',$this->approval);
        $criteria->compare('status',$this->status);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AssetTransDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
