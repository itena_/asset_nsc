<?php

/**
 * This is the model base class for the table "{{asset_non_ati_view}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "AssetNonAtiView".
 *
 * Columns in table "{{asset_non_ati_view}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $category_name
 * @property string $sub_name
 * @property string $kode_barang
 * @property string $nama_barang
 * @property string $nonati_id
 * @property string $barang_id
 * @property integer $qty
 * @property string $doc_ref
 * @property string $tdate
 * @property string $tgl
 * @property string $note
 * @property string $store_kode
 * @property string $businessunit_id
 * @property double $price
 * @property string $category_id
 * @property string $category_sub_id
 * @property string $group_id
 * @property string $user_id
 * @property integer $visible
 *
 */
abstract class BaseAssetNonAtiView extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{asset_non_ati_view}}';
	}

	public static function representingColumn() {
		return 'category_name';
	}

	public function rules() {
		return array(
			array('category_name, sub_name, kode_barang, nonati_id, barang_id, businessunit_id', 'required'),
			array('qty, visible', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('category_name, sub_name, nonati_id, barang_id, businessunit_id, category_id, category_sub_id, group_id, user_id', 'length', 'max'=>50),
			array('kode_barang, nama_barang', 'length', 'max'=>500),
			array('doc_ref', 'length', 'max'=>255),
			array('store_kode', 'length', 'max'=>30),
			array('tdate, tgl, note', 'safe'),
			array('nama_barang, qty, doc_ref, tdate, tgl, note, store_kode, price, category_id, category_sub_id, group_id, user_id, visible', 'default', 'setOnEmpty' => true, 'value' => null),
			array('category_name, sub_name, kode_barang, nama_barang, nonati_id, barang_id, qty, doc_ref, tdate, tgl, note, store_kode, businessunit_id, price, category_id, category_sub_id, group_id, user_id, visible', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'category_name' => Yii::t('app', 'Category Name'),
			'sub_name' => Yii::t('app', 'Sub Name'),
			'kode_barang' => Yii::t('app', 'Kode Barang'),
			'nama_barang' => Yii::t('app', 'Nama Barang'),
			'nonati_id' => Yii::t('app', 'Nonati'),
			'barang_id' => Yii::t('app', 'Barang'),
			'qty' => Yii::t('app', 'Qty'),
			'doc_ref' => Yii::t('app', 'Doc Ref'),
			'tdate' => Yii::t('app', 'Tdate'),
			'tgl' => Yii::t('app', 'Tgl'),
			'note' => Yii::t('app', 'Note'),
			'store_kode' => Yii::t('app', 'Store Kode'),
			'businessunit_id' => Yii::t('app', 'Businessunit'),
			'price' => Yii::t('app', 'Price'),
			'category_id' => Yii::t('app', 'Category'),
			'category_sub_id' => Yii::t('app', 'Category Sub'),
			'group_id' => Yii::t('app', 'Group'),
			'user_id' => Yii::t('app', 'User'),
			'visible' => Yii::t('app', 'Visible'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('category_name', $this->category_name, true);
		$criteria->compare('sub_name', $this->sub_name, true);
		$criteria->compare('kode_barang', $this->kode_barang, true);
		$criteria->compare('nama_barang', $this->nama_barang, true);
		$criteria->compare('nonati_id', $this->nonati_id, true);
		$criteria->compare('barang_id', $this->barang_id, true);
		$criteria->compare('qty', $this->qty);
		$criteria->compare('doc_ref', $this->doc_ref, true);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('note', $this->note, true);
		$criteria->compare('store_kode', $this->store_kode, true);
		$criteria->compare('businessunit_id', $this->businessunit_id, true);
		$criteria->compare('price', $this->price);
		$criteria->compare('category_id', $this->category_id, true);
		$criteria->compare('category_sub_id', $this->category_sub_id, true);
		$criteria->compare('group_id', $this->group_id, true);
		$criteria->compare('user_id', $this->user_id, true);
		$criteria->compare('visible', $this->visible);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}