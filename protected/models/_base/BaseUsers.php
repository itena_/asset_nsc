<?php

/**
 * This is the model base class for the table "{{users}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Users".
 *
 * Columns in table "{{users}}" available as properties of the model,
 * followed by relations of table "{{users}}" available as properties of the model.
 *
 * @property string $id
 * @property string $user_id
 * @property string $password
 * @property string $last_visit_date
 * @property integer $active
 * @property string $security_roles_id
 * @property string $name
 * @property string $store_id
 * @property integer $up
 * @property string $businessunit_id
 *
 * @property SecurityRoles $securityRoles
 * @property Store $store
 */
abstract class BaseUsers extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{users}}';
	}

	public static function representingColumn() {
		return 'user_id';
	}

	public function rules() {
		return array(
			array('id, security_roles_id, businessunit_id', 'required'),
			array('active, up', 'numerical', 'integerOnly'=>true),
			array('id, businessunit_id', 'length', 'max'=>50),
			array('user_id', 'length', 'max'=>60),
			array('password, name', 'length', 'max'=>100),
			array('security_roles_id, store_id', 'length', 'max'=>36),
			array('last_visit_date', 'safe'),
			array('user_id, password, last_visit_date, active, name, store_id, up', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, user_id, password, last_visit_date, active, security_roles_id, name, store_id, up, businessunit_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'securityRoles' => array(self::BELONGS_TO, 'SecurityRoles', 'security_roles_id'),
			'store' => array(self::BELONGS_TO, 'Store', 'store_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User'),
			'password' => Yii::t('app', 'Password'),
			'last_visit_date' => Yii::t('app', 'Last Visit Date'),
			'active' => Yii::t('app', 'Active'),
			'security_roles_id' => Yii::t('app', 'Security Roles'),
			'name' => Yii::t('app', 'Name'),
			'store_id' => Yii::t('app', 'Store'),
			'up' => Yii::t('app', 'Up'),
			'businessunit_id' => Yii::t('app', 'Businessunit'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('user_id', $this->user_id, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('last_visit_date', $this->last_visit_date, true);
		$criteria->compare('active', $this->active);
		$criteria->compare('security_roles_id', $this->security_roles_id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('store_id', $this->store_id);
		$criteria->compare('up', $this->up);
		$criteria->compare('businessunit_id', $this->businessunit_id, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}