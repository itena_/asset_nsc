<?php

/**
 * This is the model base class for the table "{{asset_barang}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "AssetBarang".
 *
 * Columns in table "{{asset_barang}}" available as properties of the model,
 * followed by relations of table "{{asset_barang}}" available as properties of the model.
 *
 * @property string $barang_id
 * @property string $kode_barang
 * @property string $nama_barang
 * @property string $ket
 * @property string $grup_id
 * @property integer $active
 * @property string $sat
 * @property integer $up
 * @property integer $tipe_barang_id
 * @property string $owner
 * @property string $businessunit_id
 * @property string $category_id
 * @property string $category_sub_id
 *
 * @property Store $owner0
 */
abstract class BaseAssetBarang extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{asset_barang}}';
	}

	public static function representingColumn() {
		return 'kode_barang';
	}

	public function rules() {
		return array(
			array('barang_id, kode_barang, businessunit_id, category_id, category_sub_id', 'required'),
			array('active, up, tipe_barang_id', 'numerical', 'integerOnly'=>true),
			array('barang_id, grup_id, owner', 'length', 'max'=>36),
			array('kode_barang', 'length', 'max'=>100),
			array('nama_barang', 'length', 'max'=>500),
			array('sat', 'length', 'max'=>10),
			array('businessunit_id, category_id, category_sub_id', 'length', 'max'=>60),
			array('ket', 'safe'),
			array('nama_barang, ket, grup_id, active, sat, up, tipe_barang_id, owner', 'default', 'setOnEmpty' => true, 'value' => null),
			array('barang_id, kode_barang, nama_barang, ket, grup_id, active, sat, up, tipe_barang_id, owner, businessunit_id, category_id, category_sub_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'owner0' => array(self::BELONGS_TO, 'Store', 'owner'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'barang_id' => Yii::t('app', 'Barang'),
			'kode_barang' => Yii::t('app', 'Kode Barang'),
			'nama_barang' => Yii::t('app', 'Nama Barang'),
			'ket' => Yii::t('app', 'Ket'),
			'grup_id' => Yii::t('app', 'Grup'),
			'active' => Yii::t('app', 'Active'),
			'sat' => Yii::t('app', 'Sat'),
			'up' => Yii::t('app', 'Up'),
			'tipe_barang_id' => Yii::t('app', 'Tipe Barang'),
			'owner' => Yii::t('app', 'Owner'),
			'businessunit_id' => Yii::t('app', 'Businessunit'),
			'category_id' => Yii::t('app', 'Category'),
			'category_sub_id' => Yii::t('app', 'Category Sub'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('barang_id', $this->barang_id, true);
		$criteria->compare('kode_barang', $this->kode_barang, true);
		$criteria->compare('nama_barang', $this->nama_barang, true);
		$criteria->compare('ket', $this->ket, true);
		$criteria->compare('grup_id', $this->grup_id, true);
		$criteria->compare('active', $this->active);
		$criteria->compare('sat', $this->sat, true);
		$criteria->compare('up', $this->up);
		$criteria->compare('tipe_barang_id', $this->tipe_barang_id);
		$criteria->compare('owner', $this->owner);
		$criteria->compare('businessunit_id', $this->businessunit_id, true);
		$criteria->compare('category_id', $this->category_id, true);
		$criteria->compare('category_sub_id', $this->category_sub_id, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}