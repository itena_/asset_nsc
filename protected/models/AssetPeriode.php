<?php
Yii::import('application.models._base.BaseAssetPeriode');

class AssetPeriode extends BaseAssetPeriode
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->asset_periode_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_periode_id = $uuid;
        }
        return parent::beforeValidate();
    }

    public static function saveAssetPeriode($model, $modeldetail, $params, $import = 0)
    {
        $lastdeprisiasi  = 0;
        $lastbalance     = 0;
        $lastaccumulated = 0;

        $days  = date_parse_from_format("Y-m-d", $model->date_acquisition);
        $tahun = $days['year'];
        $bulan = $days['month'] - 1;
        $hari  = $days['day'];
        $query = '';

        for ($j = 0; $j < $params['period']; $j++) {
            $modelperiode = new AssetPeriode;

            $modelperiode->asset_detail_id = $modeldetail->asset_detail_id;
            $modelperiode->asset_id = $model->asset_id;

            $penyusutan = $modeldetail->price_acquisition / $modeldetail->period;
            $modelperiode->penyusutanperbulan = $penyusutan;
            $lastdeprisiasi = $modelperiode->penyusutanperbulan;

            $penyusutantahun = $penyusutan * $modeldetail->period;
            $modelperiode->penyusutanpertahun = $penyusutantahun;
            if ($lastbalance == 0 || $lastdeprisiasi == 0) {
                $modelperiode->balance = $modeldetail->price_acquisition - $penyusutan;
                $lastbalance = $modelperiode->balance;

                $modelperiode->akumulasipenyusutan = $penyusutan;
                $lastaccumulated = $modelperiode->akumulasipenyusutan;
            } else {
                $modelperiode->balance = $lastbalance - $lastdeprisiasi;
                $lastbalance = $modelperiode->balance;

                $modelperiode->akumulasipenyusutan = $lastaccumulated + $penyusutan;
                $lastaccumulated = $modelperiode->akumulasipenyusutan;
            }

            if($modelperiode->balance == null){
                $modelperiode->balance = 0;
            }

            if ($bulan < 12) {
                $bulan++;
            } else {
                $bulan = 1;
                $tahun++;
            }

            $modelperiode->tdate = $tahun . '-' . $bulan . '-' . $hari;
            $modelperiode->tglpenyusutan = $modelperiode->tdate;
            if($import == 0) {
                if (!$modelperiode->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($modelperiode));
                }
            } else {
                $query .= U::getQuery([
                    DbCmd::uuid(),
                    $params['asset_id'],
                    $params['assetDetail_id'],
                    $modelperiode->tglpenyusutan,
                    $modelperiode->penyusutanperbulan,
                    $modelperiode->penyusutanpertahun,
                    $modelperiode->balance,
                    $modelperiode->akumulasipenyusutan,
                    $modelperiode->tdate,
                    1
                ]);
            }
        }
        if($import == 1)
            return $query;
    }
}