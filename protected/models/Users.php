<?php
Yii::import('application.models._base.BaseUsers');
class Users extends BaseUsers
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_override($user, $pass)
    {
        $comm = Yii::app()->db->createCommand("SELECT ns.id FROM nscc_users AS ns
        INNER JOIN nscc_security_roles AS nsc ON ns.security_roles_id = nsc.security_roles_id
        WHERE ns.`password` = :pass AND ns.user_id = :user AND nsc.sections LIKE '%209%'");
        $a = $comm->queryScalar(array(':pass' => $pass, ':user' => $user));
        return $a ? 1 : 0;
    }
    public static function get_access($user, $pass, $section)
    {
        $comm = Yii::app()->db->createCommand("SELECT ns.id FROM nscc_users AS ns
        INNER JOIN nscc_security_roles AS nsc ON ns.security_roles_id = nsc.security_roles_id
        WHERE ns.`password` = :pass AND ns.user_id = :user AND nsc.sections LIKE :section");
        return $comm->queryScalar(array(':pass' => $pass, ':user' => $user, ':section' => '%'.$section.'%'));
    }
    public static function get_security_role_id() {
        $user = Users::model()->findByPk(user()->getId());
        return $user->security_roles_id;
    }
    public static function getStoreIdUser() {
        $user = Users::model()->findByPk(user()->getId());
        return $user->store;
    }
    public static function is_Administrator() {
        $sr = self::get_security_role_id();
        return SecurityRoles::model()->findByPk($sr)->role == 'SYSTEM ADMINISTRATOR' ? 1 : 0;
    }
    public function beforeValidate()
    {
        if ($this->id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->id = $uuid;
        }
        /*if ($this->store_id == null) {
            $this->store = STOREID;
        }*/
        return parent::beforeValidate();
    }
    public function is_available_role($role)
    {
        $sections = explode(',', $this->securityRoles->sections);
        return in_array($role, $sections);
    }
}