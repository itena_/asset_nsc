<?php
Yii::import('application.models._base.BaseAssetView');

class AssetView extends BaseAssetView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->asset_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_id = $uuid;
        }
        return parent::beforeValidate();
    }
}