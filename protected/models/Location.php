<?php
Yii::import('application.models._base.BaseLocation');

class Location extends BaseLocation
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->name == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->name = $uuid;
        }
        return parent::beforeValidate();
    }
    public static function saveData($key = null) {
        $name = isset($key) ? strtoupper($key) : strtoupper($_POST['name']);

        if(isset($key)) {
            $model = Location::model()->findByPk($name);
            if(!$model)
                $model = new Location;
        }
        else {
            $model = new Location;
        }
        $model->name = $name;
        if (!$model->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Location')) . CHtml::errorSummary($model));

        return $model->name;
    }
    public static function getData() {
        $dbcmd = DbCmd::instance()
            ->addFrom("{{location}} l")
        ;

        $count = count($dbcmd->queryAll());
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $dbcmd->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }
        $dbcmd->addOrder('l.name');
        return $dbcmd;
    }
}