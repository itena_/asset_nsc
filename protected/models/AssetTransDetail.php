<?php
Yii::import('application.models._base.BaseAssetTransDetail');

class AssetTransDetail extends BaseAssetTransDetail
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function beforeValidate(){
        if ($this->trans_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->trans_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}