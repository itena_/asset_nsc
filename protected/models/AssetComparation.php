<?php
Yii::import('application.models._base.BaseAssetComparation');

class AssetComparation extends BaseAssetComparation
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->asset_comparation_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_comparation_id = $uuid;
        }

        $this->tdate = new CDbExpression('NOW()');
        $this->user_id = Yii::app()->user->getId();

        return parent::beforeValidate();
    }
}