<?php
Yii::import('application.models._base.BaseAssetCategorySub');

class AssetCategorySub extends BaseAssetCategorySub
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->category_sub_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->category_sub_id = $uuid;
        }
        return parent::beforeValidate();
    }
}