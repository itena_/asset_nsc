<?php
Yii::import('application.models._base.BaseAssetComparationDetail');

class AssetComparationDetail extends BaseAssetComparationDetail
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->asset_comparation_detail_id = $uuid;
        return parent::beforeValidate();
    }
}