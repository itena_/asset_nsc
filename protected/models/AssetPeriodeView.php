<?php
Yii::import('application.models._base.BaseAssetPeriodeView');

class AssetPeriodeView extends BaseAssetPeriodeView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->asset_periode_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_periode_id = $uuid;
        }
        return parent::beforeValidate();
    }
}