<?php
Yii::import('application.models._base.BaseStore');
class Store extends BaseStore
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function beforeValidate(){
        if ($this->store_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->store_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function checkCabangId(){
        $id = Yii::app()->user->getId();
        return Users::model()->findByPk($id)->store_id;
    }
    public static function saveStore($data)
    {
        $cabang = str_replace(' ', '', $data['Cabang']);
        $kode_bu = Businessunit::model()->findByPk($_COOKIE['businessunitid'])->businessunit_code;
        $model = Store::model()->findByAttributes(['store_kode' => $cabang, 'businessunit_id' => $_COOKIE['businessunitid']]);
        if (!$model)
            $model = new Store;

        $model->store_kode = $cabang;
        $model->nama_store = $kode_bu . ' ' . $cabang;
        $model->businessunit_id = $_COOKIE['businessunitid'];
        $model->wilayah_id = 1;
        $model->tipe = strtoupper($cabang);
        $model->up = 0;
        if (!$model->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Store')) . CHtml::errorSummary($model));
        }

        return $model->store_kode;
    }
}