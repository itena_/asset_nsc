<?php
Yii::import('application.models._base.BaseAssetNonAti');

class AssetNonAti extends BaseAssetNonAti
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->nonati_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->nonati_id = $uuid;
        }
        return parent::beforeValidate();
    }
}