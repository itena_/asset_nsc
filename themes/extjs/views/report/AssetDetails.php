<h1>Asset Details</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>ACTIVA : <?= $ati ?></h3>
<?
$this->pageTitle = 'Asset Details';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array('ati', 'asset_trans_date', 'asset_trans_name', 'asset_trans_branch','class','period','tariff'),
    'extraRowColumns' => is_report_excel() ? array() : array('ati'),
    'extraRowPos' => 'below',
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        array(
            'header' => 'Branch',
            'name' => 'asset_trans_branch',

        ),
        array(
            'header' => 'Name',
            'name' => 'asset_trans_name'
        ),

        array(
            'header' => 'Class',
            'name' => 'class',
            //'footer' => 'Total'
        ),
        array(
            'header' => 'Period',
            'name' => 'period',
            //'footer' => 'Total'
        ),
        array(
            'header' => 'Tariff (%)',
            'name' => 'tariff',

        ),
        array(
            'header' => 'Acquisition Date',
            'name' => 'asset_trans_date',

        ),
        array(
            'header' => 'Depreciation Date',
            'name' => 'tglpenyusutan',
            'footer' => 'Total'
        ),
        array(
            'header' => 'Acquisition Price',
            'name' => 'asset_trans_price',
            'value' => function ($data) {
                return format_number_report($data['asset_trans_price'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalprice, 2)
        ),
        array(
            'header' => 'Acquisition Price (New)',
            'name' => 'asset_trans_new_price',
            'value' => function ($data) {
                return format_number_report($data['asset_trans_new_price'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalnewprice, 2)
        ),
        array(
            'header' => 'Depreciation',
            'name' => 'penyusutanperbulan',
            'value' => function ($data) {
                return format_number_report($data['penyusutanperbulan'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        ),
        array(
            'header' => 'Balance',
            'name' => 'balance',
            'value' => function ($data) {
                return format_number_report($data['balance'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalbalance, 2)
        )
    )
));
?>