<h1>Report Asset Total Details</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>Asset Category : <?= $category ?></h3>
<h3>Branch/Store : <?= $branch ?></h3>

<?
$this->pageTitle = 'Report Asset Total Details';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'extraRowPos' => 'below',
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        array(
            'header' => 'Branch',
            'name' => 'branch',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'Activa',
            'name' => 'activa',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'Category',
            'name' => 'category',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'Sub Category',
            'name' => 'sub_name',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'Asset',
            'name' => 'name',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'Status',
            'name' => 'status',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
            'value' => function ($data) {
                switch(get_number($data['status'])){
                    case 0 :
                        //metaData.style += "background-color: #ff7373;";
                        return 'NON ACTIVE';
                    case 1 :
                        //metaData.style += "background-color: #9ffb8a;";
                        return 'ACTIVE';
                    case 2 :
                        //metaData.style += "background-color: #f1f26f;";
                        return 'LEND';
                    case 3 :
                        //metaData.style += "background-color: #ff7373;";
                        return 'SELL';
                    case 4 :
                        //metaData.style += "background-color: #f1f26f;";
                        return 'RENT';
                    case 5 :
                        //metaData.style += "background-color: #ff7373;";
                        return 'BROKEN';
                    case 6 :
                        //metaData.style += "background-color: #ff7373;";
                        return 'LOST';
                }
            },

        ),
        array(
            'header' => 'Golongan',
            'name' => 'desc',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'Acquisition Date',
            'name' => 'acquisitiondate',
        ),
        array(
            'header' => 'Class',
            'name' => 'class',
        ),
        array(
            'header' => 'Tariff(%)',
            'name' => 'tariff',
        ),
        array(
            'header' => 'Period',
            'name' => 'period',
            'footer' => 'Total'
        ),
        array(
            'header' => 'Price',
            'name' => 'price',
            'value' => function ($data) {
                return format_number_report($data['price'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalprice, 2)
        ),
        array(
            'header' => 'Nilai Buku',
            'name' => 'balance',
            'value' => function ($data) {
                return format_number_report($data['balance'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalbalance, 2)
        ),
    )
));
?>