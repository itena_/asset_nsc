<h1>Report Asset Total</h1>
<h3>From : <?= $start ?></h3>
<h3>To : <?= $to ?></h3>
<h3>Asset Category : <?= $category ?></h3>
<h3>Branch/Store : <?= $branch ?></h3>

<?
$this->pageTitle = 'Report Asset Total';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Branch',
            'name' => 'branch',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'Category',
            'name' => 'category',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'Sub Category',
            'name' => 'sub_name',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'Asset',
            'name' => 'asset'
        ),
        array(
            'header' => 'Qty',
            'name' => 'qty',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total',
            'footerHtmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                return format_number_report($data['total'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        ),
        array(
            'header' => 'Total Nilai Buku',
            'name' => 'balance',
            'value' => function ($data) {
                return format_number_report($data['balance'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalbalance, 2)
        )
    )
));
?>