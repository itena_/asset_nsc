<h1>Report Asset Non Aktiva Detail</h1>
<h3>From : <?= $start ?></h3>
<h3>To : <?= $to ?></h3>
<h3>Asset Category : <?= $category ?></h3>
<h3>Branch/Store : <?= $branch ?></h3>
<?
$this->pageTitle = 'Report Asset Non Aktiva Detail';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    //'mergeColumns' => is_report_excel() ? array() : array('docref'),
    
    'columns' => array(
        array(
            'header' => 'Doc.Ref',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Category',
            'name' => 'category_name'
        ),
        array(
            'header' => 'Sub Category',
            'name' => 'sub_name'
        ),

        array(
            'header' => 'Item Code',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Item Name',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Qty',
            'name' => 'qty',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total',
            'footerHtmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Price',
            'name' => 'price',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2),
        ),
        /*array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                return format_number_report($data['total'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        )*/
    )
));
?>