<h1>Report Jurnal Asset</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>Branch/Store : <?= $branch ?></h3>

<?
$this->pageTitle = 'Report Jurnal Asset';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array( 'memo_'),
    //'extraRowColumns' => is_report_excel() ? array() : array('ati'),
    'extraRowPos' => 'below',
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        array(
            'header' => 'Activa',
            'name' => 'memo_',
        ),
        array(
            'header' => 'Name',
            'name' => 'asset_trans_name',
        ),
        array(
            'header' => 'Code',
            'name' => 'kode_barang',
        ),
        array(
            'header' => 'Desc',
            'name' => 'description',
        ),
        array(
            'header' => 'Acquisition Date',
            'name' => 'tran_date'
        ),
        array(
            'header' => 'Trans Date',
            'name' => 'tdate'
        ),
        array(
            'header' => 'Status',
            'name' => 'status',
        ),
        array(
            'header' => 'Nilai Buku',
            'name' => 'depreciationprice',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'value' => function ($data) {
                return format_number_report($data['depreciationprice'], 2);
            },
            'footer' => 'Total',
            'footerHtmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Price',
            'name' => 'price',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'value' => function ($data) {
                return format_number_report($data['price'], 2);
            },
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalprice, 2)
        ),
        array(
            'header' => 'PPN',
            'name' => 'ppn',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'value' => function ($data) {
                return format_number_report($data['ppn'], 2);
            },
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalppn, 2)
        ),
        /*array(
            'header' => 'Total',
            'name' => 'total',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'value' => function ($data) {
                return format_number_report($data['total'], 2);
            },
        ),*/

        array(
            'header' => 'Debit',
            'name' => 'debit',
            'value' => function ($data) {
                return format_number_report($data['debit'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totaldebit, 2)
        ),
        array(
            'header' => 'Credit',
            'name' => 'kredit',
            'value' => function ($data) {
                return format_number_report($data['kredit'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalkredit, 2)
        ),
        array(
            'header' => 'Profit',
            'name' => 'profit',
            'value' => function ($data) {
                return format_number_report($data['profit'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalprofit, 2)
        ),
        array(
            'header' => 'Loss',
            'name' => 'loss',
            'value' => function ($data) {
                return format_number_report($data['loss'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalloss, 2)
        ),
    )
));
?>