<h1>Asset Depriciation</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>ACTIVA : <?= $ati ?></h3>
<?
$this->pageTitle = 'Asset Depriciation';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'extraRowPos' => 'below',
    'columns' => array(
        array(
            'header' => 'Branch',
            'name' => 'store_kode',
        ),
        array(
            'header' => 'Description',
            'name' => 'description'
        ),
        array(
            'header' => 'Class',
            'name' => 'class',
        ),
        array(
            'header' => 'Period',
            'name' => 'period',
        ),
        array(
            'header' => 'Tariff (%)',
            'name' => 'tariff',
        ),
        array(
            'header' => 'Acquisition Date',
            'name' => 'asset_trans_date',
        ),
        array(
            'header' => 'Depreciation Date',
            'name' => 'tglpenyusutan',
        ),
        array(
            'header' => 'Acquisition Price',
            'name' => 'price_acquisition',
            'value' => function ($data) {
                return format_number_report($data['price_acquisition'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total'
        ),
        array(
            'header' => 'Depreciation',
            'name' => 'penyusutanperbulan',
            'value' => function ($data) {
                return format_number_report($data['penyusutanperbulan'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        ),
        array(
            'header' => 'Accumulation',
            'name' => 'akumulasipenyusutan',
            'value' => function ($data) {
                return format_number_report($data['akumulasipenyusutan'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        ),
        array(
            'header' => 'Balance',
            'name' => 'balance',
            'value' => function ($data) {
                return format_number_report($data['balance'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalbalance, 2)
        )
    )
));
?>