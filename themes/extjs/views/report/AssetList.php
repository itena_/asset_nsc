<h1>Asset List</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$this->pageTitle = 'Asset List';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Code',
            'name' => 'kode_barang',
        ),
        array(
            'header' => 'Name',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Note',
            'name' => 'ket',
        ),
        array(
            'header' => 'ATI',
            'name' => 'ati',
        ),
        array(
            'header' => 'Branch',
            'name' => 'asset_trans_branch',
        ),
        array(
            'header' => 'Acquisition Date',
            'name' => 'asset_trans_date',
        ),
        array(
            'header' => 'Group',
            'name' => 'golongan',
        ),
        array(
            'header' => 'Location',
            'name' => 'location',
        ),
        array(
            'header' => 'Description',
            'name' => 'description',
        ),
        array(
            'header' => 'Price',
            'name' => 'asset_trans_price',
            'value' => function ($data) {
                return format_number_report($data['asset_trans_price'], 2);
            }
        ),
        array(
            'header' => 'Monthly Depreciation',
            'name' => 'penyusutanperbulan',
            'value' => function ($data) {
                return format_number_report($data['penyusutanperbulan'], 2);
            }
        ),
        array(
            'header' => 'Category',
            'name' => 'category_name',
        ),
    )
));
?>