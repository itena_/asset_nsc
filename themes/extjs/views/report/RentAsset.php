<h1>Report Rent Asset</h1>
<h3>From : <?= $start ?></h3>
<h3>To : <?= $to ?></h3>
<h3>Asset Category : <?= $category ?></h3>
<?
$this->pageTitle = 'Report Rent Asset';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    //'mergeColumns' => is_report_excel() ? array() : array('docref'),
    
    'columns' => array(

/*        array(
            'header' => 'Category',
            'name' => 'category'
        ),*/
        array(
            'header' => 'Activa',
            'name' => 'ati'
        ),
        array(
            'header' => 'Asset',
            'name' => 'name'
        ),
        array(
            'header' => 'Date',
            'name' => 'ttdate'
        ),
        array(
            'header' => 'To Business Unit',
            'name' => 'tobu'
        ),
        array(
            'header' => 'To Branch',
            'name' => 'tostore'
        ),
        array(
            'header' => 'Description',
            'name' => 'desc',
            'footer' => 'Total',
            'footerHtmlOptions' => array('style' => 'text-align: right;')
        ),

        array(
            'header' => 'Amount',
            'name' => 'amount',
            'value' => function ($data) {
                return format_number_report($data['amount'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalamount, 2)
        ),

    )
));
?>