<h1>Report Asset Total Details (Non Active)</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>Asset Category : <?= $category ?></h3>
<h3>Branch/Store : <?= $branch ?></h3>
<?
$this->pageTitle = 'Report Asset Total Details (Non Active)';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    //'mergeColumns' => is_report_excel() ? array() : array( 'acquisitiondate', 'name', 'branch','category','class','period'),
    //'extraRowColumns' => is_report_excel() ? array() : array('ati'),
    'extraRowPos' => 'below',
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        array(
            'header' => 'Activa',
            'name' => 'activa',

        ),
        array(
            'header' => 'Category',
            'name' => 'category'
        ),

        array(
            'header' => 'Asset',
            'name' => 'name',
            //'footer' => 'Total'
        ),
        array(
            'header' => 'Asset Code',
            'name' => 'kode',
            //'footer' => 'Total'
        ),
        array(
            'header' => 'Status',
            'name' => 'status',
            'value' => function ($data) {
                switch(get_number($data['status'])){
                    case 0 :
                        //metaData.style += "background-color: #ff7373;";
                        return 'NON ACTIVE';
                    case 1 :
                        //metaData.style += "background-color: #9ffb8a;";
                        return 'ACTIVE';
                    case 2 :
                        //metaData.style += "background-color: #f1f26f;";
                        return 'LEND';
                    case 3 :
                        //metaData.style += "background-color: #ff7373;";
                        return 'SELL';
                    case 4 :
                        //metaData.style += "background-color: #f1f26f;";
                        return 'RENT';
                    case 5 :
                        //metaData.style += "background-color: #ff7373;";
                        return 'BROKEN';
                    case 6 :
                        //metaData.style += "background-color: #ff7373;";
                        return 'LOST';
                }
            },

        ),
        array(
            'header' => 'Description',
            'name' => 'description',
            //'footer' => 'Total'
        ),
        array(
            'header' => 'Branch',
            'name' => 'branch',
            //'footer' => 'Total'
        ),
        array(
            'header' => 'Acquisition Date',
            'name' => 'acquisitiondate',

        ),
        array(
            'header' => 'Class',
            'name' => 'class',

        ),
        array(
            'header' => 'Tariff(%)',
            'name' => 'tariff',

        ),
        array(
            'header' => 'Period',
            'name' => 'period',
            'footer' => 'Total'
        ),
        array(
            'header' => 'Price',
            'name' => 'price',
            'value' => function ($data) {
                return format_number_report($data['price'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalprice, 2)
        ),
        array(
            'header' => 'Nilai Buku',
            'name' => 'balance',
            'value' => function ($data) {
                return format_number_report($data['balance'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalbalance, 2)
        ),
    )
));
?>