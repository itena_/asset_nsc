<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <link rel="shortcut icon" href="<?php echo bu(); ?>/favicon.png?v=<?php echo md5_file( 'favicon.png' ) ?>">
    <title><?php echo CHtml::encode( Yii::app()->name ); ?></title>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-gray.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
    <style>
        #drop {
            border: 2px dashed #BBBBBB;
            border-radius: 5px;
            color: #BBBBBB;
            font: 20pt bold, "Vollkorn";
            padding: 25px;
            text-align: center;
        }

        * {
            font-size: 12px;
            font-family: Candara;
        }
    </style>
</head>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<body>
<?php echo $content; ?>
<iframe id="iframe" sandbox="allow-same-origin" style="display: none"></iframe>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/login.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/sha512.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/client.min.js"></script>
</body>
</html>
