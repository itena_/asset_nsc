<iframe id="myFrame" name="myFrame" style="border:none"></iframe>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script>
    startConnection();
    var LOGOUT = false;
    DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    BASE_URL = '<?=bu() === "" ? "/" : bu();?>';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu().app()->params['url_logo']; ?>" alt=""/>';
    PT_NEGARA = '<?=PT_NEGARA;?>';
    NEGARA = '<?=NEGARA;?>';
    EDIT_TGL =  <?
    $user = Users::model()->findByPk(Yii::app()->user->getId());
    echo $user->is_available_role(246) ? 'true' : 'false';
    ?>;
    HEADOFFICE = <?if (defined('HEADOFFICE')) {
        echo HEADOFFICE ? 'true' : 'false';
    } else {
        echo 'false';
    }?>;

    PORT_CLOSED = true;
    //    Ext.chart.Chart.CHART_URL = '<?//=bu(); ?>///js/ext340/resources/charts.swf';
    VALID_CARD = <?=VALID_CARD;?>;
    INTERVALSYNC = <?=INTERVALSYNC;?>;
    ENABLESYNC = <?=ENABLESYNC ? 'true' : 'false';?>;
    STORE = '<?=STOREID;?>';
    //    if (!notReady()) {
    //        var fs = require('fs');
    //        var data = fs.readFileSync('./config.json'), myObj;
    //        try {
    //            myObj = JSON.parse(data);
    //            PRINTER_RECEIPT = myObj.PRINTER_RECEIPT;
    //            PRINTER_CARD = myObj.PRINTER_CARD;
    //            COM_POSIFLEX = myObj.COM_POSIFLEX;
    //        }
    //        catch (err) {
    //            console.log('There has been an error parsing your JSON.');
    //            console.log(err);
    //        }
    //    }
    SALES_OVERRIDE = '<?
        $id = Yii::app()->user->getId();
        $user = Users::model()->findByPk($id);
        echo Users::get_override($user->user_id, $user->password) ? 1 : 0;
        ?>';
    var PRINTER_RECEIPT = 'default';
    var PRINTER_STOCKER = 'default';
    var PRINTER_CARD = 'default';
    var COM_POSIFLEX = 'COM3';
    var LINE_SPACING = 50; // atau 25;
    var POSCUSTOMERREADONLY = false;
    var POSCUSTOMERDEFAULT = '';
    var local = localStorage.getItem("settingClient");
    if (local != null) {
        var data_ = JSON.parse(local);
        PRINTER_RECEIPT = JSON.search(data_, '//data[name_="PRINTER_RECEIPT"]/value_')[0];
        PRINTER_STOCKER = JSON.search(data_, '//data[name_="PRINTER_STOCKER"]/value_')[0];
        LINE_SPACING = JSON.search(data_, '//data[name_="LINE_SPACING"]/value_')[0];
        PRINTER_CARD = JSON.search(data_, '//data[name_="PRINTER_CARD"]/value_')[0];
        COM_POSIFLEX = JSON.search(data_, '//data[name_="COM_POSIFLEX"]/value_')[0];
        POSCUSTOMERREADONLY = JSON.search(data_, '//data[name_="POSCUSTOMERREADONLY"]/value_')[0];
        POSCUSTOMERDEFAULT = JSON.search(data_, '//data[name_="POSCUSTOMERDEFAULT"]/value_')[0];
    }
    var __cutPaper = [{type: 'raw', data: chr(27) + chr(105), options: {language: 'ESCP', dotDensity: 'double'}}];
    var __feedPaper = [{
        type: 'raw',
        data: chr(27) + chr(100) + chr(5),
        options: {language: 'ESCP', dotDensity: 'double'}
    }];
    var __openCashDrawer = [{
        type: 'raw',
        data: chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r",
        options: {language: 'ESCP', dotDensity: 'double'}
    }];
    var __printData = [
        {
            type: 'raw', data: chr(27) + chr(64) + //initial
        chr(27) + chr(51) + chr(LINE_SPACING) + //space vertical
        chr(27) + chr(77) + chr(49),// font B
            options: {language: 'ESCP', dotDensity: 'double'}
        }
    ];
    ROUNDING = <?if (defined('ROUNDING')) {
        echo ROUNDING;
    } else {
        echo 50;
    }?>;
    function nwis_round_up(e) {
        return round(Math.round(round(e / ROUNDING, 2)) * ROUNDING, 2);
    }
    function goodbye(e) {
        if (!LOGOUT) {
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
    }
    window.onbeforeunload = goodbye;
    //    if (!notReady()) {
    //        var gui = require('nw.gui');
    //        var win = gui.Window.get();
    //        win.maximize();
    //        win.on('new-win-policy', function (frame, url, policy) {
    //            policy.forceNewPopup();
    //        });
    //    }
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/0.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/businessunit_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/businessunit_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/businessunit_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/quickSalestrans_form.js"></script>

<script>
    jun.ajaxCounter = 0;
    jun.myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Processing... please wait"});
    Ext.onReady(function () {
        Ext.EventManager.addListener(document, "keypress", function (e) {
            if (jun.ajaxCounter > 0) {
                return false;
            }
        });
        Ext.Ajax.timeout = 1800000;
        var a = function () {
            Ext.get("loading").remove();
            Ext.fly("loading-mask").fadeOut({
                remove: !0
            });
        };
        Ext.QuickTips.init();
//    loadText = "Sedang proses... silahkan tunggu";
        Ext.Ajax.on("beforerequest", function (conn, opts) {
            if (opts.url == "site/Sync") return;
            jun.myMask.show();
            jun.ajaxCounter++;
        });
        Ext.Ajax.on("requestcomplete", function (conn, response, opts) {
            if (opts.url == "site/Sync") return;
            if (jun.ajaxCounter > 1) {
                jun.ajaxCounter--;
            } else {
                jun.ajaxCounter = 0;
                jun.myMask.hide();
            }
        });
        Ext.Ajax.on("requestexception", function (conn, response, opts) {
            if (opts.url == "site/Sync") return;
            if (jun.ajaxCounter > 1) {
                jun.ajaxCounter--;
            } else {
                jun.ajaxCounter = 0;
                jun.myMask.hide();
            }
            switch (response.status) {
                case 403:
                    window.location.href = 'site/logout';
                    break;
                case 500:
                    Ext.Msg.alert('Internal Server Error', response.responseText);
                    break;
                default :
                    Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                    break;
            }
        });
        displayText("Welcome To Asset Management", 0);

        var m = new jun.QuickSalesMenu({state:<?php echo $state ?>, username: "<?php echo $username ?>"});
        m.show();

    });
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/cc.js"></script>
<script type="text/javascript"></script>
<div id="SalestransView" style="height: 100%"></div>