<?php
if (count($pasien) == 0) {
    return;
}
$tgl = null;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/timeline.css"/>
    <script type="text/javascript"
            src="<?php echo Yii::app()->request->baseUrl; ?>/js/prefixfree.min.js"></script>
</head>
<body>
<div class="timeline-css3">
    <ul id='timeline'>
        <?php foreach ($pasien as $key => $row) : ?>
            <li class='work'>
                <input class='radio' id='work<?php echo $key; ?>' name='works' type='radio' checked>
                <div class="relative">
                    <label
                        for='work<?php echo $key; ?>'><?php echo $row['no_faktur'] . ' (' . $row['loc'] . ')'; ?></label>
                    <span class='date'><?php echo $row['tgl']; ?></span>
                    <span class='circle'></span>
                </div>
                <div class='content'>
                    <p><?php echo $row['comment']; ?></p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Kode Barang</th>
                            <th>Qty</th>
                            <th>Satuan</th>
                            <th>Note</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($row['item'] as $item) : ?>
                            <tr>
                                <td><?php echo $item['kd_brng']; ?></td>
                                <td><?php echo $item['qty']; ?></td>
                                <td><?php echo $item['satuan']; ?></td>
                                <td><?php echo $item['note']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
</body>
</html>